#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file accudict.py

class AccuDict(dict):
	""" Extend built-in dictionary with an accumulation method. """
	
	def increment(self, key, qty=1):
		try:
			self[key] += qty
		except KeyError:
			self[key] = qty
