#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file mediator.py


# builtin stuff
import marshal

# home made stuff
from lettermapping     import LetterMapping
from weightedforest    import WeightedForest
from pretenders        import Pretenders
from wordindex         import WordIndex
from wordtransformer   import WordTransformer
from expander          import Expander
from accentuator       import Accentuator
from tonguemodel       import TongueModel
from spacetransformer  import SpaceTransformer
from contentgetter     import ContentGetter
from acquirer          import Acquirer
from editorgetter      import EditorGetter
from editorsetter      import EditorSetter
from toothmanager      import ToothManager
from learner           import Learner
from casemapper        import CaseMapper
from favoritechars     import FavoriteChars
from lettertransformer import LetterTransformer
from calculator        import Calculator
from snipper           import Snipper
from glyphtransformer  import GlyphTransformer
from clicktransformer  import ClickTransformer
from scanner           import Scanner
from pictos            import Pictos
from pictowrapper      import PictoWrapper
from commandspeech     import CommandSpeech
from commandselect     import CommandSelect
from instantspeech     import InstantSpeech
from speechtransformer import SpeechTransformer
from jukebox           import Jukebox
from selector          import Selector
from tts               import Tts

class Mediator:
	
	def __init__(self):
		lngPro = marshal.load(open("lngPro.marshal","rb"))
		self.iniMap            = lngPro["iniMap"]
		self.wrdFor            = lngPro["wrdFor"]
		self.rawSntSep         = lngPro["rawSntSep"]
		self.wrdRex            = lngPro["wrdRex"]
		self.parRex            = lngPro["parRex"]
		self.sntRex            = lngPro["sntRex"]
		self.sntIni            = lngPro["sntIni"]
		self.letDig            = lngPro["letDig"]
		self.letOrd            = lngPro["letOrd"]
		self.utfAlf            = lngPro["utfAlf"]
		self.spell2            = lngPro["spell2"]
		lngSpe = {
			"defMap": lngPro["letMap"],
			"bytEnc": lngPro["bytEnc"],
			"utfAlf": lngPro["utfAlf"]
		}
		self.letMap            = LetterMapping(lngSpe)
		self.wrdFor            = WeightedForest(lngPro["wrdFor"])
		self.accentuator       = Accentuator(lngPro["accWrd"])
		self.expander          = Expander(self)
		self.wordTransformer   = WordTransformer(self)
		self.letterTransformer = LetterTransformer()
		self.favoriteChars     = FavoriteChars()
		self.editorGetter      = EditorGetter()
		self.editorSetter      = EditorSetter()
		self.glyphTransformer  = GlyphTransformer()
		self.nbTeeth           = len(lngPro["utfAlf"])+len(["space","eraser"])
		self.contentGetter     = ContentGetter(self)
		self.learner           = Learner(self)
		self.pictoWrapper      = PictoWrapper(self)
		self.pictos            = Pictos(self)
		self.wrdInd            = WordIndex(self)
		self.calculator        = Calculator(self)
		self.caseMapper        = CaseMapper(self)
		self.pretenders        = Pretenders(self)
		self.spaceTransformer  = SpaceTransformer(self)
		self.acquirer          = Acquirer(self)
		self.selector          = Selector(self)
		self.jukebox           = Jukebox(self)
		self.commandSelect     = CommandSelect(self)
		self.speechTransformer = SpeechTransformer(self)
		self.tts               = Tts()
		self.commandSpeech     = CommandSpeech(self)
		self.instantSpeech     = InstantSpeech(self)
		self.toothManager      = ToothManager(self)
		self.snipper           = Snipper()
		self.tongueModel       = TongueModel(self)
		self.clickTransformer  = ClickTransformer()
		self.scanner           = Scanner(self)


if __name__ == "__main__":
	from  chewing import main
	main()

