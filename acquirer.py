#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file acquirer.py

# import contentgetter
# import editorsetter
# import weightedforest
# import expander

class Acquirer (object):
	
	def __init__(self,mediator):
		self.getSelectedTextInContentGetter           = mediator.contentGetter.getSelectedText
		self.getExpandedTextInContentGetter           = mediator.contentGetter.getExpandedText
		self.getRichLastSpaceInContentGetter          = mediator.contentGetter.getRichLastSpace
		self.getRichPrefixInContentGetter             = mediator.contentGetter.getRichPrefix
		self.getErasableContentInContentGetter        = mediator.contentGetter.getErasableContent
		self.getStartOfBlockInContentGetter           = mediator.contentGetter.getStartOfBlock
		self.getPreviousBlockInContentGetter          = mediator.contentGetter.getPreviousBlock
		self.replaceInEditorSetter                    = mediator.editorSetter.replace
		self.defineInExpander                         = mediator.expander.define
		self.getPatternInExpander                     = mediator.expander.getPattern
		self.getTransformableStringInSpaceTransformer = mediator.spaceTransformer.getTransformableString
		self.mayLearnInLearner                        = mediator.learner.mayLearn
		self.getReplacementStringInCalculator         = mediator.calculator.getReplacementString
		self.hasValueInCalculator                     = mediator.calculator.hasValue
	
	def reset(self):
		self.prefix = self.getRichPrefixInContentGetter()
		self.selectedText = self.getSelectedTextInContentGetter()
		if self.prefix:
			if self.selectedText:
				self.setAfterPrefixBeforeSelectionStrategy()
			else:
				self.expandedText = self.getExpandedTextInContentGetter()
				if self.expandedText:
					self.setAfterPrefixBeforeExpansionStrategy()
				else:
					self.setAfterPrefixStrategy()
		elif self.selectedText:
			if self.hasValueInCalculator():
				self.setAfterSpacesBeforeCalculableSelectionStrategy()
			else:
				self.setAfterSpacesBeforeSelectionStrategy()
		else:
			self.expandedText = self.getExpandedTextInContentGetter()
			if self.expandedText:
				self.setAfterSpacesBeforeExpansionStrategy()
			else:
				if self.getStartOfBlockInContentGetter() == u'\n':
					self.erasableLength = len(self.getPreviousBlockInContentGetter())
					if self.erasableLength == 1:
						self.setAfterFirstCarriageReturnStrategy()
					else:
						self.setAfterSpacesStrategy()
				else:
					erasableContent = self.getErasableContentInContentGetter()
					self.erasableLength = len(erasableContent[0]+erasableContent[1])
					self.setAfterSpacesStrategy()
	
	def checkAbbreviationStrategy(self,strategy):
		if strategy:
			self.replaceWithLetterSelectionSpecifics = self.replaceWithLetterSelectionExpansionSpecifics
		else:
			self.replaceWithLetterSelectionSpecifics = self.replaceWithLetterPrefixSpecifics
	
	def eraseSome(self,*args):         self.eraseSomeSpecifics(*args)
	def eraseAll(self,*args):          self.eraseAllSpecifics(*args)
	def replaceWithSpaces(self,*args): self.replaceWithSpacesSpecifics(*args)
	def replaceWithLetter(self,*args): self.replaceWithLetterSpecifics(*args)
	def replaceWithString(self,*args): self.replaceWithStringSpecifics(*args)
	def replaceWithWord(self,*args):   self.replaceWithWordSpecifics(*args)
	
# Private stuff	
	
	def setAfterPrefixStrategy(self):
		self.eraseSomeSpecifics         = self.eraseSomeCountSpecifics
		self.eraseAllSpecifics          = self.eraseAllPrefixSpecifics
		self.replaceWithSpacesSpecifics = self.replaceWithSpacesPrefixSpecifics
		self.replaceWithLetterSpecifics = self.replaceWithLetterPrefixSpecifics
		self.replaceWithStringSpecifics = self.replaceWithStringPrefixSpecifics
		self.replaceWithWordSpecifics   = self.replaceWithWordPrefixSpecifics
	
	def setAfterPrefixBeforeSelectionStrategy(self):
		self.eraseSomeSpecifics         = self.eraseSomeCountSpecifics
		self.eraseAllSpecifics          = self.eraseAllSelectionSpecifics
		self.replaceWithSpacesSpecifics = self.replaceWithSpacesPrefixSelectionSpecifics
		self.replaceWithLetterSpecifics = self.replaceWithLetterPrefixSpecifics
		self.replaceWithStringSpecifics = self.replaceWithStringPrefixSpecifics
		self.replaceWithWordSpecifics   = self.replaceWithWordPrefixSpecifics
	
	def setAfterPrefixBeforeExpansionStrategy(self):
		self.eraseSomeSpecifics         = self.eraseSomeCountSpecifics
		self.eraseAllSpecifics          = self.eraseAllPrefixSpecifics
		self.replaceWithSpacesSpecifics = self.replaceWithSpacesExpansionSpecifics
		self.replaceWithLetterSpecifics = self.replaceWithLetterPrefixSpecifics
		self.replaceWithStringSpecifics = self.replaceWithStringExpansionSpecifics
		self.replaceWithWordSpecifics   = self.replaceWithWordExpansionSpecifics
	
	def setAfterSpacesStrategy(self):
		self.eraseSomeSpecifics         = self.eraseSomeCountSpecifics
		self.eraseAllSpecifics          = self.eraseAllSpacesSpecifics
		self.replaceWithSpacesSpecifics = self.replaceWithSpacesDeletionSpecifics
		self.replaceWithLetterSpecifics = self.replaceWithLetterPrefixSpecifics
		self.replaceWithStringSpecifics = self.replaceWithStringSpacesSpecifics
		self.replaceWithWordSpecifics   = self.replaceWithWordSpacesSpecifics
	
	def setAfterFirstCarriageReturnStrategy(self):
		self.eraseSomeSpecifics         = self.eraseSomeCountAfterFirstCarriageReturnSpecifics
		self.eraseAllSpecifics          = self.eraseAllSpacesSpecifics
		self.replaceWithSpacesSpecifics = self.replaceWithSpacesDeletionSpecifics
		self.replaceWithLetterSpecifics = self.replaceWithLetterPrefixSpecifics
		self.replaceWithStringSpecifics = self.replaceWithStringSpacesSpecifics
		self.replaceWithWordSpecifics   = self.replaceWithWordSpacesSpecifics
	
	def setAfterSpacesBeforeSelectionStrategy(self):
		self.eraseSomeSpecifics         = self.eraseSomeNoCountSpecifics
		self.eraseAllSpecifics          = self.eraseAllSelectionSpecifics
		self.replaceWithSpacesSpecifics = self.replaceWithSpacesSelectionSpecifics
		self.replaceWithLetterSpecifics = self.replaceWithLetterSelectionSpecifics
		self.replaceWithStringSpecifics = self.replaceWithStringSelectionSpecifics
		self.replaceWithWordSpecifics   = self.replaceWithWordSpacesSpecifics
	
	def setAfterSpacesBeforeCalculableSelectionStrategy(self):
		self.eraseSomeSpecifics         = self.eraseSomeNoCountSpecifics
		self.eraseAllSpecifics          = self.eraseAllSelectionSpecifics
		self.replaceWithSpacesSpecifics = self.replaceWithSpacesCalculableSelectionSpecifics
		self.replaceWithLetterSpecifics = self.replaceWithLetterSelectionSpecifics
		self.replaceWithStringSpecifics = self.replaceWithStringCalculableSelectionSpecifics
		self.replaceWithWordSpecifics   = self.replaceWithWordSpacesSpecifics
	
	def setAfterSpacesBeforeExpansionStrategy(self):
		self.eraseSomeSpecifics         = self.eraseSomeCountSpecifics
		self.eraseAllSpecifics          = self.eraseAllSelectionSpecifics
		self.replaceWithSpacesSpecifics = self.replaceWithSpacesDeletionSpecifics
		self.replaceWithLetterSpecifics = self.replaceWithLetterPrefixSpecifics
		self.replaceWithStringSpecifics = self.replaceWithStringExpansionSpecifics
		self.replaceWithWordSpecifics   = self.replaceWithWordExpansionSpecifics
	
	
# Private behaviours
	
	def eraseSomeCountSpecifics(self,i):
		self.replaceInEditorSetter(u"",i,len(self.selectedText))
	
	def eraseSomeCountAfterFirstCarriageReturnSpecifics(self, i):
		self.replaceInEditorSetter(u" ",i,len(self.selectedText))
	
	def eraseSomeNoCountSpecifics(self,_):
		self.replaceInEditorSetter(u"",0,len(self.selectedText))
	
	
	def eraseAllPrefixSpecifics(self):
		self.replaceInEditorSetter(u"",len(self.prefix)+1,0)
	
	def eraseAllSelectionSpecifics(self):
		self.replaceInEditorSetter(u"",0,len(self.selectedText))
	
	def eraseAllSpacesSpecifics(self):
		self.replaceInEditorSetter(u"",self.erasableLength,0)
	
	
	def replaceWithSpacesPrefixSpecifics(self,spaces):
		self.mayLearnInLearner(self.prefix + " ")
		self.replaceInEditorSetter(spaces,0,0)
	
	def replaceWithSpacesPrefixSelectionSpecifics(self,spaces):
		self.mayLearnInLearner(self.prefix)
		self.replaceInEditorSetter(spaces,0,len(self.selectedText))
	
	def replaceWithSpacesSelectionSpecifics(self,spaces):
		self.replaceInEditorSetter(spaces,len(self.getTransformableStringInSpaceTransformer()),len(self.selectedText))
	
	def replaceWithSpacesCalculableSelectionSpecifics(self,spaces):
		self.replaceInEditorSetter(self.getReplacementStringInCalculator(spaces),0,len(self.selectedText))
	
	def replaceWithSpacesExpansionSpecifics(self,_):
		self.defineInExpander(self.prefix,self.expandedText)
		self.mayLearnInLearner(self.prefix + " ")
		self.replaceInEditorSetter(self.expandedText,len(self.prefix),len(self.getPatternInExpander() % self.expandedText))
	
	def replaceWithSpacesDeletionSpecifics(self,spaces):
		self.replaceInEditorSetter(spaces,len(self.getTransformableStringInSpaceTransformer()),len(self.selectedText))
	
	
	def replaceWithLetterSelectionExpansionSpecifics(self,letter):
		self.replaceInEditorSetter(letter,0,len(self.selectedText),self.getPatternInExpander() % self.selectedText)
	
	def replaceWithLetterPrefixSpecifics(self,letter):
		self.replaceInEditorSetter(letter,0,len(self.selectedText))
	
	
	def replaceWithStringPrefixSpecifics(self,string):
		self.replaceInEditorSetter(string,len(self.prefix)+1,len(self.selectedText))
	
	def replaceWithStringSelectionSpecifics(self,string):
		self.replaceInEditorSetter(string,1,len(self.selectedText))
	
	def replaceWithStringCalculableSelectionSpecifics(self,string):
		self.replaceInEditorSetter(string,0,len(self.selectedText))
	
	def replaceWithStringSpacesSpecifics(self,string):
		self.replaceInEditorSetter(string,self.erasableLength,0)
	
	def replaceWithStringExpansionSpecifics(self,string):
		self.defineInExpander(string,self.expandedText)
		self.replaceInEditorSetter(self.expandedText,len(self.prefix),len(self.getPatternInExpander() % self.expandedText))
	
	
	def replaceWithWordPrefixSpecifics(self,richString,poorString):
		self.mayLearnInLearner(poorString)
		self.replaceInEditorSetter(richString,len(self.getRichLastSpaceInContentGetter())+len(self.prefix),len(self.selectedText))
	
	def replaceWithWordExpansionSpecifics(self,_,poorString):
		self.defineInExpander(poorString,self.expandedText)
		self.mayLearnInLearner(poorString)
		self.replaceInEditorSetter(self.expandedText,len(self.prefix),len(self.getPatternInExpander() % self.expandedText))
	
	def replaceWithWordSpacesSpecifics(self,richString,poorString):
		self.mayLearnInLearner(poorString)
		self.replaceInEditorSetter(richString,1,len(self.selectedText))
	

if __name__ == "__main__":
	from  chewing import main
	main()
