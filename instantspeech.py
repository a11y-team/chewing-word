#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file instantspeech.py


# builtin stuff
from threading import Thread
import time
# home-made stuff
from aspect import aspect

class InstantSpeech:
	
	def __init__(self, mediator):
		self.prepareToSpeakInTts            = mediator.tts.prepareToSpeak
		self.getHashInTts                   = mediator.tts.getHash
		self.preProcessInSpeechTransformer  = mediator.speechTransformer.preProcess
		self.postProcessInSpeechTransformer = mediator.speechTransformer.postProcess
		self.waitingSongs = []
		self.thread = None
		self.inPreparation = None
		self.bufferStart = 1-aspect.instantSpeechBufferSize
	
	def register(self,view):
		self.view = view
	
	def setCache(self, cache):
		self.cache = cache
	
	def speak(self,string):
		self.speakDependingOnRestrictAudioStrategy(string)
	
	def prepareString(self,string):
		self.prepareStringDependingOnRestrictAudioStrategy(string)
	
	def checkPictoModeStrategy(self,strategy):
		if strategy:
			self.speakPictoDependingOnCheckPictoModeStrategy = self.doSpeak
			self.prepareStringPictoDependingOnCheckPictoModeStrategy = self.doPrepareString
		else:
			self.speakPictoDependingOnCheckPictoModeStrategy = (lambda *args: None)
			self.prepareStringPictoDependingOnCheckPictoModeStrategy = (lambda *args: None)
	
	def checkNonPictoModeStrategy(self,strategy):
		if strategy:
			self.speakNonPictoDependingOnCheckNonPictoModeStrategy = self.doSpeak
			self.prepareStringNonPictoDependingOnCheckNonPictoModeStrategy = self.doPrepareString
		else:
			self.speakNonPictoDependingOnCheckNonPictoModeStrategy = (lambda *args: None)
			self.prepareStringNonPictoDependingOnCheckNonPictoModeStrategy = (lambda *args: None)
	
	def checkRestrictAudioStrategy(self,strategy):
		if strategy:
			self.speakDependingOnRestrictAudioStrategy = (lambda string: self.view.speakLink("%s/%s" % (self.cache.getAbsolutePath(),self.getHashInTts(string))))
			self.prepareStringDependingOnRestrictAudioStrategy = (lambda *args: None)
		else:
			self.speakDependingOnRestrictAudioStrategy = self.speakPictoOrNonPicto
			self.prepareStringDependingOnRestrictAudioStrategy = self.prepareStringPictoOrNonPicto
	
	def checkSpellStrategy(self,strategy):
		if strategy:
			self.preProcessDependingOnSpellStrategy = self.preProcessInSpeechTransformer
			self.postProcessDependingOnSpellStrategy = self.postProcessInSpeechTransformer
		else:
			self.preProcessDependingOnSpellStrategy = lambda string: string
			self.postProcessDependingOnSpellStrategy = lambda string: None
	
	def preProcess(self,string):
		return self.preProcessDependingOnSpellStrategy(string)
	
	def postProcess(self,string):
		self.postProcessDependingOnSpellStrategy(string)
	
# all the following is private
	
	def doSpeak(self,string):
		def threadableSpeak(string):
			self.waitingSongs = []
			pathBaseName = "%s/%s" % (self.cache.getAbsolutePath(),self.getHashInTts(string))
			if string == self.inPreparation:
				while string == self.inPreparation:
					time.sleep(0.2) # if the string is already in preparation, wait for it to finish
			else:
				self.prepareToSpeakInTts(string,pathBaseName)
			self.view.setSource(pathBaseName)
			self.view.speak()
			self.postProcess(string)
			# self.cache.purge()
		
		Thread(target=threadableSpeak,args=(self.preProcess(string.strip()),)).start()
	
	def doPrepareString(self,string):
		def prepareWaitingSongsInBackground():
			while True:
				try:
					string = self.waitingSongs.pop()
				except IndexError:
					break
				self.inPreparation = string
				pathBaseName = "%s/%s" % (self.cache.getAbsolutePath(),self.getHashInTts(string))
				self.prepareToSpeakInTts(string,pathBaseName)
				self.inPreparation = None
			self.thread = None
		
		self.waitingSongs = self.waitingSongs[self.bufferStart:] + [self.preProcess(string.strip())]
		if self.thread is None:
			self.thread = Thread(target=prepareWaitingSongsInBackground)
			self.thread.start()
	
	def speakPicto(self,string):
		self.speakPictoDependingOnCheckPictoModeStrategy(string)
	
	def prepareStringPicto(self,string):
		self.prepareStringPictoDependingOnCheckPictoModeStrategy(string)
	
	def speakNonPicto(self,string):
		self.speakNonPictoDependingOnCheckNonPictoModeStrategy(string)
	
	def prepareStringNonPicto(self,string):
		self.prepareStringNonPictoDependingOnCheckNonPictoModeStrategy(string)
	
	def updatePictoActivity(self,isActive):
		if isActive:
			self.speakDependingOnPictoActivity = self.speakPicto
			self.prepareStringDependingOnPictoActivity = self.prepareStringPicto
		else:
			self.speakDependingOnPictoActivity = self.speakNonPicto
			self.prepareStringDependingOnPictoActivity = self.prepareStringNonPicto
	
	def speakPictoOrNonPicto(self,string):
		self.speakDependingOnPictoActivity(string)
	
	def prepareStringPictoOrNonPicto(self,string):
		self.prepareStringDependingOnPictoActivity(string)
	


if __name__ == "__main__":
	from  chewing import main
	main()

