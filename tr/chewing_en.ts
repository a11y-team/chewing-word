<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="en">
<defaultcodec></defaultcodec>
<context>
    <name>FarPictoRatioSlider</name>
    <message>
        <location filename="preferences.py" line="525"/>
        <source>Relative picto size</source>
        <translation>Relative picto size</translation>
    </message>
</context>
<context>
    <name>FavoriteChars</name>
    <message>
        <location filename="preferences.py" line="457"/>
        <source>Favorite characters</source>
        <translation>Favorite characters</translation>
    </message>
</context>
<context>
    <name>FontEdit</name>
    <message>
        <location filename="preferences.py" line="549"/>
        <source>&amp;Modify...</source>
        <translation>&amp;Modify...</translation>
    </message>
    <message>
        <location filename="preferences.py" line="555"/>
        <source>Editor font</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GenericLocation</name>
    <message>
        <location filename="preferences.py" line="591"/>
        <source>&amp;Modify...</source>
        <translation>&amp;Modify...</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="chewing.py" line="179"/>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="chewing.py" line="180"/>
        <source>&amp;New</source>
        <translation>&amp;New</translation>
    </message>
    <message>
        <location filename="chewing.py" line="180"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="chewing.py" line="181"/>
        <source>&amp;Open...</source>
        <translation>&amp;Open...</translation>
    </message>
    <message>
        <location filename="chewing.py" line="181"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="chewing.py" line="183"/>
        <source>&amp;Close</source>
        <translation>&amp;Close</translation>
    </message>
    <message>
        <location filename="chewing.py" line="183"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="chewing.py" line="184"/>
        <source>&amp;Save</source>
        <translation>&amp;Save</translation>
    </message>
    <message>
        <location filename="chewing.py" line="184"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="chewing.py" line="185"/>
        <source>Save &amp;As...</source>
        <translation>Save &amp;As...</translation>
    </message>
    <message>
        <location filename="chewing.py" line="185"/>
        <source>Ctrl+Shift+S</source>
        <translation>Ctrl+Shift+S</translation>
    </message>
    <message>
        <location filename="chewing.py" line="187"/>
        <source>Reindex Archives</source>
        <translation>Reindex Archives</translation>
    </message>
    <message>
        <location filename="chewing.py" line="191"/>
        <source>&amp;Print...</source>
        <translation>&amp;Print...</translation>
    </message>
    <message>
        <location filename="chewing.py" line="191"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="chewing.py" line="193"/>
        <source>Quit Chewing Word</source>
        <translation>Quit Chewing Word</translation>
    </message>
    <message>
        <location filename="chewing.py" line="193"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="chewing.py" line="194"/>
        <source>&amp;Edit</source>
        <translation>&amp;Edit</translation>
    </message>
    <message>
        <location filename="chewing.py" line="195"/>
        <source>Undo</source>
        <translation>Undo</translation>
    </message>
    <message>
        <location filename="chewing.py" line="195"/>
        <source>Ctrl+Z</source>
        <translation>Ctrl+Z</translation>
    </message>
    <message>
        <location filename="chewing.py" line="196"/>
        <source>Redo</source>
        <translation>Redo</translation>
    </message>
    <message>
        <location filename="chewing.py" line="196"/>
        <source>Ctrl+Shift+Z</source>
        <translation>Ctrl+Shift+Z</translation>
    </message>
    <message>
        <location filename="chewing.py" line="198"/>
        <source>Select All</source>
        <translation>Select All</translation>
    </message>
    <message>
        <location filename="chewing.py" line="198"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="chewing.py" line="199"/>
        <source>Cu&amp;t</source>
        <translation>Cu&amp;t</translation>
    </message>
    <message>
        <location filename="chewing.py" line="199"/>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <location filename="chewing.py" line="201"/>
        <source>&amp;Copy</source>
        <translation>&amp;Copy</translation>
    </message>
    <message>
        <location filename="chewing.py" line="201"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="chewing.py" line="203"/>
        <source>&amp;Paste</source>
        <translation>&amp;Paste</translation>
    </message>
    <message>
        <location filename="chewing.py" line="203"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <location filename="chewing.py" line="205"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="chewing.py" line="207"/>
        <source>Ctrl+?</source>
        <translation>Ctrl+?</translation>
    </message>
    <message>
        <location filename="chewing.py" line="681"/>
        <source>&lt;p&gt;Do you want to archive this transcription?&lt;/p&gt;</source>
        <translation>&lt;p&gt;Do you want to archive this transcription?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1061"/>
        <source>%1/%2</source>
        <translation>%1/%2</translation>
    </message>
    <message>
        <location filename="chewing.py" line="698"/>
        <source>Transcription completed at %1h%2.txt</source>
        <translation>Transcription completed at %1h%2.txt</translation>
    </message>
    <message>
        <location filename="chewing.py" line="701"/>
        <source>&lt;p&gt;Cannot archive transcription.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Cannot archive transcription.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="707"/>
        <source>Transcription archived</source>
        <translation>Transcription archived</translation>
    </message>
    <message>
        <location filename="chewing.py" line="716"/>
        <source>&lt;p&gt;Do you want to create a link to this document in the archive directory?&lt;/p&gt;</source>
        <translation>&lt;p&gt;Do you want to create a link to this document in the archive directory?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="727"/>
        <source>%1/links</source>
        <translation>%1/links</translation>
    </message>
    <message>
        <location filename="chewing.py" line="738"/>
        <source>&lt;p&gt;Cannot create a link to this document.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Cannot create a link to this document.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="740"/>
        <source>Link created</source>
        <translation>Link created</translation>
    </message>
    <message>
        <location filename="chewing.py" line="749"/>
        <source>Index updated</source>
        <translation>Index updated</translation>
    </message>
    <message>
        <location filename="chewing.py" line="760"/>
        <source>&lt;p&gt;No transcription is currently archived in the directory &lt;tt&gt;%1&lt;/tt&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;No transcription is currently archived in the directory &lt;tt&gt;%1&lt;/tt&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="768"/>
        <source>&lt;p&gt;Process aborted due to a problem with your archive &lt;tt&gt;%1&lt;/tt&gt;.&lt;br&gt;You might want to delete this file.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Process aborted due to a problem with your archive &lt;tt&gt;%1&lt;/tt&gt;.&lt;br&gt;You might want to delete this file.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="769"/>
        <source>Reindexing failed.</source>
        <translation>Reindexing failed.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="780"/>
        <source>but the resulting index is empty.&lt;br&gt;You might want to check the content of the directory &lt;tt&gt;%1&lt;/tt&gt;.&lt;p&gt;</source>
        <translation>but the resulting index is empty.&lt;br&gt;You might want to check the content of the directory &lt;tt&gt;%1&lt;/tt&gt;.&lt;p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="783"/>
        <source>Reindexing successful.</source>
        <translation>Reindexing successful.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="793"/>
        <source>&lt;p&gt;Cannot access or create any archive directory.&lt;br&gt;You might want to specify another directory in the Preferences panel.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Cannot access or create any archive directory.&lt;br&gt;You might want to specify another directory in the Preferences panel.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="804"/>
        <source>Cannot write index.</source>
        <translation>Cannot write index.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="814"/>
        <source>&lt;p&gt;The archives index seems corrupted.&lt;br&gt;Would you like me to reconstruct it right now?&lt;/p&gt;</source>
        <translation>&lt;p&gt;The archives index seems corrupted.&lt;br&gt;Would you like me to reconstruct it right now?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="823"/>
        <source>Cannot read index.</source>
        <translation>Cannot read index.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="834"/>
        <source>&lt;p&gt;The expansion file seems corrupted.&lt;br&gt;You might want to check it manually.&lt;/p&gt;</source>
        <translation>&lt;p&gt;The expansion file seems corrupted.&lt;br&gt;You might want to check it manually.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="839"/>
        <source>Cannot read expansion file.</source>
        <translation>Cannot read expansion file.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="863"/>
        <source>Cannot write expansion file.</source>
        <translation>Cannot write expansion file.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1104"/>
        <source>Open text file</source>
        <translation>Open text file</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1117"/>
        <source>Plain Text (*.txt)</source>
        <translation>Plain Text (*.txt)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1116"/>
        <source>/untitled.</source>
        <translation>/untitled.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1117"/>
        <source>Save plain text as</source>
        <translation>Save plain text as</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1135"/>
        <source>&lt;p&gt;The document has been modified.&lt;br&gt;Do you want to save your changes?&lt;/p&gt;</source>
        <translation>&lt;p&gt;The document has been modified.&lt;br&gt;Do you want to save your changes?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1147"/>
        <source>&lt;p&gt;Cannot read file %1:
%2.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Cannot read file %1:%2.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1154"/>
        <source>File loaded</source>
        <translation>File loaded</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1159"/>
        <source>&lt;p&gt;Cannot write file %1:
%2.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Cannot write file %1:
%2.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1166"/>
        <source>File saved</source>
        <translation>File saved</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1177"/>
        <source>%1[*]</source>
        <translation>%1[*]</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1188"/>
        <source>Ready</source>
        <translation>Ready</translation>
    </message>
    <message>
        <location filename="chewing.py" line="45"/>
        <source>carriage return</source>
        <translation>carriage return</translation>
    </message>
    <message>
        <location filename="chewing.py" line="46"/>
        <source>tabulation</source>
        <translation>tabulation</translation>
    </message>
    <message>
        <location filename="chewing.py" line="207"/>
        <source>User Manual (offline, without videos)</source>
        <translation>Offline Documentation (without videos)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="208"/>
        <source>User Manual (online, with videos)</source>
        <translation>Online Documentation (with videos)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="209"/>
        <source>Visit Chewing Word Site</source>
        <translation>Visit Chewing Word Site</translation>
    </message>
    <message>
        <location filename="chewing.py" line="51"/>
        <source>3rdPerson</source>
        <translation>Third person</translation>
    </message>
    <message>
        <location filename="chewing.py" line="49"/>
        <source>moveClick</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="chewing.py" line="52"/>
        <source>move</source>
        <translation>Auto-click</translation>
    </message>
    <message>
        <location filename="chewing.py" line="228"/>
        <source>&amp;Profile</source>
        <translation>&amp;Profile</translation>
    </message>
    <message>
        <location filename="chewing.py" line="55"/>
        <source>technophobe</source>
        <translation>Technophobe</translation>
    </message>
    <message>
        <location filename="chewing.py" line="56"/>
        <source>beginner</source>
        <translation>Beginner</translation>
    </message>
    <message>
        <location filename="chewing.py" line="54"/>
        <source>moveSlow</source>
        <translation>Auto-click (beginner)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="110"/>
        <source>Truncation without deleting</source>
        <translation>Truncation without deleting</translation>
    </message>
    <message>
        <location filename="chewing.py" line="111"/>
        <source>Truncation by deleting</source>
        <translation>Truncation by deleting</translation>
    </message>
    <message>
        <location filename="chewing.py" line="112"/>
        <source>Composition of the whole prediction</source>
        <translation>Composition of the whole prediction</translation>
    </message>
    <message>
        <location filename="chewing.py" line="118"/>
        <source>Standard</source>
        <translation>Manual move and click</translation>
    </message>
    <message>
        <location filename="chewing.py" line="119"/>
        <source>Auto-click</source>
        <translation>Manual move and automatic click</translation>
    </message>
    <message>
        <location filename="chewing.py" line="120"/>
        <source>Scan</source>
        <translation>Automatic scanning and manual click</translation>
    </message>
    <message>
        <location filename="chewing.py" line="559"/>
        <source>Check some profile to populate this menu...</source>
        <translation>Check some profile to populate this menu...</translation>
    </message>
    <message>
        <location filename="chewing.py" line="200"/>
        <source>Cut All</source>
        <translation>Cut All</translation>
    </message>
    <message>
        <location filename="chewing.py" line="200"/>
        <source>Ctrl+Shift+X</source>
        <translation>Ctrl+Shift+X</translation>
    </message>
    <message>
        <location filename="chewing.py" line="202"/>
        <source>Copy All</source>
        <translation>Copy All</translation>
    </message>
    <message>
        <location filename="chewing.py" line="202"/>
        <source>Ctrl+Shift+C</source>
        <translation>Ctrl+Shift+C</translation>
    </message>
    <message>
        <location filename="chewing.py" line="70"/>
        <source>normal</source>
        <translation>no</translation>
    </message>
    <message>
        <location filename="chewing.py" line="71"/>
        <source>two clicks</source>
        <translation>click -&gt; click</translation>
    </message>
    <message>
        <location filename="chewing.py" line="73"/>
        <source>double click and click</source>
        <translation>double click -&gt; click</translation>
    </message>
    <message>
        <location filename="chewing.py" line="72"/>
        <source>double click</source>
        <translation>double click</translation>
    </message>
    <message>
        <location filename="chewing.py" line="75"/>
        <source>two right clicks</source>
        <translation>right click -&gt; right click</translation>
    </message>
    <message>
        <location filename="chewing.py" line="74"/>
        <source>right click</source>
        <translation>right click</translation>
    </message>
    <message>
        <location filename="chewing.py" line="57"/>
        <source>scanTechnophobe</source>
        <translation>Scan (technophobe)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="58"/>
        <source>scanPoor</source>
        <translation>Scan (beginner)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="59"/>
        <source>scan</source>
        <translation>Scan (optimal)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="60"/>
        <source>scanTrunk</source>
        <translation>Scan (with truncation)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="82"/>
        <source>no jump</source>
        <translation>minimal</translation>
    </message>
    <message>
        <location filename="chewing.py" line="83"/>
        <source>jump everywhere</source>
        <translation>+ rectifications and favorites</translation>
    </message>
    <message>
        <location filename="chewing.py" line="84"/>
        <source>jump on all letters</source>
        <translation>+ truncatable predictions</translation>
    </message>
    <message>
        <location filename="chewing.py" line="85"/>
        <source>jump after initials</source>
        <translation>+ jumps and punctuation</translation>
    </message>
    <message>
        <location filename="chewing.py" line="91"/>
        <source>nothing</source>
        <translation>doesn&apos;t change anything</translation>
    </message>
    <message>
        <location filename="chewing.py" line="92"/>
        <source>accentuation variants</source>
        <translation>changes the accentuation</translation>
    </message>
    <message>
        <location filename="chewing.py" line="93"/>
        <source>capitalization variants</source>
        <translation>changes the cases</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1062"/>
        <source>Scan traced at %1h%2.json</source>
        <translation>Scan report of %1h%2.json</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1068"/>
        <source>Cannot write trace scan file.</source>
        <translation>Cannot write trace scan report.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="81"/>
        <source>manual</source>
        <translation>minimal (scan by clicking)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="61"/>
        <source>easyPictoHunt</source>
        <translation>Picto Hunt (beginner)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="62"/>
        <source>hardPictoHunt</source>
        <translation>Picto Hunt (good reader)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="99"/>
        <source>alphabetic</source>
        <translation>alphabetic</translation>
    </message>
    <message>
        <location filename="chewing.py" line="102"/>
        <source>static</source>
        <translation>static frequencies</translation>
    </message>
    <message>
        <location filename="chewing.py" line="103"/>
        <source>semi-dynamic</source>
        <translation>semi-dynamic</translation>
    </message>
    <message>
        <location filename="chewing.py" line="104"/>
        <source>dynamic</source>
        <translation>dynamic</translation>
    </message>
    <message>
        <location filename="chewing.py" line="188"/>
        <source>Index New Pictos</source>
        <translation>Index New Pictos</translation>
    </message>
    <message>
        <location filename="chewing.py" line="872"/>
        <source>Problem with the flexion dictionary.</source>
        <translation>Problem with the flexion dictionary.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="889"/>
        <source>&lt;p&gt;Cannot access or create any picto directory.&lt;br&gt;You might want to specify another archive directory in the Preferences panel.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Cannot access or create any picto directory.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="907"/>
        <source>Key error in picto index.</source>
        <translation>Key error in picto index.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="909"/>
        <source>Malformed picto index.</source>
        <translation>Syntax error in picto index.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="981"/>
        <source>You might want to relaunch Chewing Word for the changes to take effect.&lt;/p&gt;</source>
        <translation>You might want to relaunch Chewing Word for the changes to take effect.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="981"/>
        <source>Picto reindexing successful.</source>
        <translation>Picto reindexing successful.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="883"/>
        <source>&lt;p&gt;I have just created for you a directory &lt;tt&gt;%1&lt;/tt&gt; in which you should move the pictos you want me to index.&lt;br&gt;Accepted formats: %2.&lt;/p&gt;</source>
        <translation>&lt;p&gt;I have just created for you a directory &lt;tt&gt;%1&lt;/tt&gt; in which you should move the pictos you want me to index.&lt;br&gt;Accepted formats: %2.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="972"/>
        <source>&lt;p&gt;No picto file is currently present in the directory &lt;tt&gt;%1&lt;/tt&gt;.&lt;br&gt;Accepted formats: %2.&lt;/p&gt;</source>
        <translation>&lt;p&gt;No picto file is currently present in the directory &lt;tt&gt;%1&lt;/tt&gt;.&lt;br&gt;Accepted formats: %2.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="977"/>
        <source>but all present picto files were indexed already.&lt;br&gt;You might want to add some new pictos in &lt;tt&gt;%1&lt;/tt&gt;.&lt;br&gt;Accepted formats: %2.&lt;/p&gt;</source>
        <translation>but all present picto files were indexed already.&lt;br&gt;You might want to add some new pictos in &lt;tt&gt;%1&lt;/tt&gt;.&lt;br&gt;Accepted formats: %2.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="100"/>
        <source>alphabeticScan</source>
        <translation>alphabetic for scan</translation>
    </message>
    <message>
        <location filename="chewing.py" line="101"/>
        <source>personalized</source>
        <translation>personalized</translation>
    </message>
    <message>
        <location filename="chewing.py" line="125"/>
        <source>Speak selection</source>
        <translation>Speak selection</translation>
    </message>
    <message>
        <location filename="chewing.py" line="126"/>
        <source>Create selection</source>
        <translation>Create selection</translation>
    </message>
    <message>
        <location filename="chewing.py" line="127"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="chewing.py" line="128"/>
        <source>Send selection to %s</source>
        <translation>Send selection to %s</translation>
    </message>
    <message>
        <location filename="chewing.py" line="189"/>
        <source>Reindex Audio Files</source>
        <translation>Reindex Audio Folder</translation>
    </message>
    <message>
        <location filename="chewing.py" line="266"/>
        <source>Text cursor moving. Dwell somewhere to fix the anchor.</source>
        <translation>Text cursor moving. Dwell somewhere to fix the anchor.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="267"/>
        <source>Text anchor fixed. Dwell somewhere to mark the selection.</source>
        <translation>Text anchor fixed. Dwell somewhere to mark the selection.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="268"/>
        <source>Text cursor fixed. Dwell somewhere to move it.</source>
        <translation>Text cursor fixed. Dwell somewhere to start to move it.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="999"/>
        <source>&lt;p&gt;Cannot access or create any audio directory.&lt;br&gt;You might want to specify another archive directory in the Preferences panel.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Cannot access or create any audio directory.&lt;br&gt;You might want to specify another archive directory in the Preferences panel.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1017"/>
        <source>Process file &apos;%1&apos;.</source>
        <translation>Processing file &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1033"/>
        <source>Problem with file &apos;%1&apos;.</source>
        <translation>Problem with file &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1045"/>
        <source>&lt;p&gt;No text or audio file is currently present in the directory &lt;tt&gt;%1&lt;/tt&gt;.&lt;br&gt;Accepted formats: .txt, %2.&lt;/p&gt;</source>
        <translation>&lt;p&gt;No text or audio file is currently present in any subdirectory of &lt;tt&gt;%1&lt;/tt&gt;.&lt;br&gt;Accepted formats: .txt, %2.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1050"/>
        <source>Audio indexing successful.</source>
        <translation>Audio indexing successful.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="993"/>
        <source>&lt;p&gt;I have just created for you a directory &lt;tt&gt;%1&lt;/tt&gt; in which you should move some audio files to index or some text files you want me to precalculate the reading.&lt;br&gt;Accepted formats: .txt, %2.&lt;/p&gt;</source>
        <translation>&lt;p&gt;I have just created for you a directory &lt;tt&gt;%1&lt;/tt&gt; in which you should move some audio files to index or some text files you want me to precalculate the vocalisation.&lt;br&gt;Accepted formats: .txt, %2.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="63"/>
        <source>Coverflow</source>
        <translation>Juke-box</translation>
    </message>
    <message>
        <location filename="chewing.py" line="130"/>
        <source>Once upon a midnight dreary, while I pondered weak and weary, over many a quaint and curious volume of forgotten lore, while I nodded, nearly napping, suddenly there came a tapping, as of some one gently rapping, rapping at my chamber door.</source>
        <translation>Once upon a midnight dreary, while I pondered weak and weary, over many a quaint and curious volume of forgotten lore, while I nodded, nearly napping, suddenly there came a tapping, as of some one gently rapping, rapping at my chamber door.</translation>
    </message>
    <message numerus="yes">
        <location filename="chewing.py" line="1048"/>
        <source>&lt;p&gt;%n directories have been scanned, </source>
        <translation>
            <numerusform>&lt;p&gt;%n directory has been scanned, </numerusform>
            <numerusform>&lt;p&gt;%n directories have been scanned, </numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="chewing.py" line="777"/>
        <source>&lt;p&gt;%n files have been scanned, </source>
        <translation>
            <numerusform>&lt;p&gt;%n file has been scanned, </numerusform>
            <numerusform>&lt;p&gt;%n files have been scanned, </numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="chewing.py" line="782"/>
        <source>resulting in an index of %n words.&lt;/p&gt;</source>
        <translation>
            <numerusform>resulting in an index of %n word.&lt;/p&gt;</numerusform>
            <numerusform>resulting in an index of %n words.&lt;/p&gt;</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="chewing.py" line="979"/>
        <source>resulting in %n newly indexed picto files.&lt;br&gt;</source>
        <translation>
            <numerusform>resulting in %n newly indexed picto file.&lt;br&gt;</numerusform>
            <numerusform>resulting in %n newly indexed picto files.&lt;br&gt;</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="chewing.py" line="1049"/>
        <source>resulting in %n audio files linked.&lt;br&gt;</source>
        <translation>
            <numerusform>resulting in %n audio file linked.&lt;br&gt;</numerusform>
            <numerusform>resulting in %n audio files linked.&lt;br&gt;</numerusform>
        </translation>
    </message>
    <message>
        <location filename="chewing.py" line="50"/>
        <source>moveClickSecure</source>
        <translation>Standard (secure)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="53"/>
        <source>moveSecure</source>
        <translation>Auto-click (secure)</translation>
    </message>
</context>
<context>
    <name>Preferences</name>
    <message>
        <location filename="preferences.py" line="243"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="preferences.py" line="244"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="preferences.py" line="134"/>
        <source>Editor</source>
        <translation>Editor</translation>
    </message>
    <message>
        <location filename="preferences.py" line="266"/>
        <source>Preferences</source>
        <translation>Preferences</translation>
    </message>
    <message>
        <location filename="preferences.py" line="91"/>
        <source>Resize chars along with the window</source>
        <translation>Resize chars along with the window</translation>
    </message>
    <message>
        <location filename="preferences.py" line="92"/>
        <source>Make letters appear progressively</source>
        <translation>Make letters appear progressively</translation>
    </message>
    <message>
        <location filename="preferences.py" line="93"/>
        <source>Place the dial above the editor</source>
        <translation>Place the dial above the editor</translation>
    </message>
    <message>
        <location filename="preferences.py" line="94"/>
        <source>Show phonetic alphabet</source>
        <translation>Show phonetic alphabet</translation>
    </message>
    <message>
        <location filename="preferences.py" line="126"/>
        <source>Archive untitled transcriptions</source>
        <translation>Archive untitled transcriptions</translation>
    </message>
    <message>
        <location filename="preferences.py" line="127"/>
        <source>Silently</source>
        <translation>Silently</translation>
    </message>
    <message>
        <location filename="preferences.py" line="127"/>
        <source>On demand</source>
        <translation>On demand</translation>
    </message>
    <message>
        <location filename="preferences.py" line="127"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="preferences.py" line="127"/>
        <source>Archive saved transcriptions as well</source>
        <translation>Archive saved transcriptions as well</translation>
    </message>
    <message>
        <location filename="preferences.py" line="99"/>
        <source>Use selected text to define abbreviations</source>
        <translation>Use selected text to define abbreviations</translation>
    </message>
    <message>
        <location filename="preferences.py" line="104"/>
        <source>Punctuation on space</source>
        <translation>Punctuation on space</translation>
    </message>
    <message>
        <location filename="preferences.py" line="98"/>
        <source>Contextual capitalization</source>
        <translation>Contextual capitalization</translation>
    </message>
    <message>
        <location filename="preferences.py" line="111"/>
        <source>Show prediction balloon</source>
        <translation>Show prediction balloon</translation>
    </message>
    <message>
        <location filename="preferences.py" line="112"/>
        <source>Letters</source>
        <translation>Letters</translation>
    </message>
    <message>
        <location filename="preferences.py" line="113"/>
        <source>Truncatable predictions</source>
        <translation>Truncatable predictions</translation>
    </message>
    <message>
        <location filename="preferences.py" line="115"/>
        <source>Space</source>
        <translation>Space</translation>
    </message>
    <message>
        <location filename="preferences.py" line="117"/>
        <source>When the selection is calculable, show the result</source>
        <translation>When the selection is calculable, show the result</translation>
    </message>
    <message>
        <location filename="preferences.py" line="118"/>
        <source>Eraser</source>
        <translation>Eraser</translation>
    </message>
    <message>
        <location filename="preferences.py" line="119"/>
        <source>Capitalization correction</source>
        <translation>Capitalization correction</translation>
    </message>
    <message>
        <location filename="preferences.py" line="120"/>
        <source>Accentuation correction</source>
        <translation>Accentuation correction</translation>
    </message>
    <message>
        <location filename="preferences.py" line="107"/>
        <source>Click on background to undo/redo last editing actions</source>
        <translation>Click on background to undo/redo last editing actions</translation>
    </message>
    <message>
        <location filename="preferences.py" line="171"/>
        <source>Aspect</source>
        <translation>Aspect</translation>
    </message>
    <message>
        <location filename="preferences.py" line="131"/>
        <source>Keys</source>
        <translation>Keys</translation>
    </message>
    <message>
        <location filename="preferences.py" line="170"/>
        <source>Balloon</source>
        <translation>Balloon</translation>
    </message>
    <message>
        <location filename="preferences.py" line="168"/>
        <source>Interaction</source>
        <translation>Interaction</translation>
    </message>
    <message>
        <location filename="preferences.py" line="174"/>
        <source>Persistance</source>
        <translation>Persistence</translation>
    </message>
    <message>
        <location filename="preferences.py" line="29"/>
        <source>Start with last profile</source>
        <translation>Start with last profile</translation>
    </message>
    <message>
        <location filename="preferences.py" line="30"/>
        <source>Ask for profile at startup</source>
        <translation>Ask for profile at startup</translation>
    </message>
    <message>
        <location filename="preferences.py" line="89"/>
        <source>Tabulation size</source>
        <translation>Tabulation size</translation>
    </message>
    <message>
        <location filename="preferences.py" line="54"/>
        <source>Current mode</source>
        <translation>Current mode</translation>
    </message>
    <message>
        <location filename="preferences.py" line="79"/>
        <source>00 ms</source>
        <translation>00 ms</translation>
    </message>
    <message>
        <location filename="preferences.py" line="60"/>
        <source>Auto-click duration</source>
        <translation>Stop duration before click</translation>
    </message>
    <message>
        <location filename="preferences.py" line="81"/>
        <source>Tempo</source>
        <translation>Tempo</translation>
    </message>
    <message>
        <location filename="preferences.py" line="81"/>
        <source> bpm)</source>
        <translation> bpm)</translation>
    </message>
    <message>
        <location filename="preferences.py" line="68"/>
        <source>Jump value</source>
        <translation>A long click at the initial jumps</translation>
    </message>
    <message>
        <location filename="preferences.py" line="68"/>
        <source> letters</source>
        <translation> letters</translation>
    </message>
    <message>
        <location filename="preferences.py" line="61"/>
        <source>Extra delay to start a sequence</source>
        <translation>Awareness extra delay</translation>
    </message>
    <message>
        <location filename="preferences.py" line="62"/>
        <source>Radius of tolerance</source>
        <translation>Tolerance radius</translation>
    </message>
    <message>
        <location filename="preferences.py" line="62"/>
        <source> px</source>
        <translation>px</translation>
    </message>
    <message>
        <location filename="preferences.py" line="105"/>
        <source>Click on space to add the predicted accentuation</source>
        <translation>Click on space to add the predicted accentuation</translation>
    </message>
    <message>
        <location filename="preferences.py" line="79"/>
        <source>Short click maximal duration</source>
        <translation>Short click maximal duration</translation>
    </message>
    <message>
        <location filename="preferences.py" line="63"/>
        <source>Audio feedback</source>
        <translation>Audio feedback</translation>
    </message>
    <message>
        <location filename="preferences.py" line="83"/>
        <source>Start sound</source>
        <translation>Initial sound</translation>
    </message>
    <message>
        <location filename="preferences.py" line="84"/>
        <source>Step sound</source>
        <translation>Next sounds</translation>
    </message>
    <message>
        <location filename="preferences.py" line="106"/>
        <source>Background</source>
        <translation>Background</translation>
    </message>
    <message>
        <location filename="preferences.py" line="163"/>
        <source>Trace scan mode</source>
        <translation>Scan report</translation>
    </message>
    <message>
        <location filename="preferences.py" line="169"/>
        <source>Long click</source>
        <translation>Long click</translation>
    </message>
    <message>
        <location filename="preferences.py" line="57"/>
        <source>Long click scrolls through</source>
        <translation>Long click</translation>
    </message>
    <message>
        <location filename="preferences.py" line="67"/>
        <source>Behaviour</source>
        <translation>Domain</translation>
    </message>
    <message>
        <location filename="preferences.py" line="80"/>
        <source>Simulated long click</source>
        <translation>Long click simulation</translation>
    </message>
    <message>
        <location filename="preferences.py" line="114"/>
        <source>Click before separation</source>
        <translation>Click at prediction start</translation>
    </message>
    <message>
        <location filename="preferences.py" line="164"/>
        <source>Replay speed percentage</source>
        <translation>Replay speed (percentage)</translation>
    </message>
    <message>
        <location filename="preferences.py" line="101"/>
        <source>Keyboard order</source>
        <translation>Keyboard order</translation>
    </message>
    <message>
        <location filename="preferences.py" line="125"/>
        <source>Location of archives</source>
        <translation>Location of previous transcriptions&apos; archives</translation>
    </message>
    <message>
        <location filename="preferences.py" line="125"/>
        <source>Choose location for archiving transcriptions</source>
        <translation>Choose location for archiving transcriptions</translation>
    </message>
    <message>
        <location filename="preferences.py" line="130"/>
        <source>Enable pictograms</source>
        <translation>Enable pictograms</translation>
    </message>
    <message>
        <location filename="preferences.py" line="135"/>
        <source>Display direct pictos</source>
        <translation>Display direct pictos</translation>
    </message>
    <message>
        <location filename="preferences.py" line="136"/>
        <source>Display indirect pictos</source>
        <translation>Display indirect pictos</translation>
    </message>
    <message>
        <location filename="preferences.py" line="137"/>
        <source>Display multiple pictos</source>
        <translation>Display multiple pictos</translation>
    </message>
    <message>
        <location filename="preferences.py" line="172"/>
        <source>Pictos</source>
        <translation>Pictos</translation>
    </message>
    <message>
        <location filename="preferences.py" line="102"/>
        <source>Personalized order</source>
        <translation>Personalized order</translation>
    </message>
    <message>
        <location filename="preferences.py" line="139"/>
        <source>Colors</source>
        <translation>Colors</translation>
    </message>
    <message>
        <location filename="preferences.py" line="140"/>
        <source>Vivid colors for all pictos</source>
        <translation>Vivid colors for all pictos</translation>
    </message>
    <message>
        <location filename="preferences.py" line="143"/>
        <source>Selection</source>
        <translation>Selection</translation>
    </message>
    <message>
        <location filename="preferences.py" line="150"/>
        <source>Spell word-selection</source>
        <translation>Spell selections not containing any blank character</translation>
    </message>
    <message>
        <location filename="preferences.py" line="151"/>
        <source>Prediction</source>
        <translation>Prediction</translation>
    </message>
    <message>
        <location filename="preferences.py" line="152"/>
        <source>Feedback restricted to sound files in Audio folder</source>
        <translation>Feedback restricted to sound files in Audio folder</translation>
    </message>
    <message>
        <location filename="preferences.py" line="158"/>
        <source>Spell prediction</source>
        <translation>Spell prediction</translation>
    </message>
    <message>
        <location filename="preferences.py" line="167"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="preferences.py" line="173"/>
        <source>Speech</source>
        <translation>Speech</translation>
    </message>
    <message>
        <location filename="preferences.py" line="159"/>
        <source>Engine</source>
        <translation>Engine</translation>
    </message>
    <message>
        <location filename="preferences.py" line="64"/>
        <source>Maximal number of repetitions when dwelling</source>
        <translation>Maximal number of repetitions during dwelling</translation>
    </message>
    <message>
        <location filename="preferences.py" line="44"/>
        <source>Crash barriers</source>
        <translation>Security barriers</translation>
    </message>
    <message>
        <location filename="preferences.py" line="45"/>
        <source>on the sides</source>
        <translation>on the sides</translation>
    </message>
    <message>
        <location filename="preferences.py" line="47"/>
        <source>at the top and the bottom</source>
        <translation>at the top and the bottom</translation>
    </message>
    <message>
        <location filename="preferences.py" line="48"/>
        <source>inside</source>
        <translation>inside</translation>
    </message>
    <message>
        <location filename="preferences.py" line="145"/>
        <source>Access by</source>
        <translation>Access by</translation>
    </message>
    <message>
        <location filename="preferences.py" line="146"/>
        <source>long click on Space</source>
        <translation>long click on Space key</translation>
    </message>
    <message>
        <location filename="preferences.py" line="147"/>
        <source>click on yin-yang symbol</source>
        <translation>click on yin-yang symbol</translation>
    </message>
    <message>
        <location filename="preferences.py" line="154"/>
        <source>Feedback for predictions associated to</source>
        <translation>Feedback for predictions associated to</translation>
    </message>
    <message>
        <location filename="preferences.py" line="155"/>
        <source>some pictogram</source>
        <translation>some picto</translation>
    </message>
    <message>
        <location filename="preferences.py" line="156"/>
        <source>no pictogram</source>
        <translation>no picto</translation>
    </message>
    <message>
        <location filename="preferences.py" line="46"/>
        <source>(open)</source>
        <translation>(open)</translation>
    </message>
    <message>
        <location filename="preferences.py" line="51"/>
        <source>Width factor</source>
        <translation>Relative width</translation>
    </message>
    <message>
        <location filename="preferences.py" line="149"/>
        <source>Jump on Pause symbol when speech starts</source>
        <translation>Jump on Pause symbol when speech starts</translation>
    </message>
</context>
<context>
    <name>ProfilePanel</name>
    <message>
        <location filename="preferences.py" line="302"/>
        <source>Duplicate this profile to start creating a new one</source>
        <translation>Duplicate this profile to start creating a new one</translation>
    </message>
    <message>
        <location filename="preferences.py" line="307"/>
        <source>Suppress this profile</source>
        <translation>Suppress this profile</translation>
    </message>
    <message>
        <location filename="preferences.py" line="359"/>
        <source>Apply next modifications of this profile to all checked profiles</source>
        <translation>Apply next modifications of this profile to all checked profiles</translation>
    </message>
    <message>
        <location filename="preferences.py" line="350"/>
        <source>Apply last modifications of this profile to all checked profiles</source>
        <translation>Apply last modifications of this profile to all checked profiles</translation>
    </message>
</context>
<context>
    <name>SpeechCommandLine</name>
    <message>
        <location filename="preferences.py" line="1132"/>
        <source>Your command line may be valid. Click to make sure.</source>
        <translation>Your command line may be valid. Click to make sure.</translation>
    </message>
    <message>
        <location filename="preferences.py" line="1058"/>
        <source>Click to restore the default command.</source>
        <translation>Click to restore the default command.</translation>
    </message>
    <message>
        <location filename="preferences.py" line="1074"/>
        <source>Click to stop.</source>
        <translation>Click to stop.</translation>
    </message>
    <message>
        <location filename="preferences.py" line="1116"/>
        <source>&lt;p&gt;ERROR: underlying Phonon engine does not support audio extension &quot;%1&quot; on your system.&lt;br&gt;Accepted formats: %2.&lt;/p&gt;</source>
        <translation>&lt;p&gt;ERROR: underlying Phonon engine does not support audio extension &quot;%1&quot; on your system.&lt;br&gt;Accepted formats: %2.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="preferences.py" line="1118"/>
        <source>ERROR: the command line should contain &quot;%(output)s&quot; followed by a non-empty extension.</source>
        <translation>ERROR: the command line should contain &quot;%(output)s&quot; followed by a non-empty extension.</translation>
    </message>
    <message>
        <location filename="preferences.py" line="1120"/>
        <source>ERROR: the command line should contain &quot;%(xxxxxinput)s.txt&quot;, where xxxxx is a text encoding (ascii, macroman, latin1, etc.), utf8 by default.</source>
        <translation>ERROR: the command line should contain &quot;%(xxxxxinput)s.txt&quot;, where xxxxx is a text encoding (ascii, macroman, latin1, etc.), utf8 by default.</translation>
    </message>
</context>
<context>
    <name>SplashScreen</name>
    <message>
        <location filename="splash.py" line="38"/>
        <source>Version %1 r%2</source>
        <translation>&lt;TABLE border=&quot;0&quot; width=&quot;100%&quot; padding=&quot;0&quot;&gt;&lt;TR&gt;&lt;TD align=&quot;left&quot;&gt;&lt;b&gt;&lt;font size=6&gt;Chewing Word&lt;/font&gt;&lt;/b&gt;&lt;TD align=&quot;right&quot;&gt;&lt;tt&gt;&lt;font size=2&gt;Version %1 r%2&lt;/tt&gt;&lt;/TABLE&gt;&lt;p align=&quot;left&quot;&gt;&lt;font size=2&gt;&lt;b&gt;Aristide Grange - Laboratoire d&apos;Informatique Théorique et Appliquée&lt;/b&gt;&lt;/p&gt;&lt;p align=&quot;left&quot;&gt;&lt;font size=2&gt;Icon: Don D&apos;Ablo, &lt;i&gt;Chattering Teeth&lt;/i&gt;  © 2006 Ulia Design&lt;br&gt;Sound effects: Maxime François&lt;br&gt;Demonstration pictograms: Dirceu Veiga,  &lt;A HREF=&quot;http://www.fasticon.com/&quot;&gt;Fast Icon Studio&lt;/A&gt;&lt;br&gt;Demonstration speech synthesis engine: &lt;A HREF=&quot;http://espeak.sourceforge.net/&quot;&gt;eSpeak&lt;/A&gt;&lt;br&gt;Demonstration voice: &lt;A HREF=&quot;http://tcts.fpms.ac.be/synthesis/mbrola.html&quot;&gt;MBROLA&lt;/A&gt; fr1&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>StartupDialog</name>
    <message>
        <location filename="startupdialog.py" line="13"/>
        <source>Select your profile</source>
        <translation>Profile selection</translation>
    </message>
    <message>
        <location filename="startupdialog.py" line="23"/>
        <source>Show this dialog at startup</source>
        <translation>Show this dialog at startup</translation>
    </message>
    <message>
        <location filename="startupdialog.py" line="25"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>TongueRatioSlider</name>
    <message>
        <location filename="preferences.py" line="497"/>
        <source>Relative tongue size</source>
        <translation>Relative tongue size</translation>
    </message>
</context>
</TS>
