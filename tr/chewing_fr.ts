<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="fr">
<defaultcodec></defaultcodec>
<context>
    <name>FarPictoRatioSlider</name>
    <message>
        <location filename="preferences.py" line="525"/>
        <source>Relative picto size</source>
        <translation>Taille relative maximale des pictogrammes</translation>
    </message>
</context>
<context>
    <name>FavoriteChars</name>
    <message>
        <location filename="preferences.py" line="457"/>
        <source>Favorite characters</source>
        <translation>Caractères favoris</translation>
    </message>
</context>
<context>
    <name>FontEdit</name>
    <message>
        <location filename="preferences.py" line="549"/>
        <source>&amp;Modify...</source>
        <translation>&amp;Modifier...</translation>
    </message>
    <message>
        <location filename="preferences.py" line="555"/>
        <source>Editor font</source>
        <translation>Police de l&apos;éditeur</translation>
    </message>
</context>
<context>
    <name>GenericLocation</name>
    <message>
        <location filename="preferences.py" line="591"/>
        <source>&amp;Modify...</source>
        <translation>&amp;Modifier...</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="chewing.py" line="179"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="chewing.py" line="180"/>
        <source>&amp;New</source>
        <translation>&amp;Nouveau</translation>
    </message>
    <message>
        <location filename="chewing.py" line="180"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="chewing.py" line="181"/>
        <source>&amp;Open...</source>
        <translation>&amp;Ouvrir...</translation>
    </message>
    <message>
        <location filename="chewing.py" line="181"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="chewing.py" line="183"/>
        <source>&amp;Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="chewing.py" line="183"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="chewing.py" line="184"/>
        <source>&amp;Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="chewing.py" line="184"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="chewing.py" line="185"/>
        <source>Save &amp;As...</source>
        <translation>Enregistrer sous...</translation>
    </message>
    <message>
        <location filename="chewing.py" line="185"/>
        <source>Ctrl+Shift+S</source>
        <translation>Ctrl+Shift+S</translation>
    </message>
    <message>
        <location filename="chewing.py" line="187"/>
        <source>Reindex Archives</source>
        <translation>Réindexer les archives</translation>
    </message>
    <message>
        <location filename="chewing.py" line="191"/>
        <source>&amp;Print...</source>
        <translation>Im&amp;primer...</translation>
    </message>
    <message>
        <location filename="chewing.py" line="191"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="chewing.py" line="193"/>
        <source>Quit Chewing Word</source>
        <translation>Quitter Chewing Word</translation>
    </message>
    <message>
        <location filename="chewing.py" line="193"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="chewing.py" line="194"/>
        <source>&amp;Edit</source>
        <translation>&amp;Édition</translation>
    </message>
    <message>
        <location filename="chewing.py" line="195"/>
        <source>Undo</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="chewing.py" line="195"/>
        <source>Ctrl+Z</source>
        <translation>Ctrl+Z</translation>
    </message>
    <message>
        <location filename="chewing.py" line="196"/>
        <source>Redo</source>
        <translation>Rétablir</translation>
    </message>
    <message>
        <location filename="chewing.py" line="196"/>
        <source>Ctrl+Shift+Z</source>
        <translation>Ctrl+Shift+Z</translation>
    </message>
    <message>
        <location filename="chewing.py" line="198"/>
        <source>Select All</source>
        <translation>Tout sélectionner</translation>
    </message>
    <message>
        <location filename="chewing.py" line="198"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="chewing.py" line="199"/>
        <source>Cu&amp;t</source>
        <translation>Couper</translation>
    </message>
    <message>
        <location filename="chewing.py" line="199"/>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <location filename="chewing.py" line="201"/>
        <source>&amp;Copy</source>
        <translation>&amp;Copier</translation>
    </message>
    <message>
        <location filename="chewing.py" line="201"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="chewing.py" line="203"/>
        <source>&amp;Paste</source>
        <translation>Coller</translation>
    </message>
    <message>
        <location filename="chewing.py" line="203"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <location filename="chewing.py" line="205"/>
        <source>&amp;Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="chewing.py" line="207"/>
        <source>Ctrl+?</source>
        <translation>Ctrl+?</translation>
    </message>
    <message>
        <location filename="chewing.py" line="681"/>
        <source>&lt;p&gt;Do you want to archive this transcription?&lt;/p&gt;</source>
        <translation>&lt;p&gt;Souhaitez-vous archiver cette transcription?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1061"/>
        <source>%1/%2</source>
        <translation>%1/%2</translation>
    </message>
    <message>
        <location filename="chewing.py" line="698"/>
        <source>Transcription completed at %1h%2.txt</source>
        <translation>Transcription terminée à %1h%2.txt</translation>
    </message>
    <message>
        <location filename="chewing.py" line="701"/>
        <source>&lt;p&gt;Cannot archive transcription.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Impossible d&apos;archiver cette transcription.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="707"/>
        <source>Transcription archived</source>
        <translation>Transcription archivée</translation>
    </message>
    <message>
        <location filename="chewing.py" line="716"/>
        <source>&lt;p&gt;Do you want to create a link to this document in the archive directory?&lt;/p&gt;</source>
        <translation>&lt;p&gt;Souhaitez-vous créer un lien vers ce document dans le dossier d&apos;archives?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="727"/>
        <source>%1/links</source>
        <translation>%1/liens</translation>
    </message>
    <message>
        <location filename="chewing.py" line="738"/>
        <source>&lt;p&gt;Cannot create a link to this document.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Impossible de créer un lien vers ce document.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="740"/>
        <source>Link created</source>
        <translation>Lien créé</translation>
    </message>
    <message>
        <location filename="chewing.py" line="749"/>
        <source>Index updated</source>
        <translation>Index mis à jour</translation>
    </message>
    <message>
        <location filename="chewing.py" line="769"/>
        <source>Reindexing failed.</source>
        <translation>Échec lors de la mise à jour de l&apos;index.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="783"/>
        <source>Reindexing successful.</source>
        <translation>L&apos;index est maintenant à jour.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="793"/>
        <source>&lt;p&gt;Cannot access or create any archive directory.&lt;br&gt;You might want to specify another directory in the Preferences panel.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Impossible de trouver ou de créer un dossier d&apos;archives.&lt;br&gt;Vous devriez en spécifier un autre dans le panneau de Préférences.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="804"/>
        <source>Cannot write index.</source>
        <translation>Impossible d&apos;écrire l&apos;index.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="814"/>
        <source>&lt;p&gt;The archives index seems corrupted.&lt;br&gt;Would you like me to reconstruct it right now?&lt;/p&gt;</source>
        <translation>&lt;p&gt;L&apos;index des transcriptions semble corrompu.&lt;br&gt;Souhaitez-vous que je le reconstruise en dépouillant à nouveau les archives?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="823"/>
        <source>Cannot read index.</source>
        <translation>Impossible de lire l&apos;index.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="834"/>
        <source>&lt;p&gt;The expansion file seems corrupted.&lt;br&gt;You might want to check it manually.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Le fichier d&apos;expansions semble corrompu.&lt;br&gt;Vous devriez le contrôler à la main.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="839"/>
        <source>Cannot read expansion file.</source>
        <translation>Impossible de lire le fichiers d&apos;expansions.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="863"/>
        <source>Cannot write expansion file.</source>
        <translation>Impossible d&apos;écrire le fichier d&apos;expansions.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1104"/>
        <source>Open text file</source>
        <translation>Ouvrir un fichier texte</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1117"/>
        <source>Plain Text (*.txt)</source>
        <translation>Texte brut (*.txt)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1116"/>
        <source>/untitled.</source>
        <translation>/sans titre.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1117"/>
        <source>Save plain text as</source>
        <translation>Enregistrer le texte brut comme</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1135"/>
        <source>&lt;p&gt;The document has been modified.&lt;br&gt;Do you want to save your changes?&lt;/p&gt;</source>
        <translation>&lt;p&gt;Le document a été modifié.&lt;br&gt;Souhaitez-vous le sauvegarder?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1147"/>
        <source>&lt;p&gt;Cannot read file %1:
%2.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Impossible de lire le fichier %1:%2.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1154"/>
        <source>File loaded</source>
        <translation>Fichier chargé</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1159"/>
        <source>&lt;p&gt;Cannot write file %1:
%2.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Impossible d&apos;écrire le fichier %1:%2.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1166"/>
        <source>File saved</source>
        <translation>Fichier enregistré</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1177"/>
        <source>%1[*]</source>
        <translation>%1[*]</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1188"/>
        <source>Ready</source>
        <translation>Prêt</translation>
    </message>
    <message>
        <location filename="chewing.py" line="760"/>
        <source>&lt;p&gt;No transcription is currently archived in the directory &lt;tt&gt;%1&lt;/tt&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Pour le moment, aucune transcription n&apos;est archivée dans le dossier &lt;tt&gt;%1&lt;/tt&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="768"/>
        <source>&lt;p&gt;Process aborted due to a problem with your archive &lt;tt&gt;%1&lt;/tt&gt;.&lt;br&gt;You might want to delete this file.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Échec dû à un problème avec le fichier d&apos;archive &lt;tt&gt;%1&lt;/tt&gt;.&lt;br&gt;Vous devriez le supprimer à la main.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="780"/>
        <source>but the resulting index is empty.&lt;br&gt;You might want to check the content of the directory &lt;tt&gt;%1&lt;/tt&gt;.&lt;p&gt;</source>
        <translation>mais l&apos;index résultant est vide.&lt;br&gt;Peut-être devriez-vous vérifier le contenu du dossier &lt;tt&gt;%1&lt;/tt&gt;.&lt;p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="45"/>
        <source>carriage return</source>
        <translation>retour-chariot</translation>
    </message>
    <message>
        <location filename="chewing.py" line="46"/>
        <source>tabulation</source>
        <translation>tabulation</translation>
    </message>
    <message>
        <location filename="chewing.py" line="207"/>
        <source>User Manual (offline, without videos)</source>
        <translation>Documentation intégrée (sans vidéos)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="208"/>
        <source>User Manual (online, with videos)</source>
        <translation>Documentation en ligne (dernière version, avec vidéos)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="209"/>
        <source>Visit Chewing Word Site</source>
        <translation>Visiter le site de Chewing Word</translation>
    </message>
    <message>
        <location filename="chewing.py" line="51"/>
        <source>3rdPerson</source>
        <translation>Tierce personne</translation>
    </message>
    <message>
        <location filename="chewing.py" line="49"/>
        <source>moveClick</source>
        <translation>Standard (optimal)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="52"/>
        <source>move</source>
        <translation>Clic automatique (optimal)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="228"/>
        <source>&amp;Profile</source>
        <translation>&amp;Profil</translation>
    </message>
    <message>
        <location filename="chewing.py" line="55"/>
        <source>technophobe</source>
        <translation>Standard (technophobe)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="56"/>
        <source>beginner</source>
        <translation>Standard (débutant)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="54"/>
        <source>moveSlow</source>
        <translation>Clic automatique (débutant)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="110"/>
        <source>Truncation without deleting</source>
        <translation>Tronquer sans effacer</translation>
    </message>
    <message>
        <location filename="chewing.py" line="111"/>
        <source>Truncation by deleting</source>
        <translation>Tronquer en effaçant</translation>
    </message>
    <message>
        <location filename="chewing.py" line="112"/>
        <source>Composition of the whole prediction</source>
        <translation>Composer tout</translation>
    </message>
    <message>
        <location filename="chewing.py" line="118"/>
        <source>Standard</source>
        <translation>Déplacement et clic manuels</translation>
    </message>
    <message>
        <location filename="chewing.py" line="119"/>
        <source>Auto-click</source>
        <translation>Clic automatique</translation>
    </message>
    <message>
        <location filename="chewing.py" line="120"/>
        <source>Scan</source>
        <translation>Défilement</translation>
    </message>
    <message>
        <location filename="chewing.py" line="559"/>
        <source>Check some profile to populate this menu...</source>
        <translation>Cocher des profils pour garnir ce menu...</translation>
    </message>
    <message>
        <location filename="chewing.py" line="200"/>
        <source>Cut All</source>
        <translation>Tout couper</translation>
    </message>
    <message>
        <location filename="chewing.py" line="200"/>
        <source>Ctrl+Shift+X</source>
        <translation>Ctrl+Shift+X</translation>
    </message>
    <message>
        <location filename="chewing.py" line="202"/>
        <source>Copy All</source>
        <translation>Tout copier</translation>
    </message>
    <message>
        <location filename="chewing.py" line="202"/>
        <source>Ctrl+Shift+C</source>
        <translation>Ctrl+Shift+C</translation>
    </message>
    <message>
        <location filename="chewing.py" line="70"/>
        <source>normal</source>
        <translation>non</translation>
    </message>
    <message>
        <location filename="chewing.py" line="71"/>
        <source>two clicks</source>
        <translation>clic -&gt; clic</translation>
    </message>
    <message>
        <location filename="chewing.py" line="73"/>
        <source>double click and click</source>
        <translation>double clic -&gt; clic</translation>
    </message>
    <message>
        <location filename="chewing.py" line="72"/>
        <source>double click</source>
        <translation>double clic</translation>
    </message>
    <message>
        <location filename="chewing.py" line="75"/>
        <source>two right clicks</source>
        <translation>clic droit -&gt; clic droit</translation>
    </message>
    <message>
        <location filename="chewing.py" line="74"/>
        <source>right click</source>
        <translation>clic droit</translation>
    </message>
    <message>
        <location filename="chewing.py" line="57"/>
        <source>scanTechnophobe</source>
        <translation>Défilement (technophobe)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="58"/>
        <source>scanPoor</source>
        <translation>Défilement (débutant)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="59"/>
        <source>scan</source>
        <translation>Défilement (optimal)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="60"/>
        <source>scanTrunk</source>
        <translation>Défilement (avec troncature)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="82"/>
        <source>no jump</source>
        <translation>minimal</translation>
    </message>
    <message>
        <location filename="chewing.py" line="83"/>
        <source>jump everywhere</source>
        <translation>+ sauts et ponctuation</translation>
    </message>
    <message>
        <location filename="chewing.py" line="84"/>
        <source>jump on all letters</source>
        <translation>+ rectifications et favoris</translation>
    </message>
    <message>
        <location filename="chewing.py" line="85"/>
        <source>jump after initials</source>
        <translation>+ prédictions tronquables</translation>
    </message>
    <message>
        <location filename="chewing.py" line="91"/>
        <source>nothing</source>
        <translation>ne change rien</translation>
    </message>
    <message>
        <location filename="chewing.py" line="92"/>
        <source>accentuation variants</source>
        <translation>change les accents</translation>
    </message>
    <message>
        <location filename="chewing.py" line="93"/>
        <source>capitalization variants</source>
        <translation>change les majuscules</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1062"/>
        <source>Scan traced at %1h%2.json</source>
        <translation>Rapport de défilement de %1h%2.json</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1068"/>
        <source>Cannot write trace scan file.</source>
        <translation>Impossible d&apos;écrire le rapport de défilement.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="81"/>
        <source>manual</source>
        <translation>minimal (défilement par pression)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="61"/>
        <source>easyPictoHunt</source>
        <translation>Chasse aux pictos (débutant)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="62"/>
        <source>hardPictoHunt</source>
        <translation>Chasse aux pictos (bon lecteur)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="99"/>
        <source>alphabetic</source>
        <translation>alphabétique</translation>
    </message>
    <message>
        <location filename="chewing.py" line="102"/>
        <source>static</source>
        <translation>statique fréquentiel</translation>
    </message>
    <message>
        <location filename="chewing.py" line="103"/>
        <source>semi-dynamic</source>
        <translation>semi-dynamique</translation>
    </message>
    <message>
        <location filename="chewing.py" line="104"/>
        <source>dynamic</source>
        <translation>dynamique</translation>
    </message>
    <message>
        <location filename="chewing.py" line="188"/>
        <source>Index New Pictos</source>
        <translation>Indexer les nouveaux pictos</translation>
    </message>
    <message>
        <location filename="chewing.py" line="872"/>
        <source>Problem with the flexion dictionary.</source>
        <translation>Problème avec le dictionnaire des formes fléchies.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="889"/>
        <source>&lt;p&gt;Cannot access or create any picto directory.&lt;br&gt;You might want to specify another archive directory in the Preferences panel.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Impossible de trouver ou de créer un dossier de pictos.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="907"/>
        <source>Key error in picto index.</source>
        <translation>Problème de clef dans l&apos;un des index de pictos.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="909"/>
        <source>Malformed picto index.</source>
        <translation>Problème de syntaxe dans l&apos;un des index de pictos.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="981"/>
        <source>You might want to relaunch Chewing Word for the changes to take effect.&lt;/p&gt;</source>
        <translation>Les modifications seront effectives au prochain lancement de Chewing Word.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="981"/>
        <source>Picto reindexing successful.</source>
        <translation>Les index de pictos sont maintenant à jour.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="883"/>
        <source>&lt;p&gt;I have just created for you a directory &lt;tt&gt;%1&lt;/tt&gt; in which you should move the pictos you want me to index.&lt;br&gt;Accepted formats: %2.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Je viens de créer pour vous un dossier &lt;tt&gt;%1&lt;/tt&gt;: veuillez y placer les dossiers de pictos que vous souhaitez indexer.&lt;br&gt;Formats reconnus: %2.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="972"/>
        <source>&lt;p&gt;No picto file is currently present in the directory &lt;tt&gt;%1&lt;/tt&gt;.&lt;br&gt;Accepted formats: %2.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Pour le moment, les sous-dossiers de &lt;tt&gt;%1&lt;/tt&gt; ne contiennent aucun nouveau picto.&lt;br&gt;Formats reconnus: %2.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="977"/>
        <source>but all present picto files were indexed already.&lt;br&gt;You might want to add some new pictos in &lt;tt&gt;%1&lt;/tt&gt;.&lt;br&gt;Accepted formats: %2.&lt;/p&gt;</source>
        <translation>mais tous les pictos étaient déjà indexés.&lt;br&gt;Vous pouvez placer un nouveau dossier de pictos dans &lt;tt&gt;%1&lt;/tt&gt;.&lt;br&gt;Formats reconnus: %2.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="100"/>
        <source>alphabeticScan</source>
        <translation>alphabétique pour le défilement</translation>
    </message>
    <message>
        <location filename="chewing.py" line="101"/>
        <source>personalized</source>
        <translation>personnalisé</translation>
    </message>
    <message>
        <location filename="chewing.py" line="125"/>
        <source>Speak selection</source>
        <translation>Lire la sélection</translation>
    </message>
    <message>
        <location filename="chewing.py" line="126"/>
        <source>Create selection</source>
        <translation>Modifier la sélection</translation>
    </message>
    <message>
        <location filename="chewing.py" line="127"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="chewing.py" line="128"/>
        <source>Send selection to %s</source>
        <translation>Envoyer la sélection à %s</translation>
    </message>
    <message>
        <location filename="chewing.py" line="189"/>
        <source>Reindex Audio Files</source>
        <translation>Réindexer le dossier audio</translation>
    </message>
    <message>
        <location filename="chewing.py" line="266"/>
        <source>Text cursor moving. Dwell somewhere to fix the anchor.</source>
        <translation>Curseur mobile. Stationnez quelque part pour fixer l&apos;ancre.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="267"/>
        <source>Text anchor fixed. Dwell somewhere to mark the selection.</source>
        <translation>Ancre fixée. Stationnez quelque part pour définir la sélection.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="268"/>
        <source>Text cursor fixed. Dwell somewhere to move it.</source>
        <translation>Curseur fixé. Stationnez quelque part pour commencer à le déplacer.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="999"/>
        <source>&lt;p&gt;Cannot access or create any audio directory.&lt;br&gt;You might want to specify another archive directory in the Preferences panel.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Impossible de trouver ou de créer un dossier de sons.&lt;br&gt;Vous devriez spécifier un autre dossier d&apos;archive dans le panneau de Préférences.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1017"/>
        <source>Process file &apos;%1&apos;.</source>
        <translation>Traitement du fichier &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1033"/>
        <source>Problem with file &apos;%1&apos;.</source>
        <translation>Problème avec le fichier &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1045"/>
        <source>&lt;p&gt;No text or audio file is currently present in the directory &lt;tt&gt;%1&lt;/tt&gt;.&lt;br&gt;Accepted formats: .txt, %2.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Pour le moment, les sous-dossiers de &lt;tt&gt;%1&lt;/tt&gt; ne contiennent aucun fichier indexable.&lt;br&gt;Formats reconnus: %2.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="1050"/>
        <source>Audio indexing successful.</source>
        <translation>L&apos;index des fichiers son est maintenant à jour.</translation>
    </message>
    <message>
        <location filename="chewing.py" line="993"/>
        <source>&lt;p&gt;I have just created for you a directory &lt;tt&gt;%1&lt;/tt&gt; in which you should move some audio files to index or some text files you want me to precalculate the reading.&lt;br&gt;Accepted formats: .txt, %2.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Je viens de créer pour vous un dossier &lt;tt&gt;%1&lt;/tt&gt;: veuillez y placer les sous-dossiers contenant les sons que vous souhaitez indexer.&lt;br&gt;Formats reconnus: .txt, %2.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="chewing.py" line="63"/>
        <source>Coverflow</source>
        <translation>Jukebox (démonstration)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="130"/>
        <source>Once upon a midnight dreary, while I pondered weak and weary, over many a quaint and curious volume of forgotten lore, while I nodded, nearly napping, suddenly there came a tapping, as of some one gently rapping, rapping at my chamber door.</source>
        <translation>Hé! bonjour, Monsieur du Corbeau. Que vous êtes joli! que vous me semblez beau! Sans mentir, si votre ramage se rapporte à votre plumage, vous êtes le Phénix des hôtes de ces bois.</translation>
    </message>
    <message numerus="yes">
        <location filename="chewing.py" line="1048"/>
        <source>&lt;p&gt;%n directories have been scanned, </source>
        <translation>
            <numerusform>&lt;p&gt;1 dossier a été passé en revue, </numerusform>
            <numerusform>&lt;p&gt;%n dossiers ont été passés en revue, </numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="chewing.py" line="777"/>
        <source>&lt;p&gt;%n files have been scanned, </source>
        <translation>
            <numerusform>&lt;p&gt;1 fichier a été dépouillé, </numerusform>
            <numerusform>&lt;p&gt;%n fichiers ont été dépouillés, </numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="chewing.py" line="782"/>
        <source>resulting in an index of %n words.&lt;/p&gt;</source>
        <translation>
            <numerusform>produisant un index de 1 mot.&lt;/p&gt;</numerusform>
            <numerusform>produisant un index de %n mots.&lt;/p&gt;</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="chewing.py" line="979"/>
        <source>resulting in %n newly indexed picto files.&lt;br&gt;</source>
        <translation>
            <numerusform>permettant d&apos;indexer 1 nouveau picto.&lt;/p&gt;</numerusform>
            <numerusform>permettant d&apos;indexer %n nouveaux pictos.&lt;/p&gt;</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="chewing.py" line="1049"/>
        <source>resulting in %n audio files linked.&lt;br&gt;</source>
        <translation>
            <numerusform>permettant d&apos;indexer 1 fichier son.&lt;/p&gt;</numerusform>
            <numerusform>permettant d&apos;indexer %n fichiers son.&lt;/p&gt;</numerusform>
        </translation>
    </message>
    <message>
        <location filename="chewing.py" line="50"/>
        <source>moveClickSecure</source>
        <translation>Standard (sécurisé)</translation>
    </message>
    <message>
        <location filename="chewing.py" line="53"/>
        <source>moveSecure</source>
        <translation>Clic automatique (sécurisé)</translation>
    </message>
</context>
<context>
    <name>Preferences</name>
    <message>
        <location filename="preferences.py" line="243"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="preferences.py" line="244"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="preferences.py" line="266"/>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <location filename="preferences.py" line="91"/>
        <source>Resize chars along with the window</source>
        <translation>Redimensionner les caractères de l&apos;éditeur avec la fenêtre</translation>
    </message>
    <message>
        <location filename="preferences.py" line="92"/>
        <source>Make letters appear progressively</source>
        <translation>Apparition progressive des lettres utiles</translation>
    </message>
    <message>
        <location filename="preferences.py" line="93"/>
        <source>Place the dial above the editor</source>
        <translation>Cadran au-dessus de l&apos;éditeur</translation>
    </message>
    <message>
        <location filename="preferences.py" line="94"/>
        <source>Show phonetic alphabet</source>
        <translation>Alphabet phonétique pour aider à l&apos;épellation</translation>
    </message>
    <message>
        <location filename="preferences.py" line="126"/>
        <source>Archive untitled transcriptions</source>
        <translation>Archiver les transcriptions sans titre</translation>
    </message>
    <message>
        <location filename="preferences.py" line="127"/>
        <source>Silently</source>
        <translation>Automatiquement</translation>
    </message>
    <message>
        <location filename="preferences.py" line="127"/>
        <source>On demand</source>
        <translation>À la demande</translation>
    </message>
    <message>
        <location filename="preferences.py" line="127"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="preferences.py" line="127"/>
        <source>Archive saved transcriptions as well</source>
        <translation>Archiver également les transcriptions sauvegardées</translation>
    </message>
    <message>
        <location filename="preferences.py" line="99"/>
        <source>Use selected text to define abbreviations</source>
        <translation>Définir les abréviations à partir du texte sélectionné</translation>
    </message>
    <message>
        <location filename="preferences.py" line="104"/>
        <source>Punctuation on space</source>
        <translation>Ponctuation sur la touche Espace</translation>
    </message>
    <message>
        <location filename="preferences.py" line="98"/>
        <source>Contextual capitalization</source>
        <translation>Majuscules automatiques</translation>
    </message>
    <message>
        <location filename="preferences.py" line="111"/>
        <source>Show prediction balloon</source>
        <translation>Afficher la bulle</translation>
    </message>
    <message>
        <location filename="preferences.py" line="112"/>
        <source>Letters</source>
        <translation>Lettres</translation>
    </message>
    <message>
        <location filename="preferences.py" line="113"/>
        <source>Truncatable predictions</source>
        <translation>Prédictions tronquables</translation>
    </message>
    <message>
        <location filename="preferences.py" line="115"/>
        <source>Space</source>
        <translation>Espace</translation>
    </message>
    <message>
        <location filename="preferences.py" line="117"/>
        <source>When the selection is calculable, show the result</source>
        <translation>Quand la sélection est calculable, montrer le résultat</translation>
    </message>
    <message>
        <location filename="preferences.py" line="118"/>
        <source>Eraser</source>
        <translation>Gomme</translation>
    </message>
    <message>
        <location filename="preferences.py" line="119"/>
        <source>Capitalization correction</source>
        <translation>Rectification des majuscules</translation>
    </message>
    <message>
        <location filename="preferences.py" line="120"/>
        <source>Accentuation correction</source>
        <translation>Rectification de l&apos;accentuation</translation>
    </message>
    <message>
        <location filename="preferences.py" line="107"/>
        <source>Click on background to undo/redo last editing actions</source>
        <translation>Cliquer sur le fond pour annuler et rétablir</translation>
    </message>
    <message>
        <location filename="preferences.py" line="171"/>
        <source>Aspect</source>
        <translation>Aspect</translation>
    </message>
    <message>
        <location filename="preferences.py" line="131"/>
        <source>Keys</source>
        <translation>Touches</translation>
    </message>
    <message>
        <location filename="preferences.py" line="170"/>
        <source>Balloon</source>
        <translation>Bulle</translation>
    </message>
    <message>
        <location filename="preferences.py" line="168"/>
        <source>Interaction</source>
        <translation>Accès</translation>
    </message>
    <message>
        <location filename="preferences.py" line="174"/>
        <source>Persistance</source>
        <translation>Archives</translation>
    </message>
    <message>
        <location filename="preferences.py" line="29"/>
        <source>Start with last profile</source>
        <translation>Démarrer avec le dernier profil utilisé</translation>
    </message>
    <message>
        <location filename="preferences.py" line="30"/>
        <source>Ask for profile at startup</source>
        <translation>Demander le profil au lancement</translation>
    </message>
    <message>
        <location filename="preferences.py" line="89"/>
        <source>Tabulation size</source>
        <translation>Taille des tabulations</translation>
    </message>
    <message>
        <location filename="preferences.py" line="79"/>
        <source>00 ms</source>
        <translation>00 ms</translation>
    </message>
    <message>
        <location filename="preferences.py" line="60"/>
        <source>Auto-click duration</source>
        <translation>Temps d&apos;arrêt équivalent au clic</translation>
    </message>
    <message>
        <location filename="preferences.py" line="81"/>
        <source>Tempo</source>
        <translation>Tempo</translation>
    </message>
    <message>
        <location filename="preferences.py" line="81"/>
        <source> bpm)</source>
        <translation> bpm)</translation>
    </message>
    <message>
        <location filename="preferences.py" line="68"/>
        <source>Jump value</source>
        <translation>Un clic long à l&apos;initiale saute</translation>
    </message>
    <message>
        <location filename="preferences.py" line="68"/>
        <source> letters</source>
        <translation> lettres</translation>
    </message>
    <message>
        <location filename="preferences.py" line="61"/>
        <source>Extra delay to start a sequence</source>
        <translation>Délai supplémentaire de lecture</translation>
    </message>
    <message>
        <location filename="preferences.py" line="62"/>
        <source>Radius of tolerance</source>
        <translation>Tolérance aux micro-mouvements</translation>
    </message>
    <message>
        <location filename="preferences.py" line="62"/>
        <source> px</source>
        <translation> px</translation>
    </message>
    <message>
        <location filename="preferences.py" line="105"/>
        <source>Click on space to add the predicted accentuation</source>
        <translation>Cliquer sur la touche Espace pour ajouter l&apos;accentuation prédite</translation>
    </message>
    <message>
        <location filename="preferences.py" line="79"/>
        <source>Short click maximal duration</source>
        <translation>Déclenchement après</translation>
    </message>
    <message>
        <location filename="preferences.py" line="63"/>
        <source>Audio feedback</source>
        <translation>Retour sonore</translation>
    </message>
    <message>
        <location filename="preferences.py" line="83"/>
        <source>Start sound</source>
        <translation>Son initial</translation>
    </message>
    <message>
        <location filename="preferences.py" line="84"/>
        <source>Step sound</source>
        <translation>Sons suivants</translation>
    </message>
    <message>
        <location filename="preferences.py" line="134"/>
        <source>Editor</source>
        <translation>Éditeur</translation>
    </message>
    <message>
        <location filename="preferences.py" line="106"/>
        <source>Background</source>
        <translation>Fond</translation>
    </message>
    <message>
        <location filename="preferences.py" line="163"/>
        <source>Trace scan mode</source>
        <translation>Rapport de défilement</translation>
    </message>
    <message>
        <location filename="preferences.py" line="169"/>
        <source>Long click</source>
        <translation>Clic long</translation>
    </message>
    <message>
        <location filename="preferences.py" line="54"/>
        <source>Current mode</source>
        <translation>Mode en cours</translation>
    </message>
    <message>
        <location filename="preferences.py" line="57"/>
        <source>Long click scrolls through</source>
        <translation>Le clic long</translation>
    </message>
    <message>
        <location filename="preferences.py" line="67"/>
        <source>Behaviour</source>
        <translation>Domaine</translation>
    </message>
    <message>
        <location filename="preferences.py" line="80"/>
        <source>Simulated long click</source>
        <translation>Simulation du clic long</translation>
    </message>
    <message>
        <location filename="preferences.py" line="114"/>
        <source>Click before separation</source>
        <translation>Clic en début de prédiction</translation>
    </message>
    <message>
        <location filename="preferences.py" line="164"/>
        <source>Replay speed percentage</source>
        <translation>Vitesse de relecture (pourcentage)</translation>
    </message>
    <message>
        <location filename="preferences.py" line="101"/>
        <source>Keyboard order</source>
        <translation>Ordre des touches du cadran</translation>
    </message>
    <message>
        <location filename="preferences.py" line="125"/>
        <source>Location of archives</source>
        <translation>Emplacement des archives des transcriptions précédentes</translation>
    </message>
    <message>
        <location filename="preferences.py" line="125"/>
        <source>Choose location for archiving transcriptions</source>
        <translation>Choisir un emplacement où archiver les transcriptions</translation>
    </message>
    <message>
        <location filename="preferences.py" line="130"/>
        <source>Enable pictograms</source>
        <translation>Activer les pictogrammes</translation>
    </message>
    <message>
        <location filename="preferences.py" line="135"/>
        <source>Display direct pictos</source>
        <translation>Afficher les pictogrammes directs</translation>
    </message>
    <message>
        <location filename="preferences.py" line="136"/>
        <source>Display indirect pictos</source>
        <translation>Afficher les pictogrammes indirects</translation>
    </message>
    <message>
        <location filename="preferences.py" line="137"/>
        <source>Display multiple pictos</source>
        <translation>Afficher en frise les pictogrammes multiples</translation>
    </message>
    <message>
        <location filename="preferences.py" line="172"/>
        <source>Pictos</source>
        <translation>Pictos</translation>
    </message>
    <message>
        <location filename="preferences.py" line="102"/>
        <source>Personalized order</source>
        <translation>Ordre personnalisé</translation>
    </message>
    <message>
        <location filename="preferences.py" line="139"/>
        <source>Colors</source>
        <translation>Couleurs</translation>
    </message>
    <message>
        <location filename="preferences.py" line="140"/>
        <source>Vivid colors for all pictos</source>
        <translation>Couleurs vives pour tous les pictos</translation>
    </message>
    <message>
        <location filename="preferences.py" line="143"/>
        <source>Selection</source>
        <translation>Sélection</translation>
    </message>
    <message>
        <location filename="preferences.py" line="150"/>
        <source>Spell word-selection</source>
        <translation>Épeler les sélections ne comportant aucun blanc</translation>
    </message>
    <message>
        <location filename="preferences.py" line="151"/>
        <source>Prediction</source>
        <translation>Bulle</translation>
    </message>
    <message>
        <location filename="preferences.py" line="152"/>
        <source>Feedback restricted to sound files in Audio folder</source>
        <translation>Retour sonore restreint aux sous-dossiers du dossier Audio</translation>
    </message>
    <message>
        <location filename="preferences.py" line="158"/>
        <source>Spell prediction</source>
        <translation>Épeler les prédictions lors de leur acquisition globale</translation>
    </message>
    <message>
        <location filename="preferences.py" line="167"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="preferences.py" line="173"/>
        <source>Speech</source>
        <translation>Parole</translation>
    </message>
    <message>
        <location filename="preferences.py" line="159"/>
        <source>Engine</source>
        <translation>Moteur</translation>
    </message>
    <message>
        <location filename="preferences.py" line="64"/>
        <source>Maximal number of repetitions when dwelling</source>
        <translation>Nombre maximal de répétitions après l&apos;arrêt du pointeur</translation>
    </message>
    <message>
        <location filename="preferences.py" line="44"/>
        <source>Crash barriers</source>
        <translation>Glissières de sécurité</translation>
    </message>
    <message>
        <location filename="preferences.py" line="45"/>
        <source>on the sides</source>
        <translation>verticales</translation>
    </message>
    <message>
        <location filename="preferences.py" line="47"/>
        <source>at the top and the bottom</source>
        <translation>horizontales</translation>
    </message>
    <message>
        <location filename="preferences.py" line="48"/>
        <source>inside</source>
        <translation>intermédiaires</translation>
    </message>
    <message>
        <location filename="preferences.py" line="145"/>
        <source>Access by</source>
        <translation>Accès par</translation>
    </message>
    <message>
        <location filename="preferences.py" line="146"/>
        <source>long click on Space</source>
        <translation>clic long sur la touche Espace</translation>
    </message>
    <message>
        <location filename="preferences.py" line="147"/>
        <source>click on yin-yang symbol</source>
        <translation>clic sur le symbole du yin-yang</translation>
    </message>
    <message>
        <location filename="preferences.py" line="154"/>
        <source>Feedback for predictions associated to</source>
        <translation>Retour sonore pour les prédictions</translation>
    </message>
    <message>
        <location filename="preferences.py" line="155"/>
        <source>some pictogram</source>
        <translation>illustrées d&apos;un picto</translation>
    </message>
    <message>
        <location filename="preferences.py" line="156"/>
        <source>no pictogram</source>
        <translation>non illustrées</translation>
    </message>
    <message>
        <location filename="preferences.py" line="46"/>
        <source>(open)</source>
        <translation>(ouvertes)</translation>
    </message>
    <message>
        <location filename="preferences.py" line="51"/>
        <source>Width factor</source>
        <translation>Épaisseur relative</translation>
    </message>
    <message>
        <location filename="preferences.py" line="149"/>
        <source>Jump on Pause symbol when speech starts</source>
        <translation>Quand la lecture commence, positionner le pointeur sur le symbole de pause</translation>
    </message>
</context>
<context>
    <name>ProfilePanel</name>
    <message>
        <location filename="preferences.py" line="302"/>
        <source>Duplicate this profile to start creating a new one</source>
        <translation>Dupliquer ce profil pour commencer à en créer un nouveau</translation>
    </message>
    <message>
        <location filename="preferences.py" line="307"/>
        <source>Suppress this profile</source>
        <translation>Supprimer ce profil</translation>
    </message>
    <message>
        <location filename="preferences.py" line="359"/>
        <source>Apply next modifications of this profile to all checked profiles</source>
        <translation>Appliquer les prochaines modifications de ce profil à tous les profils cochés</translation>
    </message>
    <message>
        <location filename="preferences.py" line="350"/>
        <source>Apply last modifications of this profile to all checked profiles</source>
        <translation>Appliquer les dernières modifications de ce profil à tous les profils cochés</translation>
    </message>
</context>
<context>
    <name>SpeechCommandLine</name>
    <message>
        <location filename="preferences.py" line="1132"/>
        <source>Your command line may be valid. Click to make sure.</source>
        <translation>Votre ligne de commande est peut-être valide. Cliquez pour vous en assurer.</translation>
    </message>
    <message>
        <location filename="preferences.py" line="1058"/>
        <source>Click to restore the default command.</source>
        <translation>Cliquez pour revenir à la commande par défaut.</translation>
    </message>
    <message>
        <location filename="preferences.py" line="1074"/>
        <source>Click to stop.</source>
        <translation>Cliquez pour interrompre la lecture.</translation>
    </message>
    <message>
        <location filename="preferences.py" line="1116"/>
        <source>&lt;p&gt;ERROR: underlying Phonon engine does not support audio extension &quot;%1&quot; on your system.&lt;br&gt;Accepted formats: %2.&lt;/p&gt;</source>
        <translation>&lt;p&gt;ERREUR: le composant Phonon ne gère pas le format &quot;%1&quot; sur votre système.&lt;br&gt;Formats gérés: %2.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="preferences.py" line="1118"/>
        <source>ERROR: the command line should contain &quot;%(output)s&quot; followed by a non-empty extension.</source>
        <translation>ERREUR: la ligne de commande devrait contenir « %(output)s » suivi d&apos;une extension non vide.</translation>
    </message>
    <message>
        <location filename="preferences.py" line="1120"/>
        <source>ERROR: the command line should contain &quot;%(xxxxxinput)s.txt&quot;, where xxxxx is a text encoding (ascii, macroman, latin1, etc.), utf8 by default.</source>
        <translation>ERREUR: la ligne de commande devrait contenir « %(xxxxxinput)s.txt », où xxxxx est un encodage de caractère (ascii, macroman, latin1, etc.), utf8 par défaut.</translation>
    </message>
</context>
<context>
    <name>SplashScreen</name>
    <message>
        <location filename="splash.py" line="38"/>
        <source>Version %1 r%2</source>
        <translation>&lt;TABLE border=&quot;0&quot; width=&quot;100%&quot; padding=&quot;0&quot;&gt;&lt;TR&gt;&lt;TD align=&quot;left&quot;&gt;&lt;b&gt;&lt;font size=6&gt;Chewing Word&lt;/font&gt;&lt;/b&gt;&lt;TD align=&quot;right&quot;&gt;&lt;tt&gt;&lt;font size=2&gt;Version %1 r%2&lt;/tt&gt;&lt;/TABLE&gt;&lt;p align=&quot;left&quot;&gt;&lt;font size=2&gt;&lt;b&gt;Aristide Grange - Laboratoire d&apos;Informatique Théorique et Appliquée&lt;/b&gt;&lt;/p&gt;&lt;p align=&quot;left&quot;&gt;&lt;font size=2&gt;Icone: Don D&apos;Ablo, &lt;i&gt;Chattering Teeth&lt;/i&gt;  © 2006 Ulia Design&lt;br&gt;Effets sonores: Maxime François&lt;br&gt;Pictogrammes de démonstration: Dirceu Veiga,  &lt;A HREF=&quot;http://www.fasticon.com/&quot;&gt;Fast Icon Studio&lt;/A&gt;&lt;br&gt;Moteur de synthèse vocale de démonstration: &lt;A HREF=&quot;http://espeak.sourceforge.net/&quot;&gt;eSpeak&lt;/A&gt;&lt;br&gt;Voix de démonstration: &lt;A HREF=&quot;http://tcts.fpms.ac.be/synthesis/mbrola.html&quot;&gt;MBROLA&lt;/A&gt; fr1&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>StartupDialog</name>
    <message>
        <location filename="startupdialog.py" line="13"/>
        <source>Select your profile</source>
        <translation>Choix du profil</translation>
    </message>
    <message>
        <location filename="startupdialog.py" line="23"/>
        <source>Show this dialog at startup</source>
        <translation>Afficher ce menu au lancement</translation>
    </message>
    <message>
        <location filename="startupdialog.py" line="25"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>TongueRatioSlider</name>
    <message>
        <location filename="preferences.py" line="497"/>
        <source>Relative tongue size</source>
        <translation>Taille relative de la bulle</translation>
    </message>
</context>
</TS>
