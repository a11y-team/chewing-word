#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file statusbarview.py

""" Qt transcription session. """

# builtin stuff
from PyQt4 import QtGui
# home made stuff
from aspect import aspect

class StatusBarView(QtGui.QStatusBar):
	
	def __init__(self):
		QtGui.QStatusBar.__init__(self)
		self.font().setPixelSize(aspect.statusTipSize)
		self.setFixedHeight(aspect.statusBarHeight)
