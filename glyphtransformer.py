#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file glyphtransformer.py

from aspect import aspect

class GlyphTransformer:
	
	def __init__(self):
		self.glyphs = aspect.glyphTransformer
		self.glyphs[chr(0)] = " "
	
	def call(self,s):
		return [self.glyphs.get(c,c) for c in s]
	
	def callOne(self,c):
		return self.glyphs.get(c,c)
	

if __name__ == "__main__":
	from  chewing import main
	main()

