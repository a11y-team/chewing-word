#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file startupdialog.py


# builtin stuff
from PyQt4 import QtGui, QtCore


class StartupDialog(QtGui.QDialog):
	def __init__(self, rootPrefs, profiles, parent=None):
		QtGui.QDialog.__init__(self, parent)
		self.setWindowTitle(self.trUtf8("Select your profile"))
		self.profileListBox = QtGui.QListWidget(self)
		self.profileListBox.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)
		self.profileIndex = []
		for (row,profile) in enumerate(profiles):
			if profile["isInMenu"]:
				self.profileIndex.append(row)
				self.profileListBox.addItem(profile["localName"])
				if rootPrefs["profileIndex"] == row:
					self.profileListBox.setCurrentRow(self.profileListBox.count()-1)
		self.askCheckBox = QtGui.QCheckBox(self.trUtf8("Show this dialog at startup"))
		self.askCheckBox.setChecked(True)
		okButton = QtGui.QPushButton(self.trUtf8("OK"))
		self.connect(okButton, QtCore.SIGNAL("clicked()"), self, QtCore.SLOT("accept()"))
		self.connect(okButton, QtCore.SIGNAL("clicked()"), self.closeOnProfile)
		self.connect(self.profileListBox,QtCore.SIGNAL("itemSelectionChanged()"),self.reselectCurrentItem)
		buttonLayout = QtGui.QHBoxLayout()
		buttonLayout.addStretch(1)
		buttonLayout.addWidget(okButton)
		self.layout = QtGui.QVBoxLayout()
		self.layout.addWidget(self.profileListBox)
		self.layout.addWidget(self.askCheckBox)
		self.layout.addLayout(buttonLayout)
		self.setLayout(self.layout)
		self.rootPrefs = rootPrefs
	
	def closeOnProfile(self):
		self.rootPrefs["profileIndex"] = self.profileIndex[self.profileListBox.currentRow()]
		self.rootPrefs["startupProfile"] = "ask" if self.askCheckBox.isChecked() else "last"
	
	def reselectCurrentItem(self):
		self.profileListBox.currentItem().setSelected(True)

if __name__ == "__main__":
	from  chewing import main
	main()
