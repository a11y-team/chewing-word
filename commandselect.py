#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file commandselect.py

class CommandSelect(object):
	
	def __init__(self, mediator):
		self.getAllInEditorGetter                   = mediator.editorGetter.getAll
		self.getAnchorAndPositionInEditorGetter     = mediator.editorGetter.getAnchorAndPosition
		self.getPositionInEditorGetter              = mediator.editorGetter.getPosition
		self.silentlySetCursorBetweenInEditorSetter = mediator.editorSetter.silentlySetCursorBetween
		self.silentlySetCursorAtInEditorSetter      = mediator.editorSetter.silentlySetCursorAt
		self.resetInSelector                        = mediator.selector.reset
		self.reshapeSelectionInSelector             = mediator.selector.reshapeSelection
		self.getNextExtremityInSelector             = mediator.selector.getNextExtremity
		self.autoSelectionInSelector                = mediator.selector.autoSelection
		self.MIDDLE                                 = mediator.selector.MIDDLE
		self.BEG                                    = mediator.selector.BEG
	
	def reset(self):
		self.resetInSelector(self.getAllInEditorGetter())
		self.checkAnchorStrategy(True)
		(anc,pos) = self.getAnchorAndPositionInEditorGetter()
		(anc,pos) = self.autoSelectionInSelector(anc,pos)
		self.silentlySetCursorBetweenInEditorSetter(anc,pos)
	
	def checkAnchorStrategy(self,strategy):
		def reshapeSelection(i):
			(anc,pos) = self.getAnchorAndPositionInEditorGetter()
			if i == 0:
				self.silentlySetCursorAtInEditorSetter(pos)
				self.checkAnchorStrategy(False)
			else:
				(anc,pos) = self.reshapeSelectionInSelector(anc,pos,(i/abs(i)+1)/2,self.MIDDLE-abs(i))
				self.silentlySetCursorBetweenInEditorSetter(anc,pos)
		
		def moveEmptyCursor(i):
			pos = self.getPositionInEditorGetter()
			if i == 0:
				self.checkAnchorStrategy(True)
			else:
				pos = self.getNextExtremityInSelector(self.BEG,pos,(i/abs(i)+1)/2,self.MIDDLE-abs(i))
				self.silentlySetCursorAtInEditorSetter(pos)
		
		self.anchorState = strategy
		if strategy:
			self.modifySelectionDependingOnAnchorStrategy = reshapeSelection
		else:
			self.modifySelectionDependingOnAnchorStrategy = moveEmptyCursor
	
	def getAnchorState(self):
		return self.anchorState
	
	def modifySelection(self,i):
		self.modifySelectionDependingOnAnchorStrategy(i)
	
	


if __name__ == "__main__":
	from  chewing import main
	main()

