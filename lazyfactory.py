#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file lazyfactory.py

""" Try to retrieve the value of a given identifier, or to reconstruct it.

Arguments:
- providedPaths: expected locations to load and/or dump some objects.
- providedValues: initially known identifiers, along with their values.
- providedLinks: association of constructible identifier, with the module and
  the entity (function or class) which must be invoked to reconstruct it.

Return the value of an identifier if this one is known. If not, try to reload
from the provided paths a previously dumped instance of it. If this fails, try
to reconstruct the value, by calling recursively the same process. Any
intermediate result is dumped at the associated location (if provided) for
future use. The persistence methods can be overridden for specific treatments.

Limitations:
- When a file already exists, there is no way to force its reconstruction :
  if needed, it must be manually removed.
"""

# built-in stuff
import inspect
import marshal
# home made stuff
from chrono import chrono


class NotFound(AttributeError):
	""" Raised in case a value is impossible to retrieve or construct. """

class LazyFactory:
	""" Try to retrieve the value of a given identifier, or to reconstruct it."""
	def __init__(self, providedPaths, providedValues, providedLinks):
		self.providedPaths = providedPaths
		self.known = providedValues
		self.links = providedLinks
	
	def __getitem__(self, identifier):
		chrono.printElapsed("require %s" % identifier)
		if identifier in self.known:
			return self.known[identifier]
		path = self.providedPaths.get(identifier,None)
		try:
			(module,entity) = self.links[identifier]
		except KeyError: # should only occur with optional arguments 
			raise NotFound, identifier
		exec "import %s as module" % module
		if path:
			try:
				chrono.printElapsed("loading %s..." % path)
				self.known[identifier] = self.load(module,path)
				return self.known[identifier]
			except IOError:
				chrono.printElapsed("failed to load %s" % path)
		entity = module.__getattribute__(entity)
		self._reconstructFrom(identifier,entity)
		if path:
			try:
				chrono.printElapsed("dumping %s" % path)
				self.dump(module,path,self.known[identifier])
			except IOError:
				chrono.printElapsed("failed to dump %s" % path)
		return self.known[identifier]
	
	def load(self, module, path):
		try:
			return module.load(path)
		except AttributeError:
			return marshal.load(open(path,"rb"))
	
	def dump(self, module, path, obj):
		try:
			module.dump(path,obj)
		except AttributeError:
			marshal.dump(obj, open(path,"wb"))
	
	def _reconstructFrom(self, identifier, entity):
		expected = self._extractArgs(entity)
		found = dict([(a,self.known[a]) for a in expected if a in self.known])
		while identifier not in self.known:
			try:
				self.known[identifier] = entity(**found)
				chrono.printElapsed("%s reconstructed" % identifier)
			except TypeError: # not enough arguments found
				for a in expected:
					if a not in found:
						try:
							self.known[a] = found[a] = self[a]
							break # a new argument has been constructed: retry 
						except NotFound:
							pass # try to construct another expected argument
				else: # no new argument can be constructed: fail
					raise NotFound, "%s: %s" % (identifier, [a for a in expected if a not in found])
	
	def _extractArgs(self, entity):
		""" Return the argument list of a given function or class constructor. """
		if inspect.isfunction(entity):
			return inspect.getargspec(entity)[0] # complete list
		else:
			return inspect.getargspec(entity.__init__)[0][1::] # "self" ommited
	

