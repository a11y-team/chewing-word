#!/usr/bin/env python
# encoding: utf-8
#file favoritechars.py

class FavoriteChars (object):
	
	def __init__(self):
		self.replacements = [
			(r"\n","\n"),
			(r"\t","\t"),
		]
		self.yinYangFlag = False
	
	def setVariants(self, variants):
		l = filter(None,variants.split(chr(0)))
		if l:
			self.variants = []
			for s in l:
				for (c1,c2) in self.replacements:
					s = s.replace(c1,c2)
				self.variants.append(s)
		else:
			self.variants = [u""]
		if self.yinYangFlag:
			self.variants[0] = ["yinYang"] + [c for c in self.variants[0]]
		self.index = 0
	
	def setYinYangFlag(self, yinYangFlag):
		self.yinYangFlag = yinYangFlag
	
	def getCurrentVariant(self):
		return self.variants[self.index]
	
	def getNumberOfVariants(self):
		return len(self.variants)
	
	def stepVariant(self, step = 1):
		self.index = (self.index + step) % len(self.variants)
	
	def getVariantIndex(self):
		return self.index
