#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file commandspeech.py


# builtin stuff
import itertools
from threading import Thread
import time
# home-made stuff

class CommandSpeech:
	
	(BWD,FWD) = (0,1)
	MIDDLE = 3
	(DOC,PAR,SEN) = range(MIDDLE)
	
	def __init__(self, mediator):
		self.stopInJukebox                  = mediator.jukebox.stop
		self.stepInJukebox                  = mediator.jukebox.step
		self.resetInJukebox                 = mediator.jukebox.reset
		self.pushInJukebox                  = mediator.jukebox.push
		self.skipInJukebox                  = mediator.jukebox.skip
		self.maySpeakInJukebox              = mediator.jukebox.maySpeak
		self.updatePauseStateInJukebox      = mediator.jukebox.updatePauseState
		self.prepareToSpeakInTts            = mediator.tts.prepareToSpeak
		self.getHashInTts                   = mediator.tts.getHash
		self.preProcessInSpeechTransformer  = mediator.speechTransformer.preProcess
		self.postProcessInSpeechTransformer = mediator.speechTransformer.postProcess
		mediator.jukebox.register(self)
		self.waitingSongs = []
		self.thread = None
		self.inPreparation = None
	
	def register(self,view):
		view.register(self)
		self.view = view
	
	def setCache(self, cache):
		self.cache = cache
	
	def registerToothManager(self,toothManager):
		self.killCommandInToothManager = toothManager.killCommand
	
	def registerClickTransformer(self, clickTransformer):
		self.clickTransformer = clickTransformer
	
	def registerScanner(self, scanner):
		self.scanner = scanner
	
	def checkScanStrategy(self,strategy):
		def muteWhenScanStrategyIsOn():
			self.scanner.muteSounds()
			self.clickTransformer.muteSounds()
		
		def demuteWhenScanStrategyIsOn():
			self.scanner.demuteSounds()
			self.clickTransformer.demuteSounds()
		
		if strategy:
			self.muteClicksDependingOnScanStrategy = muteWhenScanStrategyIsOn
			self.demuteClicksDependingOnScanStrategy = demuteWhenScanStrategyIsOn
		else:
			self.muteClicksDependingOnScanStrategy = (lambda *args: None)
			self.demuteClicksDependingOnScanStrategy = (lambda *args: None)
	
	def muteClicks(self):
		self.muteClicksDependingOnScanStrategy()
	
	def demuteClicks(self):
		self.demuteClicksDependingOnScanStrategy()
	
	def reset(self):
		self.resetInJukebox()
		self.setPauseState(False)
	
	def setPauseState(self,pauseState):
		def continueSpeaking():
			self.view.continueSpeaking()
			self.setPauseState(False)
		
		def pauseSpeaking():
			self.view.pauseSpeaking()
			self.setPauseState(True)
		
		self.pauseState = pauseState
		self.updatePauseStateInJukebox(pauseState)
		self.checkSkipStrategy(True)
		if pauseState:
			self.pauseOrContinueSpeakingDependingOnPauseState = continueSpeaking
			self.maySpeakDependingOnPauseState = (lambda *args:None)
		else:
			self.pauseOrContinueSpeakingDependingOnPauseState = pauseSpeaking
			self.maySpeakDependingOnPauseState = self.view.startSpeaking
		self.checkSkipToPreviousSentenceStrategy(True)
	
	def getPauseState(self):
		return self.pauseState
	
	def pauseOrContinueSpeaking(self):
		self.pauseOrContinueSpeakingDependingOnPauseState()
	
	def maySpeak(self):
		self.maySpeakDependingOnPauseState()
	
	def startSpeaking(self):
		self.muteClicks()
		self.view.enableSpeechInteractions()
		self.maySpeakInJukebox()
	
	def stopSpeaking(self):
		self.waitingSongs = [] # the current thread will gracefully end
		self.view.stopSpeaking()
		self.stopInJukebox()
		self.view.disableSpeechInteractions()
		self.demuteClicks()
	
	def step(self):
		self.stepInJukebox()
	
	def jukeboxIsExhausted(self):
		self.killCommandInToothManager()
		self.demuteClicks()
		self.view.disableSpeechInteractions()
	
	def speak(self,text):
		text = self.preProcess(text)
		pathBaseName = "%s/%s" % (self.cache.getAbsolutePath(),self.getHashInTts(text))
		if self.inPreparation == text:
			while self.inPreparation == text:
				time.sleep(0.2) # if the text is already in preparation, wait for it to finish
		else:
			self.prepareToSpeakInTts(text,pathBaseName)
		self.view.setSource(pathBaseName)
		self.maySpeak()
		self.postProcess(text)
		self.checkSkipToPreviousSentenceStrategy(True)
		# self.cache.purge()
	
	def checkSkipToPreviousSentenceStrategy(self, strategy = False):
		if strategy:
			self.skipOrReplayDependingOnSkipToPreviousSentenceStrategy = (lambda: self.skipInJukebox(self.BWD,self.SEN))
		else:
			self.skipOrReplayDependingOnSkipToPreviousSentenceStrategy = self.replayCurrentSentence
	
	def queueSongToPrepare(self,text):
		def prepareWaitingSongsInBackground():
			while True:
				try:
					text = self.waitingSongs.pop(0)
				except IndexError:
					break
				self.inPreparation = text
				pathBaseName = "%s/%s" % (self.cache.getAbsolutePath(),self.getHashInTts(text))
				self.prepareToSpeakInTts(text,pathBaseName)
				self.inPreparation = None
			self.thread = None
		
		text = self.preProcess(text)
		self.waitingSongs.append(text)
		if self.thread is None:
			self.thread = Thread(target=prepareWaitingSongsInBackground)
			self.thread.start()
	
	def checkSkipStrategy(self,strategy):
		def skipInteraction(i):
			if i != 0:
				if i == self.SEN - self.MIDDLE:
					self.skipOrReplay()
				else:
					self.skipInJukebox((i/abs(i)+1)/2,self.MIDDLE-abs(i))
			else:
				self.pauseOrContinueSpeaking()
		
		def pushInteraction(i):
			if i != 0:
				self.pushInJukebox((i/abs(i)+1)/2,self.MIDDLE-abs(i))
			else:
				self.pauseOrContinueSpeaking()
		
		if strategy:
			self.variantIndex = 0
			self.interactionDependingOnSkipStrategy = skipInteraction
		else:
			self.variantIndex = 1
			self.interactionDependingOnSkipStrategy = pushInteraction
	
	def interaction(self,i):
		self.interactionDependingOnSkipStrategy(i)
	
	def getVariantIndex(self):
		return self.variantIndex
	
	def skipOrReplay(self):
		self.skipOrReplayDependingOnSkipToPreviousSentenceStrategy()
	
	def replayCurrentSentence(self):
		self.checkSkipToPreviousSentenceStrategy(True)
		self.view.restartSpeaking()
	
	def checkSpellStrategy(self,strategy):
		def doPreProcess(string):
			if len(string.split())<=1:
				string = self.preProcessInSpeechTransformer(string)
			return string
			
		if strategy:
			self.preProcessDependingOnSpellStrategy = doPreProcess
			self.postProcessDependingOnSpellStrategy = self.postProcessInSpeechTransformer
		else:
			self.preProcessDependingOnSpellStrategy = lambda string: string
			self.postProcessDependingOnSpellStrategy = lambda string: None
	
	def preProcessWhenManualSelectionIsOn(self,string):
		return self.preProcessDependingOnSpellStrategy(string)
	
	def postProcessWhenManualSelectionIsOn(self,string):
		self.postProcessDependingOnSpellStrategy(string)
	
	def checkManualSelectionStrategy(self,strategy):
		if strategy:
			self.preProcessDependingOnManualSelectionStrategy = self.preProcessWhenManualSelectionIsOn
			self.postProcessDependingOnManualSelectionStrategy = self.postProcessWhenManualSelectionIsOn
		else:
			self.preProcessDependingOnManualSelectionStrategy = lambda string: string
			self.postProcessDependingOnManualSelectionStrategy = lambda string: None
	
	def preProcess(self,string):
		return self.preProcessDependingOnManualSelectionStrategy(string)
	
	def postProcess(self,string):
		self.postProcessDependingOnManualSelectionStrategy(string)
	

if __name__ == "__main__":
	from  chewing import main
	main()

