#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file chewing.py

""" Qt transcription session. """

# import os
# os.environ['PYCHECKER'] = '--stdlib off --limit 100 -E on --unreachable on --selfused on'
# import pychecker.checker

# builtin stuff
import sys
from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import SIGNAL
import os
import re
import codecs
import locale
import mysimplejson
import copy

# home made stuff
from mediator import Mediator
from aspect import aspect
from editorview import EditorView
from jawsview import JawsView
from statusbarview import StatusBarView
from preferences import Preferences
from qtgoodies import pathForPrint, MouseMover
from splash import SplashScreen
from startupdialog import StartupDialog
from timer import Timer, AutoClickTimer
import pictos
from speechview import SpeechView
from cachemanager import CacheManager


class MainWindow(QtGui.QMainWindow):
	
	def __init__(self, language):
		
		def appendStringsToBeTranslatedIntoAspect():
			aspect.charPaletteList = [
			  ("\\n",self.trUtf8("carriage return")),
			  ("\\t",self.trUtf8("tabulation")),
			]
			localDefaultProfileNames = {
				"moveClick" : self.trUtf8("moveClick"),
				"moveClickSecure" : self.trUtf8("moveClickSecure"),
				"3rdPerson" : self.trUtf8("3rdPerson"),
				"move" : self.trUtf8("move"),
				"moveSecure" : self.trUtf8("moveSecure"),
				"moveSlow" : self.trUtf8("moveSlow"),
				"technophobe" : self.trUtf8("technophobe"),
				"beginner": self.trUtf8("beginner"),
				"scanTechnophobe": self.trUtf8("scanTechnophobe"),
				"scanPoor": self.trUtf8("scanPoor"),
				"scan": self.trUtf8("scan"),
				"scanTrunk": self.trUtf8("scanTrunk"),
				"easyPictoHunt": self.trUtf8("easyPictoHunt"),
				"hardPictoHunt": self.trUtf8("hardPictoHunt"),
				"coverflow": self.trUtf8("Coverflow"),
			}
			for profile in aspect.defaultProfiles:
				profile["localName"] = unicode(localDefaultProfileNames[profile["localName"]])
			aspect.clickTransformerNames = {
				"keys": [d["name"] for d in aspect.clickTransformers],
				"titles" : [ # be sure this list is in the same order than the json one
					self.trUtf8("normal"),
					self.trUtf8("two clicks"),
					self.trUtf8("double click"),
					self.trUtf8("double click and click"),
					self.trUtf8("right click"),
					self.trUtf8("two right clicks"),
				]
			}
			aspect.scannerNames = {
				"keys": [d["name"] for d in aspect.scanners],
				"titles" : [ # be sure this list is in the same order than the json one
					self.trUtf8("manual"),
					self.trUtf8("no jump"),
					self.trUtf8("jump everywhere"),
					self.trUtf8("jump on all letters"),
					self.trUtf8("jump after initials"),
				]
			}
			aspect.longClickVariantNames = {
				"keys" : ["nothing","accentuation","capitalization"],
				"titles" : [
					self.trUtf8("nothing"),
					self.trUtf8("accentuation variants"),
					self.trUtf8("capitalization variants"),
				]
			}
			aspect.keyboardOrderNames = {
				"keys" : ["alphabetic","alphabeticScan","personalized","static","3static","dynamic"],
				"titles" : [
					self.trUtf8("alphabetic"),
					self.trUtf8("alphabeticScan"),
					self.trUtf8("personalized"),
					self.trUtf8("static"),
					self.trUtf8("semi-dynamic"),
					self.trUtf8("dynamic"),
				]
			}
			aspect.leftBehaviours = {
				"keys" : ["truncate","delete","whole"],
				"titles" : [
					self.trUtf8("Truncation without deleting"),
					self.trUtf8("Truncation by deleting"),
					self.trUtf8("Composition of the whole prediction"),
				]
			}
			aspect.interactionModes = {
				"keys" : ["standard","auto-click","scan"],
				"titles" : [
					self.trUtf8("Standard"),
					self.trUtf8("Auto-click"),
					self.trUtf8("Scan"),
				]
			}
			aspect.commandMessages = {
				# all reserved commands identifiers end with a chr(0), which has zero chances to occur in a file name
				"speech\x00": self.trUtf8("Speak selection"),
				"select\x00": self.trUtf8("Create selection"),
				"cancel\x00": self.trUtf8("Cancel"),
				"script\x00": self.trUtf8("Send selection to %s"),
			}
			aspect.speechTestLine = unicode(self.trUtf8("Once upon a midnight dreary, while I pondered weak and weary, over many a quaint and curious volume of forgotten lore, while I nodded, nearly napping, suddenly there came a tapping, as of some one gently rapping, rapping at my chamber door."))
		
		def viewInit():
			self.setMinimumWidth(aspect.chewingMinWidth)
			self.editorView    = EditorView()
			self.jawsView      = JawsView(self.mediator)
			self.statusBarView = StatusBarView()
			self.mouseMover    = MouseMover()
			self.farPictoView  = self.editorView.farPictoView
			# self.speechView    = SpeechView()
			self.speechCache   = CacheManager("speech")
		
		def registrations():
			self.mediator.editorGetter.register(self.editorView)
			self.mediator.editorSetter.register(self.editorView)
			self.mediator.tongueModel.register(self.jawsView.tongueView)
			self.mediator.toothManager.register(self.jawsView)
			self.mediator.scanner.register(self.jawsView)
			self.mediator.calculator.register(QtGui.QApplication.clipboard())
			self.mediator.scanner.registerMouseMover(self.mouseMover)
			self.mediator.commandSpeech.register(self.speechView)
			self.mediator.instantSpeech.register(self.speechView)
			self.mediator.tts.setCache(self.speechCache)
			self.mediator.commandSpeech.setCache(self.speechCache)
			self.mediator.instantSpeech.setCache(self.speechCache)
		
		def timers():
			self.clickTimer = Timer()
			self.shortClickTimer = Timer()
			self.longClickTimer  = Timer(singleShot = False)
			self.longClickTimer.setAudibleStrategy(None)
			self.autoClickTimer = AutoClickTimer()
			self.scanTimer = Timer(singleShot = False)
			self.scanTimer.setAudibleStrategy(None)
			self.highlightTimer = Timer()
			self.jawsView.registerAutoClickTimer(self.autoClickTimer)
			self.jawsView.registerClickTransformer(self.mediator.clickTransformer)
			self.jawsView.registerHighlightTimer(self.highlightTimer)
			self.jawsView.registerScanner(self.mediator.scanner)
			self.editorView.registerAutoClickTimer(self.autoClickTimer)
			self.mediator.clickTransformer.registerTimers(self.shortClickTimer,self.longClickTimer)
			self.mediator.scanner.registerTimer(self.scanTimer)
			self.mediator.clickTransformer.registerScanner(self.mediator.scanner)
			self.mediator.toothManager.registerScanner(self.mediator.scanner)
			self.mediator.commandSpeech.registerScanner(self.mediator.scanner)
			self.mediator.commandSpeech.registerClickTransformer(self.mediator.clickTransformer)
		
		def createMenus():
			actions = [
				self.trUtf8("&File"),
					("newAct",            self.trUtf8("&New"), self.tr("Ctrl+N"), self.newFile),
					("openAct",           self.trUtf8("&Open..."), self.tr("Ctrl+O"), self.open),
					None,                 
					("closeAct",          self.trUtf8("&Close"), self.tr("Ctrl+W"), QtCore.SLOT("close()")),
					("saveAct",           self.trUtf8("&Save"), self.tr("Ctrl+S"), self.save),
					("saveAsAct",         self.trUtf8("Save &As..."), self.tr("Ctrl+Shift+S"), self.saveAs),
					None,                 
					("reindexAct",        self.trUtf8("Reindex Archives"), None, self.reindexArchives),
					("indexPictosAct",    self.trUtf8("Index New Pictos"), None, self.indexNewPictos),
					("reindexAudiosAct",  self.trUtf8("Reindex Audio Files"), None, self.indexAudio),
					None,                 
					("printAct",          self.trUtf8("&Print..."), self.tr("Ctrl+P"), self.print_),
					None,                 
					("exitAct",           self.trUtf8("Quit Chewing Word"), self.tr("Ctrl+Q"), QtCore.SLOT("close()")),
				self.trUtf8("&Edit"),
					("undoAct",           self.trUtf8("Undo"), self.tr("Ctrl+Z"), self.editorView.document().undo),
					("redoAct",           self.trUtf8("Redo"), self.tr("Ctrl+Shift+Z"), self.editorView.document().redo),
					None,                 
					("selectAllAct",      self.trUtf8("Select All"), self.tr("Ctrl+A"), self.editorView.selectAll),
					("cutAct",            self.trUtf8("Cu&t"), self.tr("Ctrl+X"), self.editorView.cut),
					("cutAllAct",         self.trUtf8("Cut All"), self.tr("Ctrl+Shift+X"), self.cutAll),
					("copyAct",           self.trUtf8("&Copy"), self.tr("Ctrl+C"), self.editorView.copy),
					("copyAllAct",        self.trUtf8("Copy All"), self.tr("Ctrl+Shift+C"), self.copyAll),
					("pasteAct",          self.trUtf8("&Paste"), self.tr("Ctrl+V"), self.editorView.paste),
				self.trUtf8("&Profile"),
				self.trUtf8("&Help"),
					("aboutAct",          "&About", None, self.about),
					("offlineRefmanAct",  self.trUtf8("User Manual (offline, without videos)"), self.tr("Ctrl+?"), self.offlineRefman),
					("onlineRefmanAct",   self.trUtf8("User Manual (online, with videos)"), None, self.onlineRefman),
					("visitSiteAct",      self.trUtf8("Visit Chewing Word Site"), None, self.visitSite),
					("prefsAct",          "Preferences...", None, self.preferences),
				]
			for action in actions:
				if type(action) == tuple:
					(name,menuString,shortcut,slot) = action
					newAction = QtGui.QAction(menuString, self)
					fileMenu.addAction(newAction)
					if name is None:
						newAction.setEnabled(False)
					else:
						setattr(self.__class__, name, newAction)
						if slot:
							self.connect(newAction, QtCore.SIGNAL("triggered()"), slot)
						if shortcut:
							newAction.setShortcut(shortcut)
				elif action:
					fileMenu = self.menuBar().addMenu(action)
					if action == self.trUtf8("&Profile"):
						self.profileMenu = fileMenu
						self.connect(fileMenu, QtCore.SIGNAL("triggered(QAction*)"), self.updateProfileSlot)
				else:
					fileMenu.addSeparator()
			self.cutAct.setEnabled(False)
			self.copyAct.setEnabled(False)
		
		def retrieveSettings():
			self.readSettings()
			self.updateWithPrefs(self.prefs)
		
		def createLayout():
			centralWidget = QtGui.QWidget()
			self.layout = QtGui.QVBoxLayout(centralWidget)
			self.layout.setMargin(0)
			self.layout.setSpacing(0)
			self.layout.addWidget(self.editorView)
			self.layout.addWidget(self.jawsView)
			self.layout.addWidget(self.statusBarView)
			self.setCentralWidget(centralWidget)
		
		def initVarious():
			self.undoOrRedoSignal = "undo()"
			self.setCurrentFile(QtCore.QString())
			self.currentFileName = QtCore.QString()
			self.jawsView.updateSize(self.width())
			QtCore.QTextCodec.setCodecForLocale(QtCore.QTextCodec.codecForName("UTF-8"))
			self.mediator.scanner.checkDebugModeStrategy(aspect.debugMode)
			self.mediator.pictos.checkFlexionModeStrategy(aspect.flexionMode)
			self.supportedImageFormats = frozenset(".%s" % str(s).lower() for s in QtGui.QImageReader.supportedImageFormats())
			self.supportedImageFormatsForPrint = ", ".join(sorted(list(self.supportedImageFormats)))
			self.pictoIndexFileName = aspect.pictoIndexFileName % language
			self.flexionFileName = aspect.flexionFileName % language
			self.pictoBaseDirectoryName = aspect.pictoBaseDirectoryName
			self.audioBaseDirectoryName = aspect.audioBaseDirectoryName
			self.trackingBehaviourMessages = [
				self.trUtf8("Text cursor moving. Dwell somewhere to fix the anchor."),
				self.trUtf8("Text anchor fixed. Dwell somewhere to mark the selection."),
				self.trUtf8("Text cursor fixed. Dwell somewhere to move it."),
				"",
			]
		
		def retrievePersistantObjects():
			self.loadFlexionDictionary()
			self.loadPictos()
			self.loadArchiveIndex()
			self.loadExpansionFile()
			self.loadScanTrace()
		
		def connectSignals():
			self.connect(self.editorView,SIGNAL("cursorPositionChanged()"),self.textChanged)
			self.connect(self.editorView,SIGNAL("setTextChangedStrategy"),self.setTextChangedStrategy)
			self.connect(self.editorView, SIGNAL("copyAvailable(bool)"), self.cutAct, QtCore.SLOT("setEnabled(bool)"))
			self.connect(self.editorView, SIGNAL("copyAvailable(bool)"), self.copyAct, QtCore.SLOT("setEnabled(bool)"))
			self.connect(self.jawsView,SIGNAL("jawsView painted"),self.resumeToSpaceDependingOnScanStrategy)
			self.connect(self.jawsView,SIGNAL("showFarPicto"),self.farPictoView.showPicto)
			self.connect(self.jawsView,SIGNAL("hideFarPicto"),self.farPictoView.hidePicto)
			self.connect(self.editorView,SIGNAL("trackingBehaviourChanged"),self.trackingBehaviourChanged)
			self.connect(self.speechView.player,SIGNAL("metaInformationChanged"),self.metaInformationChanged)
		
		QtGui.QMainWindow.__init__(self)
		self.speechView = SpeechView()
		aspect.version += ("" if self.speechView.phononIsAvailable() else u"-ε")
		splash = SplashScreen(self)
		splash.show()
		self.language = language
		appendStringsToBeTranslatedIntoAspect()
		self.mediator = Mediator()
		viewInit()
		registrations()
		timers()
		createMenus()
		createLayout()
		retrieveSettings()
		initVarious()
		retrievePersistantObjects()
		self.setTextChangedStrategy("normal")
		self.textChanged()
		connectSignals()
		self.setMouseTracking(True)
		splash.close()
	
	
# Signals
	
	def setTextChangedStrategy(self,strategy):
		def common():
			self.mediator.calculator.reset()
			self.mediator.caseMapper.reset()
			self.mediator.acquirer.reset()
			self.mediator.spaceTransformer.reset()
			self.mediator.pictos.reset()
		
		def silentlyChange():
			self.mediator.toothManager.mayKillCommand()
			self.mediator.contentGetter.reset()
			common()
			self.mediator.toothManager.reset()
			self.mediator.tongueModel.reset()
		
		def silentlyMove():
			self.mediator.contentGetter.reset()
			self.mediator.pretenders.reset(first = [' '])
			common()
			self.mediator.tongueModel.reset()
		
		def normal():
			self.mediator.toothManager.mayKillCommand()
			self.mediator.contentGetter.reset()
			self.mediator.pretenders.reset()
			common()
			self.mediator.toothManager.reset()
			self.mediator.tongueModel.reset()
		
		if strategy == "silentlyChange":
			self.textChangedDependingOnTextChangedStrategy = silentlyChange
		elif strategy == "silentlyMove":
			self.textChangedDependingOnTextChangedStrategy = silentlyMove
		elif strategy == "normal":
			self.textChangedDependingOnTextChangedStrategy = normal
		else:
			self.textChangedDependingOnTextChangedStrategy = (lambda *args: None)
	
	def textChanged(self):
		self.textChangedDependingOnTextChangedStrategy()
	
	def resizeEvent(self, event):
		self.rootPrefs["chewingWidth"]  = self.width()
		self.rootPrefs["chewingHeight"] = self.height()
		self.mediator.editorSetter.updateChewingWidthAndFontSizeRatio(self.width(),self.prefs["editorFontSizeRatio"])
		self.prefs["editorFontSize"] = self.mediator.editorSetter.getFontSize()
		self.jawsView.updateSize(self.width())
		self.mediator.tongueModel.view.reset()
		self.mediator.toothManager.updateTargetAndStates()
		self.mediator.toothManager.updateAllPictos()
		# self.mediator.toothManager.reset()
	
	def moveEvent(self, event = None):
		self.rootPrefs["chewingPosX"] = self.x()
		self.rootPrefs["chewingPosY"] = self.y()
	
	
# Preferences
	
	def preferences(self):
		self.setTextChangedStrategy("normal")
		self.textChanged()
		rootPrefs = copy.deepcopy(self.rootPrefs)
		profiles = copy.deepcopy(self.profiles)
		if Preferences(rootPrefs,profiles,self).exec_() == QtGui.QDialog.Accepted:
			self.rootPrefs = rootPrefs
			self.profiles = profiles
			self.prefs = profiles[rootPrefs["profileIndex"]]
			self.updateWithPrefs(self.prefs)
			self.mediator.tongueModel.view.reset()
			self.mediator.toothManager.reset()
			self.resumeToSpaceDependingOnScanStrategy()
	
	def readSettings(self):
		def readValue(variant,valueType):
			if valueType in (str,unicode):
				return unicode(variant.toString())
			if valueType in (int,long):
				return variant.toInt()[0]
			if valueType is bool:
				return variant.toBool()
			if valueType is float:
				return variant.toDouble()[0]
			raise ValueError
		
		def patchPyQt443Value(d,name,defaultValue):
			if  name in ("favoriteCharsVariants","personalizedOrder") and d[name]==u'':
				d[name] = defaultValue
		
		settings = QtCore.QSettings("lita", "chewingword")
		self.rootPrefs = dict.fromkeys(["profileIndex", "lastPrefTab", "startupProfile", "nbProfiles", "previousVersion", "chewingWidth", "chewingHeight", "chewingPosX", "chewingPosY"])
		for name in self.rootPrefs:
			defaultName = "default" + name[0].upper() + name[1:]
			defaultValue = aspect.__class__.__dict__[defaultName]
			self.rootPrefs[name] = readValue(settings.value(name,QtCore.QVariant(defaultValue)),type(defaultValue))
		self.profiles = []
		hasObsoleteProfiles = 0
		if self.rootPrefs["previousVersion"] not in ["before 1.1", aspect.version]:
			hasObsoleteProfiles = aspect.defaultNbProfiles
		for i in range(-hasObsoleteProfiles,self.rootPrefs["nbProfiles"]):
			d = {}
			settings.beginGroup(str(i))
			for name in aspect.defaultProfiles[0]:
				defaultValue = aspect.defaultProfiles[i % aspect.defaultNbProfiles][name]
				try:
					d[name] = readValue(settings.value(name,QtCore.QVariant(defaultValue)),type(defaultValue))
				except:
					d[name] = defaultValue
				patchPyQt443Value(d,name,defaultValue)
			if hasObsoleteProfiles and i>=0:
				d["isInMenu"] = False
				d["localName"] = "[%s] %s" % (self.rootPrefs["previousVersion"], d["localName"])
			settings.endGroup()
			self.profiles.append(d)
		if self.rootPrefs["startupProfile"] == "ask" and [profile["isInMenu"] for profile in self.profiles].count(True) > 1:
			StartupDialog(self.rootPrefs,self.profiles,self).exec_()
		if hasObsoleteProfiles:
			self.rootPrefs["profileIndex"] = aspect.defaultProfileIndex
		self.prefs = self.profiles[self.rootPrefs["profileIndex"]]
		self.rootPrefs["previousVersion"] = aspect.version
		self.rootPrefs["nbProfiles"] += hasObsoleteProfiles
		# self.mediator.editorSetter.setFloatWidth(self.rootPrefs["chewingWidth"])
		self.move(self.rootPrefs["chewingPosX"],self.rootPrefs["chewingPosY"])
		self.resize(self.rootPrefs["chewingWidth"],self.rootPrefs["chewingHeight"])
	
	def writeSettings(self):
		self.prefs["precision"]  = self.mediator.calculator.getPrecision()
		settings = QtCore.QSettings("lita", "chewingword")
		for (k,v) in self.rootPrefs.iteritems():
			settings.setValue(k,QtCore.QVariant(v))
		for i in range(self.rootPrefs["nbProfiles"]):
			settings.beginGroup(str(i))
			for (k,v) in self.profiles[i].iteritems():
				settings.setValue(k,QtCore.QVariant(v))
			settings.endGroup()
	
	def updateWithPrefs(self, prefs):
		self.clickTimer.setInterval(prefs["longClickDelay"]*100)
		self.mediator.scanner.setScanStartSound(prefs["scanStartSound"])
		self.mediator.scanner.setScanStepSound(prefs["scanStepSound"])
		self.mediator.scanner.setLongClickStartSound(prefs["longClickStartSound"])
		self.mediator.scanner.setLongClickStepSound(prefs["longClickStepSound"])
		self.mediator.scanner.checkScanStartSoundStrategy(prefs["scanStartSound"] != "")
		self.mediator.scanner.checkScanStepSoundStrategy(prefs["scanStepSound"] != "")
		self.mediator.scanner.checkLongClickStartSoundStrategy(prefs["longClickStartSound"] != "")
		self.mediator.scanner.checkLongClickStepSoundStrategy(prefs["longClickStepSound"] != "")
		self.mediator.clickTransformer.setLongClickStartSound(prefs["longClickStartSound"])
		self.mediator.clickTransformer.setLongClickStepSound(prefs["longClickStepSound"])
		self.mediator.clickTransformer.checkLongClickStartSoundStrategy(prefs["longClickStartSound"] != "")
		self.mediator.clickTransformer.checkLongClickStepSoundStrategy(prefs["longClickStepSound"] != "")
		self.longClickTimer.setTempo(prefs["longClickTempo"])
		self.shortClickTimer.setInterval(prefs["longClickDelay"]*100)
		self.autoClickTimer.setIntervals(prefs["autoClickDelay"]*100,prefs["autoClickExtraDelay"]*100)
		self.autoClickTimer.setSound("sounds/double/%s.wav" % prefs["autoClickSound"])
		self.autoClickTimer.setAudibleStrategy("stop" if prefs["autoClickSound"] else None)
		self.autoClickTimer.setTolerance(prefs["autoClickTolerance"])
		self.scanTimer.setTempo(prefs["scanTempo"])
		self.highlightTimer.setTempo(prefs["scanTempo"])
		self.jawsView.setTongueNbMaxChars(prefs["tongueNbMaxChars"])
		self.mediator.snipper.setMaxLength(prefs["tongueNbMaxChars"])
		self.mediator.favoriteChars.setYinYangFlag(prefs["commandsByShortClick"] and prefs["interactionMode"] != "scan")
		self.mediator.favoriteChars.setVariants(prefs["favoriteCharsVariants"])
		self.mediator.calculator.setPrecision(prefs["precision"])
		self.checkDialAboveStrategy(prefs["dialAboveMode"])
		self.checkHintStrategy(prefs["hintMode"])
		self.checkUndoableStrategy(prefs["undoableMode"])
		self.jawsView.checkMouseTrackingStrategy((prefs["undoableMode"] and prefs["interactionMode"] == "auto-click") or prefs["verticalBarrierMode"] or prefs["horizontalBarrierMode"] or prefs["internalBarrierMode"])
		self.jawsView.checkBarriersStrategy(prefs["verticalBarrierMode"] or prefs["horizontalBarrierMode"] or prefs["internalBarrierMode"])
		self.jawsView.checkVerticalOpenBarrierStrategy(prefs["verticalBarrierMode"] and prefs["verticalOpenBarrierMode"])
		self.jawsView.checkHorizontalBarrierStrategy(prefs["horizontalBarrierMode"])
		self.jawsView.checkVerticalBarrierStrategy(prefs["verticalBarrierMode"])
		self.jawsView.checkInternalBarrierStrategy(prefs["internalBarrierMode"] and prefs["predictionMode"])
		self.jawsView.setBarrierWidthFactor(prefs["barrierWidthFactor"])
		self.jawsView.setBarrierNumber(prefs["barrierNumber"])
		self.autoClickTimer.checkStartStrategy(prefs["interactionMode"] == "auto-click")
		self.editorView.checkHoverSelectionStrategy(prefs["interactionMode"] == "auto-click")
		self.autoClickTimer.setAutonomy(prefs["autoClickAutonomy"])
		self.mediator.commandSpeech.checkScanStrategy(prefs["interactionMode"] == "scan")
		self.mediator.clickTransformer.checkScanStrategy(prefs["interactionMode"] == "scan")
		self.mediator.toothManager.checkScanStrategy(prefs["interactionMode"] == "scan")
		self.mediator.pretenders.setScanStrategy(prefs["interactionMode"], prefs["scannerName"])
		self.mediator.toothManager.setScanJump(prefs["scanJump"])
		# self.mediator.toothManager.checkMilestoneStrategy(prefs["interactionMode"] == "scan") # ??????????
		self.jawsView.checkPredictionStrategy(prefs["predictionMode"])
		self.mediator.acquirer.checkAbbreviationStrategy(prefs["abbreviationMode"])
		self.mediator.calculator.checkEvaluationStrategy(prefs["evaluationMode"])
		self.mediator.caseMapper.checkContextCapsStrategy(prefs["contextCapsMode"])
		self.mediator.contentGetter.checkAbbreviationStrategy(prefs["abbreviationMode"])
		self.mediator.editorSetter.checkResizeCharactersStrategy(prefs["resizeCharMode"])
		self.mediator.editorSetter.setTabSize(prefs["tabSize"])
		self.mediator.editorSetter.setFontFamily(prefs["editorFontFamily"])
		self.mediator.editorSetter.checkResizeCharactersStrategy(prefs["resizeCharMode"])
		self.mediator.editorSetter.updateFontSize(prefs["editorFontSize"])
		self.mediator.editorSetter.updateChewingWidthAndFontSizeRatio(self.width(),self.prefs["editorFontSizeRatio"])
		self.mediator.expander.checkAbbreviationStrategy(prefs["abbreviationMode"])
		self.mediator.pretenders.setDynamicStrategy(prefs["keyboardOrder"])
		self.mediator.pretenders.setPersonalizedOrder(prefs["personalizedOrder"])
		self.mediator.tongueModel.checkCorrectionStrategy(["correctCaps"],prefs["correctCapsMode"])
		self.mediator.tongueModel.checkCorrectionStrategy(["correctAcnt"],prefs["correctAcntMode"])
		self.mediator.tongueModel.checkFavoriteCharsEnabledStrategy(prefs["favoriteCharsMode"])
		self.mediator.tongueModel.setClickStrategy(prefs["truncatableMode"],prefs["leftBehaviour"])
		self.mediator.toothManager.setDynamicStrategy(prefs["keyboardOrder"])
		self.mediator.toothManager.checkProgressiveStrategy(prefs["progressiveMode"])
		self.mediator.toothManager.checkPunctuationStrategy(prefs["punctuationMode"])
		self.mediator.toothManager.checkGlobalSpaceStrategy(prefs["globalSpaceMode"])
		self.mediator.toothManager.checkPauseStrategy(prefs["interactionMode"] == "scan")
		self.mediator.toothManager.checkCrossStrategy(prefs["interactionMode"] == "scan")
		self.checkResumeToSpaceStrategy(prefs["interactionMode"] == "scan")
		self.checkScanTraceStrategy(prefs["scanTraceMode"])
		self.mediator.scanner.checkScanTraceStrategy(prefs["scanTraceMode"])
		self.mediator.scanner.setReplayRatio(prefs["replayRatio"])
		self.mediator.clickTransformer.setStrategy(aspect.clickTransformerNames["keys"].index(prefs["clickTransformerName"]))
		self.mediator.scanner.setStrategy(aspect.scannerNames["keys"].index(prefs["scannerName"]))
		self.mediator.tongueModel.setLongClickVariantStrategy(prefs["longClickVariant"])
		self.mediator.pictoWrapper.checkPictoStrategy(prefs["pictoMode"])
		self.mediator.toothManager.checkPictoStrategy(prefs["pictoMode"])
		self.mediator.toothManager.checkDirectPictoStrategy(prefs["toothDirectPictoMode"])
		self.mediator.toothManager.checkIndirectPictoStrategy(prefs["toothIndirectPictoMode"])
		self.mediator.toothManager.checkFarPictoStrategy(prefs["pictoMode"])
		self.mediator.toothManager.checkFarDirectPictoStrategy(prefs["farDirectPictoMode"])
		self.mediator.toothManager.checkFarIndirectPictoStrategy(prefs["farIndirectPictoMode"])
		self.mediator.toothManager.checkAllVividPictosMode(prefs["allVividPictosMode"])
		self.farPictoView.checkMultiplePictosStrategy(prefs["multiplePictosMode"])
		self.farPictoView.setPictoRatio(prefs["farPictoRatio"])
		commands = ["speech\x00","select\x00"] if aspect.selectMode else ["speech\x00"]
		self.mediator.toothManager.checkCommandsStrategy(commands != [] and (prefs["commandsByLongClick"] or prefs["commandsByShortClick"]))
		self.mediator.toothManager.checkCommandsByLongClickStrategy(prefs["commandsByLongClick"] and prefs["interactionMode"] != "auto-click")
		self.mediator.toothManager.setCommands(commands + ["cancel\x00"])
		self.mediator.tts.setCommandLine(prefs["speechCommandLine"])
		self.speechView.setOutputExtension(self.mediator.tts.getOutputExtension())
		self.mediator.instantSpeech.checkPictoModeStrategy(prefs["speechPictoMode"])
		self.mediator.instantSpeech.checkNonPictoModeStrategy(prefs["speechNonPictoMode"])
		self.mediator.instantSpeech.checkRestrictAudioStrategy(prefs["speechRestrictAudio"])
		self.mediator.instantSpeech.checkSpellStrategy(prefs["spellPredictionMode"])
		self.mediator.commandSpeech.checkSpellStrategy(prefs["spellWordMode"])
		self.mediator.toothManager.checkJumpOnPauseStrategy(prefs["speechJumpOnPauseMode"])
		self.profileMenu.clear()
		self.speechCache.removeAllExceptLinks()
		newAction = None
		for (i,profile) in enumerate(self.profiles):
			if profile["isInMenu"]:
				newAction = QtGui.QAction(profile["localName"], self)
				newAction.setCheckable(True)
				newAction.setChecked(i==self.rootPrefs["profileIndex"])
				self.profileMenu.addAction(newAction)
		if newAction is None:
			newAction = QtGui.QAction(self.trUtf8("Check some profile to populate this menu..."), self)
			self.profileMenu.addAction(newAction)
		self.mediator.editorSetter.updateChewingWidthAndFontSizeRatio(self.width(),self.prefs["editorFontSizeRatio"])
		self.prefs["editorFontSize"] = self.mediator.editorSetter.getFontSize()
		self.jawsView.updateSize(self.width()) # useless except in the case where the tongue is enabled / disabled
	
	def updateProfileSlot(self, action):
		i = list(self.profileMenu.actions()).index(action) + 1
		for (profileIndex,profile) in enumerate(self.profiles):
			if profile["isInMenu"]:
				i -= 1
			if i == 0:
				self.prefs = profile
				self.rootPrefs["profileIndex"] = profileIndex
				self.updateWithPrefs(self.prefs)
				self.mediator.tongueModel.view.reset()
				self.mediator.toothManager.reset()
				self.resumeToSpaceDependingOnScanStrategy()
				break
		else:
			self.preferences()
	
	def checkResumeToSpaceStrategy(self,strategy):
		def resumeToSpaceWhenScanStrategyIsOn():
			QtGui.QCursor.setPos(self.mediator.toothManager.getSpaceView().getScanPosition())
			# disconnect after the initial call
			self.disconnect(self.jawsView,SIGNAL("jawsView painted"),self.resumeToSpaceDependingOnScanStrategy)
		
		if strategy:
			self.resumeToSpaceDependingOnScanStrategy = resumeToSpaceWhenScanStrategyIsOn
		else:
			self.resumeToSpaceDependingOnScanStrategy = (lambda *args: None)
	
	
# status bar and editor related
	
	def checkHintStrategy(self,strategy):
		if strategy:
			self.connect(self.jawsView,SIGNAL("glyphChanged"),self.updateTip)
		else:
			self.disconnect(self.jawsView,SIGNAL("glyphChanged"),self.updateTip)
			self.statusBarView.clearMessage()
	
	def updateTip(self, glyph):
		self.statusBarView.showMessage(self.mediator.iniMap.get(glyph,u""))
	
	def checkUndoableStrategy(self,strategy):
		self.jawsView.checkUndoableStrategy(strategy)
		if strategy:
			self.connect(self.jawsView,SIGNAL("clickOnBackground"),self.undoOrRedo)
			self.connect(self.jawsView,SIGNAL("longClickOnBackground"),self.toggleUndoOrRedo)
			self.connect(self.jawsView,SIGNAL("autoClickUndo"),self.autoClickUndo)
			self.connect(self.jawsView,SIGNAL("autoClickRedo"),self.autoClickRedo)
		else:
			self.disconnect(self.jawsView,SIGNAL("clickOnBackground"),self.undoOrRedo)
			self.disconnect(self.jawsView,SIGNAL("longClickOnBackground"),self.toggleUndoOrRedo)
			self.disconnect(self.jawsView,SIGNAL("autoClickUndo"),self.autoClickUndo)
			self.disconnect(self.jawsView,SIGNAL("autoClickRedo"),self.autoClickRedo)
	
	def undoOrRedo(self):
		if self.undoOrRedoSignal == "redo()" and self.editorView.document().isRedoAvailable():
			self.editorView.document().redo()
		else:
			self.undoOrRedoSignal = "undo()"
			self.editorView.document().undo()
	
	def toggleUndoOrRedo(self):
		if self.undoOrRedoSignal == "undo()":
			self.undoOrRedoSignal = "redo()"
		else:
			self.undoOrRedoSignal = "undo()"
		self.undoOrRedo()
	
	def autoClickRedo(self):
		self.editorView.document().redo()
		if not self.editorView.document().isRedoAvailable():
			self.jawsView.stopAutoClickTimer()
	
	def autoClickUndo(self):
		self.editorView.document().undo()
	
	def checkDialAboveStrategy(self,strategy):
		if strategy:
			if self.layout.itemAt(0).widget() == self.editorView:
				self.layout.removeWidget(self.editorView)
				self.layout.insertWidget(1,self.editorView)
		else:
			if self.layout.itemAt(0).widget() == self.jawsView:
				self.layout.removeWidget(self.jawsView)
				self.layout.insertWidget(1,self.jawsView)
	
	def cutAll(self):
		self.copyAll()
		self.mediator.editorSetter.clear()
	
	def copyAll(self):
		text = self.mediator.editorGetter.getAll()
		QtGui.QApplication.clipboard().setText(text)
	
	def trackingBehaviourChanged(self,i):
		self.statusBarView.showMessage(self.trackingBehaviourMessages[i])
	
	def metaInformationChanged(self,message):
		self.statusBarView.showMessage(message)
	
	
# Indexing and archiving
	
	def mayArchiveTranscript(self):
		if self.currentFileName.isEmpty():
			return self.mayArchiveUnsavedTranscript()
		else:
			return self.mayArchiveSavedTranscript(self.currentFileName)
	
	def mayArchiveUnsavedTranscript(self):
		transcript = self.editorView.getAll()
		if transcript == u"":
			return True # An empty transcript doesn't need to be archived
		if self.prefs["archiveUnsaved"] == "no":
			return True
		if self.prefs["archiveUnsaved"] == "on demand":
			ret = QtGui.QMessageBox.warning(self, aspect.applicationName,
			      self.trUtf8("<p>Do you want to archive this transcription?</p>"),
			      QtGui.QMessageBox.Yes | QtGui.QMessageBox.Default,
			      QtGui.QMessageBox.No,
			      QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Escape)
			if ret == QtGui.QMessageBox.No:
				return True
			if ret == QtGui.QMessageBox.Cancel:
				return False
		return self.archiveUnsavedTranscript(transcript)
	
	def archiveUnsavedTranscript(self, transcript):
		date = QtCore.QDate.currentDate()
		path = self.validArchiveDirectory(unicode(self.trUtf8("%1/%2").arg(self.prefs["archiveDirectory"]).arg(date.toString(QtCore.Qt.ISODate))))
		if path is None:
			return True
		time = QtCore.QTime.currentTime()
		(h,m,s) = str(time.toString(QtCore.Qt.ISODate)).split(":")
		fileName = os.path.join(path,unicode(self.trUtf8("Transcription completed at %1h%2.txt").arg(h).arg(m)))
		file = QtCore.QFile(fileName)
		if not file.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
			QtGui.QMessageBox.warning(self, aspect.applicationName, self.trUtf8("<p>Cannot archive transcription.</p>"))
			return True
		outf = QtCore.QTextStream(file)
		QtGui.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
		outf << transcript
		QtGui.QApplication.restoreOverrideCursor()
		self.statusBarView.showMessage(self.trUtf8("Transcription archived"), 2000)
		self.updateIndexAndSave()
		return True
	
	def mayArchiveSavedTranscript(self, fileName):
		if self.prefs["archiveSaved"] == "no":
			return True
		if self.prefs["archiveSaved"] == "on demand":
			ret = QtGui.QMessageBox.warning(self, aspect.applicationName,
			      self.trUtf8("<p>Do you want to create a link to this document in the archive directory?</p>"),
			      QtGui.QMessageBox.Yes | QtGui.QMessageBox.Default,
			      QtGui.QMessageBox.No,
			      QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Escape)
			if ret == QtGui.QMessageBox.No:
				return True
			if ret == QtGui.QMessageBox.Cancel:
				return False
		return self.archiveSavedTranscript(fileName)
	
	def archiveSavedTranscript(self, fileName):
		path = self.validArchiveDirectory(unicode(self.trUtf8("%1/links").arg(self.prefs["archiveDirectory"])))
		if path is None:
			return True
		if fileName.startsWith(self.prefs["archiveDirectory"]):
			# The text is actually saved among the archives
			return True
		linkName = os.path.join(path,unicode(self.strippedName(fileName)))
		if os.path.exists(linkName):
			# The text is already linked in the archive directory
			return True
		if not QtCore.QFile.link(fileName,linkName):
			QtGui.QMessageBox.warning(self, aspect.applicationName, self.trUtf8("<p>Cannot create a link to this document.</p>"))
			return True
		self.statusBarView.showMessage(self.trUtf8("Link created"), 2000)
		self.updateIndexAndSave()
		return True
	
	def updateIndexAndSave(self):
		text = self.editorView.getAll()
		text = self.mediator.expander.nonExpandedVersion(text)
		self.mediator.wrdInd.addRawText(text)
		if self.saveArchiveIndex():
			self.statusBarView.showMessage(self.trUtf8("Index updated"), 2000)
	
	def reindexArchives(self):
		""" Only called on user's request. """
		path = self.validArchiveDirectory(unicode(self.prefs["archiveDirectory"]))
		if path is None:
			return
		forbiddenPath = os.path.join(path,aspect.audioBaseDirectoryName)
		textFiles = [(d,f) for (d,_,fl) in self.osWalkFollowingLinks(path) for f in fl if f.endswith(".txt") and not d.startswith(forbiddenPath)]
		nbFiles = len(textFiles)
		if nbFiles == 0:
			message = self.trUtf8("<p>No transcription is currently archived in the directory <tt>%1</tt>.</p>").arg(pathForPrint(path))
			QtGui.QMessageBox.warning(self, aspect.applicationName, message)
			return
		self.mediator.wrdInd.clear()
		for (d,f) in textFiles:
			fileName = os.path.join(d,f)
			file = QtCore.QFile(fileName)
			if not file.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
				message = self.trUtf8("<p>Process aborted due to a problem with your archive <tt>%1</tt>.<br>You might want to delete this file.</p>").arg(pathForPrint(path))
				QtGui.QMessageBox.warning(self, self.trUtf8("Reindexing failed."), message)
				return
			inf = QtCore.QTextStream(file)
			text = unicode(inf.readAll())
			text = self.mediator.expander.nonExpandedVersion(text)
			self.mediator.wrdInd.addRawText(text)
		if self.saveArchiveIndex() is None:
			return
		message = self.trUtf8("<p>%n files have been scanned, ","",nbFiles)
		nbWords = len(self.mediator.wrdInd)
		if nbWords == 0:
			message += self.trUtf8("but the resulting index is empty.<br>You might want to check the content of the directory <tt>%1</tt>.<\p>").arg(pathForPrint(path))
		else:
			message += self.trUtf8("resulting in an index of %n words.</p>","",len(self.mediator.wrdInd))
		QtGui.QMessageBox.information( self, self.trUtf8("Reindexing successful."),message,QtGui.QMessageBox.Ok)
	
	def validArchiveDirectory(self, path):
		""" Return the path of the current archive directory, creating it if needed. """
		try:
			if not os.path.isdir(path):
				os.makedirs(path)
			return path
		except OSError:
			message = self.trUtf8("<p>Cannot access or create any archive directory.<br>You might want to specify another directory in the Preferences panel.</p>")
			QtGui.QMessageBox.warning(self, aspect.applicationName, message)
	
	def saveArchiveIndex(self):
		path = self.validArchiveDirectory(unicode(self.prefs["archiveDirectory"]))
		if path is None:
			return
		try:
			fileName = os.path.join(path,"index.tsv")
			codecs.open(fileName,"w",encoding="utf8").write(self.mediator.wrdInd.tsv())
			return True
		except IOError:
			QtGui.QMessageBox.warning(self, aspect.applicationName, self.trUtf8("Cannot write index."))
	
	def loadArchiveIndex(self):
		path = self.validArchiveDirectory(unicode(self.prefs["archiveDirectory"]))
		if path is None:
			return
		try:
			fileName = os.path.join(path,"index.tsv")
			if not self.mediator.wrdInd.constructFromTsv(codecs.open(fileName,encoding="utf8").read()):
				ret = QtGui.QMessageBox.warning(self, aspect.applicationName,
				      self.trUtf8("<p>The archives index seems corrupted.<br>Would you like me to reconstruct it right now?</p>"),
				      QtGui.QMessageBox.No,
				      QtGui.QMessageBox.Yes | QtGui.QMessageBox.Default)
				if ret == QtGui.QMessageBox.No:
					return
				self.reindexArchives()
			self.mediator.wrdInd.injectInWrdFor()
			return True
		except IOError:
			self.statusBarView.showMessage(self.trUtf8("Cannot read index."), 2000)
	
	def loadExpansionFile(self):
		path = self.validArchiveDirectory(unicode(self.prefs["archiveDirectory"]))
		self.saveExpansionFileSpecifics = self.saveExpansionFileDoSpecifics
		if path is None:
			return
		try:
			fileName = os.path.join(path,"expansions.json")
			if not self.mediator.expander.setFromString(open(fileName).read()):
				message = self.trUtf8("<p>The expansion file seems corrupted.<br>You might want to check it manually.</p>")
				QtGui.QMessageBox.warning(self, aspect.applicationName, message)
				self.saveExpansionFileSpecifics = self.saveExpansionFileDontSpecifics
				return False
			return True
		except IOError:
			self.statusBarView.showMessage(self.trUtf8("Cannot read expansion file."), 2000)
	
	def loadScanTrace(self):
		path = self.validArchiveDirectory(unicode(self.prefs["archiveDirectory"]))
		if path is None:
			return
		try:
			fileName = os.path.join(path,"scantrace.json")
			self.mediator.scanner.setSavedScanTrace(mysimplejson.loads(open(fileName).read()))
		except IOError:
			self.mediator.scanner.setSavedScanTrace([])
	
	def saveExpansionFileDontSpecifics(self):
		pass
	
	def saveExpansionFileDoSpecifics(self):
		path = self.validArchiveDirectory(unicode(self.prefs["archiveDirectory"]))
		if path is None:
			return
		try:
			fileName = os.path.join(path,"expansions.json")
			codecs.open(fileName,"w",encoding="utf8").write(self.mediator.expander.getAsString())
			return True
		except IOError:
			QtGui.QMessageBox.warning(self, aspect.applicationName, self.trUtf8("Cannot write expansion file."))
	
	
# Picto persistance
	
	def loadFlexionDictionary(self):
		try:
			flexionDict = mysimplejson.loads(open(self.flexionFileName).read())
		except:
			self.statusBarView.showMessage(self.trUtf8("Problem with the flexion dictionary."), 2000)
			flexionDict = {}
		self.mediator.pictos.setFlexionDictionary(flexionDict)
	
	def validPictoDirectory(self, path, createIfNeeded = False):
		""" Return the path of the current picto directory, creating it if needed. """
		pictoPath = os.path.join(path,self.pictoBaseDirectoryName)
		try:
			if not os.path.isdir(pictoPath):
				if createIfNeeded:
					os.makedirs(pictoPath)
					message = self.trUtf8("<p>I have just created for you a directory <tt>%1</tt> in which you should move the pictos you want me to index.<br>Accepted formats: %2.</p>").arg(pathForPrint(pictoPath)).arg(self.supportedImageFormatsForPrint)
					QtGui.QMessageBox.information(self, aspect.applicationName, message)
				return
			return pictoPath
		except OSError:
			message = self.trUtf8("<p>Cannot access or create any picto directory.<br>You might want to specify another archive directory in the Preferences panel.</p>")
			QtGui.QMessageBox.warning(self, aspect.applicationName, message)
	
	def loadPictos(self):
		pictoDicts = []
		for directoryContainingPictoFolder in (".",unicode(self.prefs["archiveDirectory"])):
			path = self.validPictoDirectory(directoryContainingPictoFolder,createIfNeeded = False)
			if path is None:
				continue
			for (path, _, fileNames) in self.osWalkFollowingLinks(path):
				for fileName in fileNames:
					if fileName == self.pictoIndexFileName:
						try:
							newPictoDicts = mysimplejson.loads(open(os.path.join(path,self.pictoIndexFileName)).read())["images"]
							try:
								for d in newPictoDicts:
									d["file"] = os.path.join(path,d["file"])
								pictoDicts.extend(newPictoDicts)
							except KeyError:
								self.statusBarView.showMessage(self.trUtf8("Key error in picto index."), 2000)
						except ValueError:
							self.statusBarView.showMessage(self.trUtf8("Malformed picto index."), 2000)
		self.mediator.pictos.setPictoDictionary(pictoDicts)
	
	def indexNewPictos(self):
		""" Only called on user's request. """
		def doIndexAndMayRename(path):
			currentWorkingDirectory = os.getcwd()
			os.chdir(path)
			try:
				pictoIndex = mysimplejson.loads(open(self.pictoIndexFileName).read())
				# TODO: deal with malformed json files
			except IOError, ValueError:
				pictoIndex = dict((key,"") for key in ["version","format","set","author","url","mail","license"])
				pictoIndex["images"] = []
			for (i,picto) in enumerate(pictoIndex["images"]):
				if picto["file"] != "" and not os.path.exists(picto["file"]):
					del pictoIndex["images"][i]
			knownFiles = frozenset(picto["file"] for picto in pictoIndex["images"])
			for fileName in QtCore.QDir(".").entryList(): # cannot rely on os.listdir(".") on Mac OS X due to special NFD coding for unicode filenames
				fileName = unicode(fileName)
				(name,ext) = os.path.splitext(fileName)
				if ext.lower() in self.supportedImageFormats and fileName not in knownFiles:
					d = pictos.parseName(name)
					try:
						newFileName = d["rename"] + ext
						os.rename(fileName,newFileName)
						fileName = newFileName
					except:
						pass
					for item in d["items"]:
						pictoIndex["images"].append(item)
						pictoIndex["images"][-1]["file"] = fileName
					self.nbNewPictoFilesFound += 1
			if len(pictoIndex["images"]):
				fileNameMaxLength = max(len(d["file"]) for d in pictoIndex["images"])
				various = [
					'{\n\t"version":"%s"' % pictoIndex["version"],
					'\t"format":"%s"' % pictoIndex["format"],
					'\t"set":"%s"' % pictoIndex["set"],
					'\t"author":"%s"' % pictoIndex["author"],
					'\t"url":"%s"' % pictoIndex["url"],
					'\t"mail":"%s"' % pictoIndex["mail"],
					'\t"license":"%s"' % pictoIndex["license"],
					'\t"images":[\n%s\n\t]\n}',
				]
				images = []
				for d in pictoIndex["images"]:
					right = ", ".join(['"%s":"%s"' % (k,v) for (k,v) in d.iteritems() if k !="file"])
					images.append('\t\t{"file":"%s"%s, %s}' % (d["file"]," "*(fileNameMaxLength - len(d["file"])),right))
				contents = ",\n".join(various) % ",\n".join(images)
				codecs.open(self.pictoIndexFileName,"w",encoding="utf8").write(contents)
				self.nbPictoSubDirsFound += 1
			os.chdir(currentWorkingDirectory)
		
		pictoPath = self.validPictoDirectory(unicode(self.prefs["archiveDirectory"]),createIfNeeded = True)
		if pictoPath is None:
			return
		paths = [os.path.join(dirPath,dirName) for (dirPath, dirNames, _) in self.osWalkFollowingLinks(pictoPath) for dirName in dirNames]
		self.nbPictoSubDirsFound = 0
		self.nbNewPictoFilesFound = 0
		for path in paths:
			doIndexAndMayRename(path)
		if self.nbNewPictoFilesFound == 0:
			message = self.trUtf8("<p>No picto file is currently present in the directory <tt>%1</tt>.<br>Accepted formats: %2.</p>").arg(pathForPrint(pictoPath)).arg(self.supportedImageFormatsForPrint)
			QtGui.QMessageBox.warning(self, aspect.applicationName, message)
			return
		message = self.trUtf8("<p>%n directories have been scanned, ","",self.nbPictoSubDirsFound)
		if self.nbNewPictoFilesFound == 0:
			message += self.trUtf8("but all present picto files were indexed already.<br>You might want to add some new pictos in <tt>%1</tt>.<br>Accepted formats: %2.</p>").arg(pathForPrint(pictoPath)).arg(self.supportedImageFormatsForPrint)
		else:
			message += self.trUtf8("resulting in %n newly indexed picto files.<br>","",self.nbNewPictoFilesFound)
			message += self.trUtf8("You might want to relaunch Chewing Word for the changes to take effect.</p>")
		QtGui.QMessageBox.information(self, self.trUtf8("Picto reindexing successful."),message,QtGui.QMessageBox.Ok)
	
		
# Audio persistance
	
	def validAudioDirectory(self, path, createIfNeeded = False):
		""" Return the path of the current audio directory, creating it if needed. """
		audioPath = os.path.join(path,self.audioBaseDirectoryName)
		try:
			if not os.path.isdir(audioPath):
				if createIfNeeded:
					os.makedirs(audioPath)
					message = self.trUtf8("<p>I have just created for you a directory <tt>%1</tt> in which you should move some audio files to index or some text files you want me to precalculate the reading.<br>Accepted formats: .txt, %2.</p>").arg(pathForPrint(audioPath)).arg(self.speechView.getSupportedAudioFormatsForPrint())
					QtGui.QMessageBox.information(self, aspect.applicationName, message)
				return
			return audioPath
		except OSError:
			message = self.trUtf8("<p>Cannot access or create any audio directory.<br>You might want to specify another archive directory in the Preferences panel.</p>")
			QtGui.QMessageBox.warning(self, aspect.applicationName, message)
	
	def indexAudio(self):
		def doIndexAndMayCreateTts(path):
			d = {}
			outputExtension = self.mediator.tts.getOutputExtension()
			for fileName in QtCore.QDir(path).entryList(): # cannot rely on os.listdir(path) on Mac OS X due to special NFD coding for unicode filenames
				fileName = unicode(fileName)
				(name,ext) = os.path.splitext(fileName)
				if self.speechView.inSupportedAudioFormats(ext.lower(),text=True):
					d[name] = d.get(name,{})
					d[name]["audio"] = ext
				elif ext.lower() == ".txt":
					d[name] = d.get(name,{})
					d[name]["text"] = ext
			for (name,exts) in d.iteritems():
				try:
					self.statusBarView.showMessage(self.trUtf8("Process file '%1'.").arg(name))
					if "text" in exts:
						text = codecs.open(os.path.join(path,name+exts["text"]),encoding="utf8").read()
						self.mediator.expander.define(name,text)
						text = text.strip()
						hashNames = [self.mediator.tts.getHash(text),self.mediator.tts.getHash(self.mediator.speechTransformer.preProcess(text))]
						if "audio" in exts:
							self.speechCache.makeSymLinks(os.path.join(path,name),hashNames,exts["audio"])
						else:
							self.mediator.tts.create(os.path.join(path,name),hashNames[0],text)
							self.speechCache.makeSymLinks(os.path.join(path,name),hashNames,outputExtension)
					else:
						self.speechCache.makeSymLinks(os.path.join(path,name),[self.mediator.tts.getHash(name),self.mediator.tts.getHash(self.mediator.speechTransformer.preProcess(name))],exts["audio"])
					self.nbAudioFilesLinked += 1
					self.statusBarView.clearMessage()
				except:
					self.statusBarView.showMessage(self.trUtf8("Problem with file '%1'.").arg(name),8000)
		
		audioPath = self.validAudioDirectory(unicode(self.prefs["archiveDirectory"]),createIfNeeded = True)
		if audioPath is None:
			return
		self.speechCache.removeAll()
		paths = [os.path.join(dirPath,dirName) for (dirPath, dirNames, _) in self.osWalkFollowingLinks(audioPath) for dirName in dirNames]
		self.nbAudioSubDirsFound = len(paths)
		self.nbAudioFilesLinked = 0
		for path in paths:
				doIndexAndMayCreateTts(path)
		if self.nbAudioFilesLinked == 0:
			message = self.trUtf8("<p>No text or audio file is currently present in the directory <tt>%1</tt>.<br>Accepted formats: .txt, %2.</p>").arg(pathForPrint(audioPath)).arg(self.speechView.getSupportedAudioFormatsForPrint())
			QtGui.QMessageBox.warning(self, aspect.applicationName, message)
			return
		message = self.trUtf8("<p>%n directories have been scanned, ","",self.nbAudioSubDirsFound)
		message += self.trUtf8("resulting in %n audio files linked.<br>","",self.nbAudioFilesLinked)
		QtGui.QMessageBox.information(self, self.trUtf8("Audio indexing successful."),message,QtGui.QMessageBox.Ok)
	
	
# Tracing scan mode
	
	def checkScanTraceStrategy(self,strategy):
		def saveScanTraceWhenScanTraceStrategyIsOn():
			date = QtCore.QDate.currentDate()
			time = QtCore.QTime.currentTime()
			(h,m,s) = str(time.toString(QtCore.Qt.ISODate)).split(":")
			try:
				path = self.validArchiveDirectory(unicode(self.trUtf8("%1/%2").arg(self.prefs["archiveDirectory"]).arg(date.toString(QtCore.Qt.ISODate))))
				fileName = os.path.join(path,unicode(self.trUtf8("Scan traced at %1h%2.json").arg(h).arg(m)))
				open(fileName,"w").write(mysimplejson.dumps(self.mediator.scanner.getScanTrace()))
				path = self.validArchiveDirectory(unicode(self.prefs["archiveDirectory"]))
				fileName = os.path.join(path,"scantrace.json")
				open(fileName,"w").write(mysimplejson.dumps(self.mediator.scanner.getScanTrace()))
			except IOError:
				self.statusBarView.showMessage(self.trUtf8("Cannot write trace scan file."), 2000)
		
		if strategy:
			self.saveScanTraceDependingOnScanTraceStrategy = saveScanTraceWhenScanTraceStrategyIsOn
		else:
			self.saveScanTraceDependingOnScanTraceStrategy = (lambda *args: None)
	
	def maySaveScanTrace(self):
		self.saveScanTraceDependingOnScanTraceStrategy()
	
	
# classical application stuff
	
	def about(self):
		self.splash = SplashScreen(self)
		self.splash.show()
	
	def visitSite(self):
		QtGui.QDesktopServices.openUrl(QtCore.QUrl("http://chewing-word.wingi.net"))
	
	def onlineRefman(self):
		QtGui.QDesktopServices.openUrl(QtCore.QUrl("http://chewingword.wikidot.com/fr-refman"))
	
	def offlineRefman(self):
		QtGui.QDesktopServices.openUrl(QtCore.QUrl("file:///%s/fr-refman.html" % QtCore.QDir.currentPath()))
	
	def documentWasModified(self):
		self.setWindowModified(self.editorView.document().isModified())
	
	def newFile(self):
		if self.maySave() and self.mayArchiveTranscript():
			self.editorView.clear()
			self.setCurrentFile(QtCore.QString())
	
	def open(self):
		if self.maySave() and self.mayArchiveTranscript():
			fileName = QtGui.QFileDialog.getOpenFileName(self,self.trUtf8("Open text file"), "", self.trUtf8("Plain Text (*.txt)"))
			if not fileName.isEmpty():
				self.loadFile(fileName)
	
	def save(self):
		if self.currentFileName.isEmpty():
			return self.saveAs()
		else:
			return self.saveFile(self.currentFileName)
	
	def saveAs(self):
		format = QtCore.QString("txt")
		initialPath = QtCore.QDir.currentPath() + self.trUtf8("/untitled.") + format
		fileName = QtGui.QFileDialog.getSaveFileName(self,self.trUtf8("Save plain text as"),initialPath,self.trUtf8("Plain Text (*.txt)"))
		if fileName.isEmpty():
			return False
		return self.saveFile(fileName)
	
	def closeEvent(self, event):
		if self.maySave() and self.mayArchiveTranscript():
			self.writeSettings()
			self.saveExpansionFileSpecifics()
			self.maySaveScanTrace()
			self.speechCache.removeAllExceptLinks()
			event.accept()
		else:
			event.ignore()
	
	def maySave(self):
		if self.editorView.document().isModified() and not self.currentFileName.isEmpty():
			ret = QtGui.QMessageBox.warning(self, aspect.applicationName,
			      self.trUtf8("<p>The document has been modified.<br>Do you want to save your changes?</p>"),
			      QtGui.QMessageBox.Yes | QtGui.QMessageBox.Default,
			      QtGui.QMessageBox.No,
			      QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Escape)
			if ret == QtGui.QMessageBox.Yes:
				return self.save()
			return ret != QtGui.QMessageBox.Cancel
		return True
	
	def loadFile(self, fileName):
		file = QtCore.QFile(fileName)
		if not file.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
			QtGui.QMessageBox.warning(self, aspect.applicationName, self.trUtf8("<p>Cannot read file %1:\n%2.</p>").arg(fileName).arg(file.errorString()))
			return
		inf = QtCore.QTextStream(file)
		QtGui.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
		self.editorView.setPlainText(inf.readAll())
		QtGui.QApplication.restoreOverrideCursor()
		self.setCurrentFile(fileName)
		self.statusBarView.showMessage(self.trUtf8("File loaded"), 2000)
	
	def saveFile(self, fileName):
		file = QtCore.QFile(fileName)
		if not file.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
			QtGui.QMessageBox.warning(self, aspect.applicationName, self.trUtf8("<p>Cannot write file %1:\n%2.</p>").arg(fileName).arg(file.errorString()))
			return False
		outf = QtCore.QTextStream(file)
		QtGui.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
		outf << self.editorView.getAll()
		QtGui.QApplication.restoreOverrideCursor()
		self.setCurrentFile(fileName)
		self.statusBarView.showMessage(self.trUtf8("File saved"), 2000)
		return True
	
	def setCurrentFile(self, fileName):
		self.currentFileName = fileName
		self.editorView.document().setModified(False)
		self.setWindowModified(False)
		self.disconnect(self.editorView.document(), QtCore.SIGNAL("contentChanged()"), self.documentWasModified)
		if self.currentFileName.isEmpty():
			self.setWindowTitle(self.trUtf8("%1[*]").arg(aspect.applicationName))
		else:
			self.setWindowTitle(self.trUtf8("%1[*]").arg(self.strippedName(self.currentFileName)))
			self.connect(self.editorView.document(), QtCore.SIGNAL("contentChanged()"), self.documentWasModified)
	
	def strippedName(self, fullFileName):
		return QtCore.QFileInfo(fullFileName).fileName()
	
	def print_(self):
		printer = QtGui.QPrinter()
		dlg = QtGui.QPrintDialog(printer, self)
		if dlg.exec_() == QtGui.QDialog.Accepted:
			self.editorView.document().print_(printer)
			self.statusBarView.showMessage(self.trUtf8("Ready"), 2000)
	
	def osWalkFollowingLinks(self,dirPath): # TODO in python 2.6: suppress this function and replace it with os.walk(dirPath,followinglinks = True)
		info = QtCore.QFileInfo(dirPath)
		if info.isSymLink():
			dirPath = unicode(info.symLinkTarget())
		qdir = QtCore.QDir(dirPath)
		dirNames  = [unicode(info.fileName()) for info in qdir.entryInfoList(QtCore.QDir.Dirs | QtCore.QDir.NoDotAndDotDot)]
		fileNames = [unicode(info.fileName()) for info in qdir.entryInfoList(QtCore.QDir.Files)]
		yield (dirPath,dirNames,fileNames)
		for dirName in dirNames:
			for result in self.osWalkFollowingLinks(os.path.join(dirPath,dirName)):
				yield result

def main():
	app = QtGui.QApplication(sys.argv)
	if aspect.forceStyle:
		app.setStyle(QtGui.QStyleFactory.create(aspect.forceStyle))
	frozen = getattr(sys, 'frozen', None)
	if aspect.platform == "darwin":
		defaultArchiveDirectory = unicode((QtCore.QDir(QtCore.QDir.homePath()+"/Documents/Chewing Word")).absolutePath())
	elif aspect.platform == "win":
		settings = QtCore.QSettings(QtCore.QSettings.UserScope, "Microsoft", "Windows");
		settings.beginGroup("CurrentVersion/Explorer/Shell Folders")
		defaultArchiveDirectory = unicode(QtCore.QDir(settings.value("Personal").toString()+"/Chewing Word").absolutePath())
		app.setWindowIcon(QtGui.QIcon(QtGui.QPixmap("chattering teeth.png")))
	elif aspect.platform == "linux":
		defaultArchiveDirectory = unicode((QtCore.QDir(QtCore.QDir.homePath()+"/Chewing Word")).absolutePath())
	if aspect.platform == "darwin" and os.system("defaults read -g AppleLanguages > /tmp/languages.txt")==0:
		# os.environ["LANG"] returns "fr", except with TextMate
		# locale.getdefaultlocale returns (None, 'mac-roman')
		# the following workaround has been tested under both 10.4 (Tiger) and 10.5 (Leopard)
		language = re.search("\W*(\w+)",open("/tmp/languages.txt").read()).group(1)
		# make sure the bundled application finds the required plugins
		if frozen:
			directory = QtCore.QDir(app.applicationDirPath())
			directory.cdUp()
			directory.cd("plugins")
			app.setLibraryPaths(QtCore.QStringList(directory.absolutePath()))
		# uncomment the following line to make CW run as a standalone application, which prevent this message
		# On Mac OS X, you might be loading two sets of Qt binaries into the same process.
		# Check that all plugins are compiled against the right Qt binaries.
		# Export DYLD_PRINT_LIBRARIES=1 and check that only one set of binaries are being loaded. 
		# 
	# elif aspect.platform == "linux":
	# 	print "linux", str(app.applicationDirPath())
	# 	# QtCore.QDir.setCurrent(app.applicationDirPath()+"/Chewing Word")
	# else:
	# 	QtCore.QDir.setCurrent(app.applicationDirPath())
	# 	# uncomment the following line to launch CW from the console under Windows
	# 	# QtCore.QDir.setCurrent(r"C:\Program Files\Chewing Word\\")
	else:
		try:
			language = locale.getdefaultlocale()[0][:2]
		except:
			language = ''
	for d in aspect.defaultProfiles:
		d["archiveDirectory"] = defaultArchiveDirectory
	translator = QtCore.QTranslator(None)
	translator.load("tr/chewing_%s.qm" % language, ".")
	app.setApplicationName("Chewing Word")
	app.installTranslator(translator)
	mainWindow = MainWindow(language)
	mainWindow.show()
	# QtCore.pyqtRemoveInputHook() # uncomment this line to prevent flood message when debugging: "QCoreApplication::exec: The event loop is already running"
	sys.exit(app.exec_())

 
if __name__ == "__main__":
	# import hotshot
	# prof = hotshot.Profile("hotshot_edi_stats")
	# prof.runcall(main)
	# prof.close()
	main()
