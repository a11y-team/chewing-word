#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file timer.py

# builtin stuff
from PyQt4 import QtCore, QtGui
import itertools
import qtgoodies

class Timer(QtCore.QTimer):
	
	def __init__(self, singleShot = True):
		QtCore.QTimer.__init__(self)
		self.connect(self, QtCore.SIGNAL("timeout()"), self.timeoutEvent)
		self.setSingleShot(singleShot)
		self.timeOutAction = (lambda *args: None)
		self.setAudibleStrategy(None)
		self.checkStartStrategy(True)
		self.restartDependingOnActivity = (lambda *args: None)
	
	def setSound(self,sound):
		self.sound = qtgoodies.soundFactory(sound)
	
	def setTempo(self,bpm):
		self.setInterval(60000/bpm)
	
	def play(self):
		self.sound.play()
	
	def setAudibleStrategy(self,strategy):
		if strategy == "start":
			self.mayPlaySoundAtStartDependingOnAudibleStrategy = self.play
			self.mayPlaySoundAtStopDependingOnAudibleStrategy = self.play
		elif strategy == "stop":
			self.mayPlaySoundAtStartDependingOnAudibleStrategy = (lambda *args: None)
			self.mayPlaySoundAtStopDependingOnAudibleStrategy = self.play
		else:
			self.mayPlaySoundAtStartDependingOnAudibleStrategy = (lambda *args: None)
			self.mayPlaySoundAtStopDependingOnAudibleStrategy = (lambda *args: None)
	
	def startWhenStartStrategyIsOff(self,*args):
		pass
	
	def startWhenStartStrategyIsOn(self,timeOutAction = (lambda *args: None)):
		self.mayPlaySoundAtStartDependingOnAudibleStrategy()
		self.disconnect(self,QtCore.SIGNAL("tick(int)"),self.timeOutAction)
		self.timeOutAction = timeOutAction
		self.connect(self,QtCore.SIGNAL("tick(int)"),self.timeOutAction)
		QtCore.QTimer.start(self)
	
	def checkStartStrategy(self,strategy):
		if strategy:
			self.startDependingOnStartStrategy = self.startWhenStartStrategyIsOn
		else:
			self.startDependingOnStartStrategy = self.startWhenStartStrategyIsOff
	
	def start(self,*args):
		self.counter = 0
		self.startDependingOnStartStrategy(*args)
		self.restartDependingOnActivity = self.start
	
	def stop(self,*args):
		QtCore.QTimer.stop(self)
		self.restartDependingOnActivity = (lambda *args: None)
	
	def restart(self,*args):
		self.restartDependingOnActivity(*args)
	
	def timeoutEvent(self):
		self.counter += 1
		self.mayPlaySoundAtStopDependingOnAudibleStrategy()
		self.emit(QtCore.SIGNAL("tick(int)"),self.counter)
	


class AutoClickTimer(Timer):
	
	def __init__(self):
		Timer.__init__(self)
		self.pos = QtCore.QPoint(-10000,-10000)
		self.setTolerance(0)
		self.setAutonomy(0)
		self.setAutoRepeat(True)
	
	def setTolerance(self, tolerance):
		self.tolerance = tolerance
	
	def setIntervals(self, normalInterval, extraDelay = 0):
		self.normalInterval   = normalInterval
		self.extendedInterval = normalInterval + extraDelay
	
	def setAutonomy(self, autonomy):
		self.autonomy = autonomy
	
	def mayNeedExtraDelay(self, doNeedExtraDelay):
		if doNeedExtraDelay:
			self.setInterval(self.extendedInterval)
		
	def timeoutEvent(self):
		self.setInterval(self.normalInterval)
		Timer.timeoutEvent(self)
		if self.autoRepeat and self.counter < self.autonomy:
			self.startWhenStartStrategyIsOn(self.timeOutAction)
	
	def checkStartStrategy(self,strategy):
		def startWhenStartStrategyIsOn(widget, event):
			pos = widget.mapToGlobal(event.pos())
			if (pos - self.pos).manhattanLength() > self.tolerance:
				self.pos = pos
				self.startWhenStartStrategyIsOn(widget.autoClickEvent)
		
		if strategy:
			self.startDependingOnStartStrategy = startWhenStartStrategyIsOn
		else:
			self.startDependingOnStartStrategy = self.startWhenStartStrategyIsOff
	
	def start(self,widget,event):
		self.counter = 0
		self.startDependingOnStartStrategy(widget,event)
	
	def stop(self,*args):
		Timer.stop(self)
		self.counter = self.autonomy
	
	def setAutoRepeat(self, autoRepeat):
		self.autoRepeat = autoRepeat
	

if __name__ == "__main__":
	from  chewing import main
	main()
