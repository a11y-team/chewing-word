#!/usr/bin/python
# -*- coding: utf-8 -*-
#file depgraphparams.py

import sre
from checkbox import CheckBox

include = \
	{
		"Builtin":(None,False),
		"Tests":(r".*(?<!ref)-tests",False),
		"Chrono":(r"chrono",False),
		"Examples":(r".*ref\b",False),
	}
baseList = \
	[
	r"injectdocumentation",
	r".*graph",
	r"~-tests",
	r"__init__",
	r"unittest",
	r"checkbox",
	r"tapeview3",
	]

items = dict(CheckBox([(name,checked) for (name,(_,checked)) in include.iteritems()]).items)

includeBuiltin = items.pop("Builtin")
excludeList = [include[name][0] for (name,checked) in items.iteritems() if not checked]
excludeRex = sre.compile("|".join(baseList+excludeList),sre.VERBOSE)
