#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file pictostageview.py


# builtin stuff
from PyQt4 import QtGui, QtCore

class FarPictoView(QtGui.QWidget):
	
	def __init__(self, parent):
		QtGui.QWidget.__init__(self, parent)
		self.getWidthInEditorView = parent.getViewportWidth
		self.getHeightInEditorView = parent.getViewportHeight
		self.rects = []
		self.hide()
	
	def checkMultiplePictosStrategy(self, strategy):
		def extendRectsWhenMultiplePictosStrategyIsOn(picto,pictoActivity,pictoInfo):
			if pictoInfo["isMultiple"] and pictoInfo["isDirect"]:
				w = picto.actualSize(self.size(),pictoActivity,QtGui.QIcon.On).width()
				for x in range(int(w*1.1/2)+self.width()/2,self.width(),int(w*1.1)):
					self.rects.append(QtCore.QRect(x,0,w,self.height()))
					self.rects.append(QtCore.QRect(self.width() - w - x,0,w,self.height()))
		
		if strategy:
			self.extendRectsDependingOnMultiplePictosStrategy = extendRectsWhenMultiplePictosStrategyIsOn
		else:
			self.extendRectsDependingOnMultiplePictosStrategy = (lambda *args: None)
	
	def extendRects(self, *args):
		self.extendRectsDependingOnMultiplePictosStrategy(*args)
	
	def setPictoRatio(self, percentage):
		self.pictoRatio = percentage / 100.0
		self.updateSize()
	
	def showPicto(self,picto,pictoActivity,pictoInfo):
		self.picto = picto
		self.pictoActivity = pictoActivity
		self.rects = [self.rect()]
		self.extendRects(picto,pictoActivity,pictoInfo)
		self.show()
	
	def hidePicto(self):
		self.hide()
	
	def paintEvent(self, event):
		painter = QtGui.QPainter()
		painter.begin(self)
		for rect in self.rects:
			self.picto.paint(painter,rect,QtCore.Qt.AlignBottom | QtCore.Qt.AlignHCenter,self.pictoActivity,QtGui.QIcon.On)
		painter.end()
	
	def updateSize(self):
		width = self.getWidthInEditorView()
		height = self.getHeightInEditorView()
		self.setGeometry(QtCore.QRect(0,height*(1-self.pictoRatio),width,height*self.pictoRatio))
	

if __name__ == "__main__":
	from  chewing import main
	main()

