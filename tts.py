#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file tts.py

# built-in stuff
import os,subprocess
import pipes
import codecs
import re

class Tts:
	
	def __init__(self):
		self.rexOutputExtension = re.compile(r".+%\(output\)s(\.\S+)")
		self.rexEncoding = re.compile(r"%\((\S*)input\)s\.txt")
	
	def setCache(self,cache):
		self.cachePath = unicode(cache.getAbsolutePath())
	
	def setCommandLine(self,commandLine):
		self.outputExtension = self.extractOutputExtension(commandLine)
		self.encoding = self.extractEncoding(commandLine)
		self.commandLine = self.rexEncoding.sub("%(input)s.txt",commandLine)
	
	def getOutputExtension(self):
		return self.outputExtension
	
	def extractOutputExtension(self,commandLine):
		try:
			return self.rexOutputExtension.search(commandLine,2).groups()[0]
		except:
			return ""
	
	def extractEncoding(self,commandLine):
		try:
			encoding = self.rexEncoding.search(commandLine,2).groups()[0]
			if encoding == "":
				return "utf8"
			return encoding
		except:
			return ""
	
	def prepareToSpeak(self,text,pathBaseName):
		if os.path.exists(pathBaseName + ".lnk") or os.path.exists(pathBaseName + self.outputExtension):
			return
		try:
			if not os.path.exists(pathBaseName + ".txt"):
				codecs.open(pathBaseName + ".txt","w",self.encoding).write(text)
			command = self.commandLine % {"input": pathBaseName, "output": pathBaseName}
			subprocess.call(command,shell=True)
		except:
			return
	
	def create(self,audioPathBaseName,hashName,text):
		cachePathBaseName = os.path.join(self.cachePath,hashName)
		try:
			codecs.open(cachePathBaseName + ".txt","w",self.encoding).write(text)
			command = self.commandLine % {"input": cachePathBaseName, "output": pipes.quote(audioPathBaseName)}
			subprocess.call(command,shell=True)
		except:
			return
	
	def getHash(self,text):
		return str(hash(text.lower().strip().rstrip(".")))
	
if __name__ == "__main__":
	from  chewing import main
	main()
