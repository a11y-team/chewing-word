#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file jawsview.py

# builtin stuff
from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import SIGNAL
from qtgoodies import retrieveOrGenerateRectangularPixmap
import itertools
# home made stuff
from toothview import ToothView
from tongueview import TongueView
from aspect import aspect
from distributenumber import distributeNumber


class JawsView (QtGui.QWidget):
	
	def __init__(self, mediator):
		QtGui.QWidget.__init__(self)
		self.jawsBackgroundLightColor                = QtGui.QColor(aspect.jawsBackgroundLightColor )
		self.jawsBackgroundMediumColor               = QtGui.QColor(aspect.jawsBackgroundMediumColor)
		self.jawsBackgroundDarkColor                 = QtGui.QColor(aspect.jawsBackgroundDarkColor  )
		self.milestoneBrush                          = QtGui.QBrush(QtGui.QColor(aspect.milestoneColor))
		self.nbTeeth                                 = mediator.nbTeeth
		self.resetInTongueModel                      = mediator.tongueModel.reset
		self.globalAcquirementInTongueModel          = mediator.tongueModel.globalAcquirement
		self.stepLongClickVariantInTongueModel       = mediator.tongueModel.stepLongClickVariant
		self.setCommandStateInTongueModel            = mediator.tongueModel.setCommandState
		self.setFavoritesStrategyNameInTongueModel   = mediator.tongueModel.setFavoritesStrategyName
		self.getTargetViewInToothManager             = mediator.toothManager.getTargetView
		self.getTargetInToothManager                 = mediator.toothManager.getTarget
		self.teethView                               = [ToothView(i,self) for i in range(self.nbTeeth)]
		self.tongueView                              = TongueView(self)
		self.checkMilestoneStrategy(False)
		self.checkVerticalBarrierStrategy(False)
		self.checkHorizontalBarrierStrategy(False)
		self.checkInternalBarrierStrategy(False)
		self.currentInternalBarrier = None
	
	def registerAutoClickTimer(self, autoClickTimer):
		self.autoClickTimer = autoClickTimer
		self.tongueView.registerAutoClickTimer(autoClickTimer)
		for tooth in self.teethView:
			tooth.registerAutoClickTimer(autoClickTimer)
	
	def registerHighlightTimer(self, highlightTimer):
		for tooth in self.teethView:
			tooth.registerHighlightTimer(highlightTimer)
	
	def registerClickTransformer(self, clickTransformer):
		self.clickTransformer = clickTransformer
		self.tongueView.registerClickTransformer(clickTransformer)
		for tooth in self.teethView:
			tooth.registerClickTransformer(clickTransformer)
	
	def registerScanner(self, scanner):
		self.scanner = scanner
	
	def setBarrierNumber(self,n):
		self.barrierBrush = QtGui.QBrush(QtGui.QPixmap("symbols/" + aspect.barrierPixmapFileName % n))
	
	def setTongueNbMaxChars(self,tongueNbMaxChars):
		self.tongueNbMaxChars = tongueNbMaxChars
	
	def checkUndoableStrategy(self,strategy):
		if strategy:
			self.mayStartAutoClickTimerDependingOnUndoableStrategy = self.autoClickTimer.start
		else:
			self.mayStartAutoClickTimerDependingOnUndoableStrategy = (lambda *args: None)
	
	def checkMouseTrackingStrategy(self, strategy):
		self.setMouseTracking(strategy)
	
	def mouseMoveEvent(self,event):
		self.mayStartAutoClickTimerDependingOnUndoableStrategy(self,event)
		self.mayCorrectPosition(event)
	
	def leaveEvent(self, _):
		self.autoClickTimer.stop()
	
	def mousePressEvent(self, event):
		self.clickTransformer.setCurrentClient(self)
		self.clickTransformer.mousePressSlot(event)
	
	def mouseReleaseEvent(self, event):
		self.clickTransformer.mouseReleaseSlot(event)
	
	def shortClickEvent(self):
		self.emit(SIGNAL("clickOnBackground"))
	
	def longClickStartEvent(self, event):
		self.emit(SIGNAL("longClickOnBackground"))
	
	def longClickStepEvent(self, event):
		pass
	
	def longClickStopEvent(self, event):
		pass
	
	def autoClickEvent(self, clickNumber):
		self.autoClickTimer.mayNeedExtraDelay(True)
		if clickNumber == 1:
			self.emit(SIGNAL("autoClickUndo"))
		else:
			self.emit(SIGNAL("autoClickRedo"))
	
	def stopAutoClickTimer(self):
		self.autoClickTimer.stop()
	
	def updateSize(self,width):
		floatCharWidth = float(width)/(self.tongueNbMaxChars+2)
		self.tongueView.updateJawsDependantGeometry(floatCharWidth)
		tongueHeight = self.tongueView.getHeight()
		self.possibleGapHeight = tongueHeight + 2 * aspect.tongueOuterVerticalMargin
		QtGui.QPixmapCache.remove("background")
		hRef = width / (self.nbTeeth/2 + aspect.toothOverlapRatio * 2 * (1+self.mayZeroVertical(self.barrierWidthFactor)))
		toothOverlap = int(hRef * aspect.toothOverlapRatio)
		rejectedHeight = int(hRef * aspect.rejectedHeightRatio)
		self.verticalBarrierWidth = self.mayZeroVertical(toothOverlap*self.barrierWidthFactor)
		horizontalBarrierHeight = self.mayZeroHorizontal(toothOverlap*self.barrierWidthFactor)
		smallVerticalBarrierHeight = self.mayZeroVertical(horizontalBarrierHeight - rejectedHeight)
		internalBarrierHeight = self.mayZeroInternal(toothOverlap)
		height = self.getGapHeight() + 2 * (hRef + 2 * toothOverlap + smallVerticalBarrierHeight)
		self.setFixedHeight(height)
		self.yTongue = (self.height()-tongueHeight)/2
		xLeftTooth = toothOverlap+self.verticalBarrierWidth
		gen = itertools.izip(self.teethView,distributeNumber(2*(width-2*xLeftTooth),self.nbTeeth))
		self.milestoneCircles = []
		self.milestoneRadius = aspect.milestoneRatio*toothOverlap
		(xRef,yRef) = (xLeftTooth,smallVerticalBarrierHeight)
		yMil = yRef+hRef+rejectedHeight/2+toothOverlap-self.milestoneRadius
		self.upperInternalBarrier = QtCore.QRect(max(internalBarrierHeight,self.verticalBarrierWidth),yMil+self.milestoneRadius-internalBarrierHeight/2,width-2*max(internalBarrierHeight,self.verticalBarrierWidth),internalBarrierHeight)
		diameter = 2*self.milestoneRadius
		for (toothView,wRef) in itertools.islice(gen,self.nbTeeth/2):
			toothView.updateRefGeometry(xRef,yRef,wRef,hRef,toothOverlap)
			xMil = xRef+wRef/2-self.milestoneRadius
			self.milestoneCircles.append(QtCore.QRect(xMil,yMil,diameter,diameter))
			xRef += wRef
		(xRef,yRef) = (xRef-wRef, height-hRef-smallVerticalBarrierHeight)
		yMil = yRef-rejectedHeight/2-toothOverlap-self.milestoneRadius
		self.lowerInternalBarrier = QtCore.QRect(max(internalBarrierHeight,self.verticalBarrierWidth),yMil+self.milestoneRadius-internalBarrierHeight/2,width-2*max(internalBarrierHeight,self.verticalBarrierWidth),internalBarrierHeight)
		for (toothView,wRef) in itertools.islice(gen,self.nbTeeth/2):
			toothView.updateRefGeometry(xRef,yRef,wRef,hRef,toothOverlap)
			xMil = xRef+wRef/2-self.milestoneRadius
			self.milestoneCircles.append(QtCore.QRect(xMil,yMil,diameter,diameter))
			xRef -= wRef
		openBarrierHeight = self.verticalOpenBarrier*(horizontalBarrierHeight+hRef+rejectedHeight+toothOverlap)
		self.leftVerticalBarrier = QtCore.QRect(0,openBarrierHeight,self.verticalBarrierWidth,height-openBarrierHeight)
		self.rightVerticalBarrier = QtCore.QRect(width-self.verticalBarrierWidth,0,width,height-openBarrierHeight)
		self.upperHorizontalBarrier = QtCore.QRect(self.verticalBarrierWidth,0,width-2*self.verticalBarrierWidth,horizontalBarrierHeight)
		self.lowerHorizontalBarrier = QtCore.QRect(self.verticalBarrierWidth,height-horizontalBarrierHeight,width-2*self.verticalBarrierWidth,height)
		self.updateInternalBarrier(self.getTargetInToothManager())
	
	def paintBackground(self, painter):
		gradient = QtGui.QLinearGradient(0, 0, 0, self.height())
		gradient.setColorAt(0.0, self.jawsBackgroundLightColor)
		self.setGradientGapDependingOnPredictionStrategy(gradient)
		gradient.setColorAt(1.0, self.jawsBackgroundDarkColor)
		painter.setPen(QtCore.Qt.NoPen)
		painter.fillRect(self.rect(),QtGui.QBrush(gradient))
	
	def checkMilestoneStrategy(self, strategy):
		def paintMilestonesWhenMilestoneStrategyIsOn(painter):
			painter.setPen(QtCore.Qt.NoPen)
			painter.setBrush(self.milestoneBrush)
			painter.setRenderHint(QtGui.QPainter.Antialiasing)
			for i in self.milestoneIndices:
				painter.drawEllipse(self.milestoneCircles[i])
		
		if strategy:
			self.paintMilestonesDependingOnMilestoneStrategy = paintMilestonesWhenMilestoneStrategyIsOn
		else:
			self.paintMilestonesDependingOnMilestoneStrategy = (lambda *args: None)
	
	def paintMilestones(self, painter):
		self.paintMilestonesDependingOnMilestoneStrategy(painter)
	
	def checkVerticalBarrierStrategy(self,strategy):
		def mayCorrectPosition(pos):
			if self.leftVerticalBarrier.contains(pos):
				return self.scanner.moveToNorthWestTooth
			elif self.rightVerticalBarrier.contains(pos):
				return self.scanner.moveToSouthEastTooth
		
		def paintVerticalBarrier(painter):
			painter.drawRect(self.leftVerticalBarrier)
			painter.drawRect(self.rightVerticalBarrier)
		
		if strategy:
			self.mayZeroDependingOnVerticalBarrierStrategy = lambda x: x
			self.mayCorrectPositionDependingOnVerticalBarrierStrategy = mayCorrectPosition
			self.paintVerticalBarrierDependingOnVerticalBarrierStrategy = paintVerticalBarrier
		else:
			self.mayZeroDependingOnVerticalBarrierStrategy = lambda x: 0
			self.mayCorrectPositionDependingOnVerticalBarrierStrategy = lambda *args: None
			self.paintVerticalBarrierDependingOnVerticalBarrierStrategy = lambda *args: None
	
	def mayZeroVertical(self, value):
		return self.mayZeroDependingOnVerticalBarrierStrategy(value)
	
	def mayCorrectLateralPosition(self,pos):
		return self.mayCorrectPositionDependingOnVerticalBarrierStrategy(pos)
	
	def paintVerticalBarrier(self,painter):
		self.paintVerticalBarrierDependingOnVerticalBarrierStrategy(painter)
	
	def checkHorizontalBarrierStrategy(self,strategy):
		def mayCorrectPosition(pos):
			if self.upperHorizontalBarrier.contains(pos) or self.lowerHorizontalBarrier.contains(pos):
				return self.scanner.moveToNextTooth
		
		def paintHorizontalBarrier(painter):
			painter.drawRect(self.upperHorizontalBarrier)
			painter.drawRect(self.lowerHorizontalBarrier)
		
		if strategy:
			self.mayZeroDependingOnHorizontalBarrierStrategy = lambda x: x
			self.mayCorrectPositionDependingOnHorizontalBarrierStrategy = mayCorrectPosition
			self.paintHorizontalBarrierDependingOnHorizontalBarrierStrategy = paintHorizontalBarrier
		else:
			self.mayZeroDependingOnHorizontalBarrierStrategy = lambda x: 0
			self.mayCorrectPositionDependingOnHorizontalBarrierStrategy = lambda *args: None
			self.paintHorizontalBarrierDependingOnHorizontalBarrierStrategy = lambda *args: None
	
	def mayZeroHorizontal(self, value):
		return self.mayZeroDependingOnHorizontalBarrierStrategy(value)
	
	def mayCorrectExternalPosition(self,pos):
		return self.mayCorrectPositionDependingOnHorizontalBarrierStrategy(pos)
	
	def paintHorizontalBarrier(self,painter):
		self.paintHorizontalBarrierDependingOnHorizontalBarrierStrategy(painter)
	
	def checkInternalBarrierStrategy(self,strategy):
		def mayCorrectPosition(pos):
			if self.currentInternalBarrier.contains(pos):
				return self.scanner.moveToChar
		
		def paintInternalBarrier(painter):
			painter.drawRect(self.currentInternalBarrier)
		
		if strategy:
			self.mayZeroDependingOnInternalBarrierStrategy = lambda x: x
			self.mayCorrectPositionDependingOnInternalBarrierStrategy = mayCorrectPosition
			self.paintInternalBarrierDependingOnInternalBarrierStrategy = paintInternalBarrier
			self.mayRefreshInternalBarrierDependingOnInternalBarrierStrategy = self.update
		else:
			self.mayZeroDependingOnInternalBarrierStrategy = lambda x: 0
			self.mayCorrectPositionDependingOnInternalBarrierStrategy = lambda *args: None
			self.paintInternalBarrierDependingOnInternalBarrierStrategy = lambda *args: None
			self.mayRefreshInternalBarrierDependingOnInternalBarrierStrategy = lambda : None
	
	def mayZeroInternal(self, value):
		return self.mayZeroDependingOnInternalBarrierStrategy(value)
	
	def mayCorrectInternalPosition(self,pos):
		return self.mayCorrectPositionDependingOnInternalBarrierStrategy(pos)
	
	def paintInternalBarrier(self,painter):
		self.paintInternalBarrierDependingOnInternalBarrierStrategy(painter)
	
	def mayRefreshInternalBarrier(self):
		self.mayRefreshInternalBarrierDependingOnInternalBarrierStrategy()
	
	def updateInternalBarrier(self,i):
		if i >= self.nbTeeth/2:
			if self.currentInternalBarrier != self.upperInternalBarrier:
				self.currentInternalBarrier = self.upperInternalBarrier
				self.mayRefreshInternalBarrier()
		elif self.currentInternalBarrier != self.lowerInternalBarrier:
				self.currentInternalBarrier = self.lowerInternalBarrier
				self.mayRefreshInternalBarrier()
	
	def setBarrierWidthFactor(self,barrierWidthFactor):
		self.barrierWidthFactor = barrierWidthFactor
	
	def checkVerticalOpenBarrierStrategy(self,strategy):
		self.verticalOpenBarrier = (1 if strategy else 0)
	
	def checkBarriersStrategy(self,strategy = True):
		def paintBarriers(painter):
			painter.setBrush(self.barrierBrush)
			painter.setPen(QtCore.Qt.NoPen)
			self.paintVerticalBarrier(painter)
			self.paintHorizontalBarrier(painter)
			self.paintInternalBarrier(painter)
		
		def mayCorrectPosition(event):
			pos = event.pos()
			for correction in (self.mayCorrectLateralPosition,self.mayCorrectExternalPosition,self.mayCorrectInternalPosition):
				doCorrectPosition = correction(pos)
				if doCorrectPosition:
					self.checkBarriersStrategy(False)
					doCorrectPosition()
					break
		
		if strategy:
			self.paintBarriersDependingOnBarriersStrategy = paintBarriers
			self.mayCorrectPositionDependingOnBarriersStrategy = mayCorrectPosition
		else:
			self.paintBarriersDependingOnBarriersStrategy = (lambda *args: None)
			self.mayCorrectPositionDependingOnBarriersStrategy = (lambda *args: None)
	
	def paintBarriers(self,painter):
		self.paintBarriersDependingOnBarriersStrategy(painter)
	
	def mayCorrectPosition(self,event):
		self.mayCorrectPositionDependingOnBarriersStrategy(event)
	
	def paintEvent(self, _):
		pixmap = QtGui.QPixmap(self.size())
		painter = QtGui.QPainter()
		painter.begin(self)
		retrieveOrGenerateRectangularPixmap("background",pixmap,self.paintBackground)
		painter.drawPixmap(0,0,pixmap)
		self.paintBarriers(painter)
		self.paintMilestones(painter)
		painter.end()
		self.emit(SIGNAL("jawsView painted"))
	
	def updateTip(self, glyph):
		self.emit(SIGNAL("glyphChanged"),glyph)
	
	def showFarPicto(self, *args):
		self.emit(SIGNAL("showFarPicto"), *args)
	
	def hideFarPicto(self):
		self.emit(SIGNAL("hideFarPicto"))
	
	def setMilestoneIndices(self, milestoneIndices):
		self.milestoneIndices = milestoneIndices
	
	# Tongue stuff
	
	def checkPredictionStrategy(self, strategy):
		def setGradientGapWhenPredictionStrategyIsOn(gradient):
			h = 0.5 * self.getGapHeight() / self.height()
			gradient.setColorAt(0.5+h, self.jawsBackgroundMediumColor)
			gradient.setColorAt(0.5-h, self.jawsBackgroundMediumColor)
		
		def setGradientGapWhenPredictionStrategyIsOff(gradient):
			pass
		
		def getGapHeightWhenPredictionStrategyIsOn():
			return self.possibleGapHeight
		
		def getGapHeightWhenPredictionStrategyIsOff():
			return 0
		
		if strategy:
			self.setGradientGapDependingOnPredictionStrategy = setGradientGapWhenPredictionStrategyIsOn
			self.getGapHeightDependingOnPredictionStrategy   = getGapHeightWhenPredictionStrategyIsOn
			self.tongueView.show()
		else:
			self.setGradientGapDependingOnPredictionStrategy = setGradientGapWhenPredictionStrategyIsOff
			self.getGapHeightDependingOnPredictionStrategy   = getGapHeightWhenPredictionStrategyIsOff
			self.tongueView.hide()
	
	def setGradientGap(self,gradient):
		self.setGradientGapDependingOnPredictionStrategy(gradient)
	
	def getGapHeight(self):
		return self.getGapHeightDependingOnPredictionStrategy()
	
	def updateTonguePosition(self):
		x = max(
		        aspect.tongueOuterHorizontalMargin,
		        min(
		            self.getTargetViewInToothManager().x(),
		            self.width()-self.verticalBarrierWidth-self.tongueView.width()
		       ) - aspect.tongueOuterHorizontalMargin
		    )
		self.tongueView.move(x, self.yTongue)
	
	def updateTongueHighlightment(self, bool):
		self.tongueView.updateHighlightment(bool)
	
	def highlightTongue(self):
		self.tongueView.updateHighlightment(True)
	
	def updateTongueContent(self):
		self.resetInTongueModel()
	

if __name__ == "__main__":
	from  chewing import main
	main()

