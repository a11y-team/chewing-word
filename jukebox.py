#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file jukebox.py

# built-in stuff
import itertools

from aspect import aspect # TODO: suppress that after testing

class Jukebox:
	
	def __init__(self,mediator):
		self.addFragmentInSelector                  = mediator.selector.addFragment
		self.getRangeInSelector                     = mediator.selector.getRange
		self.reshapeSelectionInSelector             = mediator.selector.reshapeSelection
		self.getNextExtremityInSelector             = mediator.selector.getNextExtremity
		self.getExtremityInSelector                 = mediator.selector.getExtremity
		self.getPossibleNextSongsInSelector         = mediator.selector.getPossibleNextSongs
		self.getTextInSelector                      = mediator.selector.getText
		self.resetInSelector                        = mediator.selector.reset
		self.autoSelectionInSelector                = mediator.selector.autoSelection
		self.BEG                                    = mediator.selector.BEG
		self.END                                    = mediator.selector.END
		self.FWD                                    = mediator.selector.FWD
		self.BWD                                    = mediator.selector.BWD
		self.SEN                                    = mediator.selector.SEN
		self.CHR                                    = mediator.selector.CHR
		self.updateSpeechActivityInEditorSetter     = mediator.editorSetter.updateSpeechActivity
		self.clearAllSpeechActivityInEditorSetter   = mediator.editorSetter.clearAllSpeechActivity
		self.silentlySetCursorAtInEditorSetter      = mediator.editorSetter.silentlySetCursorAt
		self.silentlySetCursorsAtInEditorSetter     = mediator.editorSetter.silentlySetCursorsAt
		self.silentlySetCursorBetweenInEditorSetter = mediator.editorSetter.silentlySetCursorBetween
		self.resetSpeechActivityInEditorSetter      = mediator.editorSetter.resetSpeechActivity
		self.updatePauseStateInEditorSetter         = mediator.editorSetter.updatePauseState
		self.getAllInEditorGetter                   = mediator.editorGetter.getAll
		self.getAnchorAndPositionInEditorGetter     = mediator.editorGetter.getAnchorAndPosition

	def register(self,commandSpeech):
		self.jukeboxIsExhaustedInCommandSpeech           = commandSpeech.jukeboxIsExhausted
		self.speakInCommandSpeech                        = commandSpeech.speak
		self.queueSongToPrepareInCommandSpeech           = commandSpeech.queueSongToPrepare
		self.setPauseStateInCommandSpeech                = commandSpeech.setPauseState
		self.checkManualSelectionStrategyInCommandSpeech = commandSpeech.checkManualSelectionStrategy
	
	def reset(self):
		self.resetInSelector(self.getAllInEditorGetter())
		(anc,pos) = self.getAnchorAndPositionInEditorGetter()
		self.stopWhenExhausted = (anc != pos)
		self.checkManualSelectionStrategyInCommandSpeech(anc != pos)
		(anc,pos) = self.autoSelectionInSelector(anc,pos)
		self.silentlySetCursorBetweenInEditorSetter(anc,pos)
		self.resetSpeechActivityInEditorSetter()
		(beg,end) = sorted([anc,pos])
		self.addFragmentInSelector(beg,end,self.SEN)
		self.songs = self.getRangeInSelector(beg,end,self.SEN)
		if self.songs:
			beg = self.getExtremityInSelector(self.SEN,self.BEG,self.songs[0])
			self.landingPosition = self.getNextExtremityInSelector(self.BEG,beg,self.FWD,self.SEN)
			self.queueSongToPrepareInCommandSpeech(self.getTextInSelector(self.SEN,self.songs[0]))
		else:
			self.landingPosition = beg
			self.stopWhenExhausted = True
	
	def step(self):
		self.songs = self.songs[1:]
		self.maySpeak()
	
	def maySpeak(self):
		if self.songs == []:
			self.updateActivities()
			(anc,pos) = self.autoSelectionInSelector(self.landingPosition,self.landingPosition)
			if self.stopWhenExhausted or min(anc,pos) < self.landingPosition:
				self.landingPosition = self.getNextExtremityInSelector(self.BEG,self.landingPosition,self.FWD,self.CHR)
				self.silentlySetCursorAtInEditorSetter(self.landingPosition)
				self.jukeboxIsExhaustedInCommandSpeech()
				return
			self.silentlySetCursorAtInEditorSetter(self.landingPosition)
			(beg,end) = sorted([anc,pos])
			self.songs = self.getRangeInSelector(beg,end,self.SEN)
			self.setPauseStateInCommandSpeech(True)
		self.updateActivities()
		beg = self.getExtremityInSelector(self.SEN,self.BEG,self.songs[0])
		self.landingPosition = self.getNextExtremityInSelector(self.BEG,beg,self.FWD,self.SEN)
		self.silentlySetCursorsAtInEditorSetter([self.landingPosition,beg])
		self.speakInCommandSpeech(self.getTextInSelector(self.SEN,self.songs[0]))
		for i in self.getPossibleNextSongsInSelector(self.songs):
			self.queueSongToPrepareInCommandSpeech(self.getTextInSelector(self.SEN,i))
	
	def stop(self):
		self.songs = []
		self.clearAllSpeechActivityInEditorSetter()
	
	def updateActivities(self):
		fragments = [(self.getExtremityInSelector(self.SEN,self.BEG,i),self.getExtremityInSelector(self.SEN,self.END,i)) for i in self.songs]
		self.updateSpeechActivityInEditorSetter(fragments)
	
	def updatePauseState(self, state):
		self.updatePauseStateInEditorSetter(state)
	
	def skip(self,direction,grain):
		beg = self.getExtremityInSelector(self.SEN,self.BEG,self.songs[0])
		beg = self.getNextExtremityInSelector(self.BEG,beg,direction,grain,self.SEN)
		end = self.getNextExtremityInSelector(self.END,beg,self.FWD,grain,self.SEN)
		if beg == end:
			beg = self.getNextExtremityInSelector(self.BEG,beg,1-direction,self.SEN)
		self.songs = self.getRangeInSelector(beg,end,self.SEN)
		self.maySpeak()
	
	def push(self,direction,grain):
		beg = self.getExtremityInSelector(self.SEN,self.BEG,self.songs[0])
		end = self.getNextExtremityInSelector(self.BEG,beg,self.FWD,self.SEN)
		if self.songs[0] < self.songs[-1] or (len(self.songs) == 1 and direction == self.FWD):
			pos = self.getExtremityInSelector(self.SEN,self.END,self.songs[-1])
			pos = self.getNextExtremityInSelector(self.END,pos,direction,grain)
			end = max(pos,end)
			self.songs = self.getRangeInSelector(beg,end,self.SEN)
			self.silentlySetCursorAtInEditorSetter(end)
		else:
			pos = self.getExtremityInSelector(self.SEN,self.BEG,self.songs[1-len(self.songs)]) # self.songs[2] if it exists, or self.songs[0] otherwise
			pos = self.getNextExtremityInSelector(self.BEG,pos,direction,grain)
			beg = min(beg,pos)
			self.songs = self.getRangeInSelector(beg,end,self.SEN)
			self.songs = [self.songs[-1]] + self.songs[:-1]
			self.silentlySetCursorAtInEditorSetter(beg)
		self.updateActivities()


if __name__ == "__main__":
	from  chewing import main
	main()

