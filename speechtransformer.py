#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file speechtransformer.py


# builtin stuff
import itertools
from threading import Thread
# home-made stuff
from aspect import aspect

class SpeechTransformer:
	
	def __init__(self, mediator):
		def setSpell2(s):
			def spell2(spellMe):
				l = []
				nbLetters = 0
				firstTime = True
				previous = None
				for c in spellMe:
					nbLetters += (1 if c.isalpha() else 0)
					if c == previous:
						l[-1] = self.spell2 % c
						firstTime = False
					else:
						l.append(c)
						firstTime = True
					previous = c
				if nbLetters > 1:
					return "%s: %s" % (aspect.spellSeparator.join(l),spellMe)
				return spellMe
			
			self.spell2 = s
			if s:
				self.spellDependingOnSpell2Strategy = spell2
			else:
				self.spellDependingOnSpell2Strategy = (lambda spellMe: spellMe)
		
		setSpell2(mediator.spell2)
		self.cache = {}
	
	def preProcess(self,text):
		if text not in self.cache:
			self.cache[text] = aspect.speechHeader + self.spell(text)
		return self.cache[text]
	
	def postProcess(self,text):
		self.cache.pop(text,None)
	
	# private
	
	def spell(self,spellMe):
		return self.spellDependingOnSpell2Strategy(spellMe)
	

if __name__ == "__main__":
	from  chewing import main
	main()
