#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file selector.py

# built-in stuff
import re
import itertools
import bisect


class Selector(object):
	
	(BWD,FWD) = (0,1)
	MIDDLE = 5
	(DOC,PAR,SEN,WRD,CHR) = range(MIDDLE)
	(BEG,END) = (0,1)
	
	def __init__(self, mediator):
		self.rexes = [re.compile(rex,re.VERBOSE) for rex in ["",mediator.parRex,mediator.sntRex,mediator.wrdRex]]
		self.textHash = None
	
	def reset(self,text):
		textHash = hash(text)
		if textHash != self.textHash:
			self.textHash = textHash
			self.text = text
			nbPos = len(text) + 1
			self.fragments = [([-1,nbPos],[-1,nbPos])]
			for grain in (self.PAR,self.SEN,self.WRD):
				l = zip(*[(-1,-1)] + [m.span(0) for m in self.rexes[grain].finditer(self.text)] + [(nbPos,nbPos)])
				self.fragments.append((list(l[0]),list(l[1])))
			self.bounds = [((c[self.BEG][1],c[self.END][-2]) if len(c[self.BEG]) > 2 else (0,len(text))) for c in self.fragments + [[[]]]]
	
	def adjustExtremity(self,grain,pos):
		return max(self.bounds[grain][self.BEG],min(self.bounds[grain][self.END],pos))
	
	def getNextExtremity(self,ext,pos,direction,grain,adjustAtGrain = True):
		if grain == self.CHR:
			pos = pos+2*direction-1
		else:
			pos = self.fragments[grain][ext][bisect.bisect(self.fragments[grain][ext],pos+direction-1)+direction-1]
		if adjustAtGrain is not False:
			if adjustAtGrain is True:
				adjustAtGrain = grain
			pos = max(self.bounds[adjustAtGrain][self.BEG],min(self.bounds[adjustAtGrain][self.END],pos))
		return pos
	
	def getExtremity(self,grain,ext,index):
		return self.fragments[grain][ext][index]
	
	def reshapeSelection(self,anc,pos,direction,grain):
		sgn = cmp(pos,anc)
		if sgn == 0:
			pos = self.getNextExtremity(direction,pos,direction,grain)
		else:
			pos = self.getNextExtremity((1+sgn)/2,pos,direction,grain)
			if direction == (1-sgn)/2 and cmp(anc,pos) == sgn:
				pos = anc
		return (anc,self.adjustExtremity(grain,pos))
	
	def autoSelection(self,anc,pos):
		if anc == pos:
			self.addFragmentDependingOnAutoSelection = (lambda *args: None)
			beg = self.getNextExtremity(self.END,pos,self.FWD,self.PAR)
			anc = self.adjustExtremity(self.PAR,anc)
			if anc == self.getNextExtremity(self.BEG,beg,self.BWD,self.PAR):
				pos = beg
			elif beg == anc < pos:
				anc = self.getNextExtremity(self.END,pos,self.BWD,self.PAR,False)
				anc = self.getNextExtremity(self.BEG,anc,self.BWD,self.PAR)
				pos = self.getNextExtremity(self.END,anc,self.FWD,self.PAR)
			else:
				pos = self.getNextExtremity(self.BEG,pos,self.FWD,self.CHR,False)
				pos = self.getNextExtremity(self.BEG,pos,self.BWD,self.SEN)
				anc = self.getNextExtremity(self.END,pos,self.FWD,self.SEN)
		else:
			self.addFragmentDependingOnAutoSelection = self.addFragmentWhenNoAutoSelection
		return (anc,pos)
	
	def addFragmentWhenNoAutoSelection(self,beg,end,grain):
		l = self.fragments[grain]
		bisect.insort(l[self.BEG],beg)
		bisect.insort(l[self.BEG],end)
		bisect.insort(l[self.END],beg)
		bisect.insort(l[self.END],end)
		l = zip(*[(i,j) for (i,j) in itertools.izip(*l) if i < j or i not in (beg,end)])
		self.fragments[grain] = (list(l[0]),list(l[1]))
	
	def addFragment(self,*args):
		self.addFragmentDependingOnAutoSelection(*args)
	
	def getPossibleNextSongs(self,songs):
		def develop(pos):
			for (direction,grain) in [(self.FWD,self.SEN),(self.FWD,self.PAR),(self.BWD,self.SEN),(self.BWD,self.PAR)]:
				beg = self.getNextExtremity(self.BEG,pos,direction,grain,False)
				if beg not in existingBegs and self.bounds[self.SEN][self.BEG] <= beg <= self.bounds[self.SEN][self.END]:
					existingBegs.add(beg)
					possibleBegs.append(beg)
		#
		existingBegs = set(self.fragments[self.SEN][self.BEG][index] for index in [songs[0]]+songs[1:2])
		possibleBegs = ([self.fragments[self.SEN][self.BEG][songs[1]]] if len(songs) > 1 else [])
		develop(self.fragments[self.SEN][self.BEG][songs[0]])
		for beg in possibleBegs[:]:
			develop(beg)
		return [bisect.bisect_left(self.fragments[self.SEN][self.BEG],beg) for beg in possibleBegs]
	
	def getText(self,grain,index):
		return self.text[self.fragments[grain][self.BEG][index]:self.fragments[grain][self.END][index]]
	
	def getRange(self,beg,end,grain):
		i = bisect.bisect_left(self.fragments[grain][self.BEG],beg)
		j = bisect.bisect_right(self.fragments[grain][self.END],end)
		return range(i,j)
	
	def getTextBetween(self,anc,pos):
		(beg,end) = sorted([anc,pos])
		return self.text[beg:end]
	
if __name__ == "__main__":
	from  chewing import main
	main()

