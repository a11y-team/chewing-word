#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file cachemanager.py

from PyQt4 import QtCore

import os
from aspect import aspect

class CacheManager:
	
	def __init__(self,name):
		self.directory = QtCore.QDir("%s/ChewingWord/%s" % (QtCore.QDir.tempPath(),name))
		self.absolutePath = self.directory.absolutePath()
	
	def getAbsolutePath(self):
		if not self.directory.exists():
			# print "creating cache",
			self.directory.mkpath(".")
		return self.absolutePath
	
	def purge(self):
		freshnessDate = QtCore.QDateTime.currentDateTime().addSecs(-aspect.cacheMinimalLongevityInSecs)
		for info in self.directory.entryInfoList(QtCore.QDir.Files):
			if info.lastRead() < freshnessDate and not (info.isSymLink() and info.exists()): #  QFileInfo::exists() returns true if the symlink points to an existing file.
				self.directory.remove(info.filePath())
			# 	print "removing file '%s'" % info.fileName()
			# else:
			# 	print "preserving file '%s'" % info.fileName()
	
	def removeAll(self):
		for info in self.directory.entryInfoList(QtCore.QDir.Files):
			self.directory.remove(info.filePath())
	
	def removeAllExceptLinks(self):
		for info in self.directory.entryInfoList(QtCore.QDir.Files):
			if not (info.isSymLink() and info.exists()): #  QFileInfo::exists() returns true if the symlink points to an existing file.
				self.directory.remove(info.filePath())
	
	def makeSymLinks(self,source,dests,extension):
		for dest in dests:
			QtCore.QFile.link(source+extension,os.path.join(unicode(self.absolutePath),dest+".lnk"))
	

if __name__ == "__main__":
	from  chewing import main
	main()

