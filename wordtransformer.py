#!/usr/bin/env python
# encoding: utf-8
#file wordtransformer.py


# builtin stuff
# home made stuff
# import Transcript.expander
# import Transcript.accentuator

class WordTransformer (object):
	
	def __init__(self, mediator):
		self.expander = mediator.expander
		self.accentuator = mediator.accentuator
	
	def updateWord(self, poorWord):
		self.variants = self.expander[poorWord] + self.accentuator[poorWord]
		self.index    = 0
	
	def getCurrentVariant(self):
		return self.variants[self.index]
	
	def getNumberOfVariants(self):
		return len(self.variants)
	
	def stepVariant(self, step = 1):
		self.index = (self.index + step) % len(self.variants)
	
	def getVariants(self):
		return self.variants
	
	def getVariantIndex(self):
		return self.index
