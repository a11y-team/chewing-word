     #!/usr/bin/env python
# -*- coding: utf_8 -*-
#file weightedforest.py

""" Documentation string.

Arguments:
- myArgument

Documentation text

Notes:
- random note.
"""
# built-in stuff
from itertools import count
# home made stuff
from chrono import chrono

# import behaviour

class Learner:
	
	def __init__(self, mediator):
		self.letMap = mediator.letMap
		self.wrdFor = mediator.wrdFor
		self.mayLearnInAccentuator = mediator.accentuator.mayLearn
		self.getWordCodeInWrdFor = mediator.wrdFor.getWordCode
	
	def registerPictos(self, pictos):
		self.updateAfterLearningInPictos = pictos.updateAfterLearning
	
	def mayLearn(self, richPrefix, occ = 1):
		for i in range(len(richPrefix)):
			c = self.letMap[richPrefix[i]]
			if c == " ":
				continue
			richWord = richPrefix[i:]
			poorWord = "".join([self.letMap[c] for c in richWord])
			firstSpaceIndex = poorWord.find(" ")
			if firstSpaceIndex != -1:
				w = poorWord[:firstSpaceIndex]
				previousCode = self.getWordCodeInWrdFor(w + " ")
				# print previousCode
				poorWordExists = self.add(w, occ)
				# print self.getWordCodeInWrdFor(w + " ")
				self.updateAfterLearningInPictos(previousCode)
				# print "Adding %s" % poorWord[:firstSpaceIndex]
				self.mayLearnInAccentuator(poorWord,richWord.lower(),poorWordExists)
			break
	
	def _getIndexWeightLettersChildren(self, n):
		""" Return the values and, if needed, add weights by side-effect. """
		if len(self.wrdFor[n][0])==2:
			weight = iter(range(len(self.wrdFor[n]),0,-1))
			self.wrdFor[n] = [(weight.next(),c,child) for (c,child) in self.wrdFor[n]]
		j = count()
		return [(j.next(),triplet) for triplet in self.wrdFor[n]]
	
	def _getLettersRanks(self, s):
		rank = count()
		return [(c,rank.next()) for c in s]
	
	def add(self, w, occ = 1):
		""" Add a complete word, and update the corresponding weights and order. """
		poorWordExists = True
		n = 0
		for (c,i) in self._getLettersRanks(w+" "):
			for (j,(weight,c2,n2)) in self._getIndexWeightLettersChildren(n):
				if c==c2:
					self.wrdFor[n][j] = (weight+self.adaptation(i,occ),c2,n2)
					break
			else: # w is a neologism
				poorWordExists = False
				if c == " ":
					self.wrdFor[n].append((self.adaptation(i,occ),c,""))
					self.wrdFor[n].sort(reverse=True)
				else:
					self.wrdFor[n].append((self.adaptation(i,occ),c,len(self.wrdFor)))
					self.wrdFor[n].sort(reverse=True)
					self._addNeologism(w,i+1,len(self.wrdFor),occ)
				break
			self.wrdFor[n].sort(reverse=True)
			n = n2
		return poorWordExists
	
	def _addNeologism(self, w, depth, size,occ):
		n = count(size+1)
		self.wrdFor.extend([[(self.adaptation(i,occ),w[i],n.next())] for i in range(depth,len(w))])
		self.wrdFor.append([(self.adaptation(len(w),occ)," ","")])
	
	def adaptation(self, depth, occ):
		return (depth+1)*occ
	


#
# Study version
#

class LearnerConstantAdaptation(Learner):
	def adaptation(self, depth, occ):
		return 1


#
# Study version
#

class LearnerNonAdaptative(Learner):
	def add(self, w, occ = 1):
		return




if __name__ == "__main__":
	from  chewing import main
	main()

