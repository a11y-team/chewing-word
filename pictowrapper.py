#!/usr/bin/env python
# encoding: utf-8
#file pictowrapper.py

# builtin stuff
# home made stuff


class PictoWrapper(object):
	
	def __init__(self, mediator):
		self.updateWordInWordTransformer          = mediator.wordTransformer.updateWord
		self.getCurrentVariantInWordTransformer   = mediator.wordTransformer.getCurrentVariant
		self.getNumberOfVariantsInWordTransformer = mediator.wordTransformer.getNumberOfVariants
		self.stepVariantInWordTransformer         = mediator.wordTransformer.stepVariant
		self.getVariantsInWordTransformer         = mediator.wordTransformer.getVariants
		self.getVariantIndexInWordTransformer     = mediator.wordTransformer.getVariantIndex
	
	def extractWordsAssociatedWithDifferentPictos(self,wordAndInfoDict):
		self.differentPictos = {}
		for (richWord,info) in wordAndInfoDict.iteritems():
			if len(info["file"])>1:
				self.differentPictos[richWord+" "] = info["file"]
		
	def checkPictoStrategy(self, strategy):
		def updateWordWhenPictoStrategyIsOn(poorWord):
			self.updateWordInWordTransformer(poorWord)
			self.variants = []
			for variant in self.getVariantsInWordTransformer():
				if variant in self.differentPictos:
					nbVariantsConsideringPictos = len(self.differentPictos[variant])
				else:
					nbVariantsConsideringPictos = 1
				self.variants.extend([variant] * nbVariantsConsideringPictos)
			self.index = 0
		
		def getCurrentVariantWhenPictoStrategyIsOn():
			return self.variants[self.index]
		
		def getNumberOfVariantsWhenPictoStrategyIsOn():
			return len(self.variants)
		
		def stepVariantWhenPictoStrategyIsOn(step = 1):
			currentVariant = self.variants[self.index]
			if currentVariant in self.differentPictos:
				l = self.differentPictos[currentVariant]
				l.append(l[0])
				del l[0]
			self.index = (self.index + step) % len(self.variants)
		
		def getVariantIndexWhenPictoStrategyIsOn():
			return self.index
		
		if strategy:
			self.updateWordDependingOnPictoStrategy          = updateWordWhenPictoStrategyIsOn
			self.getCurrentVariantDependingOnPictoStrategy   = getCurrentVariantWhenPictoStrategyIsOn
			self.getNumberOfVariantsDependingOnPictoStrategy = getNumberOfVariantsWhenPictoStrategyIsOn
			self.stepVariantDependingOnPictoStrategy         = stepVariantWhenPictoStrategyIsOn
			self.getVariantIndexDependingOnPictoStrategy     = getVariantIndexWhenPictoStrategyIsOn
		else:
			self.updateWordDependingOnPictoStrategy          = self.updateWordInWordTransformer
			self.getCurrentVariantDependingOnPictoStrategy   = self.getCurrentVariantInWordTransformer
			self.getNumberOfVariantsDependingOnPictoStrategy = self.getNumberOfVariantsInWordTransformer
			self.stepVariantDependingOnPictoStrategy         = self.stepVariantInWordTransformer
			self.getVariantIndexDependingOnPictoStrategy     = self.getVariantIndexInWordTransformer
	
	def updateWord(self,*args):
		self.updateWordDependingOnPictoStrategy(*args)
	
	def getCurrentVariant(self):
		return self.getCurrentVariantDependingOnPictoStrategy()
	
	def getNumberOfVariants(self):
		return self.getNumberOfVariantsDependingOnPictoStrategy()
	
	def stepVariant(self,*args):
		self.stepVariantDependingOnPictoStrategy(*args)
	
	def getVariantIndex(self):
		return self.getVariantIndexDependingOnPictoStrategy()

	
if __name__ == "__main__":
	from  chewing import main
	main()
