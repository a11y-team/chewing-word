#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file lettertransformer.py

from aspect import aspect

class LetterTransformer:
	
	def __init__(self):
		self.d = dict([(s[i],s[(i+1)%len(s)]) for s in aspect.letterTransformerSeq for i in range(len(s))])
	
	def getVariant(self,c):
		return self.d.get(c,c)
	
