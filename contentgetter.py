#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file contentgetter.py

# builtin stuff
import re
# home made stuff
from chunkgetter import ChunkGetter
from aspect import aspect
# import editorgetter
# import lettermapping
# import rawsentences

class ContentGetter:
	
	def __init__(self, mediator):
		self.getStartOfBlockInEditorGetter  = mediator.editorGetter.getStartOfBlock
		self.getPreviousBlockInEditorGetter = mediator.editorGetter.getPreviousBlock
		self.getSelectedTextInEditorGetter  = mediator.editorGetter.getSelectedText
		self.getEndOfTextInEditorGetter     = mediator.editorGetter.getEndOfText
		self.rawSntSep                      = re.compile("(?u)%s" % mediator.rawSntSep,re.VERBOSE)
		self.chunks                         = ChunkGetter(mediator.letMap)
		self.separatorFollowedByUppercase   = re.compile(u"((?<!\.\.)\.\s|(?<!\.\.)\.(?!\.)|\?\s|!\s|\n)")
		self.openers                        = dict((o,c) for (o,(c,_)) in aspect.delimitersDict.iteritems())
		self.closers                        = set(self.openers.values())
	
	def reset(self):
		self.startOfBlock = self.getStartOfBlockInEditorGetter()
		self.previousBlock = self.getPreviousBlockInEditorGetter()
		self.chunks.update(self.startOfBlock)
		self.completableString      = None
		self.prefix                 = None
		self.spaces                 = None
		self.richLastSpace          = None
		self.poorPrefix             = None
		self.selectedText           = None
	
	def getStartOfBlock(self):
		return self.startOfBlock
	
	def getPreviousBlock(self):
		return self.previousBlock
	
	# positions
	
	def atCommandPosition(self):
		return self.atDocumentStart() or self.getSelectedText() or self.getSpaces()!=""
	
	def atDocumentStart(self):
		return self.startOfBlock == u""
	
	def getSelectedText(self):
		if self.selectedText is None:
			self.selectedText = self.getSelectedTextInEditorGetter()
		return self.selectedText
	
	def checkAbbreviationStrategy(self,strategy):
		def getExpandedTextWhenAbbreviationStrategyIsOn():
			text = self.getEndOfTextInEditorGetter()
			if text.startswith("(="):
				nbDelimitersToClose = 1
				for i in range(2,len(text)):
					if text[i] == "(":
						nbDelimitersToClose += 1
					elif text[i] == ")":
						nbDelimitersToClose -= 1
						if nbDelimitersToClose == 0:
							return text[2:i]
			return u""
		
		def getExpandedTextWhenAbbreviationStrategyIsOff():
			return u""
		
		if strategy:
			self.getExpandedTextDependingOnAbbreviationStrategy = getExpandedTextWhenAbbreviationStrategyIsOn
		else:
			self.getExpandedTextDependingOnAbbreviationStrategy = getExpandedTextWhenAbbreviationStrategyIsOff
	
	def getExpandedText(self):
		return self.getExpandedTextDependingOnAbbreviationStrategy()
	
	def getPrefixes(self):
		if type(self.chunks[0]) == tuple:
			return self.chunks[0]
		return (u"","")
	
	def getRichPrefix(self):
		""" When the cursor follows a letter, return the last chunk of letters. """
		if self.prefix is None:
			if type(self.chunks[0]) == tuple:
				self.prefix = self.chunks[0][0]
			else:
				self.prefix = u""
		return self.prefix
	
	def getPoorPrefix(self):
		""" When the cursor follows a letter, return the last chunk of letters. """
		if self.poorPrefix is None:
			if type(self.chunks[0]) == tuple:
				self.poorPrefix = self.chunks[0][1]
			else:
				self.poorPrefix = ""
		return self.poorPrefix
	
	def getSpaces(self):
		if self.spaces is None:
			chunk = self.chunks[0]
			if type(chunk) == unicode:
				self.spaces = chunk
			else:
				self.spaces = u""
		return self.spaces
	
	def getLastTwoLettersBeforeEmptyPrefix(self):
		""" Used by contextual capitalization to continue uppercase. """
		if self.separatorFollowedByUppercase.search(self.chunks[0]) is None:
			if type(self.chunks[1]) == tuple:
				if self.separatorFollowedByUppercase.search(self.chunks[2]) is None:
					if type(self.chunks[3]) == tuple:
						return (self.chunks[3][0] + self.chunks[1][0])[-2:] # branch 1
					return self.chunks[1][0][-2:]                         # branch 2
				return self.chunks[1][0][-2:]                           # branch 3
			return u""                                                # branch 4
		return u""                                                  # branch 5
	
	def getLetterBeforePrefix(self):
		""" Used by contextual capitalization. """
		if self.separatorFollowedByUppercase.search(self.chunks[1]) is None:
			if type(self.chunks[2]) == tuple: 
				return self.chunks[2][0][-1:] # branch 1
			return u""                      # branch 2
		return u""                        # branch 3
	
	def getRichLastSpace(self):
		if self.richLastSpace is None:
			chunk = self.chunks[0]
			if type(chunk) == tuple:
				chunk = self.chunks[1]
			self.richLastSpace = chunk[-1:]
		return self.richLastSpace
	
	def getPoorLastSpace(self):
		if self.getRichLastSpace():
			return " "
		return ""
	
	def getCompletableString(self):
		if self.completableString is None:
			current = self.chunks[0]
			if type(current) == tuple:
				self.completableString = current[1]
			else:
				previous = self.chunks[1]
				if previous and len(self.rawSntSep.split(previous[0] + current)) == 1:
					self.completableString = previous[1] + " "
				else:
					self.completableString = ""
		return self.completableString
	
	def getOpener(self):
		stack = u""
		for c in self.startOfBlock[::-1]:
			if c in self.openers:
				if stack == "":
					return c
				if stack[-1] == self.openers[c]:
					stack = stack[:-1]
				else:
					return c
			elif c in self.closers:
				stack += c
	
	def getErasableContent(self):
		prefix = self.getRichPrefix()
		if prefix:
			return (self.getRichLastSpace(), prefix)
		if type(self.chunks[1]) == tuple and len(self.chunks[0]) == 1:
			return (self.chunks[2][-1:], self.chunks[1][0] + self.chunks[0])
		if self.getSpaces():
			return (self.getSpaces(),u"")
		return ("",u" ")
	
	


if __name__ == "__main__":
	from  chewing import main
	main()
