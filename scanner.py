#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file scanner.py

# built-in
from PyQt4 import QtCore, QtGui
import itertools

# home made stuff
from aspect import aspect
from fsm import Fsm
import qtgoodies


class Scanner(Fsm):
	
	def __init__(self, mediator):
		Fsm.__init__(self)
		self.getEraserViewInToothManager             = mediator.toothManager.getEraserView
		self.getSpaceViewInToothManager              = mediator.toothManager.getSpaceView
		self.getNextViewInToothManager               = mediator.toothManager.getNextView
		self.getDistantViewInToothManager            = mediator.toothManager.getDistantView
		self.getTargetViewInToothManager             = mediator.toothManager.getTargetView
		self.getNorthWestViewInToothManager          = mediator.toothManager.getNorthWestView
		self.getSouthEastViewInToothManager          = mediator.toothManager.getSouthEastView
		self.onLastToothInToothManager               = mediator.toothManager.onLastTooth
		self.onSpaceInToothManager                   = mediator.toothManager.onSpace
		self.onEraserInToothManager                  = mediator.toothManager.onEraser
		self.updateSpaceViewInToothManager           = mediator.toothManager.updateSpaceView
		self.checkCrossStrategyInToothManager        = mediator.toothManager.checkCrossStrategy
		self.checkPauseStrategyInToothManager        = mediator.toothManager.checkPauseStrategy
		self.onTransformableSpaceInToothManager      = mediator.toothManager.onTransformableSpace
		self.nextMilestoneInToothManager             = mediator.toothManager.nextMilestone
		self.showMilestonesInToothManager            = mediator.toothManager.showMilestones
		self.hideMilestonesInToothManager            = mediator.toothManager.hideMilestones
		self.onLetterInToothManager                  = mediator.toothManager.onLetter
		self.resetCurrentMilestoneInToothManager     = mediator.toothManager.resetCurrentMilestone
		self.getPoorPrefixInContentGetter            = mediator.contentGetter.getPoorPrefix
		self.getSpacesInContentGetter                = mediator.contentGetter.getSpaces
		self.getViewInTongueModel                    = mediator.tongueModel.getView
		self.getNextIndexInTongueModel               = mediator.tongueModel.getNextIndex
		self.stepVariantInTongueModel                = mediator.tongueModel.stepVariant
		self.getLastCharIndexInTongueModel           = mediator.tongueModel.getLastCharIndex
		self.onLastTrunkInTongueModel                = mediator.tongueModel.onLastTrunk
		self.onLastVariantInTongueModel              = mediator.tongueModel.onLastVariant
		self.getLastTrunkIndexInTongueModel          = mediator.tongueModel.getLastTrunkIndex
		self.onLastCharInTongueModel                 = mediator.tongueModel.onLastChar
		self.getMiddleCharIndexInTongueModel         = mediator.tongueModel.getMiddleCharIndex
		self.milestonesAreShown                      = True
	
	def registerTimer(self, scanTimer):
		self.scanTimer = scanTimer
		self.checkReplayStrategy(False)
		self.compile(aspect.scanners)
	
	def registerMouseMover(self,mouseMover):
		QtCore.QObject.connect(mouseMover,QtCore.SIGNAL("automatic move"),self.refreshTooth)
		QtCore.QObject.connect(mouseMover,QtCore.SIGNAL("automatic move"),self.checkBarriersStrategyInJawsView)
		self.moveHorizontalFirstInMouseMover = mouseMover.moveHorizontalFirst
		self.moveVerticalFirstInMouseMover = mouseMover.moveVerticalFirst
	
	def register(self,jawsView):
		self.updateTongueHighlightmentInJawsView  = jawsView.updateTongueHighlightment
		self.checkBarriersStrategyInJawsView = jawsView.checkBarriersStrategy
	
	def reset(self):
		Fsm.reset(self)
	
	def setPunctuationStrategy(self,strategy):
		self._isDynamic = (strategy == "punctuated")
		self._isStatic = (strategy == "normal")
	
	def setScanStartSound(self,sound):
		self.scanStartSound = qtgoodies.soundFactory("sounds/simple/%s.wav" % sound)
	
	def setScanStepSound(self,sound):
		self.scanStepSound = qtgoodies.soundFactory("sounds/simple/%s.wav" % sound)
	
	def setLongClickStartSound(self,sound):
		self.longClickStartSound = qtgoodies.soundFactory("sounds/simple/%s.wav" % sound)
	
	def setLongClickStepSound(self,sound):
		self.longClickStepSound = qtgoodies.soundFactory("sounds/simple/%s.wav" % sound)
	
	def checkScanStartSoundStrategy(self,strategy):
		if strategy:
			self.playScanStartDependingOnScanStartSoundStrategy = self.scanStartSound.play
		else:
			self.playScanStartDependingOnScanStartSoundStrategy = (lambda *args: None)
	
	def checkScanStepSoundStrategy(self,strategy):
		if strategy:
			self.playScanStepDependingOnScanStepSoundStrategy = self.scanStepSound.play
		else:
			self.playScanStepDependingOnScanStepSoundStrategy = (lambda *args: None)
	
	def checkLongClickStartSoundStrategy(self,strategy):
		if strategy:
			self.playLongClickStartDependingOnLongClickStartSoundStrategy = self.longClickStartSound.play
		else:
			self.playLongClickStartDependingOnLongClickStartSoundStrategy = (lambda *args: None)
	
	def checkLongClickStepSoundStrategy(self,strategy):
		if strategy:
			self.playLongClickStepDependingOnLongClickStepSoundStrategy = self.longClickStepSound.play
		else:
			self.playLongClickStepDependingOnLongClickStepSoundStrategy = (lambda *args: None)
	
	def playScanStart(self):
		self.playScanStartDependingOnScanStartSoundStrategy()
	
	def playScanStep(self):
		self.playScanStepDependingOnScanStepSoundStrategy()
	
	def playLongClickStart(self):
		self.playLongClickStartDependingOnLongClickStartSoundStrategy()
	
	def playLongClickStep(self):
		self.playLongClickStepDependingOnLongClickStepSoundStrategy()
	
	def muteSounds(self):
		self.checkScanStartSoundStrategy(False)
		self.checkScanStepSoundStrategy(False)
		self.checkLongClickStartSoundStrategy(False)
		self.checkLongClickStepSoundStrategy(False)
	
	def demuteSounds(self):
		self.checkScanStartSoundStrategy(self.scanStartSound != qtgoodies.noSound)
		self.checkScanStepSoundStrategy(self.scanStepSound != qtgoodies.noSound)
		self.checkLongClickStartSoundStrategy(self.longClickStartSound != qtgoodies.noSound)
		self.checkLongClickStepSoundStrategy(self.longClickStepSound != qtgoodies.noSound)
	
	def checkScanTraceStrategy(self,strategy):
		def transitionWhenScanTraceStrategyIsOn(eventString,info=None):
			self.scanTrace.append((self.chrono.elapsed(),eventString))
			self.chrono.start()
			self.transition(eventString,info)
		
		def transitionWhenScanTraceStrategyIsOff(eventString,info=None):
			self.transition(eventString,info)
		
		if strategy:
			self.chrono = QtCore.QTime()
			self.chrono.start()
			self.scanTrace = []
			self.transitionDependingOnScanTraceStrategy = transitionWhenScanTraceStrategyIsOn
		else:
			self.transitionDependingOnScanTraceStrategy = transitionWhenScanTraceStrategyIsOff
	
	def getScanTrace(self):
		return self.scanTrace
	
	def setSavedScanTrace(self, savedScanTrace):
		self.savedScanTrace = savedScanTrace
	
	def checkReplayStrategy(self,strategy):
		def startScanTimerWhenReplayStrategyIsOn(*args):
			self.playScanStart()
		
		def startScanTimerWhenReplayStrategyIsOff(slot):
			self.playScanStart()
			self.scanTimer.start(self.scanTimerStepSlot)
		
		if strategy:
			self.startScanTimerDependingOnReplayStrategy = startScanTimerWhenReplayStrategyIsOn
		else:
			self.startScanTimerDependingOnReplayStrategy = startScanTimerWhenReplayStrategyIsOff
	
	def checkDebugModeStrategy(self, strategy):
		self.debugModeDependingOnDebugModeStrategy = (lambda *args: strategy)
	
	
	
	# Slots
	
	def shortClickSlot(self, event):
		self.playScanStart()
		self.transitionDependingOnScanTraceStrategy("shortClick")
	
	def scanTimerStepSlot(self):
		self.playScanStep()
		self.transitionDependingOnScanTraceStrategy("scanTimerStep")
	
	def longClickStepSlot(self):
		self.transitionDependingOnScanTraceStrategy("longClickStep")
	
	def longClickStartSlot(self, event):
		self.transitionDependingOnScanTraceStrategy("longClickStart", event)
	
	def longClickStopSlot(self, event):
		self.transitionDependingOnScanTraceStrategy("longClickStop",event)
	
	def commandEndedSlot(self):
		self.transitionDependingOnScanTraceStrategy("commandEnded")
	
	
	# guards
	
	def noGuard(self, *args): return True
	def notOnLastTooth(self,*args): return not self.onLastToothInToothManager()
	def onLastTooth(self,*args): return self.onLastToothInToothManager()
	def onInitialLetter(self,*args): return self.onLetterInToothManager() and self.getPoorPrefixInContentGetter() == ""
	def onLetter(self,*args): return self.onLetterInToothManager()
	def onNewLineSpace(self,*args): return self.onSpaceInToothManager() and self.getSpacesInContentGetter()[-1:] == "\n"
	def notOnNewLineSpace(self,*args): return not self.onSpaceInToothManager() or self.getSpacesInContentGetter()[-1:] != "\n"
	def onNormalLetter(self,*args): return self.onLetterInToothManager() and self.getPoorPrefixInContentGetter() != ""
	def isDynamic(self,*args): return self._isDynamic
	def isStatic(self,*args): return self._isStatic
	def onEraser(self,*args): return self.onEraserInToothManager()
	def onSpace(self,*args): return self.onSpaceInToothManager()
	def notOnSpace(self,*args): return not self.onSpaceInToothManager()
	def onTransformableSpace(self,*args): return self.onTransformableSpaceInToothManager()
	def onNormalSpace(self,*args): return not self.onTransformableSpaceInToothManager()
	def onLastVariant(self,*args): return self.onLastVariantInTongueModel()
	def notOnLastVariant(self,*args): return not self.onLastVariantInTongueModel()
	def onLastTrunk(self,*args): return self.onLastTrunkInTongueModel()
	def notOnLastTrunk(self,*args): return not self.onLastTrunkInTongueModel()
	def onLastChar(self,*args): return self.onLastCharInTongueModel()
	def notOnLastChar(self,*args): return not self.onLastCharInTongueModel()
	def debugMode(self,*args): return self.debugModeDependingOnDebugModeStrategy()
	def notDebugMode(self,*args): return not self.debugModeDependingOnDebugModeStrategy()
	
	
	# actions
	
	def beginPunctScan(self):
		def moveToTrigger():
			view = self.getTargetViewInToothManager()
			while True:
				yield self.moveHorizontalFirstInMouseMover(view.getTriggerPosition("."))
				yield self.moveHorizontalFirstInMouseMover(view.getTriggerPosition(">"))
				yield self.moveVerticalFirstInMouseMover(view.getTriggerPosition("v"))
				yield self.moveHorizontalFirstInMouseMover(view.getTriggerPosition("<"))
				yield self.moveVerticalFirstInMouseMover(view.getTriggerPosition("^"))
				
		self.moveToTrigger = moveToTrigger()
		self.moveToTrigger.next()
		self.checkCrossStrategyInToothManager(False)
		self.updateSpaceViewInToothManager()
	
	def delegateLongClickStart(self):
		pos = QtGui.QCursor.pos()
		widget = QtGui.QApplication.widgetAt(pos)
		QtGui.QApplication.widgetAt(pos).longClickStartEvent(widget.mapFromGlobal(pos))
	
	def delegateLongClickStep(self):
		pos = QtGui.QCursor.pos()
		widget = QtGui.QApplication.widgetAt(pos)
		QtGui.QApplication.widgetAt(pos).longClickStepEvent(widget.mapFromGlobal(pos))
	
	def delegateLongClickStop(self):
		pos = QtGui.QCursor.pos()
		widget = QtGui.QApplication.widgetAt(pos)
		QtGui.QApplication.widgetAt(pos).longClickStopEvent(widget.mapFromGlobal(pos))
	
	def delegateShortClick(self):
		widget = QtGui.QApplication.widgetAt(QtGui.QCursor.pos())
		widget.shortClickEvent()
	
	def highlightReverse(self): self.getViewInTongueModel().highlightOneArrow(-self.currentStepInTongue)
	def highlightResume(self):  self.getViewInTongueModel().highlightOneArrow(self.currentStepInTongue)
	def lowlightArrows(self): self.getViewInTongueModel().lowlightArrows()
	def highlightVary(self): self.getViewInTongueModel().updateEndHighlightment(True)
	def lowlightVary(self): self.getViewInTongueModel().updateEndHighlightment(False)
	def highlightTarget(self): self.getTargetViewInToothManager().highlight()
	def highlightNextDistantTooth(self):
		self.getDistantViewInToothManager().lowlight()
		self.nextMilestoneInToothManager()
		self.getDistantViewInToothManager().highlight()
	
	def highlightFirstDistantTooth(self):
		self.resetCurrentMilestoneInToothManager()
		self.getDistantViewInToothManager().highlight()
	
	def highlightNextTooth(self): self.getNextViewInToothManager().highlight()
	def highlightPreviousTooth(self): self.getNextViewInToothManager(-1).highlight()
	def highlightPrevious2Tooth(self): self.getNextViewInToothManager(-2).highlight()
	def highlightNext2Tooth(self): self.getNextViewInToothManager(2).highlight()
	def highlightPrediction(self): self.updateTongueHighlightmentInJawsView(True)
	def lowlightTarget(self): self.getTargetViewInToothManager().lowlight()
	def lowlightNextTooth(self): self.getNextViewInToothManager().lowlight()
	def lowlightPreviousTooth(self): self.getNextViewInToothManager(-1).lowlight()
	def lowlightPrevious2Tooth(self): self.getNextViewInToothManager(-2).lowlight()
	def lowlightNext2Tooth(self): self.getNextViewInToothManager(2).lowlight()
	def lowlightPrediction(self): self.updateTongueHighlightmentInJawsView(False)
	def moveInPunct(self): self.moveToTrigger.next()
	def moveToDistantTooth(self): self.getDistantViewInToothManager().lowlight();self.moveHorizontalFirstInMouseMover(self.getDistantViewInToothManager().getScanPosition())
	def moveToSpace(self): self.moveVerticalFirstInMouseMover(self.getSpaceViewInToothManager().getScanPosition())
	def moveToNorthWestTooth(self): self.moveVerticalFirstInMouseMover(self.getNorthWestViewInToothManager().getScanPosition())
	def moveToSouthEastTooth(self): self.moveVerticalFirstInMouseMover(self.getSouthEastViewInToothManager().getScanPosition())
	def moveToNextTooth(self): self.moveHorizontalFirstInMouseMover(self.getNextViewInToothManager().getScanPosition())
	def moveToNext2Tooth(self): self.moveHorizontalFirstInMouseMover(self.getNextViewInToothManager(2).getScanPosition())
	def moveToPreviousTooth(self): self.moveVerticalFirstInMouseMover(self.getNextViewInToothManager(-1).getScanPosition())
	def moveToPrevious2Tooth(self): self.moveVerticalFirstInMouseMover(self.getNextViewInToothManager(-2).getScanPosition())
	def moveToTarget(self): self.moveVerticalFirstInMouseMover(self.getTargetViewInToothManager().getScanPosition())
	def returnToTarget(self): self.moveHorizontalFirstInMouseMover(self.getTargetViewInToothManager().getScanPosition())
	def moveToTongueLeft(self): self.moveVerticalFirstInMouseMover(self.getViewInTongueModel().getLeftScanPosition())
	def moveToTongueRight(self): self.moveVerticalFirstInMouseMover(self.getViewInTongueModel().getRightScanPosition())
	def moveToFirstChar(self): self.currentStepInTongue = 1; self.moveVerticalFirstInMouseMover(self.getViewInTongueModel().getCharScanPosition(0))
	def moveToLastChar(self): self.moveVerticalFirstInMouseMover(self.getViewInTongueModel().getCharScanPosition(self.getLastCharIndexInTongueModel()))
	def moveToNextChar(self): self.moveVerticalFirstInMouseMover(self.getViewInTongueModel().getCharScanPosition(self.getNextIndexInTongueModel(self.currentStepInTongue)))
	def moveToLeftChar(self): self.moveVerticalFirstInMouseMover(self.getViewInTongueModel().getCharScanPosition(self.getNextIndexInTongueModel(-1)))
	def moveToRightChar(self): self.moveVerticalFirstInMouseMover(self.getViewInTongueModel().getCharScanPosition(self.getNextIndexInTongueModel(1)))
	def moveToChar(self): self.moveVerticalFirstInMouseMover(self.getViewInTongueModel().getCharScanPosition(self.getNextIndexInTongueModel(0)))
	def moveToLastTrunk(self): self.moveVerticalFirstInMouseMover(self.getViewInTongueModel().getCharScanPosition(self.getLastTrunkIndexInTongueModel()))
	def moveToMiddleChar(self): self.moveVerticalFirstInMouseMover(self.getViewInTongueModel().getCharScanPosition(self.getMiddleCharIndexInTongueModel()))
	def reverse(self): self.currentStepInTongue = -self.currentStepInTongue
	def refreshTooth(self): self.transition("toothChanged")
	def stepVariant(self): self.stepVariantInTongueModel()
	def showCross(self): self.checkCrossStrategyInToothManager(True)
	def startScanTimer(self): self.startScanTimerDependingOnReplayStrategy(self.scanTimerStepSlot)
	def stopScanTimer(self): self.scanTimer.stop()
	def showPause(self):
		self.stopScanTimer()
		self.checkPauseStrategyInToothManager(True)
		self.updateSpaceViewInToothManager()
	
	def hidePause(self):
		self.checkPauseStrategyInToothManager(False)
		self.updateSpaceViewInToothManager()
	
	def showMilestones(self):
		self.showMilestonesInToothManager()
		self.milestonesAreShown = True
	
	def hideMilestones(self):
		if self.milestonesAreShown:
			self.hideMilestonesInToothManager()
			self.milestonesAreShown = False
	
	def replay(self):
		def fireTransition():
			print self.eventString
			self.transition(self.eventString)
			try:
				if self.eventString == "scanTimerStep":
					self.playScanStep()
				elif self.eventString == "longClickStart":
					self.playLongClickStart()
				elif self.eventString == "longClickStep":
					self.playLongClickStep()
				(elapsedTime,self.eventString) = self.currentReplayEvent.next()
				self.replayTimer.singleShot(elapsedTime*self.replayRatio,fireTransition)
			except StopIteration:
				self.checkReplayStrategy(False)
		
		self.replayTimer = QtCore.QTimer()
		self.checkReplayStrategy(True)
		self.currentReplayEvent = itertools.chain(self.savedScanTrace)
		(_,self.eventString) = self.currentReplayEvent.next()
		fireTransition()
	
	def setReplayRatio(self, replayRatio):
		self.replayRatio = replayRatio/100.0
	

if __name__ == "__main__":
	from fsm import graphviz
	graphviz(aspect.scanners)
	from  chewing import main
	main()
