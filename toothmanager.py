#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file toothmanager.py

# builtin stuff
import itertools
# home-made stuff
from aspect import aspect

class ToothManager:
	
	def __init__(self, mediator):
		self.replaceWithStringInAcquirer             = mediator.acquirer.replaceWithString
		self.eraseSomeInAcquirer                     = mediator.acquirer.eraseSome
		self.replaceWithSpacesInAcquirer             = mediator.acquirer.replaceWithSpaces
		self.replaceWithLetterInAcquirer             = mediator.acquirer.replaceWithLetter
		self.getGlyphInSpaceTransformer              = mediator.spaceTransformer.getGlyph
		self.getTransformedStringInSpaceTransformer  = mediator.spaceTransformer.getTransformedString
		self.hasTransformationInSpaceTransformer     = mediator.spaceTransformer.hasTransformation
		self.resetInPretenders                       = mediator.pretenders.reset
		self.getCharsInPretenders                    = mediator.pretenders.getChars
		self.getCasedPretenderInCaseMapper           = mediator.caseMapper.getCasedPretender
		self.getPoorLastSpaceInContentGetter         = mediator.contentGetter.getPoorLastSpace
		self.getPoorPrefixInContentGetter            = mediator.contentGetter.getPoorPrefix
		self.getSpacesInContentGetter                = mediator.contentGetter.getSpaces
		self.atCommandPositionInContentGetter        = mediator.contentGetter.atCommandPosition
		self.getCurrentInPictos                      = mediator.pictos.getCurrent
		self.getCurrentVariantInPictoWrapper         = mediator.pictoWrapper.getCurrentVariant
		self.getVariantIndexInPictoWrapper           = mediator.pictoWrapper.getVariantIndex
		self.resetInCommandSpeech                    = mediator.commandSpeech.reset
		self.startSpeakingInCommandSpeech            = mediator.commandSpeech.startSpeaking
		self.stopSpeakingInCommandSpeech             = mediator.commandSpeech.stopSpeaking
		self.updatePictoActivityInInstantSpeech      = mediator.instantSpeech.updatePictoActivity
		self.resetInCommandSelect                    = mediator.commandSelect.reset
		self.saveCursorInEditorSetter                = mediator.editorSetter.saveCursor
		self.silentlyRestoreCursorInEditorSetter     = mediator.editorSetter.silentlyRestoreCursor
		self.checkCommandIsAliveStrategy(False)
		self.updatePictoActivityInInstantSpeech(False)
		mediator.commandSpeech.registerToothManager(self)
		self.target = 0
	
	def register(self,jawsView):
		self.updateTonguePositionInJawsView        = jawsView.updateTonguePosition
		self.updateTongueContentInJawsView         = jawsView.updateTongueContent
		self.globalAcquirementInTongueModel        = jawsView.globalAcquirementInTongueModel
		self.stepLongClickVariantInTongueModel     = jawsView.stepLongClickVariantInTongueModel
		self.setCommandStateInTongueModel          = jawsView.setCommandStateInTongueModel
		self.resetInTongueModel                    = jawsView.resetInTongueModel
		self.setFavoritesStrategyNameInTongueModel = jawsView.setFavoritesStrategyNameInTongueModel
		self.updateTipInJawsView                   = jawsView.updateTip
		self.setMilestoneIndicesInJawsView         = jawsView.setMilestoneIndices
		self.checkMilestoneStrategyInJawsView      = jawsView.checkMilestoneStrategy
		self.updateInJawsView                      = jawsView.update
		self.highlightTongueInJawsView             = jawsView.highlightTongue
		self.showFarPictoInJawsView                = jawsView.showFarPicto
		self.hideFarPictoInJawsView                = jawsView.hideFarPicto
		self.updateInternalBarrierInJawsView       = jawsView.updateInternalBarrier
		for toothView in jawsView.teethView:
			toothView.registerManager(self)
		self.teethView                             = jawsView.teethView
		self.nbTeeth                               = len(self.teethView)
		self.states                                = [None] * self.nbTeeth
		self.contents                              = [None] * self.nbTeeth
	
	def registerScanner(self, scanner):
		self.scanner = scanner
	
	def checkScanStrategy(self,strategy):
		if strategy:
			self.commandEndedSlotInScannerDependingOnScannerMode = self.scanner.commandEndedSlot
		else:
			self.commandEndedSlotInScannerDependingOnScannerMode = (lambda *args: None)
	
	def commandEndedSlotInScanner(self):
		self.commandEndedSlotInScannerDependingOnScannerMode()
	
	def checkJumpOnPauseStrategy(self, strategy):
		if strategy:
			self.jumpOnPauseDependingOnJumpOnPauseStrategy = self.scanner.moveToMiddleChar
		else:
			self.jumpOnPauseDependingOnJumpOnPauseStrategy = (lambda: None)
	
	def mayJumpOnPause(self):
		self.jumpOnPauseDependingOnJumpOnPauseStrategy()
	
	def reset(self):
		(rejectedChars,pretenders) = self.getCharsInPretenders()
		contents = self.orderedContents(pretenders,rejectedChars)
		states = ["target"] + range(1,len(pretenders)) + ["rejected"] * (len(rejectedChars))
		i = self.target
		self.updateTipInJawsView(contents[0])
		self.firstLetterIndex = None
		self.checkAtCommandPositionStrategy(self.atCommandPosition())
		for (content,state) in itertools.izip(contents,states):
			tooth = self.teethView[i]
			tooth.updateAsFarFromTarget()
			if state == "target":
				self.setStateToTarget(i)
			elif state == "rejected":
				self.setStateToRejected(i)
			else:
				self.setStateToProgressive(i,state)
			if content == "eraser":
				self.eraserIndex = i
				self.updateStrategyAsEraser(tooth)
			elif content == " ":
				self.spaceView = self.teethView[i]
				self.updateStrategyAsSpace(tooth)
			else:
				self.updateStrategyAsLetter(tooth,content)
				if self.firstLetterIndex is None:
					self.firstLetterIndex = i
				content = self.getCasedPretenderInCaseMapper(content)
			self.contents[i] = content
			i = (i + 1) % self.nbTeeth
		self.updateAllPictos()
		self.teethView[(i - 1) % self.nbTeeth].updateAsBeforeTarget()
		self.teethView[(i + 1) % self.nbTeeth].updateAsAfterTarget()
		self.checkYinYangStrategy(False)
	
	
	# Contents stuff
	
	def setDynamicStrategy(self,strategy):
		def orderedContentsWhenDynamicStrategyIsOn(pretenders,rejectedChars):
			# return ['C', 'H', 'E', 'W', 'I', 'N', 'G', 'A', 'P', 'S', 'O', 'T', 'Q', 'N', 'U', 'V', 'B', 'R', 'F', 'H', 'G', ' ', 'D', 'R', 'O', 'W', ' ', 'eraser']
			return pretenders + rejectedChars
		
		def orderedContentsWhenDynamicStrategyIsOff(pretenders,_):
			return pretenders[self.target:] + pretenders[:self.target]
		
		if strategy == "dynamic":
			self.orderedContentsDependingOnDynamicStrategy = orderedContentsWhenDynamicStrategyIsOn
		else:
			self.orderedContentsDependingOnDynamicStrategy = orderedContentsWhenDynamicStrategyIsOff
	
	def orderedContents(self,pretenders,rejectedChars):
		return self.orderedContentsDependingOnDynamicStrategy(pretenders,rejectedChars)
	
	def checkPunctuationStrategy(self,strategy):
		def updateStrategyAsSpaceWhenPunctuationStrategyIsOn(tooth):
			if self.hasTransformationInSpaceTransformer():
				self.updateStrategyAsDynamicSpaceDependingOnCrossStrategy(tooth)
			else:
				tooth.updateStrategyAsStaticSpace()
		
		def updateStrategyAsSpaceWhenPunctuationStrategyIsOff(tooth):
			space = self.getSpacesInContentGetter()[-1:]
			if space == u" ":
				tooth.updateStrategyAsStaticFirstCarriageReturn()
			elif space == u"\n":
				tooth.updateStrategyAsStaticSecondCarriageReturn()
			else:
				tooth.updateStrategyAsStaticSpace()
		
		if strategy:
			self.updateStrategyAsSpaceDependingOnPunctuationStrategy = updateStrategyAsSpaceWhenPunctuationStrategyIsOn
			self.hasTransformationDependingOnPunctuationStrategy = self.hasTransformationInSpaceTransformer
		else:          
			self.updateStrategyAsSpaceDependingOnPunctuationStrategy = updateStrategyAsSpaceWhenPunctuationStrategyIsOff
			self.hasTransformationDependingOnPunctuationStrategy = (lambda *args: False)
	
	def checkCrossStrategy(self,strategy):
		def updateStrategyAsDynamicSpaceWhenCrossStrategyIsOn(tooth):
			tooth.updateStrategyAsCrossSpace()
		
		def updateStrategyAsDynamicSpaceWhenCrossStrategyIsOff(tooth):
			zoneTrigger = tooth.getZoneTrigger()
			glyph = self.getGlyphInSpaceTransformer(zoneTrigger)
			self.transformedString = self.getTransformedStringInSpaceTransformer(zoneTrigger)
			tooth.updateStrategyAsDynamicSpace(glyph)
		
		if strategy:
			self.updateStrategyAsDynamicSpaceDependingOnCrossStrategy = updateStrategyAsDynamicSpaceWhenCrossStrategyIsOn
		else:
			self.updateStrategyAsDynamicSpaceDependingOnCrossStrategy = updateStrategyAsDynamicSpaceWhenCrossStrategyIsOff
	
	def checkPauseStrategy(self,strategy):
		def updateStrategyAsSpaceWhenPauseIsOn(tooth):
			tooth.updateStrategyAsPausedSpace()
		
		def updateStrategyAsSpaceWhenPauseIsOff(tooth):
			self.updateStrategyAsSpaceDependingOnPunctuationStrategy(tooth)
		
		if strategy:
			self.updateStrategyAsSpaceDependingOnPauseStrategy = updateStrategyAsSpaceWhenPauseIsOn
		else:
			self.updateStrategyAsSpaceDependingOnPauseStrategy = updateStrategyAsSpaceWhenPauseIsOff
	
	def checkGlobalSpaceStrategy(self, strategy):
		def staticSpaceAcquirementWhenSpaceIsGlobal():
			self.globalAcquirementInTongueModel()
		
		def staticSpaceAcquirementWhenSpaceIsLocal():
			self.replaceWithSpacesInAcquirer(" ")
		
		if strategy:
			self.staticSpaceAcquirementDependingOnLocalSpaceStrategy = staticSpaceAcquirementWhenSpaceIsGlobal
		else:
			self.staticSpaceAcquirementDependingOnLocalSpaceStrategy = staticSpaceAcquirementWhenSpaceIsLocal
	
	def updateStrategyAsSpace(self,tooth):
		self.updateStrategyAsSpaceDependingOnPauseStrategy(tooth)
	
	def updateStrategyAsEraser(self,tooth):
		tooth.updateStrategyAsEraser()
	
	def updateStrategyAsLetter(self, tooth, letter):
		tooth.updateStrategyAsLetter(letter)
	
	def updateVariant(self):
		i = self.target
		tooth = self.teethView[i]
		content = self.contents[i].upper()
		tooth.leavePictoDependingOnPictoStrategy()
		self.updatePicto(tooth,i,content)
		tooth.update()
		tooth.enterPictoDependingOnPictoStrategy()
	
	
	# State stuff
	
	def checkProgressiveStrategy(self,strategy):
		def setStateToProgressiveWhenProgressiveStrategyIsOn(i,state):
			self.teethView[i].updateAsProgressive(state)
			self.states[i] = state
		
		def setStateToProgressiveWhenProgressiveStrategyIsOff(i,_):
			self.setStateToRejected(i)
		
		if strategy:
			self.setStateToProgressiveDependingOnProgressiveStrategy = setStateToProgressiveWhenProgressiveStrategyIsOn
		else:
			self.setStateToProgressiveDependingOnProgressiveStrategy = setStateToProgressiveWhenProgressiveStrategyIsOff
		for tooth in self.teethView:
			tooth.checkProgressiveStrategy(strategy)
	
	def setStateToProgressive(self,i,state):
		self.setStateToProgressiveDependingOnProgressiveStrategy(i,state)
	
	def setStateToRejected(self,i):
		self.teethView[i].updateAsRejected()
		self.states[i] = "rejected"
	
	def setStateToTarget(self,i):
		self.teethView[i].updateAsTarget()
		self.teethView[i].mayBeDynamicKeyEvent(self.hasTransformationInSpaceTransformer())
		self.states[i] = "target"
	
	def updateTargetAndStates(self, newTarget = None):
		force = (newTarget is None)
		if newTarget is None:
			newTarget = self.target
		state = self.states[newTarget]
		if force or state != "target":
			oldTarget = self.target
			self.teethView[(oldTarget - 1) % self.nbTeeth].updateAsFarFromTarget()
			self.teethView[(oldTarget + 1) % self.nbTeeth].updateAsFarFromTarget()
			self.teethView[(newTarget - 1) % self.nbTeeth].updateAsBeforeTarget()
			self.teethView[(newTarget + 1) % self.nbTeeth].updateAsAfterTarget()
			if state == "rejected":
				self.setStateToRejected(oldTarget)
				self.setStateToTarget(newTarget)
				self.target = newTarget
			else:
				self.setStateToTarget(newTarget)
				self.target = newTarget
				for _ in range(self.nbTeeth-1):
					newTarget = (newTarget + 1) % self.nbTeeth
					toothState = self.states[newTarget]
					if type(toothState) is int and toothState>state:
						self.setStateToProgressive(newTarget,toothState-state)
					else:
						self.setStateToRejected(newTarget)
			self.updateTongueContentInJawsView()
			self.updateTonguePositionInJawsView()
			self.updateInternalBarrierInJawsView(self.target)
			self.updateTipInJawsView(self.getTargetGlyph())
			self.updatePicto(self.teethView[oldTarget],oldTarget,self.contents[oldTarget].upper())
			self.updatePicto(self.teethView[self.target],self.target,self.contents[self.target].upper())
	
	
	# Picto stuff
	
	def checkPictoStrategy(self,strategy):
		def updateAllPictosWhenPictoStrategyIsOn():
			self.updateTongueContentInJawsView()
			for (i,tooth) in enumerate(self.teethView):
				updatePictoWhenPictoStrategyIsOn(tooth,i,self.contents[i])
		
		def updatePictoWhenPictoStrategyIsOn(tooth,i,content):
			pictoInfo = self.getCurrentInPictos(content.upper())
			if pictoInfo is None:
				self.updatePictoActivityInInstantSpeech(False)
				tooth.checkPictoHoveringStrategy(False)
				tooth.checkPictoPresenceStrategy(False)
				return
			currentVariant = self.getCurrentVariantInPictoWrapper()
			for info in reversed(pictoInfo):     # FIXME: these three lines are no more than a hack.
				if info["rich"] == currentVariant: # The right correction would consist in making the
					break                            # picto variants' list parallel to the variant list.
			tooth.setPicto(info)
			if not info["isDirect"] or (i == self.target and info["rich"] != currentVariant): # changed: suppressed comparison with .upper(): still works
				tooth.setPictoActivity(self.allVividPictosMode)
				self.updatePictoActivityInInstantSpeech(False)
				tooth.checkPictoHoveringStrategy(self.farIndirectPictosAreEnabled)
				tooth.checkPictoPresenceStrategy(self.indirectPictosAreEnabled)
			else:
				tooth.setPictoActivity(True)
				self.updatePictoActivityInInstantSpeech(True)
				tooth.checkPictoHoveringStrategy(self.farDirectPictosAreEnabled)
				tooth.checkPictoPresenceStrategy(self.directPictosAreEnabled)
		
		for tooth in self.teethView:
			tooth.checkPictoStrategy(strategy)
		if strategy:
			self.updatePictoDependingOnPictoStrategy = updatePictoWhenPictoStrategyIsOn
			self.updateAllPictosDependingOnPictoStrategy = updateAllPictosWhenPictoStrategyIsOn
		else:
			self.updatePictoActivityInInstantSpeech(False)
			self.updatePictoDependingOnPictoStrategy = (lambda *args: None)
			self.updateAllPictosDependingOnPictoStrategy = (lambda *args: None)
	
	def checkDirectPictoStrategy(self,strategy):
		self.directPictosAreEnabled = strategy
	
	def checkIndirectPictoStrategy(self,strategy):
		self.indirectPictosAreEnabled = strategy
	
	def checkFarDirectPictoStrategy(self,strategy):
		self.farDirectPictosAreEnabled = strategy
	
	def checkFarIndirectPictoStrategy(self,strategy):
		self.farIndirectPictosAreEnabled = strategy
	
	def checkAllVividPictosMode(self,mode):
		self.allVividPictosMode = mode
	
	def updatePicto(self,*args):
		self.updatePictoDependingOnPictoStrategy(*args)
	
	def updateAllPictos(self,*args):
		self.updateAllPictosDependingOnPictoStrategy(*args)
	
	def checkFarPictoStrategy(self,strategy):
		def enterPictoWhenFarPictoStrategyIsOn(*args):
			self.showFarPictoInJawsView(*args)
		
		def leavePictoWhenFarPictoStrategyIsOn():
			self.hideFarPictoInJawsView()
		
		if strategy:
			self.enterPictoDependingOnFarPictoStrategy = enterPictoWhenFarPictoStrategyIsOn
			self.leavePictoDependingOnFarPictoStrategy = leavePictoWhenFarPictoStrategyIsOn
		else:
			self.enterPictoDependingOnFarPictoStrategy = (lambda *args: None)
			self.leavePictoDependingOnFarPictoStrategy = (lambda *args: None)
	
	def enterPicto(self,*args):
		self.enterPictoDependingOnFarPictoStrategy(*args)
	
	def leavePicto(self,*args):
		self.leavePictoDependingOnFarPictoStrategy(*args)
	
	
	# getters
	
	def getTargetView(self):
		return self.teethView[self.target]
	
	def getTargetGlyph(self):
		return self.teethView[self.target].getGlyph()
	
	def getTargetContent(self):
		return self.contents[self.target]
	
	def getTarget(self):
		return self.target
	
	
	# scanner related
	
	def setScanJump(self, scanJump):
		self.scanJump = scanJump
	
	def getNextView(self,n=1):
		return self.teethView[(self.target+n) % self.nbTeeth]
	
	def getNorthWestView(self):
		return self.teethView[0]
	
	def getSouthEastView(self):
		return self.teethView[self.nbTeeth/2]
	
	def getDistantView(self):
		return self.teethView[self.currentMilestone]
	
	def getEraserView(self):
		return self.teethView[self.eraserIndex]
	
	def getSpaceView(self):
		return self.spaceView
	
	def updateSpaceView(self):
		self.updateStrategyAsSpace(self.spaceView)
		self.spaceView.update()
	
	def onLastTooth(self):
		return self.target == self.nbTeeth - 1
	
	def onEraser(self):
		return self.contents[self.target] == "eraser"
	
	def onLetter(self):
		return self.contents[self.target] not in (" ","eraser")
	
	def onSpace(self):
		return self.contents[self.target] == " "
	
	def onTransformableSpace(self):
		return self.hasTransformationDependingOnPunctuationStrategy()
	
	def showMilestones(self):
		self.milestoneIndices = [(self.firstLetterIndex + i) % self.nbTeeth for i in range(0,self.nbTeeth,self.scanJump)]
		self.setMilestoneIndicesInJawsView(self.milestoneIndices)
		self.checkMilestoneStrategyInJawsView(True)
		self.updateInJawsView()
	
	def hideMilestones(self):
		self.checkMilestoneStrategyInJawsView(False)
		self.updateInJawsView()
	
	def resetCurrentMilestone(self):
		milestoneIndices = [i for i in self.milestoneIndices if i > self.target + 1]
		if milestoneIndices:
			self.currentMilestone = min(milestoneIndices)
		else:
			self.currentMilestone = min(self.milestoneIndices)
	
	def nextMilestone(self):
		i = self.milestoneIndices.index(self.currentMilestone)
		self.currentMilestone = self.milestoneIndices[(i+1) % len(self.milestoneIndices)]
	
	
	# Acquirement
	
	def normalEraserAcquirement(self):
		s = self.getPoorLastSpaceInContentGetter() + self.getPoorPrefixInContentGetter()
		self.resetInPretenders(rejectionPoorPrefix = s[:-1], rejectedRichChar = s[-1:], first = ["eraser"])
		self.eraseSomeInAcquirer(1)
	
	def normalLetterAcquirement(self):
		self.resetInPretenders()
		self.replaceWithLetterInAcquirer(self.getTargetContent())
	
	def normalStaticSpaceAcquirement(self):
		self.resetInPretenders()
		self.staticSpaceAcquirementDependingOnLocalSpaceStrategy()
	
	def normalStaticFirstCarriageReturnAcquirement(self):
		self.resetInPretenders()
		self.replaceWithSpacesInAcquirer(u"\n")
	
	def normalStaticSecondCarriageReturnAcquirement(self):
		self.resetInPretenders()
		self.replaceWithSpacesInAcquirer(u"\n\n")
	
	def normalDynamicSpaceAcquirement(self):
		self.resetInPretenders(first = [" "])
		self.replaceWithSpacesInAcquirer(self.transformedString)
	
	def longClickStart(self):
		self.highlightTongueInJawsView()
	
	def longClickStartOnSpace(self):
		self.longClickStartOnSpaceDependingOnAtCommandPositionStrategy()
	
	def stepLongClickVariant(self):
		self.stepLongClickVariantInTongueModel()
	
	def globalAcquirement(self):
		self.globalAcquirementInTongueModel()
	
	
	# Commands related
	
	def checkCommandsStrategy(self,strategy):
		if strategy:
			self.atCommandPositionDependingOnCommandsStrategy = self.atCommandPositionInContentGetter
		else:
			self.atCommandPositionDependingOnCommandsStrategy = (lambda *args: False)
	
	def atCommandPosition(self):
		return self.atCommandPositionDependingOnCommandsStrategy()
	
	def checkCommandsByLongClickStrategy(self,strategy):
		if strategy:
			self.longClickStartOnSpaceAtCommandPositionDependingOnCommandsByLongClickStrategy = self.showCommand
		else:
			self.longClickStartOnSpaceAtCommandPositionDependingOnCommandsByLongClickStrategy = self.longClickStart
	
	def longClickStartOnSpaceAtCommandPositionStrategy(self):
		self.longClickStartOnSpaceAtCommandPositionDependingOnCommandsByLongClickStrategy()
	
	def checkAtCommandPositionStrategy(self, strategy):
		if strategy:
			self.longClickStartOnSpaceDependingOnAtCommandPositionStrategy = self.longClickStartOnSpaceAtCommandPositionDependingOnCommandsByLongClickStrategy
		else:
			self.longClickStartOnSpaceDependingOnAtCommandPositionStrategy = self.longClickStart
	
	def updateCommandIndex(self, commandIndex):
		self.silentlyRestoreCursorInEditorSetter()
		self.commandIndex = commandIndex
		self.currentCommand = self.commands[commandIndex]
		self.spaceView.updateStrategyAsPendingCommand(self.currentCommand["icon"])
		self.spaceView.update()
		self.currentCommand["show"]()
		self.setCommandStateInTongueModel(False if self.currentCommand["name"] == "cancel\x00" else "pending")
		self.resetInTongueModel()
	
	def checkCommandIsAliveStrategy(self,strategy):
		if strategy:
			self.mayKillCommandDependingOnCommandIsAliveStrategy = self.killCommand
		else:
			self.mayKillCommandDependingOnCommandIsAliveStrategy = (lambda *args: None)
	
	def mayKillCommand(self):
		self.mayKillCommandDependingOnCommandIsAliveStrategy()
	
	def setCommands(self,commandIdentifiers):
		d = { # all reserved commands identifiers end with chr(0), which has zero chances to occur in a file name
		  "speech\x00" : {
				"show": self.showSpeechCommand,
				"hide": self.hideSpeechCommand,
				"fire": self.fireSpeechCommand,
				"kill": self.killSpeechCommand,
				"icon": "symbols/speech.png",
				},
		  "select\x00": {
				"show": self.showSelectCommand,
				"hide": self.hideSelectCommand,
				"fire": self.fireSelectCommand,
				"kill": self.killSelectCommand,
				"icon": "symbols/select.png",
				},
			"cancel\x00": {
				"show": self.showCancelCommand,
				"hide": self.hideCancelCommand,
				"fire": self.fireCancelCommand,
				"kill": self.killCancelCommand,
				"icon": "",
			}
		}
		self.commands = []
		for identifier in commandIdentifiers:
			try:
				d[identifier]["message"] = aspect.commandMessages[identifier]
				d[identifier]["name"] = identifier
				self.commands.append(d[identifier])
			except KeyError:
				self.commands.append({
					"show": self.showScriptCommand,
					"hide": self.hideScriptCommand,
					"message": aspect.commandMessages["script\x00"] % identifier,
					"name": identifier,
					"fire": self.fireScriptCommand,
					"kill": self.killScriptCommand,
					"icon": "symbols/script.png",
				})
		self.commandIndex = 0
		self.currentCommand = self.commands[self.commandIndex]
		for tooth in self.teethView:
			tooth.setNumberOfActualCommands(len(self.commands)-1)
	
	def showCommand(self):
		self.saveCursorInEditorSetter()
		self.commandIndex = self.commandIndex % (len(self.commands) - 1)
		self.currentCommand = self.commands.pop(self.commandIndex)
		self.commands = [self.currentCommand] + self.commands
		self.updateCommandIndex(0)
	
	def stepCommand(self):
		self.currentCommand["hide"]()
		self.updateCommandIndex((self.commandIndex + 1) % len(self.commands))
	
	def fireCommand(self):
		self.setCommandStateInTongueModel("alive")
		self.checkCommandIsAliveStrategy(True)
		self.spaceView.updateStrategyAsAliveCommand(self.currentCommand["icon"])
		self.spaceView.update()
		self.setFavoritesStrategyNameInTongueModel(self.currentCommand["name"])
		self.checkYinYangStrategy(False)
		self.resetInTongueModel()
		self.currentCommand["fire"]()
	
	def killCommand(self):
		self.currentCommand["kill"]()
		self.cancelPendingCommand()
		self.commandEndedSlotInScanner()
		self.checkAtCommandPositionStrategy(self.atCommandPosition())
	
	def cancelPendingCommand(self):
		self.checkCommandIsAliveStrategy(False)
		self.setCommandStateInTongueModel(False)
		self.setFavoritesStrategyNameInTongueModel("default\x00")
		self.resetInTongueModel()
		self.updateStrategyAsSpace(self.spaceView)
		self.spaceView.update()
	
	def checkYinYangStrategy(self,strategy):
		self.spaceView.checkYinYangStrategy(strategy)
		if strategy:
			self.shortClickOnCommandDependingOnYinYangStrategy = self.fireCommand
			self.longClickStartOnCommandDependingOnYinYangStrategy = self.stepCommand
			self.longClickStepOnCommandDependingOnYinYangStrategy = self.stepCommand
			self.longClickStopOnCommandDependingOnYinYangStrategy = self.fireCommand
			self.showCommand()
		else:
			self.shortClickOnCommandDependingOnYinYangStrategy = self.killCommand
			self.longClickStartOnCommandDependingOnYinYangStrategy = self.showCommand
			self.longClickStepOnCommandDependingOnYinYangStrategy = self.stepCommand
			self.longClickStopOnCommandDependingOnYinYangStrategy = self.fireCommand
	
	def shortClickOnCommand(self):
		self.shortClickOnCommandDependingOnYinYangStrategy()
	
	def longClickStartOnCommand(self):
		self.longClickStartOnCommandDependingOnYinYangStrategy()
	
	def longClickStepOnCommand(self):
		self.longClickStepOnCommandDependingOnYinYangStrategy()
	
	def longClickStopOnCommand(self):
		self.longClickStopOnCommandDependingOnYinYangStrategy()
	
	def showSpeechCommand(self): self.resetInCommandSpeech()
	def hideSpeechCommand(self): pass
	def fireSpeechCommand(self): self.startSpeakingInCommandSpeech(); self.mayJumpOnPause()
	def killSpeechCommand(self): self.stopSpeakingInCommandSpeech()
	def showSelectCommand(self): self.resetInCommandSelect()
	def hideSelectCommand(self): pass
	def fireSelectCommand(self): pass
	def killSelectCommand(self): pass
	def showCancelCommand(self): pass
	def hideCancelCommand(self): pass
	def fireCancelCommand(self): self.killCommand()
	def killCancelCommand(self): pass
	def showScriptCommand(self): pass
	def hideScriptCommand(self): pass
	def fireScriptCommand(self): pass
	def killScriptCommand(self): pass
	

if __name__ == "__main__":
	from  chewing import main
	main()


