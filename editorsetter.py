#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file editorsetter.py

# import editorView

class EditorSetter :
	
	def register(self,editorView):
		self.editorView = editorView
	
	def replace(self,*args):
		self.editorView.replace(*args)
	
	def clear(self):
		self.editorView.clearWithoutClearingUndoHistory()
	
	def jumpToStartOfCursor(self, cursor):
		if cursor:
			self.editorView.jumpToStartOfCursor(cursor)
		else:
			self.editorView.jumpToEndOfDocument()
	
	def checkResizeCharactersStrategy(self, strategy):
		def updateChewingWidthAndFontSizeRatioWhenResizeCharactersStrategyIsOn(chewingWidth,fontSizeRatio):
			self.fontSize = chewingWidth * fontSizeRatio
			self.editorView.setFontSize(self.fontSize)
		
		def updateFontSizeWhenResizeCharactersStrategyIsOff(fontSize):
			self.fontSize = fontSize
			self.editorView.setFontSize(self.fontSize)
		
		if strategy:
			self.updateFontSizeDependingOnResizeCharactersStrategy = (lambda *args: None)
			self.updateChewingWidthAndFontSizeRatioDependingOnResizeCharactersStrategy = updateChewingWidthAndFontSizeRatioWhenResizeCharactersStrategyIsOn
		else:
			self.updateFontSizeDependingOnResizeCharactersStrategy = updateFontSizeWhenResizeCharactersStrategyIsOff
			self.updateChewingWidthAndFontSizeRatioDependingOnResizeCharactersStrategy = (lambda *args: None)
	
	def getFontSize(self):
		return self.fontSize
	
	def updateChewingWidthAndFontSizeRatio(self,chewingWidth,fontSizeRatio):
		self.updateChewingWidthAndFontSizeRatioDependingOnResizeCharactersStrategy(chewingWidth,fontSizeRatio)
	
	def updateFontSize(self,fontSize):
		self.updateFontSizeDependingOnResizeCharactersStrategy(fontSize)
	
	def setFontFamily(self, fontFamily):
		self.editorView.setFontFamily(fontFamily)
	
	def resetSpeechActivity(self):
		self.previousFragments = []
	
	def updateSpeechActivity(self,fragments):
		activity = True
		for (i,(anc,pos)) in enumerate(fragments):
			self.editorView.updateSpeechActivity(anc,pos,activity,self.isPaused,i)
			activity = False
		for (anc,pos) in set(self.previousFragments) - set(fragments):
			self.editorView.clearSpeechActivity(anc,pos)
		self.previousFragments = fragments
	
	def clearAllSpeechActivity(self):
		self.editorView.clearAllSpeechActivity()
	
	def updatePauseState(self,isPaused):
		self.isPaused = isPaused
		self.updateSpeechActivity(self.previousFragments)
	
	def setTabSize(self,tabSize):
		self.editorView.setTabSize(tabSize)
	
	def saveCursor(self):
		self.savedCursor = self.editorView.textCursor()
	
	def silentlyRestoreCursor(self):
		self.editorView.silentlySetCursor(self.savedCursor)
	
	def silentlySetCursorBetween(self,anc,pos):
		self.editorView.silentlySetCursorBetween(anc,pos)
	
	def silentlySetCursorAt(self,pos):
		self.editorView.silentlySetCursorAt(pos)
	
	def silentlySetCursorsAt(self,positions):
		self.editorView.silentlySetCursorsAt(positions)

if __name__ == "__main__":
	from  chewing import main
	main()

