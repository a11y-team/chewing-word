#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file snipper.py

# builtin stuff
import itertools


class Snipper(object):
	"""Ensure current string fits into a given maximal length. """
	
	def setMaxLength(self,maxLength):
		self.maxlength = maxLength
		self.n2 = self.maxlength/2
		self.n1 = self.maxlength - self.n2 - 1
	
	def reset(self,s):
		def snipSequenceWhenSnippable(seq):
			return seq[:self.i+1] + seq[self.j:]
		
		def snipStringWhenSnippable(seq):
			return seq[:self.i] + chr(0) + seq[self.j:]
		
		def subSnipIndexWhenSnippable(index):
			if index < self.i:
				return index
			if index == self.i:
				return self.i - 1
			return index - self.i + self.j - 1
		
		def addSnipIndexWhenSnippable(index):
			if index <= self.i:
				return index
			if index <= self.j:
				return self.i + 1
			return index - self.j + self.i + 1
		
		if len(s) <= self.maxlength:
			self.length                 = len(s)
			self.snipSequenceDependingOnSnippability = (lambda x:x)
			self.snipStringDependingOnSnippability   = (lambda x:x)
			self.subSnipIndexDependingOnSnippability = (lambda x:x)
			self.addSnipIndexDependingOnSnippability = (lambda x:x)
			return None
		self.i = self.n1
		self.j = len(s) - self.n2
		self.length                 = len(s) - self.j + self.i + 1
		self.snipSequenceDependingOnSnippability = snipSequenceWhenSnippable
		self.snipStringDependingOnSnippability   = snipStringWhenSnippable
		self.subSnipIndexDependingOnSnippability = subSnipIndexWhenSnippable
		self.addSnipIndexDependingOnSnippability = addSnipIndexWhenSnippable
		return self.i
	
	def getLength(self):
		return self.length
	
	def snipSequence(self,seq):
		return self.snipSequenceDependingOnSnippability(seq)
	
	def snipString(self,s):
		return self.snipStringDependingOnSnippability(s)
	
	def subSnipIndex(self,index):
		return self.subSnipIndexDependingOnSnippability(index)
	
	def addSnipIndex(self,index):
		return self.addSnipIndexDependingOnSnippability(index)
	
	
	
if __name__ == "__main__":
	from  chewing import main
	main()

