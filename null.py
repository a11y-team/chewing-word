#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file null.py


class Null:
	def __call__(self, *args, **kwargs): return None
	def __getattr__(self, name): return self
