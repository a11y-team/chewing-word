#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file chewing.py

""" Qt transcription session. """

# import os
# os.environ['PYCHECKER'] = '--stdlib off --limit 100 -E on --unreachable on --selfused on'
# import pychecker.checker

# builtin stuff
from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import SIGNAL
import sys
# home made stuff
from aspect import aspect

#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file chewing.py

""" Qt transcription session. """

# import os
# os.environ['PYCHECKER'] = '--stdlib off --limit 100 -E on --unreachable on --selfused on'
# import pychecker.checker

# builtin stuff
from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import SIGNAL
# home made stuff
from aspect import aspect

class SplashScreen(QtGui.QSplashScreen):
	
	def __init__(self,parent=None):
		QtGui.QSplashScreen.__init__(self,parent,QtGui.QPixmap("symbols/upvm.png"))
		text = self.trUtf8("Version %1 r%2").arg(aspect.version).arg(aspect.revision)
		label = QtGui.QLabel(text)
		label.setOpenExternalLinks(True)
		label.connect(self,SIGNAL("linkActivated(str)"),self.onLinkActivated)
		layout = QtGui.QVBoxLayout(self)
		layout.setMargin(20)
		layout.addWidget(label)
		layout.addStretch()
		self.setLayout(layout)
	
	def onLinkActivated(self,url):
		QtGui.QDesktopServices.openUrl(QtCore.QUrl(url))
	

if __name__ == "__main__":
	app = QtGui.QApplication(sys.argv)
	mainWindow = SplashScreen()
	mainWindow.show()
	sys.exit(app.exec_())
	
