#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file weightedforest.py

""" Documentation string.

Arguments:
- myArgument

Documentation text

Notes:
- random note.
"""

# built-in stuff
from itertools import count
from array import array
# home made stuff
from chrono import chrono

# import behaviour

class WeightedForest(list):
	""" Treelike representation of a list of words, sorted by decreasing frequency. """
	
	def wordInitials(self):
		return [x[-2] for x in self[0]]
	
	def _getLetters(self, n):
		return [x[-2] for x in self[n]]
	
	def _getLettersChildren(self, n):
		return [(x[-2],x[-1]) for x in self[n]]
	
	def _getIndexOrPostfix(self,prefix):
		""" Return the index of the children of the given prefix, or its postfix. """
		n = 0
		for c in prefix: # look for c at the current level of the forest
			for (c2,n2) in self._getLettersChildren(n):
				if c==c2: # c found: go one level down
					n = n2
					break
			else: # c not found: prefix is a neologism
				raise NeologismError
		return n
	
	def lettersFollowing(self, prefix):
		""" Return the list of child-characters of a given nonempty prefix. """
		assert prefix != ""
		try:
			if prefix[-1] == " ":
				# prefix is a complete word, followed by a separator: return its postfix
				return [c for c in self._getIndexOrPostfix(prefix)]
			# normal case: the word is in the process of being spelled
			return self._getLetters(self._getIndexOrPostfix(prefix))
		except NeologismError:
			return []
	
	def _leftMostBranch(self,n,completion):
		if type(n) is str: # postfix
			return completion
		else:
			children = self._getLettersChildren(n)
			(c,n) = children[0]
			return self._leftMostBranch(n,completion+c)

	def getPoorPrediction(self, prefix, pretender):
		""" Return the leftmost branch descending from the given prefix. """
		try:
			if prefix[-1:] == " ":
				return self._leftMostBranch(self._getIndexOrPostfix(pretender),pretender)
			else:
				return prefix + self._leftMostBranch(self._getIndexOrPostfix(prefix+pretender),pretender)
		except NeologismError:
			return prefix + " "
	
	def getWordCode(self, w):
		code = array("B",[0]*len(w))
		n = 0
		for i in xrange(len(w)):
			child = 0
			for x in self[n]:
				if x[-2] == w[i]:
					code[i] = child
					n = x[-1]
					break
				child += 1
			else:
				code[i] = child
				break
		return code
	
	def updateWordCode(self, word, code, length):
		n = 0
		for i in xrange(length):
			child = 0
			for x in self[n]:
				if x[-2] == word[i]:
					code[i] = child
					n = x[-1]
					break
				child += 1
			else:
				raise NeologismError
	
class NeologismError(Exception):
	""" A given prefix is absent from the word forest. """



if __name__ == "__main__":
	from  chewing import main
	main()

