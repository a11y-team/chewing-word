#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file casemapper.py

# built-in stuff
import itertools

class CaseMapper (object):
	
	def __init__(self, mediator):
		self.getRichPrefixInContentGetter                      = mediator.contentGetter.getRichPrefix
		self.getLetterBeforePrefixInContentGetter              = mediator.contentGetter.getLetterBeforePrefix
		self.getLastTwoLettersBeforeEmptyPrefixInContentGetter = mediator.contentGetter.getLastTwoLettersBeforeEmptyPrefix
	
	def reset(self):
		richPrefix = self.getRichPrefixInContentGetter()
		self.followingCase = self._lower
		if len(richPrefix) > 1:
			if richPrefix.islower():
				self.case = self._lower
			elif richPrefix.istitle():
				self.case = self._capitalize
			elif richPrefix.isupper():
				self.case = self._upper
				self.followingCase = self._upper
			else:
				self.case = self._camelCase
				self._cases = [c.isupper() for c in richPrefix]
		elif len(richPrefix) == 1:
			if richPrefix.islower():
				self.case = self._lower
			elif not self.getLetterBeforePrefixInContentGetter().isupper():
				self.case = self._capitalize
			else:
				self.case = self._upper
				self.followingCase = self._upper
		else:
			lastTwoLetters = self.getLastTwoLettersBeforeEmptyPrefixInContentGetter()
			if len(lastTwoLetters) == 0:
				self.case = self._capitalize
				self.followingCase = self._upper
			elif len(lastTwoLetters) == 1:
				self.case = self._lower
			elif lastTwoLetters.isupper():
				self.case = self._upper
				self.followingCase = self._upper
			else:
				self.case = self._lower
	
	def checkContextCapsStrategy(self, strategy):
		def getCasedPredictionWhenContextCapsStrategyIsOn(prediction):
			if self.case != self._upper:
				l = prediction.split(None,1)
				if l:
					n = len(l[0])
					return self.case(prediction[:n]) + prediction[n:]
			return self.case(prediction)
		
		def getCasedPretenderWhenContextCapsStrategyIsOn(pretender):
			return self.followingCase(pretender)
		
		if strategy:
			self.getCasedPredictionDependingOnContextCapsStrategy = getCasedPredictionWhenContextCapsStrategyIsOn
			self.getCasedPretenderDependingOnContextCapsStrategy = getCasedPretenderWhenContextCapsStrategyIsOn
		else:
			self.getCasedPredictionDependingOnContextCapsStrategy = (lambda x:x.upper())
			self.getCasedPretenderDependingOnContextCapsStrategy = (lambda x:x.upper())
	
	def getCasedPrediction(self, prediction):
		return self.getCasedPredictionDependingOnContextCapsStrategy(prediction)
	
	def getCasedPretender(self, pretender):
		return self.getCasedPretenderDependingOnContextCapsStrategy(pretender)
	
	def _lower(self,string):
		return string.lower()
	
	def _upper(self,string):
		return string.upper()
	
	def _capitalize(self,string):
		return string.capitalize()
	
	def _camelCase(self,string):
		def gen():
			for upper in self._cases:
				if upper:
					yield self._upper
				else:
					yield self._lower
			while True:
				yield self._lower
		return "".join([f(c) for (c,f) in itertools.izip(string,gen())])
	


if __name__ == "__main__":
	from  chewing import main
	main()
