#!/usr/bin/env python
# encoding: utf-8
#file pictos.py

# built-in stuff
import bisect
# home made stuff
from aspect import aspect

CODE = 0
INFO = 1

def optimizeFileLists(infos):
	""" Make infos[n1]["file"] == infos[n2]["file"] => infos[n1]["file"] is infos[n2]["file"] """
	d = {}
	for info in infos:
		filesKey = tuple(info["file"]) # only non mutable types can serve as dictionary keys
		if filesKey in d:
			info["file"] = d[filesKey]
		else:
			d[filesKey] = info["file"]
	del d

def parseName(s):
	d = {}
	if "=" in s:
		(s,newName) = s.split("=",1)
		d["rename"] = newName.strip()
	d["items"] = []
	for s in s.split("+"):
		l = [s.strip() for s in s.strip().split(",") if s.strip()]
		if len(l) == 1:
			d["items"].append({"1": l[0]})
		elif len(l) == 2:
			(s1,s2) = l
			if "%s" in s1:
				d["items"].append({"base": s1, "model": s2})
			else:
				d["items"].append({"1": s1, "n": s2})
	return d


class Pictos():
	
	def __init__(self, mediator):
		self.getCompletableStringInContentGetter                    = mediator.contentGetter.getCompletableString
		self.getWordCodeInWrdFor                                    = mediator.wrdFor.getWordCode
		self.updateWordCodeInWrdFor                                 = mediator.wrdFor.updateWordCode
		self.utfAlf                                                 = mediator.utfAlf
		self.letMap                                                 = mediator.letMap
		self.addInLearner                                           = mediator.learner.add
		self.mayLearnInAccentuator                                  = mediator.accentuator.mayLearn
		self.isContainedInExpander                                  = mediator.expander.isContained
		self.extractWordsAssociatedWithDifferentPictosInPictoWrapper = mediator.pictoWrapper.extractWordsAssociatedWithDifferentPictos
		mediator.learner.registerPictos(self)
	
	def checkFlexionModeStrategy(self, strategy):
		def normalizeWhenFlexionModeStrategyIsOn(item):
			try:
				flexions = self.flexionDict[item["model"]]
				if type(flexions) is list:
					for s in flexions:
						item["1"] = item["base"] % s
						yield item
				else:
					for s in flexions["1"]:
						item["1"] = item["base"] % s
						yield item
					for s in flexions["n"]:
						item["n"] = item["base"] % s
						yield item
			except KeyError:
				yield item
		
		def normalizeWhenFlexionModeStrategyIsOff(item):
			try:
				flexions = self.flexionDict[item["model"]]
				if type(flexions) is list:
					item["1"] = item["base"] % flexions[0]
				else:
					item["1"] = item["base"] % flexions["1"][0]
				if "n" in item:
					del item["n"]
				yield item
			except KeyError:
				yield item
		
		if strategy:
			self.normalize = normalizeWhenFlexionModeStrategyIsOn
		else:
			self.normalize = normalizeWhenFlexionModeStrategyIsOff
	
	def setFlexionDictionary(self, flexionDict):
		self.flexionDict = flexionDict
	
	def setPictoDictionary(self, pictoDicts):
		def associateToFiles(word, isMultiple):
			if word in wordAndInfoDict:
				currentFiles = wordAndInfoDict[word]["file"]
				if item["file"] not in currentFiles:
					currentFiles.append(item["file"])
			else:
				wordAndInfoDict[word] = {"file":[item["file"]],"isMultiple":isMultiple}
		
		wordAndInfoDict = {}
		for item in pictoDicts:
			for normalizedItem in self.normalize(item):
				if "1" in normalizedItem:
					associateToFiles(normalizedItem["1"], isMultiple = False)
				if "n" in normalizedItem:
					associateToFiles(normalizedItem["n"], isMultiple = True)
		optimizeFileLists(wordAndInfoDict.itervalues())
		self.extractWordsAssociatedWithDifferentPictosInPictoWrapper(wordAndInfoDict)
		richWordsHavingEmptyPoorForms = []
		for (richWord,info) in wordAndInfoDict.iteritems():
			info["rich"] = richWord + " "
			poorWord = "".join([self.letMap[c] for c in info["rich"]])
			poorWord = poorWord[:poorWord.find(" ")]
			if poorWord:
				info["poor"] = poorWord + " "
				poorWordExists = self.addInLearner(poorWord,0)
				if not self.isContainedInExpander(info["poor"],info["rich"]):
					self.mayLearnInAccentuator(info["poor"],info["rich"],poorWordExists)
			else:
				richWordsHavingEmptyPoorForms.append(richWord)
		for richWord in richWordsHavingEmptyPoorForms:
			del wordAndInfoDict[richWord]
		self.data = self.conservativeSort([(self.getWordCodeInWrdFor(info["poor"]),info) for info in wordAndInfoDict.itervalues()])
	
	def conservativeSort(self,couples):
		return [(code,info) for (code,_,info) in sorted([(code,i,info) for (i,(code,info)) in enumerate(couples)])]
	
	def reset(self):
		completableString = self.getCompletableStringInContentGetter()
		if completableString.endswith(" "):
			completableString = ""
		codePrefix = self.getWordCodeInWrdFor(completableString)
		self.current = self.getPictosStartingWithCodePrefix(codePrefix)
	
	def getPictosStartingWithCodePrefix(self,codePrefix):
		def isDirect(code,n):
			for i in range(n,len(code)):
				if code[i] != 0:
					return False
			return True
		
		n = len(codePrefix)
		infos = {}
		for i in range(bisect.bisect_left(self.data,(codePrefix,None)),len(self.data)):
			picto = self.data[i]
			if picto[CODE][:n] == codePrefix:
				c = picto[INFO]["poor"][n:n+1]
				infos[c] = infos.get(c,[]) + [dict(picto[INFO].items()+[("isDirect",isDirect(picto[CODE],n+1))])]
			else:
				break
		return infos
	
	def getCurrent(self, pretender):
		return self.current.get(pretender,None)
	
	def updateAfterLearning(self, newWordCode):
		def pictoCodesToBeUpdatedAfterLearning(newWordCode):
			d = {}
			lo = 0
			hi = len(self.data)
			for i in range(len(newWordCode)):
				d.update([(j,i+1) for j in range(lo,hi)])
				a = newWordCode[:i+1]
				lo = bisect.bisect_left(self.data,(a,None),lo,hi)
				a[i] += 1
				hi = bisect.bisect_left(self.data,(a,None),lo,hi)
			return d
		
		for (index,length) in pictoCodesToBeUpdatedAfterLearning(newWordCode).iteritems():
			self.updateWordCodeInWrdFor(self.data[index][INFO]["poor"],self.data[index][CODE],length)
		self.data = self.conservativeSort(self.data)
	
	

if __name__ == "__main__":
	from  chewing import main
	main()

