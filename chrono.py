#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file chrono.py

# built-in stuff
import time

class Timer:
	""" A simple chronometer to pass the time during execution. """
	def __init__(self):
		self.reset()
	def reset(self):
		self.start = time.clock()
	def printElapsed(self,s):
		# print "%03d: %s" % (time.clock() - self.start, s)
		print "%s: %s" % (time.clock() - self.start, s)

class NoTimer:
	""" Skip printing of elapsed times, e.g. in unit tests. """
	def printElapsed(self,s):
		pass

chrono = NoTimer()

