#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file editorgetter.py

class EditorGetter:
	
	def register(self,editorView):
		self.editorView = editorView
	
	def getStartOfBlock(self):
		return self.editorView.getStartOfBlock()
	
	def getPreviousBlock(self):
		return self.editorView.getPreviousBlock()
	
	def getEndOfText(self):
		return self.editorView.getEndOfText()
	
	def getSelectedText(self):
		return self.editorView.getSelectedText()
	
	def getAll(self):
		return self.editorView.getAll()
	
	def getAnchorAndPosition(self):
		return self.editorView.getAnchorAndPosition()
	
	def getPosition(self):
		return self.editorView.getPosition()

if __name__ == "__main__":
	from  chewing import main
	main()

