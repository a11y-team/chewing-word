#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file tonguemodel.py

# builtin stuff
import itertools

class TongueModel:

	
# Public
	
	def __init__(self,mediator):
		self.getPoorPredictionInWrdFor           = mediator.wrdFor.getPoorPrediction
		self.eraseSomeInAcquirer                 = mediator.acquirer.eraseSome
		self.eraseAllInAcquirer                  = mediator.acquirer.eraseAll
		self.replaceWithStringInAcquirer         = mediator.acquirer.replaceWithString
		self.replaceWithWordInAcquirer           = mediator.acquirer.replaceWithWord
		self.replaceWithLetterInAcquirer         = mediator.acquirer.replaceWithLetter
		self.resetInPretenders                   = mediator.pretenders.reset
		self.getRichLastSpaceInContentGetter     = mediator.contentGetter.getRichLastSpace
		self.getPoorLastSpaceInContentGetter     = mediator.contentGetter.getPoorLastSpace
		self.getRichPrefixInContentGetter        = mediator.contentGetter.getRichPrefix
		self.getPoorPrefixInContentGetter        = mediator.contentGetter.getPoorPrefix
		self.atCommandPositionInContentGetter    = mediator.contentGetter.atCommandPosition
		self.getErasableContentInContentGetter   = mediator.contentGetter.getErasableContent
		self.getCompletableStringInContentGetter = mediator.contentGetter.getCompletableString
		self.getStartOfBlockInContentGetter      = mediator.contentGetter.getStartOfBlock
		self.getPreviousBlockInContentGetter     = mediator.contentGetter.getPreviousBlock
		self.getSelectedTextInContentGetter      = mediator.contentGetter.getSelectedText
		self.updateWordInPictoWrapper            = mediator.pictoWrapper.updateWord
		self.getNumberOfVariantsInPictoWrapper   = mediator.pictoWrapper.getNumberOfVariants
		self.getCurrentVariantInPictoWrapper     = mediator.pictoWrapper.getCurrentVariant
		self.stepVariantInPictoWrapper           = mediator.pictoWrapper.stepVariant
		self.getVariantIndexInPictoWrapper       = mediator.pictoWrapper.getVariantIndex
		self.getNumberOfVariantsInFavoriteChars  = mediator.favoriteChars.getNumberOfVariants
		self.getCurrentVariantInFavoriteChars    = mediator.favoriteChars.getCurrentVariant
		self.stepVariantInFavoriteChars          = mediator.favoriteChars.stepVariant
		self.getVariantIndexInFavoriteChars      = mediator.favoriteChars.getVariantIndex
		self.getTargetContentInToothManager      = mediator.toothManager.getTargetContent
		self.updateVariantInToothManager         = mediator.toothManager.updateVariant
		self.checkYinYangStrategyInToothManager  = mediator.toothManager.checkYinYangStrategy
		self.cancelPendingCommandInToothManager  = mediator.toothManager.cancelPendingCommand
		self.fireCommandInToothManager           = mediator.toothManager.fireCommand
		self.stepCommandInToothManager           = mediator.toothManager.stepCommand
		self.getCasedPredictionInCaseMapper      = mediator.caseMapper.getCasedPrediction
		self.getVariantInLetterTransformer       = mediator.letterTransformer.getVariant
		self.setPrecisionInCalculator            = mediator.calculator.setPrecision
		self.getPrecisionInCalculator            = mediator.calculator.getPrecision
		self.getReplacementStringInCalculator    = mediator.calculator.getReplacementString
		self.getSelectedValueInCalculator        = mediator.calculator.getSelectedValue
		self.hasValueInCalculator                = mediator.calculator.hasValue
		self.snipSequenceInSnipper               = mediator.snipper.snipSequence
		self.snipStringInSnipper                 = mediator.snipper.snipString
		self.getLengthInSnipper                  = mediator.snipper.getLength
		self.subSnipIndexInSnipper               = mediator.snipper.subSnipIndex
		self.addSnipIndexInSnipper               = mediator.snipper.addSnipIndex
		self.resetInSnipper                      = mediator.snipper.reset
		self.callInGlyphTransformer              = mediator.glyphTransformer.call
		self.modifySelectionInCommandSelect      = mediator.commandSelect.modifySelection
		self.getAnchorStateInCommandSelect       = mediator.commandSelect.getAnchorState
		self.interactionInCommandSpeech          = mediator.commandSpeech.interaction
		self.checkSkipStrategyInCommandSpeech    = mediator.commandSpeech.checkSkipStrategy
		self.getVariantIndexInCommandSpeech      = mediator.commandSpeech.getVariantIndex
		self.getPauseStateInCommandSpeech        = mediator.commandSpeech.getPauseState
		self.speakInInstantSpeech                = mediator.instantSpeech.speak
		self.prepareStringInInstantSpeech        = mediator.instantSpeech.prepareString
		self.previousContentsString = None
		self.previousPretender = None
		self.correctionStrategies = []
		self.setFavoritesStrategyName("default\x00")
		self.setCommandState(False)
	
	def register(self,view):
		self.view = view
		view.register(self)
	
	def reset(self):
		self.currentIndex = 0
		pretender = self.getTargetContentInToothManager()
		if pretender == "eraser":
			if self.getSelectedTextInContentGetter():
				self.setSelectionDeletionStrategy()
			elif self.getStartOfBlockInContentGetter() == u"\n":
				self.setBlockDeletionStrategy()
			else:
				if self.previousPretender != "eraser":
					self.correctionStrategyIndex = 0
				self.setCorrectionStrategy()
		elif pretender == " ":
			if self.commandState == "pending":
				self.setPendingCommandStrategy()
			elif self.hasValueInCalculator():
				self.setCalculatorStrategy()
			elif self.atCommandPositionInContentGetter() or self.commandState == "alive":
				self.setFavoritesStrategy()
			else:
				self.setPredictionStrategy(" ")
		else:
			self.setPredictionStrategy(pretender)
		self.previousPretender = pretender
		self.resetSpecifics()
		self.delocalizeFlags()
		self.view.setNumberOfVariantsStrategy(self.numberOfVariants)
		self.view.setCurrentVariantIndex(self.getVariantIndexSpecifics())
		self.view.setSnipStrategy(self.resetInSnipper(self.contentsString))
		self.view.reset()
		self.view.stringMayHaveChangedEvent(self.previousContentsString != self.contentsString)
		self.previousContentsString = self.contentsString
	
	def getView(self):
		return self.view
	
	def checkCorrectionStrategy(self,modes,strategy=True):
		d = {
		  "correctCaps" : self.setCapitalizationCorrectionStrategy,
		  "correctAcnt" : self.setAccentuationCorrectionStrategy,
			"default"     : self.setDefaultCorrectionStrategy
		}
		for mode in modes + ["default"]:
			if d[mode] in self.correctionStrategies:
				self.correctionStrategies.remove(d[mode])
			if strategy or mode == "default":
				self.correctionStrategies.append(d[mode])
		if len(self.correctionStrategies) > 1:
			self.correctionStrategies.pop()
		self.correctionStrategyIndex = 0
	
	def checkFavoriteCharsEnabledStrategy(self,strategy):
		if strategy:
			self.maySetFavoriteCharsStrategyDependingOnEnabled = self.setFavoriteCharsStrategy
		else:
			self.maySetFavoriteCharsStrategyDependingOnEnabled = lambda : self.setPredictionStrategy(" ")
	
	def maySetFavoriteCharsStrategy(self):
		self.maySetFavoriteCharsStrategyDependingOnEnabled()
	
	def setFavoritesStrategyName(self,name):
		if name == "speech\x00":
			self.setFavoritesStrategy = self.setSpeechCommandStrategy
		elif name == "select\x00":
			self.setFavoritesStrategy = self.setSelectCommandStrategy
		elif name == "script\x00":
			self.setFavoritesStrategy = self.setScriptCommandStrategy
		else:
			self.setFavoritesStrategy = self.maySetFavoriteCharsStrategy
	
	def checkTruncatableStrategy(self,strategy):
		if strategy:
			self.updateIndexPredictionSpecifics = self.updateIndexPredictionTruncatableSpecifics
		else:
			self.updateIndexPredictionSpecifics = self.updateIndexPredictionNotTruncatableSpecifics
	
	def setClickStrategy(self,truncatableStrategy,leftBehaviour):
		if truncatableStrategy:
			self.updateIndexPredictionSpecifics = {
				"truncate" : self.updateIndexPredictionTruncateSpecifics,
				"delete"   : self.updateIndexPredictionDeleteSpecifics,
				"whole"    : self.updateIndexPredictionWholeSpecifics,
			}[leftBehaviour]
			self.getNextIndexPredictionSpecifics = {
				"truncate" : self.getNextIndexPredictionTruncateAndWholeSpecifics,
				"delete"   : self.getNextIndexPredictionDeleteSpecifics,
				"whole"    : self.getNextIndexPredictionTruncateAndWholeSpecifics,
			}[leftBehaviour]
			self.onLastTrunkPredictionSpecifics = {
				"truncate" : self.onLastTrunkPredictionTruncateAndWholeSpecifics,
				"delete"   : self.onLastTrunkPredictionDeleteSpecifics,
				"whole"    : self.onLastTrunkPredictionTruncateAndWholeSpecifics,
			}[leftBehaviour]
		else:
			self.updateIndexPredictionSpecifics = self.updateIndexPredictionNotTruncatableSpecifics
			self.getNextIndexPredictionSpecifics = self.getNextIndexPredictionNotTruncatableSpecifics
			self.onLastTrunkPredictionSpecifics = self.onLastTrunkPredictionNotTruncatableSpecifics
	
	def globalAcquirement(self):
		self.globalAcquirementSpecifics()
	
	def setLongClickVariantStrategy(self, strategy):
		def stepLongClickVariantWhenLongClickVariantIsAccentuation():
			self.stepVariant()
			self.view.updateHighlightment(True)
		
		def stepLongClickVariantWhenLongClickVariantIsCapitalization():
			self.stepCapitalizationVariantSpecifics()
		
		if strategy == "accentuation":
			self.stepLongClickVariantDependingOnLongClickVariantStrategy = stepLongClickVariantWhenLongClickVariantIsAccentuation
		elif strategy == "capitalization":
			self.stepLongClickVariantDependingOnLongClickVariantStrategy = stepLongClickVariantWhenLongClickVariantIsCapitalization
		else:
			self.stepLongClickVariantDependingOnLongClickVariantStrategy = (lambda *args: None)
	
	def stepLongClickVariant(self):
		self.stepLongClickVariantDependingOnLongClickVariantStrategy()
	
	def setCommandState(self, state):
		self.commandState = state
	
	
# Private (known by the view)
	
	def getFlaggedChars(self):
		return itertools.izip(
			self.callInGlyphTransformer(self.snipStringInSnipper(self.contentsString)),
			self.snipSequenceInSnipper(self.contentsPresentFlags),
			self.snipSequenceInSnipper(self.contentsEnabledFlags)
		)
	
	def localAcquirement(self):
		self.localAcquirementSpecifics()
	
	def stepVariant(self):
		self.stepVariantSpecifics()
		self.view.setCurrentVariantIndex(self.getVariantIndexSpecifics())
		self.resetSpecifics()
		self.delocalizeFlags()
		self.view.setSnipStrategy(self.resetInSnipper(self.contentsString))
		self.view.reset()
		self.view.stringMayHaveChangedEvent(True)
		self.updateVariantInToothManager()
	
	def onLastVariant(self):
		return self.numberOfVariants == self.getVariantIndexSpecifics() + 1
	
	def onLastTrunk(self):
		return self.onLastTrunkSpecifics()
	
	def onLastChar(self):
		return self.currentIndex == self.contentsLength # todo: use getLengthInSnipper?
	
	def getLength(self):
		return self.getLengthInSnipper()
	
	def updateIndex(self, index):
		self.updateIndexSpecifics(self.subSnipIndexInSnipper(index))
		self.needRefresh = False
	
	def getNextIndex(self, step):
		return self.getNextIndexSpecifics(step)
	
	def getLastCharIndex(self):
		return self.contentsLength-1 # todo: use getLengthInSnipper?
	
	def getMiddleCharIndex(self):
		return self.contentsLength / 2
	
	def getLastTrunkIndex(self):
		return self.contentsStartLength - 1
	
	def getSeparationIndex(self):
		return self.addSnipIndexInSnipper(self.contentsSeparationIndex)
	
	def getStartLength(self):
		return self.contentsStartLength
	
	def delocalizeFlags(self):
		self.needRefresh = True
		self.contentsEnabledFlags = [self.delocalizationFlag] * self.contentsLength
	
	def getContentsElement(self,i):
		try:
			return self.contentsString[i]
		except:
			return ""
	
	def refreshTongue(self):
		self.needRefresh = True
		self.updateIndexSpecifics(self.currentIndex)
	
	
# Prediction stuff
	
	def setPredictionStrategy(self, pretender):
		self.resetSpecifics                     = self.resetPredictionSpecifics
		self.updateIndexSpecifics               = self.updateIndexPredictionSpecifics
		self.getNextIndexSpecifics              = self.getNextIndexPredictionSpecifics
		self.onLastTrunkSpecifics               = self.onLastTrunkPredictionSpecifics
		self.localAcquirementSpecifics          = self.localAcquirementPredictionSpecifics
		self.globalAcquirementSpecifics         = self.globalAcquirementPredictionSpecifics
		self.stepVariantSpecifics               = self.stepVariantInPictoWrapper
		self.stepCapitalizationVariantSpecifics = self.stepCapitalizationVariantPredictionSpecifics
		completableString                       = self.getCompletableStringInContentGetter()
		self.poorPrediction                     = self.getPoorPredictionInWrdFor(completableString,pretender.upper())
		self.updateWordInPictoWrapper(self.poorPrediction)
		self.numberOfVariants                   = self.getNumberOfVariantsInPictoWrapper()
		self.delocalizationFlag                 = True
		self.getVariantIndexSpecifics           = self.getVariantIndexInPictoWrapper
		self.view.setSeparationStrategy("fixed")
	
	def resetPredictionSpecifics(self):
		richPrefix                   = self.getRichPrefixInContentGetter()
		richPrediction               = self.getCurrentVariantInPictoWrapper()
		self.lastSpace               = self.getRichLastSpaceInContentGetter()
		self.contentsStartLength     = len(self.lastSpace + richPrefix)
		self.contentsSeparationIndex = self.contentsStartLength
		self.contentsPrefix          = self.getCasedPredictionInCaseMapper(richPrediction)
		self.contentsString          = self.lastSpace + self.contentsPrefix
		self.contentsLength          = len(self.contentsString)
		self.contentsPresentFlags    = [True] * self.contentsStartLength \
		                             + [False] * (self.contentsLength - self.contentsStartLength)
		self.prepareStringInInstantSpeech(self.contentsString)
	
	def updateIntegerIndexPredictionSpecifics(self, index):
		if self.needRefresh or index != self.currentIndex:
			self.contentsEnabledFlags = [True] * index + [False] * (self.contentsLength - index)
			self.currentIndex = index
			self.view.update()
	
	def updateIndexPredictionTruncateSpecifics(self, floatIndex):
		self.updateIntegerIndexPredictionSpecifics(min(max(self.contentsStartLength,int(floatIndex+1)),self.contentsLength))
	
	def updateIndexPredictionDeleteSpecifics(self, floatIndex):
		self.updateIntegerIndexPredictionSpecifics(min(int(floatIndex+1),self.contentsLength))
	
	def updateIndexPredictionWholeSpecifics(self, floatIndex):
		index  = min(int(floatIndex+1),self.contentsLength)
		if index < self.contentsStartLength:
			index = self.contentsLength
		self.updateIntegerIndexPredictionSpecifics(index)
	
	def updateIndexPredictionNotTruncatableSpecifics(self, floatIndex):
		pass
	
	def getNextIndexPredictionTruncateAndWholeSpecifics(self,step):
		index = self.currentIndex + step -1
		if index >= self.contentsLength:
			index = self.contentsStartLength
		elif index < self.contentsStartLength - 1:
			index = self.contentsLength - 1
		return index
	
	def onLastTrunkPredictionTruncateAndWholeSpecifics(self):
		return self.currentIndex == self.contentsStartLength
	
	def getNextIndexPredictionDeleteSpecifics(self,step):
		index = self.currentIndex + step -1
		if index >= self.contentsLength:
			index = 0
		elif index < -1:
			index = self.contentsLength - 1
		return index
	
	def onLastTrunkPredictionDeleteSpecifics(self):
		return self.currentIndex == 0
	
	def getNextIndexPredictionNotTruncatableSpecifics(self,step):
		return self.currentIndex
	
	def onLastTrunkPredictionNotTruncatableSpecifics(self):
		return True
	
	def localAcquirementPredictionSpecifics(self):
		i = (self.contentsEnabledFlags + [False]).index(False)
		poorPrefix = (self.lastSpace + self.poorPrediction)[:i]
		self.resetInPretenders(rejectionPoorPrefix = poorPrefix, rejectedRichChar = self.contentsString[i:i+1])
		self.replaceWithWordInAcquirer(self.contentsString[:i],poorPrefix)
		self.updateVariantInToothManager()
	
	def globalAcquirementPredictionSpecifics(self):
		self.speakInInstantSpeech(self.contentsString)
		self.resetInPretenders()
		self.replaceWithWordInAcquirer(self.contentsString,self.contentsString)
	
	def stepCapitalizationVariantPredictionSpecifics(self):
		s = self.contentsPrefix
		if s.islower():
			s = s.capitalize()
		elif s.istitle() and len(s)>2:
			s = s.upper()
		else:
			s = s.lower()
		self.contentsPrefix = s
		self.contentsString = self.lastSpace + s
		self.view.reset()
		self.view.updateHighlightment(True)
		self.view.stringMayHaveChangedEvent(True)
	
	
# Favorite chars
	
	def setFavoriteCharsStrategy(self):
		self.resetSpecifics              = self.resetFavoriteCharsSpecifics
		self.getNextIndexSpecifics       = self.getNextIndexGeneric
		self.updateIndexSpecifics        = self.updateIndexFavoriteCharsSpecifics
		self.localAcquirementSpecifics   = self.localAcquirementFavoriteCharsSpecifics
		self.globalAcquirementSpecifics  = self.localAcquirementFavoriteCharsSpecifics # useless, except in the case where all auto-select commands are disabled
		self.stepVariantSpecifics        = self.stepVariantInFavoriteChars
		self.numberOfVariants            = self.getNumberOfVariantsInFavoriteChars()
		self.delocalizationFlag          = False
		self.stepCapitalizationVariantSpecifics = self.stepVariantSpecifics
		self.getVariantIndexSpecifics    = self.getVariantIndexInFavoriteChars
		self.view.setSeparationStrategy(None)
	
	def resetFavoriteCharsSpecifics(self):
		self.contentsString              = self.getCurrentVariantInFavoriteChars()
		self.contentsLength              = len(self.contentsString)
		self.contentsPresentFlags = [False] * self.contentsLength
		self.contentsEnabledFlags = [False] * self.contentsLength
	
	def updateIndexFavoriteCharsSpecifics(self, floatIndex):
		index = int(floatIndex)
		if self.needRefresh or index != self.currentIndex:
			self.contentsEnabledFlags = [False] * self.contentsLength
			if 0 <= index < self.contentsLength:
				self.contentsEnabledFlags[index] = True
			self.currentIndex = index
			self.view.update()
	
	def localAcquirementFavoriteCharsSpecifics(self):
		contents = self.getContentsElement(self.currentIndex)
		if contents == "yinYang":
			self.checkYinYangStrategyInToothManager(True)
		else:
			self.resetInPretenders(first = [" "])
			self.replaceWithLetterInAcquirer(contents)
	
	
# Commands
	
	# select command
	
	def setSelectCommandStrategy(self):
		self.resetSpecifics                     = self.resetSelectCommandSpecifics
		self.getNextIndexSpecifics              = self.getNextIndexGeneric
		self.stepVariantSpecifics               = (lambda *args:None)
		self.updateIndexSpecifics               = self.updateIndexSelectCommandSpecifics
		self.localAcquirementSpecifics          = self.localAcquirementSelectCommandSpecifics
		self.globalAcquirementSpecifics         = self.refreshTongue
		self.numberOfVariants                   = 1
		self.contentsString                     = ["selectBwdDoc","selectBwdPar","selectBwdSen","selectBwdWrd","selectBwdChr","selectAnchor","selectFwdChr","selectFwdWrd","selectFwdSen","selectFwdPar","selectFwdDoc"]
		self.contentsLength                     = len(self.contentsString)
		self.selectMiddlePos                    = self.contentsLength/2
		self.contentsPresentFlags               = [False] * self.contentsLength
		self.delocalizationFlag                 = False
		self.view.setSeparationStrategy(None)
		self.stepCapitalizationVariantSpecifics = self.stepVariantSpecifics
		self.getVariantIndexSpecifics           = (lambda *args: 0)
	
	def resetSelectCommandSpecifics(self):
		self.currentIndex = self.selectMiddlePos
		self.contentsEnabledFlags = [False] * self.contentsLength
	
	def updateIndexSelectCommandSpecifics(self, floatIndex):
		i = int(floatIndex)
		if self.needRefresh or i != self.currentIndex:
			self.currentIndex = i
			self.contentsPresentFlags[self.selectMiddlePos] = self.getAnchorStateInCommandSelect()
			if 0 <= self.currentIndex < self.contentsLength:
				self.contentsEnabledFlags = [self.currentIndex <= j < self.selectMiddlePos or self.currentIndex==j==self.selectMiddlePos or self.selectMiddlePos < j <= self.currentIndex or self.contentsPresentFlags[j] for j in range(self.contentsLength)]
			else:
				self.contentsEnabledFlags = [False] * self.contentsLength
			self.view.update()
	
	def localAcquirementSelectCommandSpecifics(self):
		self.modifySelectionInCommandSelect(self.currentIndex-self.selectMiddlePos)
		self.refreshTongue()
	
	
	# speech command
	
	def setSpeechCommandStrategy(self):
		self.resetSpecifics                             = self.resetSpeechCommandSpecifics
		self.getNextIndexSpecifics                      = self.getNextIndexGeneric
		self.updateIndexSpecifics                       = self.updateIndexSpeechCommandSpecifics
		self.globalAcquirementSpecifics                 = self.refreshTongue
		self.localAcquirementSpecifics                  = self.localAcquirementSpeechCommandSpecifics
		self.delocalizationFlag                         = False
		self.stepVariantSpecifics                       = self.stepVariantSpeechCommandSpecifics
		self.numberOfVariants                           = 2
		self.contentsString                             = ["speechBwdDoc","speechBwdPar","speechBwdSen","speechPause","speechFwdSen","speechFwdPar","speechFwdDoc"]
		self.contentsLength                             = len(self.contentsString)
		self.speechMiddlePos                            = self.contentsLength/2
		self.contentsPresentFlags                       = [False] * self.contentsLength
		self.contentsPresentFlags[self.speechMiddlePos] = self.getPauseStateInCommandSpeech()
		self.getVariantIndexSpecifics                   = self.getVariantIndexInCommandSpeech
		self.stepCapitalizationVariantSpecifics         = self.stepVariantSpecifics
		self.view.setSeparationStrategy(None)
	
	def resetSpeechCommandSpecifics(self):
		self.currentIndex = self.speechMiddlePos
		self.contentsEnabledFlags = [False] * self.contentsLength
	
	def updateIndexSpeechCommandSpecifics(self, floatIndex):
		i = int(floatIndex)
		if self.needRefresh or i != self.currentIndex:
			self.contentsPresentFlags[self.speechMiddlePos] = self.getPauseStateInCommandSpeech()
			if 0 <= i < self.contentsLength:
				self.contentsEnabledFlags = [i <= j < self.speechMiddlePos or i==j==self.speechMiddlePos or self.speechMiddlePos < j <= i or self.contentsPresentFlags[j] for j in range(self.contentsLength)]
			else:
				self.contentsEnabledFlags = [False] * self.contentsLength
			self.currentIndex = i
			self.view.update()
	
	def localAcquirementSpeechCommandSpecifics(self):
		self.interactionInCommandSpeech(self.currentIndex-self.speechMiddlePos)
		self.refreshTongue()
	
	def stepVariantSpeechCommandSpecifics(self):
		self.checkSkipStrategyInCommandSpeech(self.getVariantIndexSpecifics()!=0)
	
	
	# pending command
	
	def setPendingCommandStrategy(self):
		self.resetSpecifics             = self.resetPendingCommandSpecifics
		self.updateIndexSpecifics       = (lambda *args: None)
		self.globalAcquirementSpecifics = self.fireCommandInToothManager
		self.localAcquirementSpecifics  = self.cancelPendingCommandInToothManager
		self.delocalizationFlag         = False
		self.stepVariantSpecifics       = (lambda *args: None)
		self.numberOfVariants           = 1
		self.getVariantIndexSpecifics   = (lambda *args: 0)
		self.view.setSeparationStrategy(None)
		self.stepCapitalizationVariantSpecifics = (lambda *args: None)
	
	def resetPendingCommandSpecifics(self):
		self.contentsString       = self.getSelectedTextInContentGetter()
		self.contentsLength       = len(self.contentsString)
		self.contentsPresentFlags = [True] * self.contentsLength
		self.contentsEnabledFlags = [True] * self.contentsLength
	
	
# Calculator
	
	def setCalculatorStrategy(self):
		self.resetSpecifics              = self.resetCalculatorSpecifics
		self.getNextIndexSpecifics       = self.getNextIndexGeneric
		self.stepVariantSpecifics        = (lambda *args:None)
		self.globalAcquirementSpecifics  = self.globalAcquirementCalculatorSpecifics
		self.localAcquirementSpecifics   = self.localAcquirementCalculatorSpecifics
		self.updateIndexSpecifics        = self.updateIndexCalculatorSpecifics
		self.numberOfVariants            = 1
		self.delocalizationFlag          = True
		self.view.setSeparationStrategy(None)
		self.getVariantIndexSpecifics    = (lambda *args: 0)
		self.stepCapitalizationVariantSpecifics = self.stepVariantSpecifics
	
	def resetCalculatorSpecifics(self):
		self.contentsString       = unicode(self.getSelectedValueInCalculator())
		self.contentsLength       = len(self.contentsString)
		self.contentsPresentFlags = [True] * self.contentsLength 
		self.exponentIndex        = (self.contentsString+"e").find("e")
		try:
			self.decimalPointIndex = self.contentsString.index(".")
		except:
			self.decimalPointIndex = self.contentsLength - 1
		self.updateIndexCalculatorSpecifics(self.getPrecisionInCalculator()+self.decimalPointIndex)
	
	def updateIndexCalculatorSpecifics(self, floatIndex):
		index = min(max(int(floatIndex),0),self.contentsLength)
		if self.needRefresh or index != self.currentIndex:
			self.contentsEnabledFlags = [True ] * (index + 1) + \
			                            [False] * (self.exponentIndex - index - 1) + \
			                            [True ] * (self.contentsLength - self.exponentIndex)
			self.currentIndex = index
			self.setPrecisionInCalculator(index - self.decimalPointIndex)
			self.view.update()
	
	def localAcquirementCalculatorSpecifics(self):
		self.resetInPretenders()
		self.replaceWithStringInAcquirer(self.getReplacementStringInCalculator())
	
	def globalAcquirementCalculatorSpecifics(self):
		localPrecision = self.getPrecisionInCalculator()
		self.setPrecisionInCalculator(self.contentsLength-self.decimalPointIndex)
		self.resetInPretenders()
		self.replaceWithStringInAcquirer(self.getReplacementStringInCalculator())
		self.setPrecisionInCalculator(localPrecision)
	
	
# Generic correction
	
	def setCorrectionStrategy(self):
		self.resetSpecifics             = self.resetCorrectionSpecifics
		self.getNextIndexSpecifics      = self.getNextIndexGeneric
		self.stepVariantSpecifics       = self.stepVariantCorrectionSpecifics
		self.updateIndexSpecifics       = self.updateIndexCorrectionSpecifics
		self.localAcquirementSpecifics  = self.localAcquirementCorrectionSpecifics
		self.globalAcquirementSpecifics = self.globalAcquirementCorrectionSpecifics
		self.numberOfVariants           = len(self.correctionStrategies)
		self.correctionStrategies[self.correctionStrategyIndex]()
		self.delocalizationFlag         = True
		self.view.setSeparationStrategy(None)
		self.stepCapitalizationVariantSpecifics = self.stepVariantSpecifics
		self.getVariantIndexSpecifics   = (lambda *args: self.correctionStrategyIndex)
	
	def resetCorrectionSpecifics(self):
		(lastSpace,prefix)           = self.getErasableContentInContentGetter()
		self.contentsString          = lastSpace + prefix
		self.contentsLength          = len(self.contentsString)
		self.contentsPresentFlags    = [True] * self.contentsLength
		self.contentsSeparationIndex = self.contentsLength - 1
	
	def stepVariantCorrectionSpecifics(self):
		self.correctionStrategyIndex = (self.correctionStrategyIndex + 1) % self.numberOfVariants
		self.correctionStrategies[self.correctionStrategyIndex]()
	
	def updateIndexCorrectionSpecifics(self, floatIndex):
		i = min(max(int(floatIndex),0),self.contentsLength-1)
		if self.needRefresh or i != self.currentIndex:
			self.contentsPresentFlags = [True] * self.contentsLength
			self.contentsPresentFlags[i] = False
			self.currentIndex = i
			self.view.update()
	
	def localAcquirementCorrectionSpecifics(self):
		self.resetInPretenders(first = ["eraser"])
		s = self.substituteCorrectionSpecifics(self.contentsString,self.currentIndex)
		self.replaceWithStringInAcquirer(s)
	
	def globalAcquirementCorrectionSpecifics(self):
		self.resetInPretenders(first = ["eraser"])
		self.eraseAllInAcquirer()
	
	
# Capitalization Correction
	
	def setCapitalizationCorrectionStrategy(self):
		self.substituteCorrectionSpecifics = self.substituteCapitalizationCorrectionSpecifics
	
	def substituteCapitalizationCorrectionSpecifics(self,s,i):
		c = s[i].swapcase()
		if c == s[i]:
			return s[:i] + self.getVariantInLetterTransformer(s[i]) + s[i+1:]
		if s[:i].isupper() and c.isupper():
			return s.upper()
		if s[:i].islower() and c.islower():
			return s.lower()
		return s[:i] + c + s[i+1:]
	
	
# Accentuation Correction
	
	def setAccentuationCorrectionStrategy(self):
		self.substituteCorrectionSpecifics = self.substituteAccentuationCorrectionSpecifics
	
	def substituteAccentuationCorrectionSpecifics(self,s,i):
		return s[:i] + self.getVariantInLetterTransformer(s[i]) + s[i+1:]
	
	
# Default correction
	
	def setDefaultCorrectionStrategy(self):
		self.updateIndexSpecifics      = (lambda *args:None)
		self.localAcquirementSpecifics = self.globalAcquirementSpecifics
	
	
# Block Deletion
	
	def setBlockDeletionStrategy(self):
		self.resetSpecifics             = self.resetBlockDeletionSpecifics
		self.updateIndexSpecifics       = (lambda *args:None)
		self.globalAcquirementSpecifics = self.globalAcquirementBlockDeletionSpecifics
		self.localAcquirementSpecifics  = self.globalAcquirementBlockDeletionSpecifics
		self.delocalizationFlag         = True
		self.numberOfVariants           = 1
		self.view.setSeparationStrategy(None)
		self.stepCapitalizationVariantSpecifics = self.stepVariantSpecifics
	
	def resetBlockDeletionSpecifics(self):
		self.contentsString       = self.getPreviousBlockInContentGetter()
		self.contentsLength       = len(self.contentsString)
		self.contentsPresentFlags = [True] * self.contentsLength
	
	def globalAcquirementBlockDeletionSpecifics(self):
		self.resetInPretenders()
		self.eraseAllInAcquirer()
	
	
# Selection Deletion
	
	def setSelectionDeletionStrategy(self):
		self.resetSpecifics             = self.resetSelectionDeletionSpecifics
		self.updateIndexSpecifics       = (lambda *args:None)
		self.globalAcquirementSpecifics = self.globalAcquirementSelectionDeletionSpecifics
		self.localAcquirementSpecifics  = self.globalAcquirementSelectionDeletionSpecifics
		self.delocalizationFlag         = True
		self.numberOfVariants           = 1
		self.view.setSeparationStrategy(None)
		self.stepCapitalizationVariantSpecifics = self.stepVariantSpecifics
	
	def resetSelectionDeletionSpecifics(self):
		self.contentsString       = self.getSelectedTextInContentGetter()
		self.contentsLength       = len(self.contentsString)
		self.contentsPresentFlags = [True] * self.contentsLength
	
	def globalAcquirementSelectionDeletionSpecifics(self):
		self.resetInPretenders()
		self.eraseAllInAcquirer()
	
	
# Generic Behaviours

	def getNextIndexGeneric(self,step):
		return (self.currentIndex + step) % self.contentsLength


if __name__ == "__main__":
	from  chewing import main
	main()
