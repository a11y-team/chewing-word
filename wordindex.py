#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file wordindex.py

""" Documentation string.

Arguments:
- myArgument

Documentation text

Notes:
- random note.
"""

# built-in stuff
import re
# home made stuff
from accudict import AccuDict

class WordIndex(AccuDict):
	""" TODO """
	
	def __init__(self,mediator):
		self.mayLearnInLearner = mediator.learner.mayLearn
		self.sntSepWithoutSeparatorCapture = re.compile("(?u)%s" % mediator.rawSntSep,re.VERBOSE)
		self.letMap       = mediator.letMap
		AccuDict.__init__(self)
	
	def injectInWrdFor(self):
		""" Update word forest with all index words. """
		for (w,occ) in self.iteritems():
			self.mayLearnInLearner(w+" ",occ)
	
	def addRawText(self,rawTxt):
		""" Add the words of the given raw text into the current index. """
		for rawSnt in self.sntSepWithoutSeparatorCapture.split(rawTxt):
			lc = []
			for richChar in rawSnt:
				poorChar = self.letMap[richChar]
				if poorChar != ' ':
					lc.append(richChar)
				elif lc:
					self.increment(''.join(lc).lower())
					lc = []
			if lc:
				self.increment(''.join(lc).lower())
	
	def tsv(self):
		l = self.items()
		l.sort()
		return "\n".join(["%s\t%s" % (occ,wrd) for (wrd,occ) in l])
	
	def constructFromTsv(self, wrdOccTsv):
		self.clear()
		try:
			for wrdOcc in wrdOccTsv.split("\n"):
				c = wrdOcc.split("\t")
				if c != [""]: # tolerance for blank lines
					self[c[1]] = int(c[0])
			return True
		except:
			return False



if __name__ == "__main__":
	from  chewing import main
	main()

