#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file clicktransformer.py

# built-in
from PyQt4 import QtCore, QtGui

# home made stuff
from aspect import aspect
from fsm import Fsm
import qtgoodies


class ClickTransformer(Fsm):
	"""Convert low level move events into short and long clicks events"""
	
	def __init__(self):
		Fsm.__init__(self)
		self.timers = []
	
	def registerTimers(self, shortClickTimer, longClickTimer):
		self.shortClickTimer = shortClickTimer
		self.timers.append(shortClickTimer)
		self.longClickTimer = longClickTimer
		self.timers.append(longClickTimer)
		QtCore.QObject.connect(self.longClickTimer,QtCore.SIGNAL("tick(int)"),self.sendLongClickStep)
		self.compile(aspect.clickTransformers)
	
	def registerScanner(self, scanner):
		self.scanner = scanner
	
	def checkScanStrategy(self,strategy):
		def sendShortClickWhenScannerIsActive():
			self.scanner.shortClickSlot(self.currentExternalEvent)
		
		def sendLongClickStartWhenScannerIsActive():
			self.scanner.longClickStartSlot(self.currentExternalEvent)
		
		def sendLongClickStepWhenScannerIsActive(n):
			self.scanner.longClickStepSlot()
		
		def sendLongClickStopWhenScannerIsActive():
			self.scanner.longClickStopSlot(self.currentExternalEvent)
		
		def sendShortClickWhenScannerIsInactive():
			self.currentClient.shortClickEvent()
		
		def sendLongClickStartWhenScannerIsInactive():
			self.currentClient.longClickStartEvent(self.currentExternalEvent)
		
		def sendLongClickStepWhenScannerIsInactive(n):
			self.currentClient.longClickStepEvent(self.currentExternalEvent)
		
		def sendLongClickStopWhenScannerIsInactive():
			self.currentClient.longClickStopEvent(self.currentExternalEvent)
		
		if strategy:
			self.sendShortClickDependingOnScannerMode     = sendShortClickWhenScannerIsActive
			self.sendLongClickStartDependingOnScannerMode = sendLongClickStartWhenScannerIsActive
			self.sendLongClickStepDependingOnScannerMode  = sendLongClickStepWhenScannerIsActive
			self.sendLongClickStopDependingOnScannerMode  = sendLongClickStopWhenScannerIsActive
		else:
			self.sendShortClickDependingOnScannerMode     = sendShortClickWhenScannerIsInactive
			self.sendLongClickStartDependingOnScannerMode = sendLongClickStartWhenScannerIsInactive
			self.sendLongClickStepDependingOnScannerMode  = sendLongClickStepWhenScannerIsInactive
			self.sendLongClickStopDependingOnScannerMode  = sendLongClickStopWhenScannerIsInactive
	
	def setLongClickStartSound(self,sound):
		self.longClickStartSound = qtgoodies.soundFactory("sounds/simple/%s.wav" % sound)
	
	def setLongClickStepSound(self,sound):
		self.longClickStepSound = qtgoodies.soundFactory("sounds/simple/%s.wav" % sound)
	
	def checkLongClickStartSoundStrategy(self,strategy):
		if strategy:
			self.playLongClickStartDependingOnLongClickStartSoundStrategy = self.longClickStartSound.play
		else:
			self.playLongClickStartDependingOnLongClickStartSoundStrategy = (lambda *args: None)
	
	def checkLongClickStepSoundStrategy(self,strategy):
		if strategy:
			self.playLongClickStepDependingOnLongClickStepSoundStrategy = self.longClickStepSound.play
		else:
			self.playLongClickStepDependingOnLongClickStepSoundStrategy = (lambda *args: None)
	
	def muteSounds(self):
		self.checkLongClickStartSoundStrategy(False)
		self.checkLongClickStepSoundStrategy(False)
	
	def demuteSounds(self):
		self.checkLongClickStartSoundStrategy(self.longClickStartSound != "")
		self.checkLongClickStepSoundStrategy(self.longClickStepSound != "")
	


	def reset(self):
		Fsm.reset(self)
		for timer in self.timers:
			timer.stop()
	
	
	# Starting and stopping timers
	
	def startShortClickTimer(self):
		self.shortClickTimer.start(self.shortClickTimerStopSlot)
	
	def startLongClickTimer(self):
		self.longClickTimer.start()
	
	def stopShortClickTimer(self):
		self.shortClickTimer.stop()
	
	def stopLongClickTimer(self):
		self.longClickTimer.stop()
	
	# Sent Messages
	
	def sendShortClick(self,*args):
		self.sendShortClickDependingOnScannerMode(*args)
	
	def sendLongClickStart(self,*args):
		self.playLongClickStartDependingOnLongClickStartSoundStrategy()
		self.sendLongClickStartDependingOnScannerMode(*args)
	
	def sendLongClickStep(self,*args):
		self.playLongClickStepDependingOnLongClickStepSoundStrategy()
		self.sendLongClickStepDependingOnScannerMode(*args)
	
	def sendLongClickStop(self,*args):
		self.sendLongClickStopDependingOnScannerMode(*args)
	
	
	# Slots
	
	def shortClickTimerStopSlot(self):
		self.transition("shortClickTimerStop")
	
	def mousePressSlot(self, event):
		self.currentExternalEvent = event
		self.transition("mousePress",event)
		self.scanner.stopScanTimer()
	
	def mouseReleaseSlot(self, event):
		self.currentExternalEvent = event
		self.transition("mouseRelease",event)
	
	def longClickTimerStepSlot(self):
		self.transition("longClickTimerStep")
	
	# guards
	
	def noGuard(self, *args):
		return True
	
	def isLeft(self, event):
		return event.buttons() & QtCore.Qt.LeftButton
	
	def isRight(self, event):
		return event.buttons() & QtCore.Qt.RightButton
	

if __name__ == "__main__":
	from fsm import graphviz
	graphviz(aspect.clickTransformers)
