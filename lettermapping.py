#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file lettermapping.py

""" Mapping of every unicode character onto the given poor-man alphabet.

Arguments:
- utfAlf: poor-man alphabet as a list of unicode strings (in uppercase).
    Limitations: only one-char strings are actually supported
- bytEnc: internal encoding for the poor-man alphabet.
- defMap: explicit mapping when no regular rule applies.
    e.g., {u'Æ':u"AE", u'Ö':u"OE"} holds the following cases:
    - LATIN CAPITAL LETTER AE should be named LATIN CAPITAL LIGATURE AE
    - LATIN CAPITAL LETTER O WITH DIAERESIS must become "OE"

Description:
- The dictionary is initally constructed with trivial and provided mappings
  only. Others will be calculated and added on request every time the special
  method __getitem__ is called.

Notes:
- Keys are encoded in utf-8, but values in bytEnc. Since the poor-man alphabet
  should contain less than 255 symbols, using an 8-bits encoding may save some
  space in the forest.
"""

# built-in stuff
import unicodedata
import re

# import languagespecifics


class LetterMapping(dict):
	
	def __init__(self, lngSpe):
		""" letMap: mapping of every unicode character onto the poor-man alphabet. """
		def initWithIdentityDictionary(utfAlf, bytEnc):
			""" Any symbol maps onto itself. """
			dict.__init__(self,[(c,c.encode(bytEnc)) for c in utfAlf])
		def symbolUnicodeNameList():
			""" list of unicode name of symbols, with the more complex presented first. """
			names = [(unicodedata.name(uc)+" ",c) for (uc,c) in self.iteritems()]
			names.sort(lambda x,y:cmp(len(y),len(x))) # Python 2.4: names.sort(key=len,reverse=True)
			return names
		def addExplicitelySpecifiedMappings(defMap, bytEnc):
			self.update(dict([(uc,uec.encode(bytEnc)) for (uc,uec) in defMap.items()]))
		def addLowercaseMappings():
			self.update(dict([(uc.lower(),c) for (uc,c) in self.iteritems()]))
		#
		initWithIdentityDictionary(lngSpe["utfAlf"],lngSpe["bytEnc"])
		self.names = symbolUnicodeNameList()
		addExplicitelySpecifiedMappings(lngSpe["defMap"],lngSpe["bytEnc"])
		addLowercaseMappings()
	
	def __getitem__(self,c):
		""" Return the poor-man equivalent of any given character. If it has not
		been encountered yet, its entry is magically created with help of its
		Unicode name. Return a space when no sensible mapping can be calculated.
		"""
		try:
			return dict.__getitem__(self,c)
		except KeyError:
			for tryGet in (self._tryLigature, self._tryPrefix, self._getDefault):
				try:
					self[c] = tryGet(c.upper())
					return dict.__getitem__(self,c)
				except (AttributeError, ValueError, KeyError):
					pass
	
	def _tryLigature(self,c):
		""" Mapping of simple (i.e., latin) ligature cases, e.g.:
		LATIN CAPITAL LIGATURE OE, LATIN CAPITAL LIGATURE FFL, etc."""
		ligature = re.search(re.compile(r"LIGATURE (\w+)\Z"), unicodedata.name(c))
		lc = [pc for pc in ligature.groups()[0]]
		return ''.join(map(lambda c: dict.__getitem__(self,c),lc))
	
	def _tryPrefix(self,c):
		""" Mapping of letters with diacritics, e.g.:
		LATIN CAPITAL LETTER A WITH GRAVE, LATIN CAPITAL LETTER D WITH STROKE. """
		cname = unicodedata.name(c) + " "
		for (s,pc) in self.names:
			if cname.startswith(s):
				return pc
		raise KeyError
	
	def _getDefault(self,c):
		""" Mapping onto space when everything has failed."""
		return ' '
	
