#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file distributenumber.py

def distributeNumber(n,m):
	""" Generate m integers m_i : sum(m_i)==n and \forall i,j: m_i>m_j <=> m_i=1+m_j """
	(q,r) = divmod(n,m)
	for _ in range(r/2):
		yield q+1
	for _ in range(m-r):
		yield q
	for _ in range(r-r/2):
		yield q+1

