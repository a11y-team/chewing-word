#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file editorview.py


# builtin stuff
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import SIGNAL
# home made stuff
from aspect import aspect
from farpictoview import FarPictoView

class EditorView(QtGui.QTextEdit):
	
	CURSORMOVING = 0
	ANCHORFIXED  = 1
	CURSORFIXED  = 2
	NOTRACK      = 3
	
	def __init__(self):
		QtGui.QTextEdit.__init__(self)
		self.setFrameStyle(QtGui.QFrame.Panel | QtGui.QFrame.Raised)
		self.insertPlainText(aspect.editorLoremIpsum)
		self.setMinimumHeight(aspect.editorMinHeight)
		self.setAcceptRichText(False)
		self.font = QtGui.QFont()
		self.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
		self.farPictoView = FarPictoView(self)
		self.updateSizeInFarPictoView = self.farPictoView.updateSize
		self.speakingBrush = QtGui.QBrush(QtGui.QColor(aspect.editorSpeakingColor))
		self.speakingPausedBrush = QtGui.QBrush(QtGui.QColor(aspect.editorSpeakingPausedColor))
		self.speakableBrushes = [QtGui.QBrush(QtGui.QColor(color)) for color in aspect.editorSpeakableColors]
		self.numberOfSpeakableColors = len(self.speakableBrushes)
		self.trackingBehaviour = self.CURSORFIXED
	
	def setFontFamily(self,fontFamily):
		self.font.fromString(fontFamily)
		self.document().setDefaultFont(self.font)
	
	def getFontString(self):
		return "%s, %s" % (self.font.family(),self.font.pointSizeF())
	
	def makeBigger(self):
		self.font.setPointSizeF(min(256,self.font.pointSizeF()+1))
		self.document().setDefaultFont(self.font)
	
	def makeSmaller(self):
		self.font.setPointSizeF(max(6,self.font.pointSizeF()-1))
		self.document().setDefaultFont(self.font)
	
	def getWidth(self):
		return self.width()
	
	def setFontSize(self,size):
		self.font.setPointSizeF(size)
		self.document().setDefaultFont(self.font)
	
	def setTabSize(self,size):
		self.setTabStopWidth(size*QtGui.QFontMetrics(self.font).width("M"))
	
	def registerAutoClickTimer(self, autoClickTimer):
		self.autoClickTimer = autoClickTimer
	
	def leaveEvent(self,*args):
		self.leaveEventDependingOnHoverSelection(*args)
	
	def enterEvent(self,*args):
		self.enterEventDependingOnHoverSelection(*args)
	
	def autoClickEvent(self, *args):
		self.trackingBehaviour = (self.trackingBehaviour + 1) % 3
		self.emit(SIGNAL("trackingBehaviourChanged"),self.trackingBehaviour)
		if self.trackingBehaviour == self.CURSORMOVING:
			cursor = self.cursorForPosition(self.mapFromGlobal(QtGui.QCursor.pos()))
			self.setTextCursor(cursor)
		self.autoClickTimer.mayNeedExtraDelay(True)
		self.autoClickTimer.startWhenStartStrategyIsOn(self.autoClickEvent)
			
	
	def mouseMoveEvent(self, event):
		self.mouseMoveEventDependingOnHoverSelection(event)
	
	def checkHoverSelectionStrategy(self, strategy):
		def mouseMoveEventWhenHoverSelectionIsOn(event):
			if self.trackingBehaviour < self.CURSORFIXED:
				cursor = self.cursorForPosition(event.pos())
				if self.trackingBehaviour == self.ANCHORFIXED:
					cursor.setPosition(self.textCursor().position(),QtGui.QTextCursor.KeepAnchor)
				elif event.y() < 30:
					cursor.movePosition(QtGui.QTextCursor.Up)
				elif event.y() > self.height() - 30:
					cursor.movePosition(QtGui.QTextCursor.Down)
				self.setTextCursor(cursor)
				self.ensureCursorVisible()
				self.emit(SIGNAL("cursorPositionChanged()"))
			self.autoClickTimer.mayNeedExtraDelay(True)
			self.autoClickTimer.start(self,event)
		
		def mouseMoveEventWhenHoverSelectionIsOff(event):
			QtGui.QTextEdit.mouseMoveEvent(self, event)
		
		def leaveEvent(event=None):
			self.autoClickTimer.stop()
			self.emit(SIGNAL("trackingBehaviourChanged"),self.NOTRACK)
		
		def enterEvent(event=None):
			self.emit(SIGNAL("trackingBehaviourChanged"),self.trackingBehaviour)
		
		self.setMouseTracking(strategy)
		if strategy:
			self.mouseMoveEventDependingOnHoverSelection = mouseMoveEventWhenHoverSelectionIsOn
			self.enterEventDependingOnHoverSelection = enterEvent
			self.leaveEventDependingOnHoverSelection = leaveEvent
		else:
			leaveEvent()
			self.mouseMoveEventDependingOnHoverSelection = mouseMoveEventWhenHoverSelectionIsOff
			self.enterEventDependingOnHoverSelection = (lambda *args: None)
			self.leaveEventDependingOnHoverSelection = (lambda *args: None)
	
	def mouseDoubleClickEvent(self, event):
		QtGui.QTextEdit.mouseDoubleClickEvent(self, event)
		self.emit(SIGNAL("cursorPositionChanged()"))
	
	
# Cursor getters
	
	def getStartOfBlock(self):
		cursor = self.textCursor()
		cursor.setPosition(cursor.selectionStart())
		cursor.movePosition(QtGui.QTextCursor.StartOfBlock,QtGui.QTextCursor.KeepAnchor)
		cursor.movePosition(QtGui.QTextCursor.PreviousCharacter,QtGui.QTextCursor.KeepAnchor,1)
		return self.getText(cursor)
	
	def getPreviousBlock(self):
		cursor = self.textCursor()
		cursor.setPosition(cursor.selectionStart())
		cursor.movePosition(QtGui.QTextCursor.PreviousCharacter,QtGui.QTextCursor.KeepAnchor,1)
		cursor.movePosition(QtGui.QTextCursor.StartOfBlock,QtGui.QTextCursor.KeepAnchor)
		return self.getText(cursor)
	
	def getCurrentBlock(self):
		cursor = self.textCursor()
		cursor.select(QtGui.QTextCursor.BlockUnderCursor)
		cursor.movePosition(QtGui.QTextCursor.NextCharacter,QtGui.QTextCursor.KeepAnchor,1)
		return self.getText(cursor)
	
	def getEndOfText(self):
		cursor = self.textCursor()
		cursor.setPosition(cursor.selectionStart())
		cursor.movePosition(QtGui.QTextCursor.End,QtGui.QTextCursor.KeepAnchor)
		return self.getText(cursor)
	
	def getAnchorAndPosition(self):
		return (self.textCursor().anchor(),self.textCursor().position())
	
	def getPosition(self):
		return self.textCursor().position()
	
	def getCursorBetween(self,anchor,position):
		cursor = self.textCursor()
		cursor.setPosition(anchor)
		if anchor <= position:
			cursor.movePosition(QtGui.QTextCursor.NextCharacter,QtGui.QTextCursor.KeepAnchor,position-anchor)
		else:
			cursor.movePosition(QtGui.QTextCursor.PreviousCharacter,QtGui.QTextCursor.KeepAnchor,anchor-position)
		return cursor
	
	def getCursorAt(self,position):
		cursor = self.textCursor()
		cursor.setPosition(position)
		return cursor
	
	
# Text getters
	
	def getAll(self):
		cursor = self.textCursor()
		cursor.select(QtGui.QTextCursor.Document)
		return unicode(cursor.selectedText()).replace(u"\u2029","\n") # beware to QTextEdit.toPlainText which silently transforms unbreakable spaces into normal spaces
	
	def getText(self, cursor):
		return unicode(cursor.selectedText()).replace(u"\u2029","\n")
	
	def getSelectedText(self):
		return self.getText(self.textCursor())
	
	
# Property getters
	
	def atBlockStart(self):
		return self.textCursor().atBlockStart()
	
	def atBlockEnd(self):
		return self.textCursor().atBlockEnd()
	
# Setters

	def replace(self,textToInsertBeforeCursor,nbPreviousCharsToDelete,nbNextCharsToDelete,textToInsertAfterCursor = ""):
		self.emit(SIGNAL("setTextChangedStrategy"),"silentlyChange")
		cursor = self.textCursor()
		cursor.setPosition(cursor.selectionStart())
		cursor.movePosition(QtGui.QTextCursor.PreviousCharacter,QtGui.QTextCursor.MoveAnchor,nbPreviousCharsToDelete)
		cursor.movePosition(QtGui.QTextCursor.NextCharacter,QtGui.QTextCursor.KeepAnchor,nbPreviousCharsToDelete+nbNextCharsToDelete)
		cursor.insertText(textToInsertBeforeCursor + textToInsertAfterCursor)
		cursor.movePosition(QtGui.QTextCursor.PreviousCharacter,QtGui.QTextCursor.MoveAnchor,len(textToInsertAfterCursor))
		self.setTextCursor(cursor)
		self.ensureCursorVisible()
		self.emit(SIGNAL("setTextChangedStrategy"),"normal")
	
	def clearWithoutClearingUndoHistory(self):
		cursor = self.textCursor()
		cursor.select(QtGui.QTextCursor.Document)
		cursor.removeSelectedText()
	
	def silentlySetCursor(self, cursor):
		self.emit(SIGNAL("setTextChangedStrategy"),"ignore")
		self.setTextCursor(cursor)
		self.ensureCursorVisible()
		self.emit(SIGNAL("setTextChangedStrategy"),"silentlyMove")
		self.emit(SIGNAL("cursorPositionChanged()")) # ensure if omitted, only position (not anchor) move is detected
		self.emit(SIGNAL("setTextChangedStrategy"),"normal")
	
	def silentlySetCursorBetween(self,anchor,position):
		self.silentlySetCursor(self.getCursorBetween(anchor,position))
	
	def silentlySetCursorAt(self,position):
		self.silentlySetCursor(self.getCursorAt(position))
	
	def silentlySetCursorsAt(self,positions):
		self.emit(SIGNAL("setTextChangedStrategy"),"ignore")
		for position in positions:
			self.setTextCursor(self.getCursorAt(position))
			self.ensureCursorVisible()
		self.emit(SIGNAL("setTextChangedStrategy"),"silentlyMove")
		self.emit(SIGNAL("cursorPositionChanged()")) # ensure if omitted, only position (not anchor) move is detected
		self.emit(SIGNAL("setTextChangedStrategy"),"normal")
	
	def selectAll(self):
		cursor = self.textCursor()
		cursor.select(QtGui.QTextCursor.Document)
		self.setTextCursor(cursor)
	
# Pictos
	
	def resizeEvent(self, event = None):
		QtGui.QTextEdit.resizeEvent(self, event)
		self.farPictoView.updateSize()
	
	def getViewportWidth(self):
		return self.viewport().width()
	
	def getViewportHeight(self):
		return self.viewport().height()
	
# Speech

	def updateSpeechActivity(self, anchor, position, activity, isPaused, i):
		format = QtGui.QTextCharFormat()
		cursor = self.getCursorBetween(anchor,position)
		cursor.joinPreviousEditBlock()
		if activity:
			format.setBackground(self.speakingPausedBrush if isPaused else self.speakingBrush)
		else:
			format.setBackground(self.speakableBrushes[i % self.numberOfSpeakableColors])
		cursor.setCharFormat(format)
		cursor.endEditBlock()
	
	def clearSpeechActivity(self, anchor, position):
		format = QtGui.QTextCharFormat()
		cursor = self.getCursorBetween(anchor,position)
		cursor.joinPreviousEditBlock()
		cursor.setCharFormat(format)
		cursor.endEditBlock()
	
	def clearAllSpeechActivity(self):
		format = QtGui.QTextCharFormat()
		cursor = self.textCursor()
		cursor.select(QtGui.QTextCursor.Document)
		cursor.joinPreviousEditBlock()
		cursor.setCharFormat(format)
		cursor.endEditBlock()
	
if __name__ == "__main__":
	from  chewing import main
	main()

