#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file toothview.py

""" Qt transcription session. """

# builtin stuff
from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import SIGNAL
import itertools
# home made stuff
from aspect import aspect
from qtgoodies import retrieveOrGeneratePixmap

quarter = {
	(True , True ): "v",
	(True , False): "<",
	(False, True ): ">",
	(False, False): "^",
}

class ToothView(QtGui.QToolButton):
	
	def __init__(self, i, jawsView):
		QtGui.QToolButton.__init__(self, jawsView)
		self.toothLightColor          = QtGui.QColor(aspect.toothLightColor)
		self.toothDarkColor           = QtGui.QColor(aspect.toothDarkColor)
		self.toothHighlightLightColor = QtGui.QColor(aspect.toothHighlightLightColor)
		self.toothHighlightDarkColor  = QtGui.QColor(aspect.toothHighlightDarkColor)
		self.toothZoomLightColor      = QtGui.QColor(aspect.toothZoomLightColor)
		self.toothZoomDarkColor       = QtGui.QColor(aspect.toothZoomDarkColor)
		self.toothZoneLightColor      = QtGui.QColor(aspect.toothZoneLightColor)
		self.toothZoneDarkColor       = QtGui.QColor(aspect.toothZoneDarkColor)
		self.toothPlayLightColor      = QtGui.QColor(aspect.toothPlayLightColor)
		self.toothPlayDarkColor       = QtGui.QColor(aspect.toothPlayDarkColor)
		self.toothFixedZoneLightColor = QtGui.QColor(aspect.toothFixedZoneLightColor)
		self.toothFixedZoneDarkColor  = QtGui.QColor(aspect.toothFixedZoneDarkColor)
		self.toothShadowColor         = QtGui.QColor(*aspect.toothShadowColor)
		self.eraserBlueColor          = QtGui.QColor(aspect.eraserBlueColor)
		self.eraserPinkColor          = QtGui.QColor(aspect.eraserPinkColor)
		self.eraserPenLightColor      = QtGui.QColor(aspect.eraserPenLightColor)
		self.eraserPenDarkColor       = QtGui.QColor(aspect.eraserPenDarkColor)
		self.eraserLightDarkening     = QtGui.QColor(*aspect.eraserLightDarkening)
		self.eraserMediumDarkening    = QtGui.QColor(*aspect.eraserMediumDarkening)
		self.eraserDarkDarkening      = QtGui.QColor(*aspect.eraserDarkDarkening)
		self.toothCrossColor          = QtGui.QColor(aspect.toothCrossColor)
		self.toothGlyphPen            = QtGui.QPen(QtCore.Qt.black)
		self.toothGlyphProgressivePen = [QtGui.QPen(QtGui.QColor(0,0,0,c)) for c in aspect.toothGlyphProgressiveFade] + [QtCore.Qt.NoPen] * 40
		self.toothTargetSquaredRadius = pow(aspect.toothTargetDiameter/2,2)
		self.toothZonePen             = QtGui.QPen(QtGui.QColor(aspect.toothZonePenColor),0)
		self.carriageReturnGlyph      = aspect.glyphTransformer["\n"]
		self.prohibitIcon             = QtGui.QIcon("symbols/prohibit.png")
		self.setMouseTracking(True)
		self.setFont(QtGui.QFont(aspect.toothFont))
		self.zoneTrigger = "."
		self.highlighted = False
		self.index = i
		self.updateTongueHighlightmentInJawsView = jawsView.updateTongueHighlightment
		nbTeeth = jawsView.nbTeeth
		if  i < nbTeeth/2:
			self.getGlyphPosition       = self.getTopGlyphPosition
			self.getPictoRect           = self.getTopPictoRect
			self.getScanPosition        = self.getTopScanPosition
			self.updatePosition         = self.updateTopPosition
			self.verticalZoomAdjustment = self.topVerticalZoomAdjustment
			self.updateAsAfterTarget    = self.updateAsRightToTarget
			self.updateAsBeforeTarget   = self.updateAsLeftToTarget
		else:
			self.getGlyphPosition       = self.getBottomGlyphPosition
			self.getPictoRect           = self.getBottomPictoRect
			self.getScanPosition        = self.getBottomScanPosition
			self.updatePosition         = self.updateBottomPosition
			self.verticalZoomAdjustment = self.bottomVerticalZoomAdjustment
			self.updateAsBeforeTarget   = self.updateAsRightToTarget
			self.updateAsAfterTarget    = self.updateAsLeftToTarget
		if i in (0,nbTeeth/2):
			self.updateAsAfterTarget = self.updateAsFarFromTarget
		if i+1 in (nbTeeth/2,nbTeeth):
			self.updateAsBeforeTarget = self.updateAsFarFromTarget
		self.paintPictoStrategies = {"farPictoMode": True}
		self.bwIconStyle = QtGui.QStyleFactory.create("windows")
		self.setStyle(self.bwIconStyle)
	
	def registerManager(self,manager):
		self.normalEraserAcquirementInManager                     = manager.normalEraserAcquirement
		self.normalLetterAcquirementInManager                     = manager.normalLetterAcquirement
		self.normalStaticSpaceAcquirementInManager                = manager.normalStaticSpaceAcquirement
		self.normalStaticFirstCarriageReturnAcquirementInManager  = manager.normalStaticFirstCarriageReturnAcquirement
		self.normalStaticSecondCarriageReturnAcquirementInManager = manager.normalStaticSecondCarriageReturnAcquirement
		self.normalDynamicSpaceAcquirementInManager               = manager.normalDynamicSpaceAcquirement
		self.shortClickOnCommandInManager                         = manager.shortClickOnCommand
		self.longClickStartOnCommandInManager                     = manager.longClickStartOnCommand
		self.longClickStepOnCommandInManager                      = manager.longClickStepOnCommand
		self.longClickStopOnCommandInManager                      = manager.longClickStopOnCommand
		self.updateCommandIndexInManager                          = manager.updateCommandIndex
		self.updateTargetAndStatesInManager                       = manager.updateTargetAndStates
		self.updateStrategyAsSpaceInManager                       = manager.updateStrategyAsSpace
		self.globalAcquirementInManager                           = manager.globalAcquirement
		self.stepLongClickVariantInManager                        = manager.stepLongClickVariant
		self.longClickStartInManager                              = manager.longClickStart
		self.longClickStartOnSpaceInManager                       = manager.longClickStartOnSpace
		self.enterPictoInManager                                  = manager.enterPicto
		self.leavePictoInManager                                  = manager.leavePicto
		
	def registerClickTransformer(self, clickTransformer):
		self.clickTransformer = clickTransformer
	
	def registerAutoClickTimer(self, autoClickTimer):
		self.autoClickTimer = autoClickTimer
	
	def registerHighlightTimer(self, highlightTimer):
		self.highlightTimer = highlightTimer
	
	def checkProgressiveStrategy(self,strategy):
		if strategy:
			self.paintFixedZone = self.paintCentralZone
		else:
			self.paintFixedZone = self.paintNoZone
	
	
	
# pictos
	
	def checkPictoStrategy(self,strategy):
		if strategy:
			self.paintPictoDependingOnPictoStrategy = self.mayPaintPicto
			self.enterPictoDependingOnPictoStrategy = self.mayEnterPicto
			self.leavePictoDependingOnPictoStrategy = self.mayLeavePicto
		else:
			self.paintPictoDependingOnPictoStrategy = (lambda *args: None)
			self.enterPictoDependingOnPictoStrategy = (lambda *args: None)
			self.leavePictoDependingOnPictoStrategy = (lambda *args: None)
	
	def mayPaintPicto(self,painter):
		self.paintPictoDependingOnPictoPresenceStrategy(painter)
	
	def mayEnterPicto(self):
		self.enterPictoDependingOnPictoHoveringStrategy()
	
	def mayLeavePicto(self):
		self.leavePictoDependingOnPictoHoveringStrategy()
	
	def checkPictoPresenceStrategy(self, strategy):
		def paintPictoWhenPictoPresenceStrategyIsOn(painter):
			self.picto.paint(painter,self.pictoRect,QtCore.Qt.AlignCenter,self.pictoActivity,QtGui.QIcon.On)
		
		def enterPictoWhenPictoPresenceStrategyIsOn():
			self.enterPictoInManager(self.picto,self.pictoActivity,self.pictoInfo)
		
		if strategy:
			self.paintPictoDependingOnPictoPresenceStrategy = paintPictoWhenPictoPresenceStrategyIsOn
			self.enterPictoDependingOnPictoPresenceStrategy = enterPictoWhenPictoPresenceStrategyIsOn
			self.leavePictoDependingOnPictoPresenceStrategy = self.leavePictoInManager
		else:
			self.paintPictoDependingOnPictoPresenceStrategy = (lambda *args: None)
			self.enterPictoDependingOnPictoPresenceStrategy = self.leavePictoInManager
			self.leavePictoDependingOnPictoPresenceStrategy = (lambda *args: None)
	
	def checkPictoHoveringStrategy(self, strategy):
		def enterPictoWhenPictoHoveringStrategyIsOn():
			self.enterPictoInManager(self.picto,self.pictoActivity,self.pictoInfo)
		
		if strategy:
			self.enterPictoDependingOnPictoHoveringStrategy = enterPictoWhenPictoHoveringStrategyIsOn
			self.leavePictoDependingOnPictoHoveringStrategy = self.leavePictoInManager
		else:
			self.enterPictoDependingOnPictoHoveringStrategy = self.leavePictoInManager
			self.leavePictoDependingOnPictoHoveringStrategy = (lambda *args: None)
	
	def setPicto(self, pictoInfo):
		self.pictoInfo = pictoInfo
		self.picto = QtGui.QIcon(pictoInfo["file"][0])
	
	def setPictoActivity(self, activity):
		self.pictoActivity = (QtGui.QIcon.Normal if activity else QtGui.QIcon.Disabled)
	
	
# Transformable events
	
	def mousePressEvent(self, event):
		self.clickTransformer.setCurrentClient(self)
		self.clickTransformer.mousePressSlot(event)
	
	def mouseReleaseEvent(self, event):
		self.clickTransformer.mouseReleaseSlot(event)
	
	
# Transformed events
	
	def shortClickEvent(self):
		self.shortClick()
		self.enterPictoDependingOnPictoStrategy()
	
	def longClickStartEvent(self, event):
		self.longClickStart()
		self.update()
	
	def longClickStepEvent(self, event):
		point = self.mapFromGlobal(QtGui.QCursor.pos())
		if self.rect().contains(point) and self.underMouse(): # both conditions seem necessary, don't ask why
			self.longClickStep()
	
	def longClickStopEvent(self, event):
		if 0 <= event.x() < self.wRef and 0 <= event.y() < self.hRef:
			self.longClickStop()
			self.enterPictoDependingOnPictoStrategy()
	
	
# Other events
	
	def autoClickEvent(self, *args):
		pos = self.mapFromGlobal(QtGui.QCursor.pos())
		if 0 <= pos.x() < self.wRef and 0 <= pos.y() < self.hRef:
			self.shortClick()
	
	def mouseMoveEvent(self,event):
		self.autoClickTimer.start(self,event)
		if event.buttons() == QtCore.Qt.NoButton:
			self.specificUpdateZone(event)
		else:
			self.updateTongueHighlightmentInJawsView(0 <= event.x() < self.wRef and 0 <= event.y() < self.hRef)
	
	def enterEvent(self, event):
		# print "entering %s, " % self.index
		self.autoClickTimer.setAutoRepeat(self.autoRepeat)
		self.updateTargetAndStatesInManager(self.index)
		self.enterPictoDependingOnPictoStrategy()
	
	def leaveEvent(self, event):
		self.autoClickTimer.setAutoRepeat(True)
		self.autoClickTimer.stop()
		self.specificUpdateZone(QtCore.QPoint(0,0))
		self.leavePictoDependingOnPictoStrategy()
		# the following line has been commented to make the scanner work: TODO: test the behaviour in other modes
		# self.clickTransformer.reset()
	
	def paintEvent(self,_):
		painter = QtGui.QPainter()
		painter.begin(self)
		self.specificPaint(painter)
		painter.end()
	
	def mayBeDynamicKeyEvent(self, isDynamicKey):
		self.autoClickTimer.mayNeedExtraDelay(isDynamicKey)
	
	
# Geometry
	
	def updateRefGeometry(self,xRef,yRef,wRef,hRef,toothOverlap):
		def fixedZoneBrush():
			fillGradient = QtGui.QLinearGradient(0, 0, wRef, hRef)
			fillGradient.setColorAt(0.0, self.toothFixedZoneLightColor)
			fillGradient.setColorAt(1.0, self.toothFixedZoneDarkColor)
			return QtGui.QBrush(fillGradient)
		
		def playBrush():
			fillGradient = QtGui.QLinearGradient(0, 0, wRef, hRef)
			fillGradient.setColorAt(0.0, self.toothPlayLightColor)
			fillGradient.setColorAt(1.0, self.toothPlayDarkColor)
			return QtGui.QBrush(fillGradient)
		
		def zoneBrush():
			fillGradient = QtGui.QLinearGradient(0, 0, wRef, hRef)
			fillGradient.setColorAt(0.0, self.toothZoneLightColor)
			fillGradient.setColorAt(1.0, self.toothZoneDarkColor)
			return QtGui.QBrush(fillGradient)
		
		def crossBrush():
			return QtGui.QBrush(self.toothCrossColor)
		
		def zonePaths():
			w = wRef * aspect.toothTargetDiameter
			h = hRef * aspect.toothTargetDiameter
			x = 0.5 * (wRef - w)
			y = 0.5 * (hRef - h)
			d = self.getPenWidth(wRef)
			wd = wRef-2*d
			hd = hRef-2*d
			#
			cPath = QtGui.QPainterPath()
			cPath.moveTo(x+w,y+h/2)
			cPath.arcTo(x,y,w,h,0.0,360.0)
			cPath.closeSubpath()
			#
			nPath = QtGui.QPainterPath()
			nPath.moveTo(wRef/4,hRef/4)
			nPath.arcTo(d,d,wd,hd,135.0,-90.0)
			nPath.arcTo(x,y,w,h,45.0,90.0)
			nPath.closeSubpath()
			#
			wPath = QtGui.QPainterPath()
			wPath.moveTo(wRef/4,hRef/4)
			wPath.arcTo(d,d,wd,hd,135.0,90.0)
			wPath.arcTo(x,y,w,h,225.0,-90.0)
			wPath.closeSubpath()
			#
			sPath = QtGui.QPainterPath()
			sPath.moveTo(3*wRef/4,3*hRef/4)
			sPath.arcTo(d,d,wd,hd,315.0,-90.0)
			sPath.arcTo(x,y,w,h,225.0,90.0)
			sPath.closeSubpath()
			#
			ePath = QtGui.QPainterPath()
			ePath.moveTo(3*wRef/4,3*hRef/4)
			ePath.arcTo(d,d,wd,hd,315.0,90.0)
			ePath.arcTo(x,y,w,h,45.0,-90.0)
			ePath.closeSubpath()
			return dict([c for c in itertools.izip(".^<v>",[cPath,nPath,wPath,sPath,ePath])])
		
		def crossPath():
			dx = wRef * aspect.toothCrossDiameter
			dy = hRef * aspect.toothCrossDiameter
			x = 0.5 * (wRef - dx)
			y = 0.5 * (hRef - dy)
			dw = wRef / 4
			dh = hRef / 4
			path = QtGui.QPainterPath()
			path.addEllipse(x,y,dx,dy)
			path.addEllipse(x-dw,y,dx,dy)
			path.addEllipse(x+dw,y,dx,dy)
			path.addEllipse(x,y-dh,dx,dy)
			path.addEllipse(x,y+dh,dx,dy)
			path.closeSubpath()
			return path
		
		def playPath():
			path = QtGui.QPainterPath()
			path.moveTo(wRef/4,hRef/4)
			path.lineTo(wRef/4,3*hRef/4)
			path.lineTo(3*wRef/4,hRef/2)
			path.closeSubpath()
			return path
		
		def blankToothBrush():
			fillGradient = QtGui.QLinearGradient(0, 0, wRef, hRef)
			fillGradient.setColorAt(0.0, self.toothLightColor)
			fillGradient.setColorAt(1.0, self.toothDarkColor)
			return QtGui.QBrush(fillGradient)
		
		def blankToothPen():
			penGradient = QtGui.QLinearGradient(wRef, 0, 0, hRef)
			penGradient.setColorAt(0.0, self.toothLightColor)
			penGradient.setColorAt(1.0, self.toothDarkColor)
			return QtGui.QPen(QtGui.QBrush(penGradient),self.getPenWidth(wRef))
		
		def highlightedToothPen():
			penGradient = QtGui.QLinearGradient(wRef, 0, 0, hRef)
			penGradient.setColorAt(0.0, self.toothHighlightLightColor)
			penGradient.setColorAt(1.0, self.toothHighlightDarkColor)
			return QtGui.QPen(QtGui.QBrush(penGradient),self.getPenWidth(wRef))
		
		def blankToothSize():
			return (wRef-2*self.getPenWidth(wRef),hRef-2*self.getPenWidth(wRef))
		
		def fontSize():
			return hRef * aspect.toothGlyphSizeRatio
		
		def glyphPosition():
			return self.getGlyphPosition(wRef,hRef)
		
		def pictoRect():
			return self.getPictoRect(wRef,hRef)
		
		def iconRect():
			return QtCore.QRect(wRef * aspect.toothIconXRatio, hRef * aspect.toothIconYRatio, wRef * aspect.toothIconSizeRatio, hRef * aspect.toothIconSizeRatio)
		
		def iconInsideRect():
			return QtCore.QRect(wRef * aspect.toothIconInsideXRatio, hRef * aspect.toothIconInsideYRatio, wRef * aspect.toothIconInsideSizeRatio, hRef * aspect.toothIconInsideSizeRatio)
		
		def iconOutsideRect():
			return QtCore.QRect(wRef * aspect.toothIconOutsideXRatio, hRef * aspect.toothIconOutsideYRatio, wRef * aspect.toothIconOutsideSizeRatio, hRef * aspect.toothIconOutsideSizeRatio)
		
		def eraserPen():
			penGradient = QtGui.QLinearGradient(0, 0, wRef, hRef)
			penGradient.setColorAt(0.0, self.eraserPenLightColor)
			penGradient.setColorAt(1.0, self.eraserPenDarkColor)
			return QtGui.QPen(QtGui.QBrush(penGradient),aspect.eraserPenWidth)
		
		def eraserColorBrush():
			fillGradient = QtGui.QLinearGradient(0, 0, wRef, 0)
			fillGradient.setColorAt(0.0, self.eraserPinkColor)
			fillGradient.setColorAt(0.618, self.eraserPinkColor)
			fillGradient.setColorAt(0.62, self.eraserBlueColor)
			fillGradient.setColorAt(1.0, self.eraserBlueColor)
			return QtGui.QBrush(fillGradient)
		
		def eraserVerticalLayerBrush():
			fillGradient = QtGui.QLinearGradient(0, 0, 0, hRef)
			fillGradient.setColorAt(0.0, self.eraserLightDarkening)
			fillGradient.setColorAt(0.1, self.eraserMediumDarkening)
			fillGradient.setColorAt(0.75, self.eraserMediumDarkening)
			fillGradient.setColorAt(1.0, self.eraserDarkDarkening)
			return QtGui.QBrush(fillGradient)
		
		def eraserHorizontalLayerBrush():
			fillGradient = QtGui.QLinearGradient(wRef, 0, 0, 0)
			fillGradient.setColorAt(0.0, self.eraserDarkDarkening)
			fillGradient.setColorAt(0.25, self.eraserMediumDarkening)
			fillGradient.setColorAt(0.85, self.eraserMediumDarkening)
			fillGradient.setColorAt(1.0, self.eraserLightDarkening)
			return QtGui.QBrush(fillGradient)
		
		def eraserSize():
			return (wRef-self.getPenWidth(wRef),hRef-self.getPenWidth(wRef))
		
		def triggerPosition():
			return {
				"." : QtCore.QPoint(wRef*aspect.triggerRatioCX,hRef*aspect.triggerRatioCY),
				"<" : QtCore.QPoint(wRef*aspect.triggerRatioWX,hRef*aspect.triggerRatioWY),
				">" : QtCore.QPoint(wRef*aspect.triggerRatioEX,hRef*aspect.triggerRatioEY),
				"^" : QtCore.QPoint(wRef*aspect.triggerRatioNX,hRef*aspect.triggerRatioNY),
				"v" : QtCore.QPoint(wRef*aspect.triggerRatioSX,hRef*aspect.triggerRatioSY),
			}
		
		#
		def leftShadowBrush():
			fillGradient = QtGui.QLinearGradient(0, 0, wRef, 0)
			fillGradient.setColorAt(aspect.leftShadowLightRatio, QtCore.Qt.transparent)
			fillGradient.setColorAt(aspect.leftShadowDarkRatio, self.toothShadowColor)
			return QtGui.QBrush(fillGradient)
		
		def rightShadowBrush():
			fillGradient = QtGui.QLinearGradient(wRef, 0, 0, 0)
			fillGradient.setColorAt(aspect.rightShadowLightRatio, QtCore.Qt.transparent)
			fillGradient.setColorAt(aspect.rightShadowDarkRatio, self.toothShadowColor)
			return QtGui.QBrush(fillGradient)
		
		def verticalZoomAdjustment():
			return self.verticalZoomAdjustment(yRef)
		
		#
		self.rejectedHeight                   = int(hRef * aspect.rejectedHeightRatio)
		self.normalXRef                       = xRef
		self.normalYRef                       = yRef
		self.normalWRef                       = wRef
		self.normalHRef                       = hRef
		#
		self.normalGlyphPosition              = glyphPosition()
		self.normalPictoRect                  = pictoRect()
		self.normalIconRect                   = iconRect()
		self.normalIconInsideRect            = iconInsideRect()
		self.normalIconOutsideRect            = iconOutsideRect()
		self.normalZoneBrush                  = zoneBrush()
		self.normalCrossBrush                 = crossBrush()
		self.normalFixedZoneBrush             = fixedZoneBrush()
		self.normalZonePaths                  = zonePaths()
		self.normalCrossPath                  = crossPath()
		self.normalPlayPath                   = playPath()
		self.normalPlayBrush                  = playBrush()
		self.normalBlankToothBrush            = blankToothBrush()
		self.normalBlankToothPen              = blankToothPen()
		self.normalHighlightedToothPen        = highlightedToothPen()
		self.normalBlankToothSize             = blankToothSize()
		self.normalFontSize                   = fontSize()
		self.normalEraserPen                  = eraserPen()
		self.normalEraserColorBrush           = eraserColorBrush()
		self.normalEraserVerticalLayerBrush   = eraserVerticalLayerBrush()
		self.normalEraserHorizontalLayerBrush = eraserHorizontalLayerBrush()
		self.normalEraserSize                 = eraserSize()
		self.normalTriggerPosition            = triggerPosition()
		#
		wRef += 2* toothOverlap
		hRef += 2* toothOverlap
		self.toothOverlap                     = toothOverlap
		self.leftShadowBrush                  = leftShadowBrush()
		self.rightShadowBrush                 = rightShadowBrush()
		self.zoomedXRef                       = xRef - toothOverlap
		self.zoomedYRef                       = verticalZoomAdjustment()
		self.zoomedWRef                       = wRef
		self.zoomedHRef                       = hRef
		self.zoomedGlyphPosition              = glyphPosition()
		self.zoomedPictoRect                  = pictoRect()
		self.zoomedIconRect                   = iconRect()
		self.zoomedIconInsideRect            = iconInsideRect()
		self.zoomedIconOutsideRect            = iconOutsideRect()
		self.zoomedZoneBrush                  = zoneBrush()
		self.zoomedCrossBrush                 = crossBrush()
		self.zoomedFixedZoneBrush             = fixedZoneBrush()
		self.zoomedZonePaths                  = zonePaths()
		self.zoomedCrossPath                  = crossPath()
		self.zoomedPlayPath                   = playPath()
		self.zoomedPlayBrush                  = playBrush()
		self.zoomedBlankToothBrush            = blankToothBrush()
		self.zoomedBlankToothPen              = blankToothPen()
		self.zoomedHighlightedToothPen        = highlightedToothPen()
		self.zoomedBlankToothSize             = blankToothSize()
		self.zoomedFontSize                   = fontSize()
		self.zoomedEraserPen                  = eraserPen()
		self.zoomedEraserColorBrush           = eraserColorBrush()
		self.zoomedEraserVerticalLayerBrush   = eraserVerticalLayerBrush()
		self.zoomedEraserHorizontalLayerBrush = eraserHorizontalLayerBrush()
		self.zoomedEraserSize                 = eraserSize()
		self.zoomedTriggerPosition            = triggerPosition()
	
	def updateTopPosition(self):
		self.move(self.xRef,self.yRef+self.clipHeight)
	
	def updateBottomPosition(self):
		self.move(self.xRef,self.yRef-self.clipHeight)
	
	def topVerticalZoomAdjustment(self,y):
		return y
	
	def bottomVerticalZoomAdjustment(self,y):
		return y - 2 * self.toothOverlap
	
	def getTopGlyphPosition(self,wRef,hRef):
		return QtCore.QPoint(wRef * aspect.toothGlyphTopXRatio, hRef * aspect.toothGlyphTopYRatio)
	
	def getBottomGlyphPosition(self,wRef,hRef):
		return QtCore.QPoint(wRef * aspect.toothGlyphBottomXRatio, hRef * aspect.toothGlyphBottomYRatio)
	
	def getTopPictoRect(self,wRef,hRef):
		return QtCore.QRect(wRef * aspect.toothPictoTopXRatio, hRef * aspect.toothPictoTopYRatio, wRef * aspect.toothPictoSizeRatio, hRef * aspect.toothPictoSizeRatio)
	
	def getBottomPictoRect(self,wRef,hRef):
		return QtCore.QRect(wRef * aspect.toothPictoBottomXRatio, hRef * aspect.toothPictoBottomYRatio, wRef * aspect.toothPictoSizeRatio, hRef * aspect.toothPictoSizeRatio)
	
	def getTopScanPosition(self):
		return self.mapToGlobal(QtCore.QPoint(self.normalWRef * aspect.toothScanTopWidthRatio, aspect.toothScanTopHeightDistance))
	
	def getBottomScanPosition(self):
		return self.mapToGlobal(QtCore.QPoint(self.normalWRef * aspect.toothScanBottomWidthRatio, self.normalHRef-1-aspect.toothScanBottomHeightDistance))
	
	def getPenWidth(self,factor):
		return max(aspect.toothMinPenWidth,factor * aspect.toothPenWidthRatio)
	
	
# State specifics
	
	def updateSizeAndPosition(self):
		self.updatePosition()
		self.setFixedSize(self.wRef,self.hRef)
		font = QtGui.QFont(self.font())
		font.setPointSizeF(self.fontSize)
		self.setFont(font)
		self.update()
	
	def updateAsRejected(self):
		self._updateAsNormalSize()
		self.clipHeight = self.rejectedHeight
		self.glyphPen = self.toothGlyphPen
		self.updateSizeAndPosition()
	
	def updateAsTarget(self):
		self._updateAsZoomedSize()
		self.clipHeight = 0
		self.glyphPen = self.toothGlyphPen
		self.updateSizeAndPosition()
	
	def updateAsProgressive(self, i):
		self._updateAsNormalSize()
		self.clipHeight = self.rejectedHeight
		self.glyphPen = self.toothGlyphProgressivePen[i]
		self.updateSizeAndPosition()
	
	def updateAsLeftToTarget(self):
		self.specificPaintShadow = self.paintShadow
		self.shadowBrush         = self.leftShadowBrush
		self.shadowKey           = "left"
		self.lower()
	
	def updateAsRightToTarget(self):
		self.specificPaintShadow = self.paintShadow
		self.shadowBrush         = self.rightShadowBrush
		self.shadowKey           = "right"
		self.lower()
	
	def updateAsFarFromTarget(self):
		self.specificPaintShadow = self.paintNoShadow
		self.shadowKey           = "none"
		self.update()
	
	def _updateAsNormalSize(self):
		self.xRef                                  = self.normalXRef
		self.yRef                                  = self.normalYRef
		self.wRef                                  = self.normalWRef
		self.hRef                                  = self.normalHRef
		self.zoneBrush                             = self.normalZoneBrush
		self.crossBrush                            = self.normalCrossBrush
		self.playBrush                             = self.normalPlayBrush
		self.playPath                              = self.normalPlayPath
		self.fixedZoneBrush                        = self.normalFixedZoneBrush
		self.zonePaths                             = self.normalZonePaths
		self.crossPath                             = self.normalCrossPath
		self.blankToothBrush                       = self.normalBlankToothBrush
		self.blankToothPen                         = self.normalBlankToothPen
		self.highlightedToothPen                   = self.normalHighlightedToothPen
		self.blankToothSize                        = self.normalBlankToothSize
		self.glyphPosition                         = self.normalGlyphPosition
		self.pictoRect                             = self.normalPictoRect
		self.iconRect                              = self.normalIconRect
		self.iconInsideRect                       = self.normalIconInsideRect
		self.iconOutsideRect                       = self.normalIconOutsideRect
		self.fontSize                              = self.normalFontSize
		self.eraserPen                             = self.normalEraserPen
		self.eraserColorBrush                      = self.normalEraserColorBrush
		self.eraserVerticalLayerBrush              = self.normalEraserVerticalLayerBrush
		self.eraserHorizontalLayerBrush            = self.normalEraserHorizontalLayerBrush
		self.eraserSize                            = self.normalEraserSize
		self.triggerPosition                       = self.normalTriggerPosition
		self.blankToothPenDependingOnHighlightment = self.blankToothPen
		self.eraserPenDependingOnHighlightment     = QtCore.Qt.NoPen
	
	def _updateAsZoomedSize(self):
		self.xRef                                  = self.zoomedXRef
		self.yRef                                  = self.zoomedYRef
		self.wRef                                  = self.zoomedWRef
		self.hRef                                  = self.zoomedHRef
		self.zoneBrush                             = self.zoomedZoneBrush
		self.crossBrush                            = self.zoomedCrossBrush
		self.playBrush                             = self.zoomedPlayBrush
		self.playPath                              = self.zoomedPlayPath
		self.fixedZoneBrush                        = self.zoomedFixedZoneBrush
		self.zonePaths                             = self.zoomedZonePaths
		self.crossPath                             = self.zoomedCrossPath
		self.blankToothBrush                       = self.zoomedBlankToothBrush
		self.blankToothPen                         = self.zoomedBlankToothPen
		self.highlightedToothPen                   = self.zoomedHighlightedToothPen
		self.blankToothSize                        = self.zoomedBlankToothSize
		self.glyphPosition                         = self.zoomedGlyphPosition
		self.pictoRect                             = self.zoomedPictoRect
		self.iconRect                              = self.zoomedIconRect
		self.iconInsideRect                       = self.zoomedIconInsideRect
		self.iconOutsideRect                       = self.zoomedIconOutsideRect
		self.fontSize                              = self.zoomedFontSize
		self.eraserPen                             = self.zoomedEraserPen
		self.eraserColorBrush                      = self.zoomedEraserColorBrush
		self.eraserVerticalLayerBrush              = self.zoomedEraserVerticalLayerBrush
		self.eraserHorizontalLayerBrush            = self.zoomedEraserHorizontalLayerBrush
		self.eraserSize                            = self.zoomedEraserSize
		self.triggerPosition                       = self.zoomedTriggerPosition
		self.blankToothPenDependingOnHighlightment = self.blankToothPen
		self.eraserPenDependingOnHighlightment     = QtCore.Qt.NoPen
	
	
# Nature specifics
	
	def updateStrategyAsEraser(self):
		self.glyph              = None
		self.specificPaint      = self.paintEraser
		self.specificPaintPicto = self.paintPicto
		self.specificUpdateZone = self.updateNoZoneTrigger
		self.shortClick         = self.normalEraserAcquirementInManager
		self.longClickStart     = self.longClickStartInManager
		self.longClickStep      = self.stepLongClickVariantInManager
		self.longClickStop      = self.globalAcquirementInManager
		self.autoRepeat         = True
	
	def updateStrategyAsLetter(self,letter):
		self.glyph              = letter
		self.specificPaint      = self.paintNonEraser
		self.specificPaintZone  = (lambda *args: None)
		self.specificPaintPicto = self.paintPicto
		self.specificUpdateZone = self.updateNoZoneTrigger
		self.shortClick         = self.normalLetterAcquirementInManager
		self.longClickStart     = self.longClickStartInManager
		self.longClickStep      = self.stepLongClickVariantInManager
		self.longClickStop      = self.globalAcquirementInManager
		self.autoRepeat         = True
	
	def updateStrategyAsStaticSpace(self):
		self.glyph              = u" "
		self.specificPaint      = self.paintNonEraser
		self.specificPaintZone  = self.paintFixedZone
		self.specificPaintPicto = self.paintPicto
		self.specificUpdateZone = self.updateNoZoneTrigger
		self.shortClick         = self.normalStaticSpaceAcquirementInManager
		self.longClickStart     = self.longClickStartOnSpaceInManager
		self.longClickStep      = self.stepLongClickVariantInManager
		self.longClickStop      = self.globalAcquirementInManager
		self.autoRepeat         = False
	
	def updateStrategyAsStaticFirstCarriageReturn(self):
		self.glyph              = self.carriageReturnGlyph
		self.specificPaint      = self.paintNonEraser
		self.specificPaintZone  = self.paintFixedZone
		self.specificPaintPicto = (lambda *args: None)
		self.specificUpdateZone = self.updateNoZoneTrigger
		self.shortClick         = self.normalStaticFirstCarriageReturnAcquirementInManager
		self.longClickStart     = self.longClickStartOnSpaceInManager
		self.longClickStep      = self.stepLongClickVariantInManager
		self.longClickStop      = self.globalAcquirementInManager
		self.autoRepeat         = False
	
	def updateStrategyAsStaticSecondCarriageReturn(self):
		self.glyph              = self.carriageReturnGlyph
		self.specificPaint      = self.paintNonEraser
		self.specificPaintZone  = self.paintFixedZone
		self.specificPaintPicto = (lambda *args: None)
		self.specificUpdateZone = self.updateNoZoneTrigger
		self.shortClick         = self.normalStaticSecondCarriageReturnAcquirementInManager
		self.longClickStart     = self.longClickStartOnSpaceInManager
		self.longClickStep      = self.stepLongClickVariantInManager
		self.longClickStop      = self.globalAcquirementInManager
		self.autoRepeat         = False
	
	def updateStrategyAsDynamicSpace(self,glyph):
		self.glyph              = glyph
		self.specificPaint      = self.paintNonEraser
		self.specificUpdateZone = self.updateZoneTrigger
		self.specificPaintZone  = self.paintMovingZone
		self.specificPaintPicto = (lambda *args: None)
		self.shortClick         = self.normalDynamicSpaceAcquirementInManager
		self.longClickStart     = self.longClickStartOnSpaceInManager
		self.longClickStep      = self.stepLongClickVariantInManager
		self.longClickStop      = self.globalAcquirementInManager
		self.autoRepeat         = False
	
	def updateStrategyAsCrossSpace(self):
		self.glyph              = u" "
		self.specificPaint      = self.paintNonEraser
		self.specificUpdateZone = self.updateNoZoneTrigger
		self.specificPaintZone  = self.paintCrossZone
		self.specificPaintPicto = (lambda *args: None)
		self.shortClick         = self.normalStaticSpaceAcquirementInManager
		self.longClickStart     = self.longClickStartInManager
		self.longClickStep      = self.stepLongClickVariantInManager
		self.longClickStop      = self.globalAcquirementInManager
		self.autoRepeat         = False
	
	def updateStrategyAsPausedSpace(self):
		self.glyph              = u" "
		self.specificPaint      = self.paintNonEraser
		self.specificPaintZone  = self.paintPlayZone
		self.specificPaintPicto = (lambda *args: None)
		self.specificUpdateZone = self.updateNoZoneTrigger
		self.shortClick         = self.normalStaticSpaceAcquirementInManager
		self.longClickStart     = self.longClickStartOnSpaceInManager
		self.longClickStep      = self.stepLongClickVariantInManager
		self.longClickStop      = self.globalAcquirementInManager
		self.autoRepeat         = False
	
	def updateStrategyAsPendingCommand(self, iconPath):
		self.glyph              = u" "
		self.icon               = QtGui.QIcon(iconPath)
		self.specificPaint      = self.paintNonEraser
		self.specificPaintZone  = self.paintNoZone
		self.specificPaintPicto = self.paintIcon
		self.specificUpdateZone = self.updateZoneCommandTrigger
		self.shortClick         = self.shortClickOnCommandInManager
		self.longClickStart     = self.longClickStartOnCommandInManager
		self.longClickStep      = self.longClickStepOnCommandInManager
		self.longClickStop      = self.longClickStopOnCommandInManager
		self.autoRepeat         = False
	
	def updateStrategyAsAliveCommand(self, iconPath):
		self.glyph              = u" "
		self.icon               = QtGui.QIcon(iconPath)
		self.specificPaint      = self.paintNonEraser
		self.specificPaintZone  = self.paintNoZone
		self.specificPaintPicto = self.paintKillIcon
		self.specificUpdateZone = self.updateZoneCommandTrigger
		self.shortClick         = self.shortClickOnCommandInManager
		self.longClickStart     = self.longClickStartInManager
		self.longClickStep      = self.stepLongClickVariantInManager
		self.longClickStop      = (lambda *args: None)
		self.autoRepeat         = False
	
	def updateZoneTrigger(self, pos):
		self.zoneTrigger = self._getZoneTriggerFromPosition(pos)
		self.updateStrategyAsSpaceInManager(self)
		self.update()
	
	def updateNoZoneTrigger(self, *args):
		return
	
	def _getZoneTriggerFromPosition(self, pos):
		x = float(pos.x())/self.wRef - 0.5
		y = float(pos.y())/self.hRef - 0.5
		if (-0.5 < x < 0.5) and (-0.5 < y < 0.5):
			if x*x+y*y >= self.toothTargetSquaredRadius:
				return quarter[(x<y,x>-y)]
			return "."
		return "." # outer position is equivalent to central position
	
	def getZoneTrigger(self):
		return self._getZoneTriggerFromPosition(self.mapFromGlobal(QtGui.QCursor.pos()))
	
	def getTriggerPosition(self,quarter):
		return self.mapToGlobal(self.triggerPosition[quarter])
	
	def checkYinYangStrategy(self,strategy):
		def updateYinYangTrigger(pos):
			(x,y) = (float(pos.x())/self.wRef,float(pos.y())/self.hRef)
			self.updateCommandIndexInManager(((int(x*self.numberOfActualCommands) + int(y*self.numberOfActualCommands)) % self.numberOfActualCommands) if (0 < x < 1) and (0 < y < 1) else 0)
		
		if strategy:
			self.updateZoneCommandTriggerDependingOnYinYangStrategy = updateYinYangTrigger
		else:
			self.updateZoneCommandTriggerDependingOnYinYangStrategy = (lambda *args: None)
	
	def updateZoneCommandTrigger(self, pos):
		self.updateZoneCommandTriggerDependingOnYinYangStrategy(pos)
	
	def getGlyph(self):
		return self.glyph
	
	def setNumberOfActualCommands(self,n):
		self.numberOfActualCommands = n
	
	
# painting
	
	def paintEraser(self, painter):
		pixmap = QtGui.QPixmap(self.size())
		key = "eraser:%s,%s,%s,%s" % (self.wRef,self.hRef,self.shadowKey,self.highlighted)
		retrieveOrGeneratePixmap(key,pixmap,self.generateEraserPixmap)
		painter.drawPixmap(0,0,pixmap)
		self.specificPaintPicto(painter)
	
	def generateEraserPixmap(self, painter):
		painter.setPen(self.eraserPen)
		corner = self.getPenWidth(self.wRef)/2
		(w,h) = self.eraserSize
		painter.setBrush(self.eraserColorBrush)
		painter.drawRoundRect(corner,corner,w,h,25,70)
		painter.setPen(self.eraserPenDependingOnHighlightment)
		painter.setBrush(self.eraserVerticalLayerBrush)
		painter.drawRoundRect(corner,corner,w,h,25,70)
		painter.setBrush(self.eraserHorizontalLayerBrush)
		painter.drawRoundRect(corner,corner,w,h,25,70)
		self.specificPaintShadow(painter)
	
	def paintNonEraser(self,painter):
		pixmap = QtGui.QPixmap(self.size())
		key = "tooth:%s,%s,%s,%s" % (self.wRef,self.hRef,self.shadowKey,self.highlighted)
		retrieveOrGeneratePixmap(key,pixmap,self.generateBlankToothPixmap)
		painter.drawPixmap(0,0,pixmap)
		self.specificPaintZone(painter)
		self.specificPaintPicto(painter)
		self.paintGlyph(painter)
	
	def paintGlyph(self, painter):
		painter.setPen(self.glyphPen)
		ratio = float(self.wRef - self.glyphPosition.x()) / QtGui.QFontMetrics(self.font()).width(self.glyph)
		if ratio < 1:
			font = QtGui.QFont(self.font())
			font.setPointSizeF(0.8 * font.pointSizeF() * ratio)
			painter.setFont(font)
			painter.drawText(self.glyphPosition,self.glyph)
			painter.setFont(self.font())
		else:
			painter.drawText(self.glyphPosition,self.glyph)
	
	def generateBlankToothPixmap(self, painter):
		painter.setBrush(self.blankToothBrush)
		painter.setPen(self.blankToothPenDependingOnHighlightment)
		painter.drawRoundRect(self.getPenWidth(self.wRef),self.getPenWidth(self.wRef),self.wRef-2*self.getPenWidth(self.wRef),self.hRef-2*self.getPenWidth(self.wRef))
		self.specificPaintShadow(painter)
	
	def paintShadow(self, painter):
		painter.setBrush(self.shadowBrush)
		painter.setPen(QtCore.Qt.NoPen)
		painter.drawRoundRect(0,0,self.wRef,self.hRef)
	
	def paintNoShadow(self, painter):
		pass
	
	def paintMovingZone(self, painter):
		painter.setBrush(self.zoneBrush)
		painter.setPen(QtCore.Qt.NoPen)
		painter.setRenderHint(QtGui.QPainter.Antialiasing)
		painter.drawPath(self.zonePaths[self.zoneTrigger])
		self.paintGlyph(painter)
	
	def paintCrossZone(self, painter):
		painter.setBrush(self.crossBrush)
		painter.setPen(QtCore.Qt.NoPen)
		painter.setRenderHint(QtGui.QPainter.Antialiasing)
		painter.drawPath(self.crossPath)
	
	def paintCentralZone(self, painter):
		painter.setBrush(self.fixedZoneBrush)
		painter.setPen(QtCore.Qt.NoPen)
		painter.setRenderHint(QtGui.QPainter.Antialiasing)
		painter.drawPath(self.zonePaths["."])
	
	def paintPlayZone(self,painter):
		painter.setPen(QtCore.Qt.NoPen)
		painter.setBrush(self.playBrush)
		painter.setRenderHint(QtGui.QPainter.Antialiasing)
		painter.drawPath(self.playPath)
	
	def paintNoZone(self, painter):
		pass
	
	def paintPicto(self, painter):
		self.paintPictoDependingOnPictoStrategy(painter)
	
	def paintIcon(self,painter):
		self.icon.paint(painter,self.iconRect,QtCore.Qt.AlignCenter,QtGui.QIcon.Normal,QtGui.QIcon.On)
	
	def paintKillIcon(self,painter):
		self.icon.paint(painter,self.iconInsideRect,QtCore.Qt.AlignCenter,QtGui.QIcon.Normal,QtGui.QIcon.On)
		self.prohibitIcon.paint(painter,self.iconOutsideRect,QtCore.Qt.AlignCenter,QtGui.QIcon.Normal,QtGui.QIcon.On)
	
	def highlight(self):
		self.highlighted = True
		self.blankToothPenDependingOnHighlightment = self.highlightedToothPen
		self.eraserPenDependingOnHighlightment = self.highlightedToothPen
		self.update()
	
	def lowlight(self):
		self.highlighted = False
		self.blankToothPenDependingOnHighlightment = self.blankToothPen
		self.eraserPenDependingOnHighlightment = QtCore.Qt.NoPen
		self.update()
		
	
	

if __name__ == "__main__":
	from  chewing import main
	main()

