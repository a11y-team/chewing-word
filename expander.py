#!/usr/bin/env python
# encoding: utf-8
#expander.py

import mysimplejson
import re

from aspect import aspect

class Expander:
	
	def __init__(self,mediator):
		self.letMap = mediator.letMap
		self.expansionDict = {}
		self.setPattern(aspect.expansionString)
	
	def setFromString(self,s):
		try:
			self.expansionDict = mysimplejson.loads(s)
			return True
		except ValueError:
			return False
	
	def getAsString(self):
		return mysimplejson.dumps(self.expansionDict,indent=2,sort_keys=True,ensure_ascii=False)
	
	def nonExpandedVersion(self,rawText):
		invertedExpansionDict = dict((x.upper(),a) for (a,x) in self.expansionDict.iteritems())
		escapedExpansions = [re.escape(x) for x in invertedExpansionDict]
		rex = re.compile("(%s)" % "|".join(escapedExpansions),re.IGNORECASE)
		nonExpandedTextAsList = rex.split(rawText)
		for i in range(1,len(nonExpandedTextAsList),2):
			nonExpandedTextAsList[i] = invertedExpansionDict[nonExpandedTextAsList[i].upper()]
		return "".join(nonExpandedTextAsList)
	
	def checkAbbreviationStrategy(self,strategy):
		def defineWhenAbbreviationStrategyIsOn(expandableRichString, expandedText):
			expandableString = "".join([self.letMap[c] for c in expandableRichString]).strip()
			expandableString = expandableString.split()[0] + " "
			poorExpandedText = "".join([self.letMap[c] for c in expandedText]).strip() + " "
			if poorExpandedText == expandableString:
				if expandableString in self.expansionDict:
					del self.expansionDict[expandableString]
			else:
				self.expansionDict[expandableString] = expandedText
		
		def defineWhenAbbreviationStrategyIsOff(*args):
			pass
		
		def getItemWhenAbbreviationStrategyIsOn(word):
			if word in self.expansionDict:
				return [self.expansionDict[word]]
			return []
		
		def getItemWhenAbbreviationStrategyIsOff(*args):
			return []
		
		if strategy:
			self.defineDependingOnAbbreviationStrategy = defineWhenAbbreviationStrategyIsOn
			self.getItemDependingOnAbbreviationStrategy = getItemWhenAbbreviationStrategyIsOn
		else:
			self.defineDependingOnAbbreviationStrategy = defineWhenAbbreviationStrategyIsOff
			self.getItemDependingOnAbbreviationStrategy = getItemWhenAbbreviationStrategyIsOff
	
	def define(self,*args):
		self.defineDependingOnAbbreviationStrategy(*args)
	
	def __getitem__(self,word):
		return self.getItemDependingOnAbbreviationStrategy(word)
	
	def setPattern(self,pattern):
		self.pattern = pattern
	
	def getPattern(self):
		return self.pattern
	
	def isContained(self,poor,rich):
		try:
			return rich in self.expansionDict[poor]
		except KeyError:
			return False

