#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file calculator.py

# builtin stuff
from __future__ import division
import math
import random
import decimal

class Calculator:
	
	def __init__(self, mediator):
		self.getSelectedTextInContentGetter = mediator.contentGetter.getSelectedText
		self.globals = math.__dict__
		self.globals.update(random.__dict__)
	
	def register(self,clipboard):
		self.copyInClipboard = clipboard.setText
	
	def reset(self):
		def getCalculatedValueWhenTypeIsDecimal():
			if self.exponent == 0:
				if self.precision < 0:
					return unicode(int(round(self.value,self.precision + 1)))
				else:
					return unicode(round(self.value,self.precision))
			else:
				if self.precision == - 1:
					return unicode(int(self.value))
					return unicode(int(round(self.value,self.exponent)))
				else:
					return unicode(round(self.value,self.precision-self.exponent))
		
		def getCalculatedValueWhenTypeIsInteger():
			if self.precision < 0:
				p = pow(10,-self.precision)
				return unicode(int(round(float(self.value)/p))*p)
			else:
				return unicode(self.value)
		
		self.value = self.getSelectedValue()
		if type(self.value) == float:
			string = str(self.value)
			self.value = decimal.Decimal(string)
			self.getCalculatedValueDependingOnType = getCalculatedValueWhenTypeIsDecimal
			try:
				i = string.index("e")
				self.exponent = int(string[i+1:])
			except:
				self.exponent = 0
		elif type(self.value) in (int,long):
			self.getCalculatedValueDependingOnType = getCalculatedValueWhenTypeIsInteger
		else:
			self.value = None
	
	def getReplacementString(self,spaces=None):
		s = self.getCalculatedValueDependingOnType()
		if spaces == "suffix":
			return "%s = %s" % (self.getSelectedTextInContentGetter(),s)
		if spaces == "prefix":
			return "%s = %s" % (s,self.getSelectedTextInContentGetter())
		if spaces == "copy":
			self.copyInClipboard(s)
			return self.getSelectedTextInContentGetter()
		if spaces == "evaluate":
			return s
		if spaces == "duplicate":
			return "%s = %s" % (self.getSelectedTextInContentGetter(),self.getSelectedTextInContentGetter())
		return s
	
	def checkEvaluationStrategy(self,strategy):
		def getSelectedValueWhenEvaluationStrategyIsOn():
			try:
				return eval(self.getSelectedTextInContentGetter().replace(" ","").lower(),self.globals,{})
			except:
				return None
		
		def getSelectedValueWhenEvaluationStrategyIsOff():
			return None
		
		if strategy:
			self.getSelectedValueDependingOnEvaluationStrategy = getSelectedValueWhenEvaluationStrategyIsOn
		else:
			self.getSelectedValueDependingOnEvaluationStrategy = getSelectedValueWhenEvaluationStrategyIsOff
	
	def getSelectedValue(self):
		return self.getSelectedValueDependingOnEvaluationStrategy()
	
	def hasValue(self):
		return self.value is not None
	
	def setPrecision(self,precision):
		self.precision = precision
	
	def getPrecision(self):
		return self.precision
	
	

if __name__ == "__main__":
	from  chewing import main
	main()

