#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file chunkgetter.py


# import lettermapping

class ChunkGetter:
	""" Get alternate chunks of spaces and letters, starting from the end.
	
	The space (i.e., non-letter) runs are returned as they appear in the rich text.
	The letter runs are returned along with their poor man version.
	"""
	
	def __init__(self, letMap):
		self.letMap = letMap
	
	def update(self, richText):
		self.genLastChunks = self._lastChunkGenerator(richText)
		self.chunks = []
	
	def __getitem__(self,n):
		for _ in range(len(self.chunks),n+1):
			self.chunks.append(self.genLastChunks.next())
		return self.chunks[n]
	
	def _lastChunkGenerator(self,richText):
		if richText:
			n = len(richText)
			poorChunk = previousPoorChar = ""
			for i in range(n,0,-1):
				currentPoorChar = self.letMap[richText[i-1]]
				if currentPoorChar == " ":
					if previousPoorChar.isalpha():
						yield (richText[i:n],poorChunk)
						n = i
				else:
					if previousPoorChar.isspace():
						yield richText[i:n]
						n = i
						poorChunk = currentPoorChar
					else:
						poorChunk = currentPoorChar + poorChunk
				previousPoorChar = currentPoorChar
			if previousPoorChar.isalpha():
				yield (richText[:n],poorChunk)
			else:
				yield richText[:n]
		while True:
			yield u""

