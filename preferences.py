#!/usr/bin/env python
# encoding: utf-8
#file preferences.py

# builtin stuff
from PyQt4 import QtCore, QtGui
from aspect import aspect
from timer import AutoClickTimer, Timer
import copy
import itertools
import re
import os
from threading import Thread
# home-made stuff
from speechview import SpeechView
from cachemanager import CacheManager
from tts import Tts
import qtgoodies


class Preferences(QtGui.QDialog):
	def __init__(self, rootPrefs, profiles, parent):
		def startupWidget():
			startupStrings = [self.trUtf8("Start with last profile"),self.trUtf8("Ask for profile at startup")]
			self.startupKeys = ["last","ask"]
			self.startupWidget = QtGui.QComboBox()
			for startupString in startupStrings:
				self.startupWidget.addItem(startupString)
			self.startupWidget.setCurrentIndex(self.startupKeys.index(rootPrefs["startupProfile"]))
			return self.startupWidget
		
		def tabWidget():
			def enableAlongWithState(key,function):
				self.connect(widgets[key],QtCore.SIGNAL("stateChanged(int)"),function)
				function(widgets[key].checkState())
			
			interactionWidgets = [
				GenericTag("begin QHBoxLayout"),
				GenericLabel("barrierLabel",self.trUtf8("Crash barriers")),
				GenericCheckBox("verticalBarrierMode",self.trUtf8("on the sides")),
				GenericCheckBox("verticalOpenBarrierMode",self.trUtf8("(open)")),
				GenericCheckBox("horizontalBarrierMode",self.trUtf8("at the top and the bottom")),
				GenericCheckBox("internalBarrierMode",self.trUtf8("inside")),
				GenericTag("end QHBoxLayout"),
				GenericTag("begin QHBoxLayout"),
				GenericInteger("barrierWidthFactor",self.trUtf8("Width factor"),aspect.minBarrierWidthFactor,aspect.maxBarrierWidthFactor),
				BarrierSlider("barrierNumber",self.trUtf8(""),aspect.numberOfBarrierPixmaps),
				GenericTag("end QHBoxLayout"),
				GenericStack("interactionMode",self.trUtf8("Current mode"),aspect.interactionModes,
						[
							[
								GenericComboBox("longClickVariant",self.trUtf8("Long click scrolls through"),aspect.longClickVariantNames),
							],
							[
								GenericInteger("autoClickDelay",self.trUtf8("Auto-click duration"),aspect.minAutoClickDelay,aspect.maxAutoClickDelay,self.trUtf8("00 ms")),
								GenericInteger("autoClickExtraDelay",self.trUtf8("Extra delay to start a sequence"),aspect.minAutoClickExtraDelay,aspect.maxAutoClickExtraDelay,self.trUtf8("00 ms")),
								GenericInteger("autoClickTolerance",self.trUtf8("Radius of tolerance"),aspect.minAutoClickTolerance,aspect.maxAutoClickTolerance,self.trUtf8(" px")),
								GenericSound("autoClickSound", self.trUtf8("Audio feedback"), os.path.join("sounds","double")),
								GenericInteger("autoClickAutonomy",self.trUtf8("Maximal number of repetitions when dwelling"),aspect.minAutoClickAutonomy,aspect.maxAutoClickAutonomy),
							],
							[
								GenericComboBox("scannerName",self.trUtf8("Behaviour"),aspect.scannerNames),
								GenericInteger("scanJump",self.trUtf8("Jump value"),aspect.minJump,aspect.maxJump,self.trUtf8(" letters")),
								TempoSlider("scanTempo",self.trUtf8("Tempo"),aspect.minTempo,aspect.maxTempo,self.trUtf8(" bpm)")),
								GenericSound("scanStartSound", self.trUtf8("Start sound"), os.path.join("sounds","simple")),
								GenericSound("scanStepSound", self.trUtf8("Step sound"), os.path.join("sounds","simple")),
								# SpaceMenu(),
								# EraserMenu(),
							],
						]
					),
			]
			longClickWidgets = [
				GenericInteger("longClickDelay",self.trUtf8("Short click maximal duration"),aspect.minLongClickDelay,aspect.maxLongClickDelay,self.trUtf8("00 ms")),
				GenericComboBox("clickTransformerName",self.trUtf8("Simulated long click"),aspect.clickTransformerNames),
				TempoSlider("longClickTempo",self.trUtf8("Tempo"),aspect.minTempo,aspect.maxTempo,self.trUtf8(" bpm)")),
				GenericTag("begin QHBoxLayout"),
				GenericSound("longClickStartSound", self.trUtf8("Start sound"), os.path.join("sounds","simple")),
				GenericSound("longClickStepSound", self.trUtf8("Step sound"), os.path.join("sounds","simple")),
				GenericTag("end QHBoxLayout"),
			]
			presentationWidgets = [
				TongueRatioSlider("tongueNbMaxChars"),
				GenericInteger("tabSize",self.trUtf8("Tabulation size"),2,32),
				FontEdit(rootPrefs["chewingWidth"]),
				GenericCheckBox("resizeCharMode",self.trUtf8("Resize chars along with the window")),
				GenericCheckBox("progressiveMode",self.trUtf8("Make letters appear progressively")),
				GenericCheckBox("dialAboveMode",self.trUtf8("Place the dial above the editor")),
				GenericCheckBox("hintMode",self.trUtf8("Show phonetic alphabet")),
			]
			keysWidgets = [
				GenericSeparation(self.trUtf8("Editor")),
				GenericCheckBox("contextCapsMode",self.trUtf8("Contextual capitalization")),
				GenericCheckBox("abbreviationMode",self.trUtf8("Use selected text to define abbreviations")),
				GenericSeparation(self.trUtf8("Letters")),
				KeyboardOrderComboBox("keyboardOrder",self.trUtf8("Keyboard order"),aspect.keyboardOrderNames),
				GenericLineEdit("personalizedOrder",self.trUtf8("Personalized order"),self.personalizedOrderValidator),
				GenericSeparation(self.trUtf8("Space")),
				GenericCheckBox("punctuationMode",self.trUtf8("Punctuation on space")),
				GenericCheckBox("globalSpaceMode",self.trUtf8("Click on space to add the predicted accentuation")),
				GenericSeparation(self.trUtf8("Background")),
				GenericCheckBox("undoableMode",self.trUtf8("Click on background to undo/redo last editing actions")),
#				GenericCheckBox("precision"            ,self.trUtf8("")),
			]
			balloonWidgets = [
				GenericCheckBox("predictionMode",self.trUtf8("Show prediction balloon")),
				GenericSeparation(self.trUtf8("Letters")),
				GenericCheckBox("truncatableMode",self.trUtf8("Truncatable predictions")),
				GenericComboBox("leftBehaviour",self.trUtf8("Click before separation"),aspect.leftBehaviours),
				GenericSeparation(self.trUtf8("Space")),
				FavoriteChars("favoriteCharsMode", self.profiles[0]["favoriteCharsVariants"].count(chr(0))+1),
				GenericCheckBox("evaluationMode",self.trUtf8("When the selection is calculable, show the result")),
				GenericSeparation(self.trUtf8("Eraser")),
				GenericCheckBox("correctCapsMode",self.trUtf8("Capitalization correction")),
				GenericCheckBox("correctAcntMode",self.trUtf8("Accentuation correction")),
				# GenericCheckBox("spacingMode"          ,self.trUtf8("Space insertion")),
				# GenericCheckBox("deletionMode"         ,self.trUtf8("Character suppression")),
			]
			persistanceWidgets = [
				GenericLocation("archiveDirectory",self.trUtf8("Location of archives"),self.trUtf8("Choose location for archiving transcriptions")),
				GenericButtonGroup("archiveUnsaved",self.trUtf8("Archive untitled transcriptions"),["silently","on demand","no"],[self.trUtf8("Silently"), self.trUtf8("On demand"), self.trUtf8("No")]),
				GenericButtonGroup("archiveSaved",self.trUtf8("Archive saved transcriptions as well"),["silently","on demand","no"],[self.trUtf8("Silently"), self.trUtf8("On demand"), self.trUtf8("No")]),
			]
			pictosWidgets = [
				GenericCheckBox("pictoMode",self.trUtf8("Enable pictograms")),
				GenericSeparation(self.trUtf8("Keys")),
				GenericCheckBox("toothDirectPictoMode",self.trUtf8("Display direct pictos")),
				GenericCheckBox("toothIndirectPictoMode",self.trUtf8("Display indirect pictos")),
				GenericSeparation(self.trUtf8("Editor")),
				GenericCheckBox("farDirectPictoMode",self.trUtf8("Display direct pictos")),
				GenericCheckBox("farIndirectPictoMode",self.trUtf8("Display indirect pictos")),
				GenericCheckBox("multiplePictosMode",self.trUtf8("Display multiple pictos")),
				FarPictoRatioSlider(),
				GenericSeparation(self.trUtf8("Colors")),
				GenericCheckBox("allVividPictosMode",self.trUtf8("Vivid colors for all pictos")),
			]
			speechWidgets = [
				GenericSeparation(self.trUtf8("Selection")),
				GenericTag("begin QHBoxLayout"),
				GenericLabel("accessLabel",self.trUtf8("Access by")),
				GenericCheckBox("commandsByLongClick",self.trUtf8("long click on Space")),
				GenericCheckBox("commandsByShortClick",self.trUtf8("click on yin-yang symbol")),
				GenericTag("end QHBoxLayout"),
				GenericCheckBox("speechJumpOnPauseMode",self.trUtf8("Jump on Pause symbol when speech starts")),
				GenericCheckBox("spellWordMode",self.trUtf8("Spell word-selection")),
				GenericSeparation(self.trUtf8("Prediction")),
				GenericCheckBox("speechRestrictAudio",self.trUtf8("Feedback restricted to sound files in Audio folder")),
				GenericTag("begin QHBoxLayout"),
				GenericLabel("feedbackLabel",self.trUtf8("Feedback for predictions associated to")),
				GenericCheckBox("speechPictoMode",self.trUtf8("some pictogram")),
				GenericCheckBox("speechNonPictoMode",self.trUtf8("no pictogram")),
				GenericTag("end QHBoxLayout"),
				GenericCheckBox("spellPredictionMode",self.trUtf8("Spell prediction")),
				GenericSeparation(self.trUtf8("Engine")),
				SpeechCommandLine("speechCommandLine",parent),
			]
			if aspect.debugMode:
				persistanceWidgets.append(GenericCheckBox("scanTraceMode",self.trUtf8("Trace scan mode")))
				persistanceWidgets.append(GenericInteger("replayRatio",self.trUtf8("Replay speed percentage"),10,200))
			widget = QtGui.QTabWidget()
			tabs = [
				(keysWidgets,self.trUtf8("General")),
				(interactionWidgets,self.trUtf8("Interaction")),
				(longClickWidgets,self.trUtf8("Long click")),
				(balloonWidgets,self.trUtf8("Balloon")),
				(presentationWidgets,self.trUtf8("Aspect")),
				(pictosWidgets,self.trUtf8("Pictos")),
				(speechWidgets,self.trUtf8("Speech")),
				(persistanceWidgets,self.trUtf8("Persistance")),
			]
			self.tabs = []
			row = self.rootPrefs["profileIndex"]
			for (widgets,title) in tabs:
				tab = GenericTab(widgets)
				self.tabs.append(tab)
				widget.addTab(tab,title)
			widgets = {}
			for (ws,_) in tabs:
				for w in ws:
					try:
						widgets[w.prefKey] = w
					except:
						pass
			for w in interactionWidgets[11].widgets: # FIXME: hard-coded
				try:
					widgets[w.prefKey] = w
				except:
					pass
			widgets["autoClickSound"].registerTimerParameterGetters(widgets["autoClickDelay"].value,widgets["autoClickExtraDelay"].value,widgets["autoClickAutonomy"].value)
			widgets["longClickStartSound"].registerTimerParameterGetters(widgets["autoClickDelay"].value,widgets["autoClickExtraDelay"].value,widgets["autoClickAutonomy"].value)
			widgets["longClickStepSound"].registerTimerParameterGetters(widgets["autoClickDelay"].value,widgets["autoClickExtraDelay"].value,widgets["autoClickAutonomy"].value)
			widgets["scanStartSound"].registerTimerParameterGetters(widgets["autoClickDelay"].value,widgets["autoClickExtraDelay"].value,widgets["autoClickAutonomy"].value)
			widgets["scanStepSound"].registerTimerParameterGetters(widgets["autoClickDelay"].value,widgets["autoClickExtraDelay"].value,widgets["autoClickAutonomy"].value)
			widgets["autoClickSound"].registerMetronomeSetter(lambda *args:None)
			widgets["scanStartSound"].registerMetronomeSetter(widgets["scanTempo"].setStartSound)
			widgets["scanStepSound"].registerMetronomeSetter(widgets["scanTempo"].setStepSound)
			widgets["longClickStartSound"].registerMetronomeSetter(widgets["longClickTempo"].setStartSound)
			widgets["longClickStepSound"].registerMetronomeSetter(widgets["longClickTempo"].setStepSound)
			enableAlongWithState("truncatableMode",(lambda b: widgets["leftBehaviour"].setEnabled(b and widgets["predictionMode"].checkState())))
			enableAlongWithState("predictionMode",(lambda b: widgets["leftBehaviour"].setEnabled(b and widgets["truncatableMode"].checkState())))
			enableAlongWithState("predictionMode",widgets["truncatableMode"].setEnabled)
			enableAlongWithState("predictionMode",widgets["favoriteCharsMode"].setEnabled)
			enableAlongWithState("predictionMode",widgets["evaluationMode"].setEnabled)
			enableAlongWithState("predictionMode",widgets["correctCapsMode"].setEnabled)
			enableAlongWithState("predictionMode",widgets["correctAcntMode"].setEnabled)
			enableAlongWithState("predictionMode",widgets["tongueNbMaxChars"].setEnabled)
			enableAlongWithState("predictionMode",widgets["internalBarrierMode"].setEnabled)
			enableAlongWithState("pictoMode",widgets["toothDirectPictoMode"].setEnabled)
			enableAlongWithState("pictoMode",widgets["toothIndirectPictoMode"].setEnabled)
			enableAlongWithState("pictoMode",widgets["farDirectPictoMode"].setEnabled)
			enableAlongWithState("pictoMode",widgets["farIndirectPictoMode"].setEnabled)
			enableAlongWithState("verticalBarrierMode",widgets["verticalOpenBarrierMode"].setEnabled)
			enableAlongWithState("verticalBarrierMode",(lambda b: widgets["barrierNumber"].setEnabled(widgets["verticalBarrierMode"].checkState() or widgets["internalBarrierMode"].checkState() or widgets["horizontalBarrierMode"].checkState())))
			enableAlongWithState("internalBarrierMode",(lambda b: widgets["barrierNumber"].setEnabled(widgets["verticalBarrierMode"].checkState() or widgets["internalBarrierMode"].checkState() or widgets["horizontalBarrierMode"].checkState())))
			enableAlongWithState("horizontalBarrierMode",(lambda b: widgets["barrierNumber"].setEnabled(widgets["verticalBarrierMode"].checkState() or widgets["internalBarrierMode"].checkState() or widgets["horizontalBarrierMode"].checkState())))
			enableAlongWithState("pictoMode",(lambda b:widgets["multiplePictosMode"].setEnabled(b and (widgets["farDirectPictoMode"].checkState() or widgets["farIndirectPictoMode"].checkState()))))
			enableAlongWithState("pictoMode",(lambda b:widgets["farPictoRatio"].setEnabled(b and (widgets["farDirectPictoMode"].checkState() or widgets["farIndirectPictoMode"].checkState()))))
			enableAlongWithState("pictoMode",widgets["allVividPictosMode"].setEnabled)
			enableAlongWithState("farDirectPictoMode",(lambda b: widgets["multiplePictosMode"].setEnabled(widgets["pictoMode"].checkState() and (b or widgets["farIndirectPictoMode"].checkState()))))
			enableAlongWithState("farIndirectPictoMode",(lambda b: widgets["multiplePictosMode"].setEnabled(widgets["pictoMode"].checkState() and (b or widgets["farDirectPictoMode"].checkState()))))
			enableAlongWithState("farDirectPictoMode",(lambda b: widgets["farPictoRatio"].setEnabled(widgets["pictoMode"].checkState() and (b or widgets["farIndirectPictoMode"].checkState()))))
			enableAlongWithState("farIndirectPictoMode",(lambda b: widgets["farPictoRatio"].setEnabled(widgets["pictoMode"].checkState() and (b or widgets["farDirectPictoMode"].checkState()))))
			enableAlongWithState("speechRestrictAudio",(lambda b: widgets["speechNonPictoMode"].setEnabled(not b)))
			enableAlongWithState("speechRestrictAudio",(lambda b: widgets["speechPictoMode"].setEnabled(not b)))
			enableAlongWithState("speechRestrictAudio",(lambda b: widgets["feedbackLabel"].setEnabled(not b)))
			enableAlongWithState("speechRestrictAudio",(lambda b: widgets["spellPredictionMode"].setEnabled(not b)))
			self.connect(widgets["personalizedOrder"],QtCore.SIGNAL("textEdited(const QString&)"),widgets["keyboardOrder"].setToPersonalized)
			self.connect(widgets["barrierWidthFactor"],QtCore.SIGNAL("valueChanged(int)"),widgets["barrierNumber"].setBarrierWidthFactor)
			self.connect(cancelButton,QtCore.SIGNAL("clicked()"),widgets["speechCommandLine"].stop)
			self.connect(okButton,QtCore.SIGNAL("clicked()"),widgets["speechCommandLine"].stop)
			for tab in self.tabs:
				tab.updatePanelFromPrefs(self.profiles[row])
			return widget
		
		QtGui.QDialog.__init__(self, parent)
		self.rootPrefs = rootPrefs
		self.profiles = profiles
		okButton = QtGui.QPushButton(self.trUtf8("OK"))
		cancelButton = QtGui.QPushButton(self.trUtf8("Cancel"))
		okButton.setDefault(True)
		buttonLayout = QtGui.QHBoxLayout()
		buttonLayout.addStretch(1)
		buttonLayout.addWidget(cancelButton)
		buttonLayout.addWidget(okButton)
		self.startupWidget = startupWidget()
		self.listWidget = ProfilePanel(profiles,rootPrefs["profileIndex"],self)
		self.personalizedOrderValidator = PersonalizedOrderValidator(parent.mediator.utfAlf)
		self.tabWidget = tabWidget()
		listLayout = QtGui.QVBoxLayout()
		listLayout.addWidget(self.startupWidget)
		listLayout.addWidget(self.listWidget)
		panelLayout = QtGui.QVBoxLayout()
		panelLayout.addWidget(self.tabWidget)
		panelLayout.addLayout(buttonLayout)
		mainLayout = QtGui.QHBoxLayout()
		mainLayout.addLayout(listLayout)
		mainLayout.addLayout(panelLayout)
		mainLayout.setStretchFactor(listLayout,0)
		mainLayout.setStretchFactor(panelLayout,1)
		self.setLayout(mainLayout)
		self.setWindowTitle(self.trUtf8("Preferences"))
		self.tabWidget.setCurrentIndex(rootPrefs["lastPrefTab"])
		self.connect(cancelButton, QtCore.SIGNAL("clicked()"), self, QtCore.SLOT("reject()"))
		self.connect(okButton, QtCore.SIGNAL("clicked()"), self, QtCore.SLOT("accept()"))
		self.connect(okButton, QtCore.SIGNAL("clicked()"), self.listWidget.endMulProfile)
		self.connect(okButton, QtCore.SIGNAL("clicked()"), self.rememberRootPrefs)
		self.connect(okButton, QtCore.SIGNAL("clicked()"), self.updatePrefsFromPanel)
	
	def swapPreviousRowWith(self, newRow):
		# print "from %s to %s" % (self.rootPrefs["profileIndex"], newRow)
		self.rootPrefs["profileIndex"] = newRow
		for tab in self.tabs:
			tab.updatePrefsFromPanel()
			tab.updatePanelFromPrefs(self.profiles[newRow])
	
	def updatePrefsFromPanel(self):
		for tab in self.tabs:
			tab.updatePrefsFromPanel()
	
	def updateProfileIndex(self, newRow):
		self.rootPrefs["profileIndex"] = newRow
	
	def rememberRootPrefs(self):
		self.rootPrefs["startupProfile"] = self.startupKeys[self.startupWidget.currentIndex()]
		self.rootPrefs["lastPrefTab"] = self.tabWidget.currentIndex()
		self.rootPrefs["nbProfiles"] = len(self.profiles)
	
	
	
class ProfilePanel(QtGui.QWidget):
	def __init__(self,profiles,profileIndex,parent):
		QtGui.QWidget.__init__(self, parent)
		self.profileList = ProfileList(profiles,profileIndex,self)
		self.addButton = QtGui.QToolButton()
		self.addButton.setIcon(QtGui.QIcon("symbols/button-add.png"))
		self.addButton.setIconSize(QtCore.QSize(19,16))
		self.addButton.setToolTip(self.trUtf8("Duplicate this profile to start creating a new one"))
		self.delButton = QtGui.QToolButton()
		self.delButton.setIcon(QtGui.QIcon("symbols/button-del.png"))
		self.delButton.setIconSize(QtCore.QSize(19,16))
		self.delButton.setEnabled(self.profileList.count()>1)
		self.delButton.setToolTip(self.trUtf8("Suppress this profile"))
		self.mulButton = QtGui.QToolButton()
		self.mulButton.setIcon(QtGui.QIcon("symbols/button-mul.png"))
		self.mulButton.setIconSize(QtCore.QSize(19,16))
		self.mulButton.setEnabled(self.profileList.count()>1)
		self.mulButton.setToolTip(self.trUtf8("Apply next modifications of this profile to all checked profiles"))
		buttonLayout = QtGui.QHBoxLayout()
		buttonLayout.setSpacing(1)
		buttonLayout.addWidget(self.addButton)
		buttonLayout.addWidget(self.delButton)
		buttonLayout.addWidget(self.mulButton)
		buttonLayout.addStretch(1)
		self.connect(self.addButton, QtCore.SIGNAL("clicked()"), self.profileList.addProfile)
		self.connect(self.delButton, QtCore.SIGNAL("clicked()"), self.profileList.delProfile)
		self.connect(self.mulButton, QtCore.SIGNAL("clicked()"), self.beginMulProfile)
		self.connect(self.delButton, QtCore.SIGNAL("clicked()"), self.refreshDelButton)
		self.connect(self.addButton, QtCore.SIGNAL("clicked()"), self.refreshDelButton)
		self.connect(self.delButton, QtCore.SIGNAL("clicked()"), self.refreshMulButton)
		self.connect(self.addButton, QtCore.SIGNAL("clicked()"), self.refreshMulButton)
		layout = QtGui.QVBoxLayout()
		layout.setSpacing(1)
		layout.addWidget(self.profileList)
		layout.addLayout(buttonLayout)
		self.setLayout(layout)
		self.profiles = profiles
		self.parent = parent
		self.pristine = None
	
	def refreshDelButton(self):
		self.delButton.setEnabled(self.profileList.count()>1)
	
	def refreshMulButton(self):
		self.mulButton.setEnabled(self.profileList.count()>1)
	
	def swapPreviousRowWith(self, newRow):
		self.parent.swapPreviousRowWith(newRow)
	
	def updateProfileIndex(self, newRow):
		self.parent.updateProfileIndex(newRow)
	
	def beginMulProfile(self):
		self.addButton.setEnabled(False)
		self.delButton.setEnabled(False)
		self.mulButton.setToolTip(self.trUtf8("Apply last modifications of this profile to all checked profiles"))
		self.profileList.setEnabled(False)
		self.connect(self.mulButton, QtCore.SIGNAL("clicked()"), self.endMulProfile)
		self.disconnect(self.mulButton, QtCore.SIGNAL("clicked()"), self.beginMulProfile)
		self.pristine = copy.deepcopy(self.profileList.profiles[self.profileList.currentRow()])
	
	def endMulProfile(self):
		if self.pristine is None:
			return
		self.mulButton.setToolTip(self.trUtf8("Apply next modifications of this profile to all checked profiles"))
		self.addButton.setEnabled(True)
		self.delButton.setEnabled(True)
		self.profileList.setEnabled(True)
		self.profileList.setFocus()
		self.disconnect(self.mulButton, QtCore.SIGNAL("clicked()"), self.endMulProfile)
		self.connect(self.mulButton, QtCore.SIGNAL("clicked()"), self.beginMulProfile)
		self.parent.updatePrefsFromPanel()
		i = self.profileList.currentRow()
		if self.pristine != self.profiles[i]:
			updates = dict((k,v) for (k,v) in self.profiles[i].iteritems() if self.pristine[k] != v)
			for profile in self.profiles:
				if profile["isInMenu"]:
					profile.update(updates)
		self.pristine = None
	

class ProfileList(QtGui.QListWidget):
	def __init__(self,profiles,profileIndex,parent):
		QtGui.QListWidget.__init__(self,parent)
		for (row,profile) in enumerate(profiles):
			self.addItem(profile["localName"])
			item = self.item(row)
			if profile["isInMenu"]:
				item.setCheckState(QtCore.Qt.Checked)
			else:
				item.setCheckState(QtCore.Qt.Unchecked)
			item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable)
		self.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)
		self.setAlternatingRowColors(True)
		self.setDragDropMode(QtGui.QAbstractItemView.InternalMove)
		self.setEditTriggers(QtGui.QAbstractItemView.AnyKeyPressed | QtGui.QAbstractItemView.DoubleClicked)
		self.setTextElideMode(QtCore.Qt.ElideMiddle)
		self.setCurrentRow(profileIndex)
		self.profiles = profiles
		self.parent = parent
		self.connect(self,QtCore.SIGNAL("currentRowChanged(int)"),self.parent.swapPreviousRowWith)
		self.connect(self,QtCore.SIGNAL("itemChanged(QListWidgetItem*)"),self.updateState)
		self.connect(self,QtCore.SIGNAL("itemSelectionChanged()"),self.reselectCurrentItem)
	
	def addProfile(self):
		i = self.currentRow() + 1
		self.profiles[i-1:i-1] = copy.deepcopy([self.profiles[i-1]])
		newName = self.makeNewName(self.profiles[i-1]["localName"])
		self.profiles[i]["localName"] = newName
		self.insertItem(i,newName)
		self.setCurrentRow(i) # doesn't work if this line is exchanged with the following: don't ask why
		self.item(i).setCheckState(QtCore.Qt.Checked)
		self.item(i).setFlags(self.item(i).flags() | QtCore.Qt.ItemIsEditable)
	
	def delProfile(self):
		i = self.currentRow()
		self.disconnect(self,QtCore.SIGNAL("currentRowChanged(int)"),self.parent.swapPreviousRowWith)
		self.setCurrentRow(-1) # otherwise, doesn't work when the first row is deleted
		self.takeItem(i)
		del self.profiles[i]
		n = self.count()
		self.connect(self,QtCore.SIGNAL("currentRowChanged(int)"),self.parent.swapPreviousRowWith)
		self.setCurrentRow(min(i,n-1))
	
	def makeNewName(self,name):
		names = set([p["localName"] for p in self.profiles])
		name = re.sub(r" - \d+$","",name)
		for i in itertools.count(1):
			newName = "%s - %d" % (name,i)
			if newName not in names:
				return newName
	
	def updateState(self,item):
		i = self.currentRow()
		self.profiles[i]["isInMenu"] = item.checkState() == QtCore.Qt.Checked
		self.profiles[i]["localName"] = unicode(item.text())
	
	def dropEvent(self,event):
		draggedItem = self.item(self.currentRow())
		d = dict((self.item(i),copy.deepcopy(self.profiles[i])) for i in range(self.count()))
		self.disconnect(self,QtCore.SIGNAL("currentRowChanged(int)"),self.parent.swapPreviousRowWith)
		QtGui.QListWidget.dropEvent(self,event)
		for i in range(self.count()):
			self.profiles[i] = d[self.item(i)]
			if self.item(i) == draggedItem:
				self.setCurrentRow(i)
				self.parent.updateProfileIndex(i)
		self.connect(self,QtCore.SIGNAL("currentRowChanged(int)"),self.parent.swapPreviousRowWith)
	
	def reselectCurrentItem(self):
		try:
			self.currentItem().setSelected(True)
		except:
			pass
	
	

class FavoriteChars(QtGui.QComboBox):
	
	def __init__(self, prefKey, nbVariants, parent = None):
		self.prefKey = prefKey
		QtGui.QComboBox.__init__(self)
		self.mode = QtGui.QCheckBox(self.trUtf8("Favorite characters"))
		self.setEditable(True)
		self.setInsertPolicy(self.InsertAtCurrent)
		lineToolTip = '<table border="1">%s</table>' % "".join(["<tr> <td>&nbsp;%s&nbsp;</td> <td>&nbsp;%s&nbsp;</td> </tr>" % (c,s) for (c,s) in aspect.charPaletteList])
		self.setToolTip(lineToolTip)
		self.addItems([""]*nbVariants)
		self.connect(self, QtCore.SIGNAL("editTextChanged(const QString &)"),self.refresh)
		
	def addToLayout(self, mainLayout):
		layout = QtGui.QHBoxLayout()
		layout.addWidget(self.mode)
		layout.addWidget(self)
		layout.setStretchFactor(self.mode,0)
		layout.setStretchFactor(self,1)
		mainLayout.addLayout(layout)
	
	def updateValueFromPrefs(self, prefs):
		l = prefs["favoriteCharsVariants"].split(chr(0))
		for i in range(self.count()):
			self.setItemText(i,l[i])
		self.setCurrentIndex(0)
		self.mode.setChecked(prefs[self.prefKey])
	
	def updatePrefsFromValue(self, prefs):
		prefs["favoriteCharsVariants"] = chr(0).join([unicode(self.itemText(i)) for i in range(self.count())])
		prefs[self.prefKey] = self.mode.isChecked()
	
	def refresh(self,s):
		self.setItemText(self.currentIndex(),s)
	
	def setEnabled(self, b):
		self.mode.setEnabled(b)
		QtGui.QComboBox.setEnabled(self,b)
	

class TongueRatioSlider(QtGui.QSlider):
	
	def __init__(self, prefKey):
		self.prefKey = prefKey
		QtGui.QSlider.__init__(self,QtCore.Qt.Horizontal)
		self.label = QtGui.QLabel(self.trUtf8("Relative tongue size"))
		self.setRange(aspect.tongueNbMaxCharsMin,aspect.tongueNbMaxCharsMax)
		self.setTickPosition(QtGui.QSlider.TicksAbove)
		self.setTickInterval(5)
	
	def addToLayout(self, mainLayout):
		layout = QtGui.QHBoxLayout()
		layout.addWidget(self.label)
		layout.addWidget(self)
		mainLayout.addLayout(layout)
	
 	def updateValueFromPrefs(self, prefs):
		self.setValue(aspect.tongueNbMaxCharsMin + aspect.tongueNbMaxCharsMax - prefs[self.prefKey])
 	
 	def updatePrefsFromValue(self, prefs):
		prefs[self.prefKey] = aspect.tongueNbMaxCharsMin + aspect.tongueNbMaxCharsMax - self.value()
	
	def setEnabled(self, n):
		QtGui.QSlider.setEnabled(self, n == QtCore.Qt.Checked)
		self.label.setEnabled(n == QtCore.Qt.Checked)

class FarPictoRatioSlider(QtGui.QSlider):
	
	def __init__(self):
		QtGui.QSlider.__init__(self,QtCore.Qt.Horizontal)
		self.setRange(aspect.farPictoMinPercentage,aspect.farPictoMaxPercentage)
		self.setTickPosition(QtGui.QSlider.TicksAbove)
		self.setTickInterval(20)
		self.label = QtGui.QLabel(self.trUtf8("Relative picto size"))
		self.prefKey = "farPictoRatio"
	
	def addToLayout(self, mainLayout):
		layout = QtGui.QHBoxLayout()
		layout.addWidget(self.label)
		layout.addWidget(self)
		mainLayout.addLayout(layout)
	
	def updateValueFromPrefs(self, prefs):
		self.setValue(prefs[self.prefKey])
	
	def updatePrefsFromValue(self, prefs):
		prefs[self.prefKey] = self.value()
	
	def setEnabled(self, b):
		self.label.setEnabled(b)
		QtGui.QSlider.setEnabled(self,b)

class FontEdit(QtGui.QLabel):
	
	def __init__(self, chewingWidth):
		QtGui.QLabel.__init__(self)
		self.setFrameStyle(QtGui.QFrame.Panel | QtGui.QFrame.Sunken)
		self.browseButton = QtGui.QPushButton(self.trUtf8("&Modify..."))
		self.connect(self.browseButton, QtCore.SIGNAL("clicked()"), self.browse)
		self.chewingWidth = chewingWidth
	
	def addToLayout(self, mainLayout):
		layout = QtGui.QHBoxLayout()
		layout.addWidget(QtGui.QLabel(self.trUtf8("Editor font")))
		layout.addWidget(self)
		layout.addWidget(self.browseButton)
		layout.setStretchFactor(self,1)
		mainLayout.addLayout(layout)
	
	def browse(self):
		(family,sizeString) = unicode(self.text()).split(",")[:2]
		(font,ok) = QtGui.QFontDialog.getFont(QtGui.QFont(family,float(sizeString)), self)
		if ok:
			self.setText(self.humanReadableFontText(unicode(font.toString())))
	
	def humanReadableFontText(self,fontText):
		(family,sizeString) = fontText.split(",")[:2]
		return "%s, %.1f" % (family,float(sizeString))
	
	def updateValueFromPrefs(self, prefs):
		if prefs["resizeCharMode"]:
			prefs["editorFontSize"] = prefs["editorFontSizeRatio"] * self.chewingWidth
		self.setText(self.humanReadableFontText("%s,%s" % (prefs["editorFontFamily"],prefs["editorFontSize"])))
	
	def updatePrefsFromValue(self, prefs):
		(prefs["editorFontFamily"],sizeString) = unicode(self.text()).split(",")[:2]
		prefs["editorFontSize"] = float(sizeString)
		prefs["editorFontSizeRatio"] = prefs["editorFontSize"] / self.chewingWidth
	

class GenericLocation(QtGui.QGroupBox):
	
	def __init__(self, prefKey, title, message, parent = None):
		QtGui.QGroupBox.__init__(self)
		self.setTitle(title)
		self.message = message
		self.prefKey = prefKey
		self.location = QtGui.QLabel()
		self.location.setFrameStyle(QtGui.QFrame.Panel | QtGui.QFrame.Sunken)
		browseButton = QtGui.QPushButton(self.trUtf8("&Modify..."))
		self.connect(browseButton, QtCore.SIGNAL("clicked()"), self.browse)
		layout = QtGui.QHBoxLayout()
		layout.addWidget(self.location)
		layout.addWidget(browseButton)
		layout.setStretchFactor(self.location,1)
		self.setLayout(layout)
	
	def addToLayout(self, mainLayout):
		mainLayout.addWidget(self)
	
	def browse(self):
		directory = QtGui.QFileDialog.getExistingDirectory(self, self.message, self.location.text())
		if directory:
			self.location.setText(QtCore.QDir.convertSeparators(directory))
	
	def updateValueFromPrefs(self, prefs):
		self.location.setText(prefs[self.prefKey])
	
	def updatePrefsFromValue(self, prefs):
		prefs[self.prefKey] = str(self.location.text())
	

class GenericButtonGroup(QtGui.QGroupBox):
	
	def __init__(self, prefKey, title, keys, labels, parent = None):
		QtGui.QGroupBox.__init__(self,title)
		self.buttons = [QtGui.QRadioButton(label) for label in labels]
		layout = QtGui.QVBoxLayout()
		for b in self.buttons:
			layout.addWidget(b)
		self.setLayout(layout)
		self.keys = keys
		self.prefKey = prefKey
	
	def addToLayout(self, mainLayout):
		mainLayout.addWidget(self)
	
	def updateValueFromPrefs(self, prefs):
		self.buttons[self.keys.index(prefs[self.prefKey])].setChecked(True)
	
	def updatePrefsFromValue(self, prefs):
		prefs[self.prefKey] = self.keys[[b.isChecked() for b in self.buttons].index(True)]
	

class GenericSeparation(QtGui.QLabel):
	
	def addToLayout(self, mainLayout):
		layout = QtGui.QHBoxLayout()
		frame = QtGui.QFrame()
		frame.setFrameStyle(QtGui.QFrame.HLine)
		frame.setSizePolicy(QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Fixed))
		layout.addWidget(frame)
		layout.addWidget(self)
		self.setEnabled(False)
		mainLayout.addLayout(layout)
	
	def updateValueFromPrefs(self, *args):
		pass
	
	def updatePrefsFromValue(self, *args):
		pass
	

class GenericLabel(QtGui.QLabel):
	
	def __init__(self, prefKey, title):
		self.prefKey = prefKey
		QtGui.QLabel.__init__(self,title)
	
	def addToLayout(self, mainLayout):
		mainLayout.addWidget(self)
	
	def updateValueFromPrefs(self, *args):
		pass
	
	def updatePrefsFromValue(self, *args):
		pass
	

class GenericTag:
	
	def __init__(self,tag):
		self.tag = tag
	
	def getTag(self):
		return self.tag
	
	def updateValueFromPrefs(self, *args):
		pass
	
	def updatePrefsFromValue(self, *args):
		pass

class GenericInteger(QtGui.QSlider):
	
	def __init__(self,prefKey,title,minValue,maxValue,suffix = ""):
		QtGui.QSlider.__init__(self,QtCore.Qt.Horizontal)
		self.setRange(minValue,maxValue)
		self.spinBox = QtGui.QSpinBox()
		self.spinBox.setRange(minValue,maxValue)
		self.spinBox.setSuffix(suffix)
		self.spinBox.setFrame(False)
		self.spinBox.setCorrectionMode(QtGui.QSpinBox.CorrectToNearestValue)
		self.connect(self, QtCore.SIGNAL("valueChanged(int)"), self.spinBox, QtCore.SLOT("setValue(int)"))
		self.connect(self.spinBox, QtCore.SIGNAL("valueChanged(int)"), self.setValue)
		self.label = QtGui.QLabel(title)
		self.prefKey = prefKey
	
	def addToLayout(self, mainLayout):
		layout = QtGui.QHBoxLayout()
		layout.addWidget(self.label)
		layout.addWidget(self)
		layout.addWidget(self.spinBox)
		mainLayout.addLayout(layout)
	
	def updateValueFromPrefs(self, prefs):
		self.spinBox.setValue(prefs[self.prefKey])
		self.setValue(prefs[self.prefKey])
	
	def updatePrefsFromValue(self, prefs):
		prefs[self.prefKey] = self.value()
	
	def setEnabled(self, n):
		QtGui.QSlider.setEnabled(self, n == QtCore.Qt.Checked)
		self.label.setEnabled(n == QtCore.Qt.Checked)
		self.spinBox.setEnabled(n == QtCore.Qt.Checked)

class TempoSlider(GenericInteger):
	
	def __init__(self,*args):
		GenericInteger.__init__(self,*args)
		self.spinBox.setFixedWidth(QtGui.QFontMetrics(self.spinBox.font()).width(aspect.longestTempoString))
		self.setMouseTracking(True)
		self.spinBox.setMouseTracking(True)
		self.spinBox.enterEvent = self.enterEvent
		self.spinBox.leaveEvent = self.leaveEvent
		self.metronome = Timer(singleShot=False)
		self.metronome.setAudibleStrategy("stop")
		self.connect(self, QtCore.SIGNAL("valueChanged(int)"), self.setPrefix)
		self.connect(self, QtCore.SIGNAL("valueChanged(int)"), self.metronome.setTempo)
	
	def updateValueFromPrefs(self, prefs):
		self.spinBox.setValue(prefs[self.prefKey])
		self.setValue(prefs[self.prefKey])
		self.setPrefix(prefs[self.prefKey])
		self.metronome.setTempo(prefs[self.prefKey])
	
	def setPrefix(self, bpm):
		for (string,bpmMin,bpmMax) in aspect.tempi:
			if bpmMin <= bpm < bpmMax:
				self.spinBox.setPrefix("%s (" % string)
				return
	
	def enterEvent(self, event):
		self.startSound.play()
		self.metronome.start(lambda *args: None)
	
	def leaveEvent(self, event):
		self.metronome.stop()
	
	def setStartSound(self, startSound):
		self.startSound = qtgoodies.soundFactory(startSound)
	
	def setStepSound(self, stepSound):
		self.metronome.setSound(stepSound)
	

class GenericTab(QtGui.QWidget):
	
	def __init__(self, widgets, parent = None):
		QtGui.QWidget.__init__(self, parent)
		self.widgets = widgets
		currentLayout = mainLayout = QtGui.QVBoxLayout()
		for widget in self.widgets:
			try:
				widget.addToLayout(currentLayout)
			except:
				if widget.getTag() == "begin QHBoxLayout":
					currentLayout = QtGui.QHBoxLayout()
				elif widget.getTag() == "end QHBoxLayout":
					# currentLayout.addStretch(1)
					mainLayout.addLayout(currentLayout)
					currentLayout = mainLayout
		mainLayout.addStretch(1)
		self.setLayout(mainLayout)
	
	def updatePanelFromPrefs(self, prefs):
		for widget in self.widgets:
			widget.updateValueFromPrefs(prefs)
		self.prefs = prefs
	
	def updatePrefsFromPanel(self):
		for widget in self.widgets:
			widget.updatePrefsFromValue(self.prefs)
	

class GenericStack(QtGui.QStackedWidget):
	
	def __init__(self, prefKey, label, keysTitles, widgetsList, parent = None):
		QtGui.QStackedWidget.__init__(self, parent)
		self.widgets = []
		self.comboBox = QtGui.QComboBox()
		self.layout = QtGui.QVBoxLayout()
		layout = QtGui.QHBoxLayout()
		layout.addWidget(QtGui.QLabel(label))
		layout.addWidget(self.comboBox)
		layout.setStretchFactor(self.comboBox,1)
		self.layout.addLayout(layout)
		for (title,widgets) in zip(keysTitles["titles"],widgetsList):
			self.comboBox.addItem(title)
			layout = QtGui.QVBoxLayout()
			for widget in widgets:
				self.widgets.append(widget)
				widget.addToLayout(layout)
			layout.addStretch(1)
			groupBox = QtGui.QGroupBox()
			groupBox.setLayout(layout)
			self.addWidget(groupBox)
		self.connect(self.comboBox, QtCore.SIGNAL("activated(int)"), self, QtCore.SLOT("setCurrentIndex(int)"))
		self.layout.addWidget(self)
		self.keys = keysTitles["keys"]
		self.prefKey = prefKey
	
	def addToLayout(self, mainLayout):
		mainLayout.addLayout(self.layout)
	
	def updateValueFromPrefs(self, prefs):
		self.comboBox.setCurrentIndex(self.keys.index(prefs[self.prefKey]))
		self.setCurrentIndex(self.keys.index(prefs[self.prefKey]))
		for widget in self.widgets:
			widget.updateValueFromPrefs(prefs)
	
	def updatePrefsFromValue(self, prefs):
		prefs[self.prefKey] = self.keys[self.comboBox.currentIndex()]
		for widget in self.widgets:
			widget.updatePrefsFromValue(prefs)
	

class GenericComboBox(QtGui.QComboBox):
	def __init__(self, prefKey, label, keysTitles):
		QtGui.QComboBox.__init__(self)
		self.label = QtGui.QLabel(label)
		self.layout = QtGui.QHBoxLayout()
		self.layout.addWidget(self.label)
		self.layout.addWidget(self)
		self.addItems(keysTitles["titles"])
		self.keys = keysTitles["keys"]
		self.prefKey = prefKey
	
	def addToLayout(self, mainLayout):
		mainLayout.addLayout(self.layout)
	
	def updateValueFromPrefs(self, prefs):
		self.setCurrentIndex(self.keys.index(prefs[self.prefKey]))
	
	def updatePrefsFromValue(self, prefs):
		prefs[self.prefKey] = self.keys[self.currentIndex()]
	
	def setEnabled(self, n):
		QtGui.QComboBox.setEnabled(self, n == QtCore.Qt.Checked)
		self.label.setEnabled(n == QtCore.Qt.Checked)
	

class KeyboardOrderComboBox(GenericComboBox):
	
	def setToPersonalized(self):
		self.setCurrentIndex(self.keys.index("personalized"))

class GenericSound(QtGui.QComboBox):
	
	def __init__(self, prefKey, label, directory):
		QtGui.QComboBox.__init__(self)
		self.label = QtGui.QLabel(label)
		self.layout = QtGui.QHBoxLayout()
		self.layout.addWidget(self.label)
		self.layout.addWidget(self)
		self.names = [""]+[name[:-4] for name in list(os.walk(directory))[0][2] if name.lower().endswith(".wav")]
		self.addItems(self.names)
		self.prefKey = prefKey
		self.timer = AutoClickTimer()
		self.directory = directory
		self.setMouseTracking(True)
		self.connect(self, QtCore.SIGNAL("currentIndexChanged(int)"),self.refresh)
	
	def addToLayout(self, mainLayout):
		mainLayout.addLayout(self.layout)
	
	def updateValueFromPrefs(self, prefs):
		if prefs[self.prefKey] != "" and prefs[self.prefKey] in self.names:
			self.setCurrentIndex(self.names.index(prefs[self.prefKey]))
			self.setSoundInMetronome(self.getCurrentSound())
		else:
			self.setCurrentIndex(0)
			self.setSoundInMetronome(aspect.defaultMetronomeSound)
	
	def updatePrefsFromValue(self, prefs):
		prefs[self.prefKey] = self.names[self.currentIndex()]
	
	def registerTimerParameterGetters(self,getAutoClickDelay,getAutoClickExtraDelay,getAutoClickAutonomy):
		self.getAutoClickDelay = getAutoClickDelay
		self.getAutoClickExtraDelay = getAutoClickExtraDelay
		self.getAutoClickAutonomy = getAutoClickAutonomy
	
	def registerMetronomeSetter(self,metronomeSetter):
		self.setSoundInMetronome = metronomeSetter
	
	def mouseMoveEvent(self, event):
		self.timer.setSound(self.getCurrentSound())
		self.timer.setIntervals(100 * self.getAutoClickDelay(), 100 * self.getAutoClickExtraDelay())
		self.timer.setAutonomy(self.getAutoClickAutonomy())
		self.timer.setAudibleStrategy("stop" if self.currentIndex()>0 else None)
		self.timer.start(self, event)
	
	def getCurrentSound(self):
		return "%s/%s.wav" % (self.directory,self.names[self.currentIndex()])
	
	def refresh(self, i):
		if i:
			self.setSoundInMetronome(self.getCurrentSound())
		else:
			self.setSoundInMetronome(aspect.defaultMetronomeSound)
	
	def autoClickEvent(self):
		pass
	
	def leaveEvent(self, _):
		self.timer.stop()

class GenericCheckBox(QtGui.QCheckBox):
	
	def __init__(self, prefKey, text, parent = None):
		QtGui.QCheckBox.__init__(self, text, parent)
		self.prefKey = prefKey
	
	def addToLayout(self, mainLayout):
		mainLayout.addWidget(self)
	
	def updateValueFromPrefs(self, prefs):
		self.setChecked(prefs[self.prefKey])
	
	def updatePrefsFromValue(self, prefs):
		prefs[self.prefKey] = self.isChecked()

class GenericLineEdit(QtGui.QLineEdit):
	
	def __init__(self, prefKey, title, validator, parent = None):
		QtGui.QLineEdit.__init__(self)
		self.prefKey = prefKey
		self.title = title
		self.setValidator(validator)
	
	def addToLayout(self, mainLayout):
		layout = QtGui.QHBoxLayout()
		layout.addWidget(QtGui.QLabel(self.title))
		layout.addWidget(self)
		mainLayout.addLayout(layout)
	
	def updateValueFromPrefs(self, prefs):
		self.setText(prefs[self.prefKey])
	
	def updatePrefsFromValue(self, prefs):
		prefs[self.prefKey] = unicode(self.text())

class PersonalizedOrderValidator(QtGui.QValidator):
	
	def __init__(self, utfAlf):
		QtGui.QValidator.__init__(self, None)
		self.validChars = u" <" + u"".join(utfAlf)
	
	def validate(self,s,pos):
		text = unicode(s).upper()
		remainingPossibleChars = list(self.validChars)
		for c in text:
			if c not in self.validChars:
				return (self.Invalid,pos)
			if c not in remainingPossibleChars:
				return (self.Intermediate,pos)
			remainingPossibleChars.remove(c)
		if remainingPossibleChars:
			return (self.Intermediate,pos)
		return (self.Acceptable,pos)
	
	def fixup(self,inputString):
		text = unicode(inputString).upper()
		inputString.remove(0, len(inputString)) # QString is mutable and should be modified
		result = u""
		for c in text + self.validChars:
			if c in self.validChars and c not in result:
				result += c
		inputString.append(result)
	

class BarrierSlider(GenericInteger):
	
	def __init__(self,prefKey,title,nbValues):
		GenericInteger.__init__(self,prefKey,title,0,nbValues-1)
		self.brushes = [QtGui.QBrush(QtGui.QPixmap("symbols/" + aspect.barrierPixmapFileName % i)) for i in range(nbValues)]
	
	def paintEvent(self,_):
		barrierHeight = self.barrierWidthFactor * self.height() / aspect.maxBarrierWidthFactor
		painter = QtGui.QPainter()
		painter.begin(self)
		painter.setPen(QtCore.Qt.NoPen)
		painter.setBrush(self.brushes[self.value()])
		painter.drawRect(0,(self.height()-barrierHeight)/2,self.width(),barrierHeight)
		painter.end()
	
	def setBarrierWidthFactor(self, barrierWidthFactor):
		self.barrierWidthFactor = barrierWidthFactor
		self.update()
	
	def updateValueFromPrefs(self, prefs):
		GenericInteger.updateValueFromPrefs(self,prefs)
		self.barrierWidthFactor = prefs["barrierWidthFactor"]
	
class SpeechCommandLine(QtGui.QTextEdit):
	
	def __init__(self, prefKey, parent):
		QtGui.QTextEdit.__init__(self)
		self.prefKey = prefKey
		self.speechView = parent.speechView
		self.setAcceptRichText(False)
		self.setMaximumHeight(self.minimumSizeHint().height())
		self.setCurrentFont(QtGui.QFont(aspect.commandLineFontFamily,aspect.commandLineFontSize))
		self.setWordWrapMode(QtGui.QTextOption.WrapAnywhere)
		(anc,pos) = parent.mediator.editorGetter.getAnchorAndPosition()
		parent.mediator.selector.reset(parent.mediator.editorGetter.getAll())
		(anc,pos) = parent.mediator.selector.autoSelection(anc,pos)
		s = parent.mediator.selector.getTextBetween(anc,pos)
		self.speechTestLine = (s if re.search(r"(?u)\w",s) else aspect.speechTestLine)
		palette = QtGui.QPalette()
		palette.setColor(palette.Text, QtGui.QColor(aspect.commandLineForegroundColor))
		palette.setColor(palette.HighlightedText, QtGui.QColor(aspect.commandLineBackgroundColor))
		palette.setColor(palette.Base, QtGui.QColor(aspect.commandLineBackgroundColor))
		palette.setColor(palette.Highlight, QtGui.QColor(aspect.commandLineSelectionColor))
		self.viewport().setPalette(palette)
		self.setPalette(palette)
		self.speechTestButton = QtGui.QToolButton()
		self.speechTestButton.setIconSize(QtCore.QSize(aspect.speechTestIconHeight,aspect.speechTestIconWidth))
		self.speechTestButton.setIcon(QtGui.QIcon("symbols/speech.png"))
		self.connect(self, QtCore.SIGNAL("textChanged()"), self.stop)
		self.connect(self, QtCore.SIGNAL("textChanged()"), self.validate)
		self.connect(self.speechTestButton, QtCore.SIGNAL("clicked()"), self.speechTest)
		self.connect(self, QtCore.SIGNAL("play"), self.speechView.speak)
		self.cache = CacheManager("Preferences")
		self.tts = Tts()
		self.tts.setCache(self.cache)
		self.waitingIcons = [QtGui.QIcon("symbols/prohibit-speech-%s.png" % i) for i in range(aspect.speechTestNumberOfFrames)]
		self.initialWaitingIcon = self.waitingIcons[0]
		self.timer = Timer(singleShot=False)
		self.timer.setTempo(aspect.speechTestRotorTempo)
	
	def addToLayout(self, mainLayout):
		layout = QtGui.QHBoxLayout()
		layout.addWidget(self.speechTestButton)
		layout.addWidget(self)
		mainLayout.addLayout(layout)
	
	def enableTestButtonAsPossible(self):
		self.speechTestButton.setEnabled(True)
		self.speechTestDependingOnSpeechTestStrategy = self.speak
		self.speechTestButton.setToolTip(self.trUtf8("Your command line may be valid. Click to make sure."))
	
	def enableTestButtonAsRestoreDefault(self):
		self.speechTestButton.setEnabled(True)
		self.speechTestDependingOnSpeechTestStrategy = self.restoreDefault
		self.speechTestButton.setToolTip(self.trUtf8("Click to restore the default command."))
	
	def speechTest(self):
		self.speechTestDependingOnSpeechTestStrategy()
	
	def speak(self):
		def threadableSpeak():
			if self.alive:
				self.tts.prepareToSpeak(self.speechTestLine,pathBaseName)
				self.timerShouldStop = True
				if self.alive:
					self.speechView.setSource(pathBaseName)
					if self.alive:
						self.emit(QtCore.SIGNAL("play"))
		
		self.speechTestDependingOnSpeechTestStrategy = self.stop
		self.speechTestButton.setToolTip(self.trUtf8("Click to stop."))
		self.tts.setCommandLine(unicode(self.toPlainText()))
		self.speechView.setOutputExtension(self.tts.getOutputExtension())
		pathBaseName = "%s/%s" % (self.cache.getAbsolutePath(),self.tts.getHash(self.speechTestLine))
		self.alive = True
		self.timerShouldStop = False
		self.refreshWaitingIcon()
		self.timer.start(self.refreshWaitingIcon)
		self.connect(self.speechView.player, QtCore.SIGNAL("finished()"), self.finished)
		Thread(target=threadableSpeak).start()
	
	def refreshWaitingIcon(self):
		icon = self.waitingIcons[0]
		self.speechTestButton.setIcon(icon)
		if self.timerShouldStop and icon is self.initialWaitingIcon:
			self.timer.stop()
		else:
			self.waitingIcons = self.waitingIcons[1:] + [icon]
	
	def restoreDefault(self):
		self.setPlainText(aspect.defaultSpeechCommandLine)
	
	def disableSpeechTextButton(self, message):
		self.speechTestButton.setEnabled(False)
		self.speechTestButton.setToolTip(message)
	
	def updateValueFromPrefs(self, prefs):
		self.setPlainText(prefs[self.prefKey])
	
	def updatePrefsFromValue(self, prefs):
		if self.toPlainText() == "":
			self.restoreDefault()
		prefs[self.prefKey] = unicode(self.toPlainText())
	
	def validate(self):
		s = unicode(self.toPlainText())
		if s:
			if self.tts.extractEncoding(s):
				outputExtension = self.tts.extractOutputExtension(s)
				if outputExtension:
					if self.speechView.inSupportedAudioFormats(outputExtension):
						self.enableTestButtonAsPossible()
					else:
						self.disableSpeechTextButton(self.trUtf8('<p>ERROR: underlying Phonon engine does not support audio extension "%1" on your system.<br>Accepted formats: %2.</p>').arg(outputExtension).arg(self.speechView.getSupportedAudioFormatsForPrint()))
				else:
					self.disableSpeechTextButton(self.trUtf8('ERROR: the command line should contain "%(output)s" followed by a non-empty extension.'))
			else:
				self.disableSpeechTextButton(self.trUtf8('ERROR: the command line should contain "%(xxxxxinput)s.txt", where xxxxx is a text encoding (ascii, macroman, latin1, etc.), utf8 by default.'))
		else:
			self.enableTestButtonAsRestoreDefault()
	
	def checkSkipToPreviousSentenceStrategy(self):
		pass
	
	def finished(self):
		self.alive = False
		self.speechTestButton.setIcon(QtGui.QIcon("symbols/speech.png"))
		self.speechTestButton.setEnabled(True)
		self.speechTestDependingOnSpeechTestStrategy = self.speak
		self.speechTestButton.setToolTip(self.trUtf8("Your command line may be valid. Click to make sure."))
		self.cache.removeAll()
		self.disconnect(self.speechView.player, QtCore.SIGNAL("finished()"), self.finished)
	
	def stop(self):
		self.speechView.stopSpeaking()
		self.finished()
		self.timer.stop()
		
	

if __name__ == "__main__":
	from  chewing import main
	main()
