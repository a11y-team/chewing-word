#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file aspect.py

# builtin stuff
import os
import sys
import locale
import mysimplejson

def newProfile(updates={}):
	d = {
			"isInMenu"                : True,
			"tongueNbMaxChars"        : 25,
			"tabSize"                 : 4,
			"resizeCharMode"          : True,
			"progressiveMode"         : True,
			"dialAboveMode"           : False,
			"hintMode"                : False,
			"voiceMode"               : False,
			"longClickDelay"          : 5, # tenth seconds
			"interactionMode"         : "standard",
			"archiveDirectory"        : u"", # platform dependant
			"archiveUnsaved"          : "silently",
			"archiveSaved"            : "silently",
			"abbreviationMode"        : True,
			"precision"               : 2,
			"punctuationMode"         : True,
			"favoriteCharsMode"       : True,
			"favoriteCharsVariants"   : u" 1234567890,.+-*/=%°\x00 &§÷≠<≤≥>\|≈€$£¥@©®™\x00\x00",
			"evaluationMode"          : True,
			"correctCapsMode"         : True,
			"correctAcntMode"         : True,
			"spacingMode"             : False,
			"deletionMode"            : False,
			"contextCapsMode"         : True,
			"undoableMode"            : True,
			"keyboardOrder"           : "dynamic",
			"personalizedOrder"       : u"",
			"predictionMode"          : True,
			"truncatableMode"         : True,
			"globalSpaceMode"         : False,
			"leftBehaviour"           : "truncate",
			"autoClickDelay"          : 4,
			"autoClickExtraDelay"     : 3,
			"autoClickTolerance"      : 0,
			"autoClickAutonomy"       : 30,
			"autoClickSound"          : "Click 080",
			"scanStartSound"          : "Wood 028",
			"scanStepSound"           : "Wood 023",
			"longClickStartSound"     : "",
			"longClickStepSound"      : "",
			"scanTempo"               : 60,
			"longClickTempo"          : 60,
			"scanJump"                : 6,
			"scannerName"             : "jump on all letters",
			"clickTransformerName"    : "normal",
			"longClickVariant"        : "accentuation",
			"scanTraceMode"           : False,
			"replayRatio"             : 100,
			"pictoMode"               : False,
			"toothDirectPictoMode"    : True,
			"toothIndirectPictoMode"  : True,
			"farDirectPictoMode"      : True,
			"farIndirectPictoMode"    : True,
			"multiplePictosMode"      : True,
			"allVividPictosMode"      : False,
			"farPictoRatio"           : 50,
			"speechSpeed"             : 170,
			"speechNonPictoMode"      : False,
			"speechPictoMode"         : False,
			"speechRestrictAudio"     : True,
			"speechJumpOnPauseMode"   : True,
			"spellPredictionMode"     : False,
			"spellWordMode"           : True,
			"commandsByLongClick"     : True,
			"commandsByShortClick"    : False,
			"editorFontSizeRatio"     : 0.04,
			"verticalBarrierMode"     : False,
			"verticalOpenBarrierMode" : False,
			"horizontalBarrierMode"   : False,
			"internalBarrierMode"     : False,
			"barrierWidthFactor"      : 1,
			"barrierNumber"           : 0,
	}
	d.update(updates)
	return d

class Aspect:
	
	applicationName              = "Chewing Word"
	defaultPreviousVersion       = u"before 1.1"
	version                      = u"1.5"
	revision                     = "1040"
	expansionString              = "(=%s)"
	defaultProfiles              = [
		newProfile({
			"localName"               : "technophobe",
			"isInMenu"                : False,
			"progressiveMode"         : False,
			"abbreviationMode"        : False,
			"punctuationMode"         : False,
			"evaluationMode"          : False,
			"correctCapsMode"         : False,
			"correctAcntMode"         : False,
			"undoableMode"            : False,
			"keyboardOrder"           : "static",
			"truncatableMode"         : False,
			"predictionMode"          : False,
			"longClickDelay"          : 12, # tenth seconds
			"longClickVariant"        : "nothing",
		}),       
		newProfile({
			"localName"               : "beginner",
			"progressiveMode"         : False,
			"abbreviationMode"        : False,
			"punctuationMode"         : False,
			"evaluationMode"          : False,
			"correctAcntMode"         : False,
			"undoableMode"            : False,
			"truncatableMode"         : False,
			"globalSpaceMode"         : True,
			"longClickVariant"        : "nothing",
		}),       
		newProfile({
			"localName"               : "moveClick",
		}),
		newProfile({
			"localName"               : "moveClickSecure",
			"verticalBarrierMode"     : True,
			"horizontalBarrierMode"   : True,
		}),
		newProfile({
			"localName"               : "moveSlow",
			"interactionMode"         : "auto-click",
			"commandsByLongClick"     : False,
			"leftBehaviour"           : "delete",
			"autoClickAudible"        : True,
			"autoClickDelay"          : 10,
			"autoClickExtraDelay"     : 5,
			"undoableMode"            : False,
			"globalSpaceMode"         : True,
		}),       
		newProfile({
			"localName"               : "move",
			"interactionMode"         : "auto-click",
			"commandsByLongClick"     : False,
			"leftBehaviour"           : "whole",
			"isInMenu"                : False,
			"undoableMode"            : True,
			"globalSpaceMode"         : True,
			"commandsByShortClick"    : True,
		}),       
		newProfile({
			"localName"               : "moveSecure",
			"interactionMode"         : "auto-click",
			"commandsByLongClick"     : False,
			"leftBehaviour"           : "whole",
			"isInMenu"                : False,
			"undoableMode"            : True,
			"globalSpaceMode"         : True,
			"commandsByShortClick"    : True,
			"verticalBarrierMode"     : True,
			"horizontalBarrierMode"   : True,
		}),
		newProfile({
			"localName"               : "scanTechnophobe",
			"interactionMode"         : "scan",
			"commandsByShortClick"    : False,
			"scanTempo"               : 40,
			"longClickTempo"          : 60,
			"scannerName"             : "no jump",
			"keyboardOrder"           : "alphabeticScan",
			"progressiveMode"         : False,
			"isInMenu"                : False,
			"abbreviationMode"        : False,
			"punctuationMode"         : False,
			"evaluationMode"          : False,
			"correctCapsMode"         : False,
			"favoriteCharsMode"       : False,
			"correctAcntMode"         : False,
			"undoableMode"            : False,
			"truncatableMode"         : False,
			"longClickDelay"          : 12, # tenth seconds
			"longClickVariant"        : "nothing",
			"longClickStartSound"     : "Sticks 066",
			"longClickStepSound"      : "Sticks 024",
		}),        
		newProfile({
			"localName"               : "scanPoor",
			"interactionMode"         : "scan",
			"commandsByShortClick"    : False,
			"scanTempo"               : 40,
			"longClickTempo"          : 40,
			"scannerName"             : "jump everywhere",
			"progressiveMode"         : False,
			"longClickStartSound"     : "Sticks 066",
			"longClickStepSound"      : "Sticks 024",
		}),        
		newProfile({
			"localName"               : "scan",
			"interactionMode"         : "scan",
			"commandsByShortClick"    : False,
			"progressiveMode"         : False,
			"scanTempo"               : 60,
			"longClickTempo"          : 60,
			"isInMenu"                : False,
			"longClickStartSound"     : "Sticks 066",
			"longClickStepSound"      : "Sticks 024",
		}),        
		newProfile({
			"localName"               : "scanTrunk",
			"interactionMode"         : "scan",
			"commandsByShortClick"    : False,
			"scannerName"             : "jump on initials",
			"scanTempo"               : 80,
			"longClickTempo"          : 80,
			"isInMenu"                : False,
			"longClickStartSound"     : "Sticks 066",
			"longClickStepSound"      : "Sticks 024",
		}),        
		newProfile({
			"localName"               : "3rdPerson",
			"hintMode"                : True,
			"dialAboveMode"           : True,
		}),        
		newProfile({
			"localName"               : "easyPictoHunt",
			"pictoMode"               : True,
			"leftBehaviour"           : "delete",
			"progressiveMode"         : False,
			"abbreviationMode"        : False,
			"punctuationMode"         : False,
			"evaluationMode"          : False,
			"correctCapsMode"         : False,
			"favoriteCharsMode"       : False,
			"correctAcntMode"         : False,
			"undoableMode"            : False,
			"longClickDelay"          : 12, # tenth seconds
			"spellPredictionMode"     : True,
			"speechNonPictoMode"      : True,
			"speechPictoMode"         : True,
			"speechRestrictAudio"     : False,
		}),        
		newProfile({
			"localName"               : "hardPictoHunt",
			"isInMenu"                : False,
			"pictoMode"               : True,
			"toothDirectPictoMode"    : False,
			"toothIndirectPictoMode"  : False,
			"farDirectPictoMode"      : True,
			"farIndirectPictoMode"    : False,
			"multiplePictosMode"      : False,
			"spellPredictionMode"     : True,
			"speechNonPictoMode"      : True,
			"speechPictoMode"         : True,
			"speechRestrictAudio"     : False,
		}),        
		newProfile({
			"localName"               : "coverflow",
			"isInMenu"                : False,
			"pictoMode"               : True,
			"toothDirectPictoMode"    : True,
			"toothIndirectPictoMode"  : True,
			"farDirectPictoMode"      : True,
			"farIndirectPictoMode"    : True,
			"multiplePictosMode"      : False,
			"farPictoRatio"           : 100,
			"speechRestrictAudio"     : True,
			"resizeCharMode"          : False,
		}),
	]
	defaultProfileIndex          = [d["localName"] for d in defaultProfiles].index("moveClick")
	defaultLastPrefTab           = 0
	defaultStartupProfile        = "last"
	defaultNbProfiles            = len(defaultProfiles)
	defaultChewingWidth          = 480
	defaultChewingHeight         = 276
	defaultChewingPosX           = 0
	defaultChewingPosY           = 22

def aspectFactory():
	aspect = Aspect()
	if sys.platform.lower().startswith("linux"):
		aspect.platform = "linux"
	elif sys.platform.lower().startswith("win"):
		aspect.platform = "win"
	elif sys.platform.lower().startswith("darwin"):
		aspect.platform = "darwin"
	else:
		print "default version for %s" % sys.platform
	 	aspect.platform = "linux"
	path = sys.path[0]
	if os.path.isdir(path):
		os.chdir(path)
	else:
		os.chdir(os.path.dirname(path))
	complement = mysimplejson.loads(open("aspect.json").read())
	aspect.__dict__.update(complement["crossplatform"])
	aspect.__dict__.update(complement["platform"][aspect.platform])
	for profile in aspect.defaultProfiles:
		profile["editorFontFamily"] = aspect.defaultEditorFontFamily
		profile["editorFontSize"] = aspect.defaultEditorFontSize
		profile["speechCommandLine"] = aspect.defaultSpeechCommandLine
	return aspect

aspect = aspectFactory()

if __name__ == "__main__":
	from  chewing import main
	main()
