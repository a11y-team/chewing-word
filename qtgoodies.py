#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file qtgoodies.py

# builtin stuff
from PyQt4 import QtCore, QtGui
import math
from aspect import aspect

def pathForPrint(path):
	""" Return a wrappable and cross-platform version of a path name. """
	return QtCore.QDir.convertSeparators(path).replace(QtCore.QDir.separator(),QtCore.QDir.separator()+"&#8203;")

def retrieveOrGeneratePixmap(key, pixmap, generatePixmap, *args):
	""" Build the given pixmap from its previously stored value, or construct and store it """
	if not QtGui.QPixmapCache.find(key,pixmap):
		bitmap = QtGui.QBitmap(pixmap.size())
		bitmap.clear()
		pixmap.setMask(bitmap)
		painter = QtGui.QPainter(pixmap)
		painter.setRenderHint(QtGui.QPainter.Antialiasing)
		generatePixmap(painter,*args)
		QtGui.QPixmapCache.insert(key,pixmap)
	
def retrieveOrGenerateRectangularPixmap(key, pixmap, generatePixmap, *args):
	""" Build the given pixmap from its previously stored value, or construct and store it """
	if not QtGui.QPixmapCache.find(key,pixmap):
		painter = QtGui.QPainter(pixmap)
		generatePixmap(painter,*args)
		QtGui.QPixmapCache.insert(key,pixmap)
	

def toGray(image): # slow and doesn't work
	for x in range(image.width()):
		for y in range(image.height()):
			gray = QtGui.qGray(image.pixel(x,y))
			alpha = QtGui.qAlpha(image.pixel(x,y))
			image.setPixel(x,y,QtGui.QColor(gray,gray,gray,alpha).rgb())
	

class MouseMover(QtGui.QWidget):
	
	def __init__(self):
		QtGui.QWidget.__init__(self)
		self.stepLength = aspect.mouseMoverStepLength
	
	def moveVerticalFirst(self, pos):
		def positions(x,y,pos):
			(dx,dy) = (pos.x()-x,pos.y()-y)
			nbSteps = abs(dy/self.stepLength)
			for j in range(nbSteps):
				yield QtCore.QPoint(x,y+dy*j/nbSteps)
			else:
				yield QtCore.QPoint(x,pos.y())
			y = pos.y()
			nbSteps = abs(dx/self.stepLength)
			for i in range(nbSteps):
				yield QtCore.QPoint(x+dx*i/nbSteps,y)
			else:
				yield pos
		
		self.positions = positions(QtGui.QCursor.pos().x(), QtGui.QCursor.pos().y(),pos)
		self.start()
	
	def moveHorizontalFirst(self, pos):
		def positions(x,y,pos):
			(dx,dy) = (pos.x()-x,pos.y()-y)
			nbSteps = abs(dx/self.stepLength)
			for i in range(nbSteps):
				yield QtCore.QPoint(x+dx*i/nbSteps,y)
			else:
				yield QtCore.QPoint(pos.x(),y)
			x = pos.x()
			nbSteps = abs(dy/self.stepLength)
			for j in range(nbSteps):
				yield QtCore.QPoint(x,y+dy*j/nbSteps)
			else:
				yield pos
		
		self.positions = positions(QtGui.QCursor.pos().x(), QtGui.QCursor.pos().y(),pos)
		self.start()
	
	def start(self):
		pos = QtGui.QCursor.pos()
		previousWidget = QtGui.QApplication.widgetAt(pos)
		QtCore.QCoreApplication.postEvent(previousWidget,QtGui.QMouseEvent(QtCore.QEvent.MouseMove,previousWidget.mapFromGlobal(pos),pos,QtCore.Qt.NoButton,QtCore.Qt.NoButton,QtCore.Qt.NoModifier))
		for pos in self.positions:
			QtGui.QCursor.setPos(pos)
			currentWidget = QtGui.QApplication.widgetAt(pos)
			if previousWidget != currentWidget:
				QtCore.QCoreApplication.postEvent(previousWidget,QtGui.QMouseEvent(QtCore.QEvent.Leave,previousWidget.mapFromGlobal(pos),pos,QtCore.Qt.NoButton,QtCore.Qt.NoButton,QtCore.Qt.NoModifier))
				QtCore.QCoreApplication.postEvent(currentWidget,QtGui.QMouseEvent(QtCore.QEvent.Enter,currentWidget.mapFromGlobal(pos),pos,QtCore.Qt.NoButton,QtCore.Qt.NoButton,QtCore.Qt.NoModifier))
			QtCore.QCoreApplication.postEvent(currentWidget,QtGui.QMouseEvent(QtCore.QEvent.MouseMove,currentWidget.mapFromGlobal(pos),pos,QtCore.Qt.NoButton,QtCore.Qt.NoButton,QtCore.Qt.NoModifier))
			previousWidget = currentWidget
		# a QTimer with a timeout of 0 will time out as soon as all the events in the window system's event queue have been processed
		QtCore.QTimer.singleShot(0,self.notify)
	
	def notify(self):
		self.emit(QtCore.SIGNAL("automatic move"))
	

def soundFactory(filename):
	if filename.endswith("/.wav"):
		return noSound
	return QtGui.QSound(filename)

class NoSound:
	def play(self): pass
	def stop(self): pass

noSound = NoSound()

if __name__ == "__main__":
	from  chewing import main
	main()
