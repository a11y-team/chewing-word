#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file speechview.py

# built-in stuff
from PyQt4 import QtGui, QtCore
import re
import wave
# home made stuff
from aspect import aspect
import qtgoodies

class SpeechView:
	
	def __init__(self):
		try:
			from PyQt4.phonon import Phonon
			self.supportedAudioFormats = frozenset(".%s" % str(s).lower()[6:] for s in Phonon.BackendCapabilities.availableMimeTypes() if str(s).lower().startswith("audio/"))
			self.supportedAudioFormatsForPrint = ", ".join(sorted(list(self.supportedAudioFormats)))
			self.setPhononAvailability(Phonon if self.supportedAudioFormatsForPrint else None)
		except ImportError:
			self.setPhononAvailability(False)
		self.player = self.createPlayer()
		self.restartSpeakingAfter = QtCore.QTimer()
		self.restartSpeakingAfter.setSingleShot(True)
		QtCore.QObject.connect(self.player,QtCore.SIGNAL("metaDataChanged()"),self.retrieveMetaInformation)
		QtCore.QObject.connect(self.player,QtCore.SIGNAL("finished()"),self.clearMetaInformation)
	
	def setPhononAvailability(self, Phonon):
		def sourceFromFilenameWithPhonon(filename):
			return Phonon.MediaSource(filename)
		
		def createPlayerWithPhonon():
			self.tags = re.findall(r"%\((\w+)\)s",aspect.speechMetaDataString)
			# The following line prevents an obscure warning thrown the first time self.player.setCurrentSource is called inside a thread
			registerMe = QtCore.QVariant(Phonon.MediaSource(None))
			# The following should prevent a second warning if QMultiMap was available in PyQt, which is not the case
			# registerMe = QtCore.QVariant(QtCore.QMap.QMultiMap())
			# More info on: http://www.qtcentre.org/forum/archive/index.php/t-18097.html
			return Phonon.createPlayer(Phonon.MusicCategory)
		
		def createPlayerWithoutPhonon():
			self.tags = []
			return PlayerWithoutPhonon()
		
		def inSupportedAudioFormatsWithPhonon(format, text):
			return format in self.supportedAudioFormats or (text and format == ".txt")
		
		if Phonon:
			self.sourceFromFilenameDependingOnPhononAvailability = sourceFromFilenameWithPhonon
			self.createPlayerDependingOnPhononAvailability = createPlayerWithPhonon
			self.inSupportedAudioFormatsDependingOnPhononAvailability = inSupportedAudioFormatsWithPhonon
		else:
			self.supportedAudioFormatsForPrint = '.???'
			self.sourceFromFilenameDependingOnPhononAvailability = lambda filename: filename
			self.createPlayerDependingOnPhononAvailability = createPlayerWithoutPhonon
			self.inSupportedAudioFormatsDependingOnPhononAvailability = lambda *args: True
	
	def phononIsAvailable(self):
		return self.supportedAudioFormatsForPrint != ".???"
	
	def createPlayer(self):
		return self.createPlayerDependingOnPhononAvailability()
	
	def sourceFromFilename(self,filename):
		return self.sourceFromFilenameDependingOnPhononAvailability(filename)
	
	def inSupportedAudioFormats(self, format, text = False):
		return self.inSupportedAudioFormatsDependingOnPhononAvailability(format, text)
	
	def getSupportedAudioFormatsForPrint(self):
		return self.supportedAudioFormatsForPrint
	
	def setOutputExtension(self,outputExtension):
		self.outputExtension = outputExtension
	
	def register(self, model):
		self.model = model
	
	def enableSpeechInteractions(self):
		QtCore.QObject.connect(self.player,QtCore.SIGNAL("finished()"),self.model.step)
		QtCore.QObject.connect(self.restartSpeakingAfter,QtCore.SIGNAL("timeout()"),self.model.checkSkipToPreviousSentenceStrategy)
	
	def disableSpeechInteractions(self):
		QtCore.QObject.disconnect(self.player,QtCore.SIGNAL("finished()"),self.model.step)
		QtCore.QObject.disconnect(self.restartSpeakingAfter,QtCore.SIGNAL("timeout()"),self.model.checkSkipToPreviousSentenceStrategy)
	
	def setSource(self,pathBaseName):
		symLink = QtCore.QFile(pathBaseName + ".lnk")
		if symLink.exists():
			self.player.setCurrentSource(self.sourceFromFilename(symLink.symLinkTarget()))
		else:
			self.player.setCurrentSource(self.sourceFromFilename(pathBaseName + self.outputExtension))
	
	def startSpeaking(self):
		self.player.play()
		self.restartSpeakingAfter.start(aspect.restartSpeakingAfterDelay)
	
	def speak(self):
		self.player.play()
	
	def speakLink(self,pathBaseName):
		symLink = QtCore.QFile(pathBaseName + ".lnk")
		if symLink.exists():
			self.player.setCurrentSource(self.sourceFromFilename(symLink.symLinkTarget()))
			self.player.play()
	
	def stopSpeaking(self):
		self.player.stop()
		self.clearMetaInformation()
	
	def pauseSpeaking(self):
		self.player.pause()
		self.clearMetaInformation()
	
	def continueSpeaking(self):
		self.player.play()
	
	def restartSpeaking(self):
		self.player.stop()
		self.player.play()
		self.restartSpeakingAfter.start(aspect.restartSpeakingAfterDelay)
	
	def retrieveMetaInformation(self):
		metaData = {}
		empty = True
		for tag in self.tags:
			metaData[tag] = self.player.metaData(tag)[0]
			if metaData[tag]:
				empty = False
		if empty:
			self.clearMetaInformation()
		else:
			QtCore.QObject.emit(self.player,QtCore.SIGNAL("metaInformationChanged"),aspect.speechMetaDataString % metaData)
	
	def clearMetaInformation(self):
		QtCore.QObject.emit(self.player,QtCore.SIGNAL("metaInformationChanged"),"")
	

class PlayerWithoutPhonon(QtCore.QObject):
	
	def __init__(self):
		QtCore.QObject.__init__(self)
		self.endChecker = QtCore.QTimer()
		QtCore.QObject.connect(self.endChecker,QtCore.SIGNAL("timeout()"),self.processEnd)
		self.player = qtgoodies.noSound
	
	def setCurrentSource(self,source):
		source = unicode(source)
		self.player = QtGui.QSound(source)
		try:
			w = wave.open(source,"rb")
			self.duration = 1000 * w.getnframes() / w.getframerate()
			self.checkSingleShotStrategy(True)
		except: # polling if it's not a wav file (implies: not playable on windows, which doesn't support polling)
			self.duration = 200
			self.checkSingleShotStrategy(False)
	
	def play(self):
		self.player.play()
		self.endChecker.start(self.duration)
	
	def stop(self):
		self.endChecker.stop()
		self.player.stop()
	
	def pause(self):
		self.stop() # QSound offers no pause method
	
	def checkSingleShotStrategy(self, strategy):
		def checkIfFinished():
			if self.player.isFinished(): # Cuation: always True on Windows
				QtCore.QObject.emit(self,QtCore.SIGNAL("finished()"))
		
		def announceEnd():
			self.stop()
			QtCore.QObject.emit(self,QtCore.SIGNAL("finished()"))
		
		self.endChecker.setSingleShot(strategy)
		if strategy:
			self.processEndDependingOnSingleShotStrategy = announceEnd
		else:
			self.processEndDependingOnSingleShotStrategy = checkIfFinished
	
	def processEnd(self):
		self.processEndDependingOnSingleShotStrategy()
	

if __name__ == "__main__":
	from  chewing import main
	main()
