#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file spacetransformer.py

from aspect import aspect

class SpaceTransformer:
	
	def __init__(self, mediator):
		
		def transformGlyphes():
			for (i,(transformableString,trigger,glyph,transformedString)) in enumerate(aspect.spaceTransformationSequence):
				aspect.spaceTransformationSequence[i] = (transformableString,trigger,"".join(self.callInGlyphTransformer(glyph)),transformedString)
		
		def updateNonSequentialStructures():
			for (transformableString,trigger,glyph,transformedString) in aspect.spaceTransformationSequence:
				self.transformableStringSet.add(transformableString)
				self.transfoDict[(transformableString,trigger)] = (glyph,transformedString)
				self.transformableStringMaxLen = max(self.transformableStringMaxLen,len(transformableString))
		
		def closure():
			for i in range(self.transformableStringMaxLen,0,-1):
				for transformableString in self.transformableStringSet:
					for trigger in ".<>^v":
						if (transformableString,trigger) not in self.transfoDict and (transformableString[-i:],trigger) in self.transfoDict:
							(glyph,transformedString) = self.transfoDict[(transformableString[-i:],trigger)]
							print '(u"%s", "%s", u"%s", u"%s"),' % (transformableString.replace("\n","\\n").replace("\t","\\t").encode("utf8"),trigger,glyph.encode("utf8"),(transformableString[:-i]+transformedString).replace("\n","\\n").replace("\t","\\t").encode("utf8"))
							aspect.spaceTransformationSequence.append((transformableString,trigger,glyph,transformableString[:-i]+transformedString))
		
		def makePunctuationIndex():
			def recurs(transformableString,triggers,depth):
				if depth == 0 or transformableString in alreadyExplored:
					return
				alreadyExplored.add(transformableString)
				for trigger in ".<>^v":
					for i in range(len(transformableString)):
						if i>1 and depth<0:
							depth = 2
						start = transformableString[:i]
						end = transformableString[i:]
						if (end,trigger) in d:
							for (_,transformedString) in d[(end,trigger)]:
								ts = start + transformedString
								self.punctuationIndex[ts] = self.punctuationIndex.get(ts,[]) + [triggers+trigger]
								recurs(ts,triggers+trigger,depth-1)
							break
					else:
						self.punctuationIndex["Todo%s" % self.numTodo] = [triggers+trigger]
						self.numTodo += 1
			
			self.numTodo = 1
			self.punctuationIndex = {" ": [""]}
			d = {}
			for k in self.transfoDict:
				(glyph,transformedString) = self.transfoDict[k]
				if "$RIGHT" in glyph:
					d[k] = [(glyph.replace("$RIGHT",g),transformedString.replace("$RIGHT",t)) for (g,t) in self.delimitersDict.values()]
				else:
					d[k] = [self.transfoDict[k]]
			alreadyExplored = set()
			recurs(" ","",-1)
			for transformedString in self.punctuationIndex:
				self.punctuationIndex[transformedString].sort(cmp = lambda s1,s2:cmp(len(s1),len(s2)))
		
		self.getSpacesIncontentGetter  = mediator.contentGetter.getSpaces
		self.getOpenerInContentGetter  = mediator.contentGetter.getOpener
		self.atDocumentStartInContentGetter = mediator.contentGetter.atDocumentStart
		self.hasValueInCalculator      = mediator.calculator.hasValue
		self.callInGlyphTransformer = mediator.glyphTransformer.call
		self.delimitersDict            = dict((k,("".join(self.callInGlyphTransformer(g)),t)) for  (k,(g,t)) in aspect.delimitersDict.iteritems())
		self.transformableStringSet    = set()
		self.transformableStringMaxLen = 0
		self.transfoDict               = {}
		transformGlyphes()
		updateNonSequentialStructures()
		# uncomment the following line to control punctuation closure
		# closure()
		updateNonSequentialStructures()
		makePunctuationIndex()
		# uncomment the following line to print punctuation sequences
		# for line in self.toList(): print line
	
	def reset(self):
		self.transfoDelimitersDict = {}
		if self.hasValueInCalculator():
			self._transformableString = "expression"
			self._hasTransformation = True
			return
		if self.atDocumentStartInContentGetter():
			self._transformableString = u""
			self._hasTransformation = True
			return
		spaces = self.getSpacesIncontentGetter()
		self._transformableString = u""
		self._hasTransformation = False
		for i in range(min(len(spaces),self.transformableStringMaxLen),0,-1):
			s = spaces[-i:]
			if s in self.transformableStringSet:
				self._hasTransformation = True
				self._transformableString = s
				for trigger in ".<>^v":
					if (s,trigger) in self.transfoDict:
						(glyph,transformedString) = self.transfoDict[(s,trigger)]
						if "$RIGHT" in transformedString:
							(g,t) = self.delimitersDict.get(self.getOpenerInContentGetter(),(")",") "))
							self.transfoDelimitersDict[s,trigger] = (glyph.replace("$RIGHT",g),transformedString.replace("$RIGHT",t))
				break
	
	def hasTransformation(self):
		return self._hasTransformation
	
	def getTransformableString(self):
		return self._transformableString
	
	def getGlyph(self,trigger):
		return self._getGlyphAndTransformedString((self._transformableString,trigger),(u" ",None))[0]
	
	def getTransformedString(self,trigger):
		return self._getGlyphAndTransformedString((self._transformableString,trigger),(None,self._transformableString + u" "))[1]
	
	def _getGlyphAndTransformedString(self,key,defaultValue):
		return self.transfoDelimitersDict.get(key,self.transfoDict.get(key,defaultValue))
	
	def getPunctuationIndex(self):
		return self.punctuationIndex
	
	def toList(self):
		l = self.getPunctuationIndex().items()
		l.sort(cmp=lambda (s1,t1),(s2,t2):cmp((len(t1[0]),t1[0]),(len(t2[0]),t2[0])))
		return [("%s\t%s" % ("".join(self.callInGlyphTransformer(transformedString)).ljust(12).encode("utf8"),triggers)) for (transformedString,triggers) in l]

if __name__ == "__main__":
	from  chewing import main
	main()

