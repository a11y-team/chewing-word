#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file tongueview.py


# builtin stuff
from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import SIGNAL
# home made stuff
from aspect import aspect
from qtgoodies import retrieveOrGeneratePixmap


# Public stuff

class TongueView (QtGui.QWidget):
	
	def __init__(self,jawsView):
		QtGui.QWidget.__init__(self, jawsView)
		self.tongueBackgroundBottomColor    = QtGui.QColor(aspect.tongueBackgroundBottomColor)
		self.tongueBackgroundTopColor       = QtGui.QColor(aspect.tongueBackgroundTopColor)
		self.updateTonguePositionInJawsView = jawsView.updateTonguePosition
		self.ends                           = Ends(self)
		self.middle                         = Middle(self)
		self.ends.setArrowStrategy(None)
	
	def register(self,model):
		self.ends.model = model
		self.middle.model = model
		self.getLengthInModel = model.getLength
	
	def reset(self):
		w = self.getLengthInModel() * self.charWidth
		self.setFixedWidth(w + self.textMargins)
		self.ends.setFixedWidth(w + self.textMargins)
		self.middle.setFixedWidth(w)
		self.middle.reset()
		self.ends.reset()
		self.updateTonguePositionInJawsView()
	
	def setNumberOfVariantsStrategy(self,*args):
		self.ends.setNumberOfVariantsStrategy(*args)
	
	def setCurrentVariantIndex(self, *args):
		self.ends.setCurrentVariantIndex(*args)
	
	def setSeparationStrategy(self,*args):
		self.middle.setSeparationStrategy(*args)
	
	def setSnipStrategy(self,*args):
		self.middle.setSnipStrategy(*args)
	
	def updateJawsDependantGeometry(self, floatCharWidth):
		self.charWidth        = int(floatCharWidth)
		self.textMargin       = int(floatCharWidth * aspect.tongueTextMarginWrtCharWidth)
		self.textMargins      = 2 * self.textMargin
		h = int(floatCharWidth * aspect.tongueHeightWrtCharWidth)
		self.setFixedHeight(h)
		backgroundGradient    = QtGui.QLinearGradient(0, h, 0, 0)
		backgroundGradient.setColorAt(0.0, self.tongueBackgroundBottomColor)
		backgroundGradient.setColorAt(aspect.tongueReflectRatio, self.tongueBackgroundTopColor)
		self.backgroundBrush  = QtGui.QBrush(backgroundGradient)
		self.ends.setFixedHeight(h)
		self.ends.updateJawsDependantGeometry(self.textMargin,self.backgroundBrush)
		self.middle.setFixedHeight(h)
		self.middle.updateJawsDependantGeometry(floatCharWidth,self.charWidth,self.backgroundBrush)
	
	def getHeight(self):
		return self.height()
	
	def paintEvent(self,_):
		self.middle.move(self.textMargin,0)
	
	def updateHighlightment(self,bool):
		self.middle.updateHighlightment(bool)
	
	def updateEndHighlightment(self,bool):
		self.ends.updateHighlightment(bool)
	
	def registerClickTransformer(self, clickTransformer):
		self.ends.registerClickTransformer(clickTransformer)
		self.middle.registerClickTransformer(clickTransformer)
	
	def registerAutoClickTimer(self, autoClickTimer):
		self.autoClickTimer = autoClickTimer
		self.ends.registerAutoClickTimer(autoClickTimer)
		self.middle.registerAutoClickTimer(autoClickTimer)
	
	def stringMayHaveChangedEvent(self, stringDidChanged):
		self.autoClickTimer.mayNeedExtraDelay(stringDidChanged)
	
	def getLeftScanPosition(self):
		return self.ends.getLeftScanPosition()
	
	def getRightScanPosition(self):
		return self.ends.getRightScanPosition()
	
	def getCharScanPosition(self, i):
		return self.middle.getCharScanPosition(i)
	
	def highlightOneArrow(self, step):
		self.ends.highlightOneArrow(step)
	
	def lowlightArrows(self):
		self.ends.lowlightArrows()
	
	def longClickStepEvent(self):
		self.ends.model.stepVariant() # the previous version: self.ends.shortClickEvent() had a counter-intuitive side-effect: a long click on a prediction used to acquire this one, and then the following ones
		self.ends.model.delocalizeFlags()

# Private stuff
	

class AbstractTonguePart(QtGui.QWidget):
	
	def __init__(self, tongueView):
		QtGui.QWidget.__init__(self, tongueView)
		self.tongueBackgroundPen = QtCore.Qt.NoPen
		self.tongueColorForTag = { # hovered or highlighted / present / enabled
		  (True,  True,  True ) : QtGui.QColor(aspect.tongueColorHighlightedKeep),
		  (True,  True,  False) : QtGui.QColor(aspect.tongueColorHighlightedDelete),
		  (True,  False, True ) : QtGui.QColor(aspect.tongueColorHighlightedAdd),
		  (True,  False, False) : QtGui.QColor(aspect.tongueColorHighlightedNotAdd),
		  (False, True,  True ) : QtGui.QColor(aspect.tongueColorLowlightedKeep),
		  (False, True,  False) : QtGui.QColor(aspect.tongueColorLowlightedDelete),
		  (False, False, True ) : QtGui.QColor(aspect.tongueColorLowlightedAdd),
		  (False, False, False) : QtGui.QColor(aspect.tongueColorLowlightedNotAdd),
		}
		self.tonguePieColor = {
			True  : QtGui.QColor(aspect.tonguePieColorHighlighted),
			False : QtGui.QColor(aspect.tonguePieColorLowlighted),
		}
		self.resetInTongueView = tongueView.reset
		self.updateHighlightmentInTongueView = tongueView.updateHighlightment
		self.longClickStepEventInTongueView = tongueView.longClickStepEvent
		self.setMouseTracking(True)
	
	def updateHighlightment(self,bool):
		self.highlighted = bool
		self.update()
	
	def registerClickTransformer(self, clickTransformer):
		self.clickTransformer = clickTransformer
	
	def registerAutoClickTimer(self, autoClickTimer):
		self.autoClickTimer = autoClickTimer
	
# Transformable events
	def mousePressEvent(self, event):
		self.clickTransformer.setCurrentClient(self)
		self.clickTransformer.mousePressSlot(event)
	
	def mouseReleaseEvent(self, event):
		self.clickTransformer.mouseReleaseSlot(event)
	
# Transformed events
	def longClickStartEvent(self, event):
		self.model.delocalizeFlags()
		self.updateHighlightmentInTongueView(True)
	
	def longClickStepEvent(self, event):
		self.longClickStepEventInTongueView()
	
	def longClickStopEvent(self, event):
		if 0 <= event.x() < self.width() and 0 <= event.y() < self.height():
			self.model.globalAcquirement()
	
# Other events
	def leaveEvent(self,event=None):
		self.autoClickTimer.stop()
		self.autoClickTimer.setAutoRepeat(True)
		self.model.delocalizeFlags()
		self.updateHighlightment(False)
		self.resetInTongueView()
	
	def autoClickEvent(self, *args):
		self.shortClickEvent()
	
	

class Ends(AbstractTonguePart):
	
	def setNumberOfVariantsStrategy(self,numberOfVariants):
		def paintWhenNumberOfVariantsIsOne(*args):
			pass
		
		def paintWhenNumberOfVariantsIsSeveral(pixmapPainter):
			pixmapPainter.setPen(QtCore.Qt.NoPen)
			pixmapPainter.setBrush(self.tonguePieColor[self.highlighted])
			pixmapPainter.drawPie(self.leftPie,self.pieStarts[self.currentVariantIndex],self.pieSpans[self.currentVariantIndex])
			pixmapPainter.translate(self.width()-self.textMargins,0)
			pixmapPainter.drawPie(self.rightPie,2880 + self.pieStarts[self.currentVariantIndex],self.pieSpans[self.currentVariantIndex])
			pixmapPainter.translate(self.width()+self.textMargins,0)
		
		def shortClickEventWhenNumberOfVariantsIsSeveral():
			self.model.stepVariant()
		
		def shortClickEventWhenNumberOfVariantsIsOne():
			self.model.localAcquirement()
		
		self.numberOfVariants = numberOfVariants
		if numberOfVariants == 1:
			self.paintDependingOnNumberOfVariants           = paintWhenNumberOfVariantsIsOne
			self.shortClickEventDependingOnNumberOfVariants = shortClickEventWhenNumberOfVariantsIsOne
			self.shouldHighlightDependingOnNumberOfVariants = True
		else:
			self.pieStarts = [2880-1440*n/numberOfVariants for n in range(1,numberOfVariants+1)] # 2880 = 16 * 180 (degrees, cf. QPainter.QPie documentation)
			self.pieSpans  = [2880*n/numberOfVariants      for n in range(1,numberOfVariants+1)]
			self.paintDependingOnNumberOfVariants           = paintWhenNumberOfVariantsIsSeveral
			self.shortClickEventDependingOnNumberOfVariants = shortClickEventWhenNumberOfVariantsIsSeveral
			self.shouldHighlightDependingOnNumberOfVariants = False
	
	def setArrowStrategy(self, step):
		def paintArrow(pixmapPainter):
			pixmapPainter.setPen(QtCore.Qt.NoPen)
			if self.arrowStep == -1:
				pixmapPainter.setBrush(self.tonguePieColor[True])
				pixmapPainter.drawConvexPolygon(self.leftArrow)
			pixmapPainter.translate(self.width()-1,0)
			if self.arrowStep == 1:
				pixmapPainter.setBrush(self.tonguePieColor[True])
				pixmapPainter.drawConvexPolygon(self.rightArrow)
			pixmapPainter.translate(1-self.width(),0)
		
		def paintNormal(painter):
			self.paintDependingOnNumberOfVariants(painter)
		
		self.arrowStep = step
		if step is None:
			self.paintDependingOnNumberOfVariantsAndarrowStep = paintNormal
		else:
			self.paintDependingOnNumberOfVariantsAndarrowStep = paintArrow
	
	def setCurrentVariantIndex(self,index):
		self.currentVariantIndex = index
	
	def reset(self):
		point = self.mapFromGlobal(QtGui.QCursor.pos())
		if self.rect().contains(point) and self.underMouse(): # both conditions seem necessary, don't ask why
			self.updateHighlightment(True)
		else:
			self.updateHighlightment(False)
	
	def updateJawsDependantGeometry(self, textMargin, backgroundBrush):
		self.backgroundBrush = backgroundBrush
		self.textMargins     = 2*textMargin
		d                    = int(aspect.tonguePieMarginWrtTextMargin * textMargin)
		self.leftPie         = QtCore.QRect(d,d,self.textMargins-2*d,self.height()-2*d)
		self.rightPie        = QtCore.QRect(d,d,self.textMargins-2*d,self.height()-2*d)
		m                    = textMargin / 5
		h                    = self.height() / 2 - 1
		t                    = textMargin - 2
		self.leftArrow       = QtGui.QPolygon([QtCore.QPoint(t-m,h-t+2*m),QtCore.QPoint(m,h),QtCore.QPoint(t-m,h+t-2*m)])
		self.rightArrow      = QtGui.QPolygon([QtCore.QPoint(-t+m,h-t+2*m),QtCore.QPoint(-m,h),QtCore.QPoint(-t+m,h+t-2*m)])
		self.leftScanX       = self.textMargins * aspect.tongueScanRatioX
		self.scanY           = self.height() * aspect.tongueScanRatioY
	
	def getLeftScanPosition(self):
		return self.mapToGlobal(QtCore.QPoint(self.leftScanX, self.scanY))
	
	def getRightScanPosition(self):
		return self.mapToGlobal(QtCore.QPoint(self.width() - self.leftScanX, self.scanY))
	
	def highlightOneArrow(self, step):
		self.setArrowStrategy(step)
		self.update()
	
	def lowlightArrows(self):
		self.setArrowStrategy(None)
		self.update()
	
	def shortClickEvent(self):
		self.shortClickEventDependingOnNumberOfVariants()
	
	def mouseMoveEvent(self,event):
		self.autoClickTimer.start(self,event)
		if 0 <= event.x() < self.width() and 0 <= event.y() < self.height():
			if event.x() < self.width() / 2:
				self.model.updateIndex(-1)
			else:
				self.model.updateIndex(self.width())
		else:
			self.updateHighlightmentInTongueView(False)
	
	def enterEvent(self,event=None):
		self.autoClickTimer.stop()
		self.autoClickTimer.setAutoRepeat(True)
		self.updateHighlightment(True)
		self.updateHighlightmentInTongueView(self.shouldHighlightDependingOnNumberOfVariants)
	
	def paintEvent(self,event):
		def paint(pixmapPainter):
			pixmapPainter.setPen(self.tongueBackgroundPen)
			pixmapPainter.setBrush(self.backgroundBrush)
			pixmapPainter.drawRoundRect(0,0,self.textMargins,self.height(),aspect.tongueRoundedCorner,aspect.tongueRoundedCorner)
			pixmapPainter.drawRoundRect(self.width()-self.textMargins,0,self.textMargins,self.height(),aspect.tongueRoundedCorner,aspect.tongueRoundedCorner)
			self.paintDependingOnNumberOfVariantsAndarrowStep(pixmapPainter)
		#
		pixmap = QtGui.QPixmap(self.size())
		painter = QtGui.QPainter()
		painter.begin(self)
		identifier = (self.width(),self.height(),self.textMargins,self.numberOfVariants,self.currentVariantIndex,self.highlighted,self.arrowStep)
		retrieveOrGeneratePixmap("end:%s,%s,%s,%s,%s,%s,%s" % identifier,pixmap,paint)
		painter.drawPixmap(0,0,pixmap)
		painter.end()
	
	

class Middle(AbstractTonguePart):
	
	def __init__(self, tongueView):
		def symbolPixmaps(): # optimization IDEA: make the redundant colors point on the same reference
			d = {}
			for fileName in aspect.tongueSymbols.values():
				mask = QtGui.QBitmap("symbols/%s" % fileName)
				d[fileName] = {}
				for (tag,color) in self.tongueColorForTag.iteritems():
					d[fileName][tag] = QtGui.QPixmap(mask.size())
					d[fileName][tag].fill(color)
					d[fileName][tag].setMask(mask)
			return d
		#
		AbstractTonguePart.__init__(self, tongueView)
		self.tongueSnipColor             = QtGui.QColor(aspect.tongueSnipColor)
		self.tongueSeparationBottomColor = QtGui.QColor(aspect.tongueSeparationBottomColor)
		self.tongueSeparationTopColor    = QtGui.QColor(aspect.tongueSeparationTopColor)
		self.font                        = QtGui.QFont(aspect.tongueFont)
		self.font.setBold(aspect.tongueFontIsBold)
		self.symbolPixmaps = symbolPixmaps()
	
	def setSeparationStrategy(self, strategy):
		def paintWhenSeparationStrategyIsFixed(painter):
			x = self.model.getSeparationIndex() * self.charWidth
			painter.setPen(self.separationPen)
			painter.drawLine(x,0,x,self.height()-1)
		
		def paintWhenSeparationStrategyIsMoving(painter):
			separationIndex = self.model.getSeparationIndex()
			if separationIndex == self.model.getStartLength():
				painter.setPen(self.separationPen)
			else:
				painter.setPen(self.insertionPen)
			x = separationIndex * self.charWidth
			painter.drawLine(x,0,x,self.height()-1)
		
		def paintWhenSeparationStrategyIsNone(*args):
			pass
		
		if strategy == "fixed":
			self.paintDependingOnSeparationStrategy = paintWhenSeparationStrategyIsFixed
		elif strategy == "moving":
			self.paintDependingOnSeparationStrategy = paintWhenSeparationStrategyIsMoving
		else:
			self.paintDependingOnSeparationStrategy = paintWhenSeparationStrategyIsNone
	
	def setSnipStrategy(self, snip):
		def paintWhenSnipStrategyIsOn(painter):
			painter.setPen(self.snipPen)
			painter.setRenderHint(QtGui.QPainter.Antialiasing)
			painter.drawLine(self.xSnip + self.charWidth * 3/4, 0, self.xSnip + self.charWidth / 4, self.height()-1)
		
		def paintWhenSnipStrategyIsOff(*args):
			pass
		
		if snip is None:
			self.paintDependingOnSnipStrategy = paintWhenSnipStrategyIsOff
		else:
			self.paintDependingOnSnipStrategy = paintWhenSnipStrategyIsOn
			self.xSnip = snip * self.charWidth
	
	def reset(self):
		point = self.mapFromGlobal(QtGui.QCursor.pos())
		if self.rect().contains(point) and self.underMouse(): # both conditions seem necessary, don't ask why
			self.updateHighlightment(True)
			self.model.updateIndex(float(point.x()) / self.charWidth)
		else:
			self.updateHighlightment(False)
	
	def updateJawsDependantGeometry(self, floatCharWidth, charWidth, backgroundBrush):
		def scaledSymbolPixmaps(): # optimization IDEA: ignore unchecked symbol options
			d = {}
			for (fileName,pixmaps) in self.symbolPixmaps.iteritems():
				d[fileName] = {}
				for (tag,pixmap) in pixmaps.iteritems():
					d[fileName][tag] = pixmap.scaled(self.charWidth,self.charWidth,QtCore.Qt.IgnoreAspectRatio,QtCore.Qt.SmoothTransformation)
			return dict((contents,d[filename]) for (contents,filename) in aspect.tongueSymbols.iteritems())
		#
		self.font.setPointSize(int(floatCharWidth * aspect.tongueFontSizeWrtCharWidth))
		separationGradient       = QtGui.QLinearGradient(0, self.height(), 0, 0)
		separationGradient.setColorAt(0.0, self.tongueSeparationBottomColor)
		separationGradient.setColorAt(aspect.tongueReflectRatio, self.tongueSeparationTopColor)
		separationBrush          = QtGui.QBrush(separationGradient)
		self.separationPen       = QtGui.QPen(separationBrush,1)
		self.insertionPen        = QtGui.QPen(self.tongueColorForTag[(True,False,True)])
		self.snipPen             = QtGui.QPen(self.tongueSnipColor,charWidth/4)
		self.charWidth           = charWidth
		self.backgroundBrush     = backgroundBrush
		self.scaledSymbolPixmaps = scaledSymbolPixmaps()
		self.symbolYPos          = (self.height()-self.charWidth)/2
	
	def getCharScanPosition(self, index):
		return self.mapToGlobal(QtCore.QPoint((index + 0.5) * self.charWidth, self.height() * aspect.tongueScanRatioY))
	
	def shortClickEvent(self):
		self.model.localAcquirement()
	
	def mouseMoveEvent(self,event):
		self.autoClickTimer.start(self,event)
		if 0 <= event.x() < self.width() and 0 <= event.y() < self.height():
			self.model.updateIndex(float(event.x()) / self.charWidth)
		else:
			self.leaveEvent()
	
	def enterEvent(self,event=None):
		self.autoClickTimer.setAutoRepeat(False)
		self.autoClickTimer.stop()
		self.updateHighlightment(True)
		self.resetInTongueView()
	
	def paintEvent(self,event):
		
		def paintBackground(pixmapPainter):
			pixmapPainter.setPen(self.tongueBackgroundPen)
			pixmapPainter.setBrush(self.backgroundBrush)
			pixmapPainter.drawRect(self.rect())
		
		def paintFlaggedChars(flaggedChars):
			painter.setFont(self.font)
			rect = QtCore.QRect(0,0,self.charWidth,self.height())
			for (contents,present,enabled) in flaggedChars:
				if len(contents) == 1:
					painter.setPen(QtGui.QPen(self.tongueColorForTag[(self.highlighted,present,enabled)]))
					painter.drawText(rect,QtCore.Qt.AlignCenter,contents)
				else:
					painter.drawPixmap(rect.x(),self.symbolYPos,self.scaledSymbolPixmaps[contents][(self.highlighted,present,enabled)])
				rect.translate(self.charWidth,0)
		#
		pixmap = QtGui.QPixmap(self.size())
		painter = QtGui.QPainter()
		painter.begin(self)
		retrieveOrGeneratePixmap("middle:%s,%s" % (self.width(),self.height()),pixmap,paintBackground)
		painter.drawPixmap(0,0,pixmap)
		paintFlaggedChars(self.model.getFlaggedChars())
		self.paintDependingOnSeparationStrategy(painter)
		self.paintDependingOnSnipStrategy(painter)
		painter.end()
	
	


if __name__ == "__main__":
	from  chewing import main
	main()
