#!/usr/bin/env python
# -*- coding: utf_8 -*-
#file pretenders.py

""" Deliver a multi-sequence of candidate letters after a given prefix."""

class Pretenders:
	
	def __init__(self, mediator):
		self.getCompletableStringInContentGetter = mediator.contentGetter.getCompletableString
		self.sntIni                              = mediator.sntIni
		self.letDig                              = mediator.letDig
		self.letOrd                              = mediator.letOrd
		self.utfAlf                              = mediator.utfAlf
		self.letMap                              = mediator.letMap
		self.wordInitials                        = mediator.wrdFor.wordInitials
		self.lettersFollowing                    = mediator.wrdFor.lettersFollowing
		self.wrdIni                              = self.wordInitials()
		self.setResetStrategy(True)
	
	def setPersonalizedOrder(self, personalizedOrder):
		self.personalizedOrder = list(personalizedOrder)
		try:
			self.personalizedOrder[self.personalizedOrder.index("<")] = "eraser"
		except:
			pass
	
	def setResetStrategy(self, strategy):
		def resetWhenResetStrategyIsOn(rejectionPoorPrefix = None, rejectedRichChar = None, first = []):
			self.first = first
			if rejectionPoorPrefix is None:
				self.rejected = []
				self.rejectionPoorPrefix = None
			elif rejectedRichChar:
				rejectedPoorChar = self.letMap[rejectedRichChar]
				if self.rejectionPoorPrefix == rejectionPoorPrefix:
					if rejectedPoorChar not in self.rejected:
						self.rejected.append(rejectedPoorChar)
				else:
					self.rejectionPoorPrefix = rejectionPoorPrefix
					self.rejected = [rejectedPoorChar]
		
		if strategy == " ":
			self.reset = (lambda *args: resetWhenResetStrategyIsOn(first = [" "]))
		elif strategy:
			self.reset = resetWhenResetStrategyIsOn
		else:
			self.reset = (lambda *args: None)
	
	def setDynamicStrategy(self,strategy):
		def getCharsWhenDynamicStrategyIsOn():
			completableString = self.getCompletableStringInContentGetter()
			pretenders = self._pretenders(completableString)
			special = frozenset(self.rejected + self.first)
			return (self.rejected, self.first + [c for c in pretenders if c not in special])
		
		def getCharsWhenDynamicStrategyIs3Static():
			completableString = self.getCompletableStringInContentGetter()
			if completableString == "":
				return ([],[" "] + [c for c in self.sntIni] + ["eraser"])
			elif completableString[-1] == " ":
				return ([],[" "] + [c for c in self.wrdIni] + ["eraser"])
			else:
				return ([],[c for c in self.letOrd] + ["eraser"])
		
		def getCharsWhenDynamicStrategyIsStatic():
			return ([],[c for c in self.letOrd] + ["eraser"])
		
		def getCharsWhenDynamicStrategyIsAlphabetic():
			return ([], [" "] + self.utfAlf[:len(self.utfAlf)/2] + self.utfAlf[len(self.utfAlf)-1:len(self.utfAlf)/2-1:-1] + ["eraser"])
		
		def getCharsWhenDynamicStrategyIsAlphabeticScan():
			return ([], [" "] + ["eraser"] + self.utfAlf)
		
		def getCharsWhenDynamicStrategyIsPersonalized():
			return ([], self.personalizedOrder)
		
		if strategy == "dynamic":
			self.getCharsDependingOnDynamicStrategy = getCharsWhenDynamicStrategyIsOn
		elif strategy == "static":
			self.getCharsDependingOnDynamicStrategy = getCharsWhenDynamicStrategyIsStatic
		elif strategy == "3static":
			self.getCharsDependingOnDynamicStrategy = getCharsWhenDynamicStrategyIs3Static
		elif strategy == "alphabeticScan":
			self.getCharsDependingOnDynamicStrategy = getCharsWhenDynamicStrategyIsAlphabeticScan
		elif strategy == "alphabetic":
			self.getCharsDependingOnDynamicStrategy = getCharsWhenDynamicStrategyIsAlphabetic
		elif strategy == "personalized":
			self.getCharsDependingOnDynamicStrategy = getCharsWhenDynamicStrategyIsPersonalized
	
	def getChars(self):
		return self.getCharsDependingOnDynamicStrategy()
	
	def _pretenders(self, completableString):
		self.rest = set(self.utfAlf)
		if completableString == "":
			return(self.afterEmpty())
		elif completableString[-1] == " ":
			return(self.afterComplete(completableString))
		else:
			return(self.afterDefault(completableString))
	
	def setScanStrategy(self,interactionMode,scannerName=None):
		def afterEmptyWhenScanStrategyIsJump():
			return [" ", "eraser"] + self._sentenceInitials() + self._wordInitials() + self._letterOrder()
		
		def afterCompleteWhenScanStrategyIsJump(completableString):
			return [" ", "eraser"] + self._lettersFollowing(completableString) + self._wordInitials() + self._letterOrder()
		
		def afterDefaultWhenScanStrategyIsJump(completableString):
			self.rest.add(" ")
			return self._lettersFollowing(completableString) + ["eraser"] + self._digramClosers(completableString) + self._letterOrder()
		
		def afterEmptyWhenScanStrategyIsNoJump():
			return [" ", "eraser"] + self._sentenceInitials() + self._wordInitials() + self._letterOrder()
		
		def afterCompleteWhenScanStrategyIsNoJump(completableString):
			return [" "] + self._lettersFollowing(completableString) + self._wordInitials() + self._letterOrder() + ["eraser"]
		
		def afterDefaultWhenScanStrategyIsNoJump(completableString):
			self.rest.add(" ")
			return self._lettersFollowing(completableString) + self._digramClosers(completableString) + self._letterOrder() + ["eraser"]
		
		def afterEmptyWhenScanStrategyIsOff():
			self.rest.add(" ")
			return self._sentenceInitials() + self._wordInitials() + self._letterOrder() + ["eraser"]
		
		def afterCompleteWhenScanStrategyIsOff(completableString):
			self.rest.add(" ")
			return self._lettersFollowing(completableString) + self._wordInitials() + self._letterOrder() + ["eraser"]
		
		def afterDefaultWhenScanStrategyIsOff(completableString):
			self.rest.add(" ")
			return self._lettersFollowing(completableString) + self._digramClosers(completableString) + self._letterOrder() + ["eraser"]
		
		if interactionMode == "scan" and scannerName != "manual":
			if scannerName == "no jump":
				self.afterEmptyDependingOnScanStrategy = afterEmptyWhenScanStrategyIsNoJump
				self.afterCompleteDependingOnScanStrategy = afterCompleteWhenScanStrategyIsNoJump
				self.afterDefaultDependingOnScanStrategy = afterDefaultWhenScanStrategyIsNoJump
			else:
				self.afterEmptyDependingOnScanStrategy = afterEmptyWhenScanStrategyIsJump
				self.afterCompleteDependingOnScanStrategy = afterCompleteWhenScanStrategyIsJump
				self.afterDefaultDependingOnScanStrategy = afterDefaultWhenScanStrategyIsJump
		else:
			self.afterEmptyDependingOnScanStrategy = afterEmptyWhenScanStrategyIsOff
			self.afterCompleteDependingOnScanStrategy = afterCompleteWhenScanStrategyIsOff
			self.afterDefaultDependingOnScanStrategy = afterDefaultWhenScanStrategyIsOff
	
	def afterEmpty(self):
		return self.afterEmptyDependingOnScanStrategy()
	
	def afterComplete(self, completableString):
		return self.afterCompleteDependingOnScanStrategy(completableString)
	
	def afterDefault(self, completableString):
		return self.afterDefaultDependingOnScanStrategy(completableString)
	
	def _filter(self, pretenders):
		l = []
		for c in pretenders:
			if c in self.rest:
				self.rest.remove(c)
				l.append(c)
		return l
	
	def _wordInitials(self):
		return self._filter(self.wordInitials())
	
	def _letterOrder(self):
		return self._filter(self.letOrd)
	
	def _digramClosers(self, completableString):
		return self._filter(self.letDig[completableString[-1]])
	
	def _lettersFollowing(self, completableString):
		return self._filter(self.lettersFollowing(completableString))
	
	def _sentenceInitials(self):
		return self._filter(self.sntIni)


#
# Study version
#

class PretendersStatic3(Pretenders):
	
	def __init__(self, mediator, behaviour):
		self.static_strings = behaviour.static_strings
		Pretenders.__init__(self,mediator)
	
	def afterEmpty(self):
		return [c for c in self.static_strings[0]] + ["eraser"]
	
	def afterComplete(self, _):
		return [c for c in self.static_strings[1]] + ["eraser"]
	
	def afterDefault(self, _):
		return [c for c in self.static_strings[2]] + ["eraser"]



#
# Study version
#

class PretendersNoPostfix(Pretenders):
	
	def _lettersFollowing(self, _):
		return []
	
	def _sentenceInitials(self):
		return []



#
# Study version
#

class PretendersStatic1(Pretenders):
	
	def __init__(self, mediator, behaviour):
		self.static_string = behaviour.static_string
		Pretenders.__init__(self,mediator)
	
	def _pretenders(self, _):
		return [c for c in self.static_string]
	


	

if __name__ == "__main__":
	from  chewing import main
	main()
