#!/usr/bin/env python
# encoding: utf-8
"""
accentuator.py

"""

class Accentuator:
	
	def __init__(self, accWrd):
		self.accWrd = accWrd
	
	def __getitem__(self, word):
		return self.accWrd.get(word,[word.lower()])
	
	def mayLearn(self, poorWord, richWord, existingPoorWord):
		# print "current list", poorWord, self.accWrd.get(poorWord,None)
		if poorWord in self.accWrd:
			if richWord not in self.accWrd[poorWord]:
				self.accWrd[poorWord].append(richWord)
		elif poorWord.lower() != richWord:
			if existingPoorWord:
				self.accWrd[poorWord] = [poorWord.lower(), richWord]
			else:
				self.accWrd[poorWord] = [richWord]
		# print "  next list", poorWord, self.accWrd.get(poorWord,None)
	


if __name__ == "__main__":
	from  chewing import main
	main()
